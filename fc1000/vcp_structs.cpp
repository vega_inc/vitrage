#include  "vcp_structs.h"

using namespace ControlPoint;

/* Pos */
QString Pos::toString()
{
	QString s = QString(" ");
    return QString(QString::number(x) + s + QString::number(y) 
	+ s + QString::number(w) + s + QString::number(h));
}
/* Struct */
QString Struct::setBrackets(QString str) 
{
	return QString ("{ ")  + str + QString(" }");
}
/* WinId */
WinId_t::WinId_t(QString str) 
{
	m_id = str.toULong();
}


WinId_t::WinId_t(int64_t id) 
{
	m_id = id;
}

int64_t WinId_t::_id(void)
{
	return m_id;
}

void WinId_t::setId(int64_t id)
{
	m_id = id;
}


QString WinId_t::toString(void) 
{
	return setBrackets(QString::number(m_id));
}




/* TWindowState */



/**
* @brief Create object from requested strinf 
* (string must be without brackets i) 
* "{ 3 } 2 1 7138 100 100 320 320 { 1 }"
*
* @param str - [in] input string 
*/
TWindowState::TWindowState(QString str) 
{
/* parse string */
	QStringList list = str.split(QRegExp(" "));
	setId(list[1].toInt());
	setKind(static_cast<SubSystemKind>(list[3].toInt()));
	setNState(list[4].toInt());
	setNStateChange(list[5].toInt());
	Pos pos;
	pos.x = list[6].toInt();
	pos.y = list[7].toInt();
	pos.w = list[8].toInt();
	pos.h = list[9].toInt();
	setPosition(pos);
	setZAfter(list[11].toInt());
}

TWindowState::TWindowState(int64_t id,
                           SubSystemKind kind,
                           uint32_t state,
						   uint32_t state_ch,
                           Pos pos,
						   int64_t zafter)
{
	m_id.setId(id);
	m_zafter.setId(zafter);
	m_kind = kind;
	m_nstate = state;
	m_nstate_ch = state_ch;
	m_position = pos;
}
         


void TWindowState::setId(int64_t id) 
{
	m_id.setId(id);
}

int64_t TWindowState::id()
{
	return m_id._id();
}

void TWindowState::setZAfter(int64_t id) 
{
	m_zafter.setId(id);
}

int64_t TWindowState::zafter()
{
	return m_zafter._id();
}


void TWindowState::setKind(SubSystemKind  kind)
{
	m_kind = kind;
}


SubSystemKind TWindowState::kind(void) 
{
	return m_kind;
}


void TWindowState::setNState(uint32_t state)
{
	m_nstate = state;
}

uint32_t TWindowState::nstate() 
{
	return m_nstate;
}


void TWindowState::setNStateChange(uint32_t state)
{
	m_nstate_ch = state;
}


uint32_t TWindowState::nstatechange(void)
{
	return m_nstate_ch;
}

void TWindowState::setPosition(Pos pos)
{
	m_position = pos;
}

Pos TWindowState::position(void)
{
	return m_position;
}



QString TWindowState::toString(void)
{
	QString s(" ");
	QString str =m_id.toString();
   	str += s + QString::number(m_kind) + s +  QString::number(m_nstate) 
	+ s + QString::number(m_nstate_ch) + s + m_position.toString() 
	+ s + m_zafter.toString();
	return setBrackets(str);
}















































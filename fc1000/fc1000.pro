#-------------------------------------------------
#
# Project created by QtCreator 2012-12-13T14:59:03
#
#-------------------------------------------------
include(../common.pri)


TARGET = fc1000 
CONFIG +=   rtti \
            debug \
            plugin

QT += network

DESTDIR = $$INSTALL_PATH_CTRL_PLUGINS

TEMPLATE = lib

DEFINES += VCONTROLLER_LIBRARY

INCLUDEPATH += $$INSTALL_PATH_INCLUDE \
	       /usr/include/

SOURCES += fc1000.cpp                 \
		   vcontrolpoint.cpp	      \
		   vcp_objects.cpp            \
		   vcp_structs.cpp            \
		   frame.cpp
HEADERS += fc1000.h      \
           vcontrolpoint.h \
            vcp_objects.h \
            vcp_structs.h \
            frame.h


unix {
       target.path = /$(DESTDIR)
       INSTALLS += target
     }


#ifndef VCONTROLPOINT_H
#define VCONTROLPOINT_H

#include <QString>
#include <QUrl>

#include "stdint.h"
#include "vcp_objects.h"

using namespace ControlPoint;


class VControlPoint
{
	public:
	VControlPoint();
	~VControlPoint();
	
	QString conn(QString username, QString passwd);
	Window Window_o;
	PictureViewerSys PictureViewerSys_o;
	GalWinSys GalWinSys_o;
	WinServer WinServer_o;
	CPWebSys CPWebSys_o;

};
#endif /* VCONTROLPOINT_H */

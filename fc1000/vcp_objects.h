#ifndef VCP_OBJECTS_H
#define VCP_OBJECTS_H

#include "vcp_structs.h"

/* classes for implementing VControlPoint Protocol objects and Functions */

namespace ControlPoint  {

/**
* @brief basic class for object for Contor Point Protocol 
*/
class Object {
	public : 
	Object() {};
	virtual ~Object() {};
				
	QString name() {return m_name;}				
	void setName(QString name) 
	{
	m_name = name;
	}
	QString	setRequest(QString);
	private:
	QString m_name;

};

/**
* @brief 
*/
class Window : public Object{
	public:
	Window();
	~Window(){};
	QString SetState(TWindowState state); 
	QString GetState(WinId_t id);

};



/**
* @brief basic class for Control Point Objects
* working with View windows
*/
class Viewer : public Object {
	public:
	Viewer() {};
	~Viewer() {};
	/**
	* @brief crean new window
	*
	* @return QString - prepeared sring 
	*/
	QString	NewWindow(void);	
	/**
	* @brief create window with specified id
	*
	* @param id  - [in] WinId_t specified id
	*
	* @return  prepared QString
	*/
	QString NewWindowWithId(WinId_t  id);

};


class PictureViewerSys : public Viewer 	{
	public:
	PictureViewerSys(){setName("PictureViewerSys");}; 
	~PictureViewerSys(){};
	/**
	* @brief show picture from path in window with id
	*
	* @param id - [in] WinId_t 
	* @param path - [in] QString
	*
	* @return prepared QString
	*/
	QString ShowPicture(WinId_t id, QString path);
    QString GetFileName(WinId_t id);
};




class GalWinSys : public Viewer {
	public:
	GalWinSys() {setName("GalWinSys");};
	~GalWinSys() {};
    QString QueryAllWindows(void);
    QString  SelectInput(WinId_t id,QString name);
    QString  GetInput(WinId_t id);
    QString  Start(WinId_t id);
    QString  SetCrop(WinId_t id, Pos crop_pos);
    QString  GetCrop(WinId_t id);
    QString  GetInputSize(WinId_t id);

};



class WinServer : public Object {
	public:
	WinServer() {setName("WinServer");};
	~WinServer() {};
	QString DeleteWindow(WinId_t &);
	QString QueryAllWindows(void);
	QString InvokeAppWindow(QString name);
	QString GetAppWinInfo(WinId_t&);

};


class CPWebSys : public Object {
	public:
	CPWebSys() {setName("CPWebSys");};
	
    ~CPWebSys() {};
	QString NewWindow(void);
	QString NewWindowWithId(WinId_t);
	QString SetURL(WinId_t id,QString string);
	QString GetURL(WinId_t );

};

}





#endif /* VCP_OBJECTS_H */




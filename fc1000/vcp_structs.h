#ifndef VCP_STRUCTS_H
#define VCP_STRUCTS_H

#include "stdint.h"
#include <QString>
#include <QStringList>

namespace ControlPoint {

/* Flags and enums */

enum nState {
	wsVisible_c      =  0x0001,
	wsMaximized_c    =  0x0004,
	wsFramed_c       =  0x0008,
	wsLockAspect_c   =  0x0010,
	wsAlwaysOnTop_c  =  0x0020
};	

enum nStateChange {
	wsVisible       = 0x0001,
	wsMinimazed     = 0x0002,
	wsMaximazed     = 0x0004,
	wsFramed        = 0x0008,
	wsLockAspect    = 0x0010,
	wsAlwaysOnTop   = 0x0020,
	wsCreated       = 0x0100,
	wsDestroyed     = 0x0200,
	wsPosition      = 0x0401,
	wsSize          = 0x0800,
	wsZOrder        = 0x1000,
	wsTitle         = 0x2000,
	wsKind          = 0x4000,

	wsChannel       = 0x00010000,
	wsBalace        = 0x00020000,
	wsFormat        = 0x00040000,
	wsCrop          = 0x00080000,
	wsInput         = 0x00200000

};

class Pos {
	public:
	int32_t x;
	int32_t y;
	int32_t w;
	int32_t h;

    int32_t cx;
    int32_t cy;
    int32_t cw;
    int32_t ch;
	QString toString(void);
};

enum SubSystemKind {
	SubSystemKind_None = 0,
	SubSystemKind_Galileo,
	SubSystemKind_LiveVideo,
	SubSystemKind_RGBCapture,
	SubSystemKind_SystemWindow,
	SubSystemKind_CPShare,
	SubSystemKind_VidStream,
	SubSystemKind_CPWeb,
	SubSystemKind_PictureViewer,
	SubSystemKind_CatalistLink,
	SubSystemKind_IPStream
};

class Struct {
public:
    Struct() {}
	virtual ~Struct() {}

	QString setBrackets(QString);
	virtual bool parse(QString) { return false; }
	virtual QString  toString(void) {
	return m_str;} 

private:
	QString m_str;

};

class WinId_t : public Struct {
	public:
	WinId_t(QString);
	WinId_t(int64_t _id);
    WinId_t() { }
	void setId(int64_t );
	int64_t _id(void);
	virtual QString  toString(void); 
	private:
	int64_t m_id;
};

class TWindowState : public Struct  {
	public:
	TWindowState(QString);
    TWindowState(int64_t id,
                SubSystemKind kind,
                uint32_t state,
                uint32_t state_ch,
                Pos pos,
			    int64_t zafter);

	TWindowState();

	void setId(int64_t  id);
	int64_t id(void);

	void setZAfter(int64_t id);
	int64_t zafter(void);

	void setKind(SubSystemKind kind);
	SubSystemKind kind();

	void setNState(uint32_t state);
	uint32_t nstate(void);

	void setNStateChange(uint32_t state);
	uint32_t nstatechange(void);

	void setPosition(Pos pos);
	Pos position(void); 
    QString  toString(void); 

	private:
	WinId_t m_id,m_zafter;
	SubSystemKind m_kind;
	uint32_t m_nstate;
	uint32_t m_nstate_ch;
	Pos m_position;
	
};

}

#endif /* VCP_STRUCTS */

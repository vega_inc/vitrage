#include "frame.h"

Frame::Frame(const QString &name, const QString &clonename, const QString &type, WinId_t id)
    : m_name(name),
      m_cloneName(clonename),
      m_type(type),
      m_id(id)
{}

void Frame::setName(QString name)
{
	m_name = name;
}
void Frame::setCloneName(QString name)
{
	m_cloneName = name;
}


void Frame::setType(QString type)
{
	m_type = type;
}
void Frame::setId(WinId_t id)
{
	m_id = id;	
}
void Frame::setId(uint64_t id)
{
	m_id = WinId_t(id);
}

QString Frame::getName(void)
{
	return m_name;
}

QString Frame::getCloneName(void)
{
	return m_cloneName;
}


QString Frame::getType(void)
{
	return m_type;

}
WinId_t Frame::getId(void)
{
	return m_id;
}

uint64_t Frame::getIdInt(void)
{
	return m_id._id();
}





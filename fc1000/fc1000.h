#ifndef FC1000_H
#define FC1000_H

#include <QtCore/qplugin.h>
#include <QTimer>

#include <vsource/vsource.h>
#include <vcontroller/vcontroller.h>
#include "vcontrolpoint.h"
#include "frame.h"

typedef void (*REQ_PARSER)(QString);

#define DEF_LAG 300         /* default lag in milliseconds */
#define DEF_HOST "localhost" /* default host */
#define DEF_PORT 25456       /* default port */
#define DEF_DIF 5            /* default difference between sizes */

static const QString PATH_P = QString("CP_Path"); /* property for sytem window
	                                                 path to application exe */

class FC1000 : public VController {
    Q_OBJECT
    Q_INTERFACES(AbstractVController)
public:
    FC1000();
    ~FC1000();
    void conn() override;
    void disconn() override;
    bool createSource(VSource & ) override;
    bool startSource(VSource &) override;
    bool stopSource(VSource &) override;
    bool enableSourceFlag(VSource &,bool f) override;
    bool setScene(const Sources &) override;
    VController *clone() override;
    bool replaceSource2(VSource &src,SubSystemKind kind,int old_id);
    bool setToTop(TWindowState &state);
    void run();
    bool isBusy() const { return m_busy; }

private slots:
    void newState(QAbstractSocket::SocketState);
    void conn_slot();
    void disconnected();
	void sock_err(QAbstractSocket::SocketError );
    void ready();
    void timer_timeout();
    bool checkRespForSucc();
    void readFinished();
    void setSceneSlot();
    void createSourceSlot();
    void disSlot();
    void conSlot();
private:
	static const uint32_t CODE_LENGHT = 8;
    bool sendRequest(char *data,uint32_t count,int32_t delay = 0);
    bool compareSize(TWindowState &, QSharedPointer<VSource>);
    bool compareFrames(TWindowState &, QSharedPointer<VSource>);
    bool comparePictureWindow(TWindowState &state, QSharedPointer<VSource> src);
    bool compareCropSize(TWindowState &, QSharedPointer<VSource>, QSize &);
    bool remakeCrop(VSource &, QSize);

	const QString m_success;
    QTcpSocket *m_socket;
	uint32_t m_lag;
	QMutex m_mutex;
	uint32_t m_wpixs; /* pixels in small cell */
	uint32_t m_hpixs; /* pixels in big cell */
    QSharedPointer<VSource> m_src;
	QList<Frame> m_frames; /* inner frames */
	QList<TWindowState> m_framesTmp; /* frames for query all frames */
    QList<QSharedPointer<VSource> > m_sources; /*list for set Scene method */
    QList<QSharedPointer<VSource> > m_sources_; /*list for set Scene method */
	QMap<QString,SubSystemKind> m_types; /* VSource->type to SubSystemKind */
    bool m_busy;

	QString m_resp; /* response */
    bool m_respFlag;
	QUrl m_url;
	QTimer m_timer;
	VControlPoint CP;
	REQ_PARSER current_req;
	bool compareFrames(void);
	void getFrames(void);
	void startTimer(void);
    bool getResponse(bool packet = true);
	bool replaceSource(VSource &,SubSystemKind kind);
    bool enableSource(VSource &,SubSystemKind kind);

	bool createPicture(VSource &);
    bool createNamed(VSource &src);
	bool createApplication(VSource &);
	bool createWebPage(VSource &);
	bool closeWindow(WinId_t &id);
    QString getApplication(TWindowState &state); 
    bool compareSystemWindow(TWindowState &, QSharedPointer<VSource>);
    bool compareWebPage(TWindowState &, QSharedPointer<VSource>);
    bool compareNamed(TWindowState &, QSharedPointer<VSource>);
signals:
    void createSourceSig();
    void setSceneSig();
    void discSig();
    void conSig();
};

#endif /* FC100_H */

#include "fc1000.h"
#include <QStringList>

#define NSTATECHANGE_ 7183 /* change x,y,w,h,z */
#define NSTATECHANGE_ALL 0x0C00 /* change x,y,w,h,z */
#define NSTATECHANGE_ALL_Z 0x1000 /* change x,y,w,h,z */

FC1000::FC1000()
    : m_success("=00000000")
{
    m_name = "fc1000";
    m_socket  = new QTcpSocket();
    connect(m_socket,SIGNAL(connected()),this,SLOT(conn_slot()));
    connect(m_socket,SIGNAL(disconnected()),this,SLOT(disconnected()));
    qRegisterMetaType<QAbstractSocket::SocketError>("QAbstractSocket::SocketError") ;
    connect(m_socket,SIGNAL(error(QAbstractSocket::SocketError)),
			this,SLOT(sock_err(QAbstractSocket::SocketError)));
    connect(m_socket,SIGNAL(readyRead()),this,SLOT(ready()));
    connect(&m_timer,SIGNAL(timeout()),this,SLOT(timer_timeout()));
    qRegisterMetaType<QAbstractSocket::SocketState>("QAbstractSocket::SocketState");
    connect(m_socket,SIGNAL(stateChanged(QAbstractSocket::SocketState)),this
            ,SLOT(newState(QAbstractSocket::SocketState)));
    connect(m_socket,SIGNAL(readChannelFinished()),this,SLOT(readFinished()));
    connect(this,SIGNAL(discSig()),this,SLOT(disSlot()));
    connect(this,SIGNAL(conSig()),this,SLOT(conSlot()));
	/* connnects fot methods */
    connect(this,SIGNAL(createSourceSig()),this, SLOT(createSourceSlot()),Qt::QueuedConnection);
    connect(this,SIGNAL(setSceneSig()),this, SLOT(setSceneSlot()), Qt::QueuedConnection);

    setProperty("model", "FC1000");
    m_errorMsg = "Not started";
    setProperty(CONTR_NAME,"admin");
    setProperty(CONTR_PASSWD,"");
    m_address = QHostAddress::LocalHost;
    m_port = 25456;
    setStatus(status_t::stopped);
    m_busy = false;
}

void FC1000::newState(QAbstractSocket::SocketState state)
{
    qDebug() << "State changed" << state;
}

void FC1000::run()
{
    exec();
}

FC1000::~FC1000()
{
    this->exit();
    this->quit();
}

void FC1000::conSlot()
{
    m_socket->close();
    if (m_socket->isOpen()){
        m_socket->close();
    }

    QVariant host_var , port_var,lag;
    if(getProperty(QString("host"),host_var)) {
        m_url.setHost(host_var.toString());
    } else m_url.setHost(DEF_HOST);

    if(getProperty(QString("port"),port_var)) {
        m_url.setPort(port_var.toInt());
    } else m_url.setPort(DEF_PORT);

    m_lag = DEF_LAG;

    startTimer();
    if (!m_socket->isOpen()) {
        m_socket->connectToHost(m_url.host(),m_url.port());

        if (!isRunning()) {
            moveToThread(this);
            start();
        }
    }
}

void FC1000::conn()
{
    emit conSig();
}

void FC1000::timer_timeout()
{
    m_timer.stop();
    QString str("FC1000 ERROR:Timout error");
    setStatus(status_t::error);
    emit statusChanged();
    emit errorSig(str);
    m_busy = false;
}

void FC1000::startTimer()
{	
	quint32 lag_int;
	lag_int = DEF_LAG;
	m_timer.start(lag_int);
}

void FC1000::disconn()
{
    emit discSig();
}

VController *FC1000::clone()
{
    return new FC1000();
}

void FC1000::createSourceSlot()
{
	qDebug() << "createSourceSlot";

	QVariant type;
    m_src->getProperty(QString("type"),type);
	QString type_str =  type.toString();
	if (type_str == PIC_TYPE) {
		createPicture(*m_src);
	}
	if ((type_str == VNC_TYPE) ||
	    (type_str == IPCAMERA_TYPE)) {
		createApplication(*m_src);
	}
 	if (type_str == WBPAGE_TYPE) {
		createWebPage(*m_src);
	}
    if (type_str == NAMED_TYPE) {
        createNamed(*m_src);
    }

    if (type_str == EXTRONMIXER_TYPE) {
        createNamed(*m_src);
    }

    if (type_str == EXTRON_TYPE) {
        createNamed(*m_src);
    }

    if (type_str == MIXER_TYPE) {
        createNamed(*m_src);
    }
}

void FC1000::disSlot()
{
    // if (m_busy) return true;
     QString dummy("dlENameummy\r\n");
     m_socket->write(dummy.toAscii().data());
     m_socket->waitForBytesWritten(m_lag);
     m_socket->disconnectFromHost();

     setStatus(status_t::disc);
     //m_socket->close();
    // m_socket->abort();

     m_errorMsg = QString(tr("Disconnected"));
     m_socket->waitForDisconnected();
     emit statusChanged();

     m_busy = false;
     //return false;
}

bool FC1000::createSource(VSource &src)
{
    m_src = QSharedPointer<VSource>(&src);
	emit createSourceSig();
	return false;
}

bool FC1000::startSource(VSource &)
{
	return false;
}

bool FC1000::stopSource(VSource &)
{
    return false;
}

bool FC1000::setToTop(TWindowState &state)
{
    state.setZAfter(-1);
    QString str = CP.Window_o.SetState(state);
    if (sendRequest(str.toAscii().data(),str.count())) return true;
    return false;
}

void FC1000::conn_slot()
{
	qDebug() << "Connected Signal" ;
	QVariant name;
	QVariant passwd;
    getProperty(QString("username"),name);
	getProperty(QString("passwd"),passwd);

    QString str = name.toString() + ("\r\n");
    m_socket->write(str.toAscii().data(),str.count());
    str = passwd.toString() + ("\r\n");

    m_socket->waitForBytesWritten(300000);
    m_socket->write(str.toAscii().data(),str.count());
    QString er_str;
	m_timer.stop();
    m_socket->waitForBytesWritten(300000);
    m_respFlag= false;
    if ((!getResponse())  || (m_resp != QString("OK\r\n"))) {
        er_str = QString("FC1000 ERROR: Connect command error") + m_resp;
        setStatus(status_t::error);
        m_errorMsg = er_str;
        //m_socket->close();
        emit errorSig(er_str);
    } else {
        er_str = QString("FC1000 Connected");
        m_errorMsg = er_str;
        setStatus(status_t::connected);
        messSig(er_str);
    }
    emit statusChanged();
}

void FC1000::disconnected()
{
    qDebug() << "Disconnnected signal;";
}

void FC1000::sock_err(QAbstractSocket::SocketError err)
{
    bool flag  = false;
    QString msg;
    switch (err) {
    case (QAbstractSocket::ConnectionRefusedError) :
        flag = true;
        msg = QString("ConnectionRefusedError");
    break;
    case (QAbstractSocket::NetworkError) :
        flag = true;
        msg = QString("NetworkError");
    break;
    default:
    break;

    }
    if (flag) {
        setStatus(status_t::error);
        m_errorMsg = msg;
        emit statusChanged();
    }
}

void FC1000::ready()
{
//    qDebug() << "Ready slot";
    m_respFlag = true;
}

bool FC1000::createPicture(VSource &src)
{
	QString str = CP.PictureViewerSys_o.NewWindow();
	qDebug() << str;
	QVariant path;

	if (!src.getProperty(QString("host"),path)) {
	return true;
	}


	if (sendRequest(str.toAscii().data(),str.count())) return true;
    QString str1 = m_resp.remove(0,12);
    m_resp.remove(5,4);
	qDebug() << m_resp;
	src.setProperty(QString("id"),QVariant(m_resp.toInt()));

	str = CP.PictureViewerSys_o.ShowPicture 
	                                (WinId_t(m_resp.toInt()),path.toString());
	qDebug() << str;
	if (sendRequest(str.toAscii().data(),str.count())) return true;
	qDebug () << m_resp;
    replaceSource(src,SubSystemKind_PictureViewer);
    return false;
}

bool FC1000::replaceSource(VSource &src,SubSystemKind kind)
{
	QVariant id;
	QVariant pixs_w;
	QVariant pixs_h;

	src.getProperty(QString("id"),id);
	getProperty(QString("pixs_w"),pixs_w);
	getProperty(QString("pixs_h"),pixs_h);

	Pos pos;
	VPosition src_pos = src.getPosition();

    pos.x = (src_pos.lt.x() * pixs_w.toInt())/1000;
    pos.y = (src_pos.lt.y() * pixs_h.toInt())/1000;
    pos.w = ((src_pos.rb.x()  - src_pos.lt.x()) * pixs_w.toInt())/1000;
    pos.h = ((src_pos.rb.y()  - src_pos.lt.y()) * pixs_h.toInt())/1000;

    TWindowState state(id.toInt(),kind,1,NSTATECHANGE_ALL,pos,-1);
	
	QString str = CP.Window_o.SetState(state);
	if (sendRequest(str.toAscii().data(),str.count())) return true;

    return false;
}

bool FC1000::remakeCrop(VSource &src, QSize origSize)
{
    QVariant id;
    QVariant pixs_w;
    QVariant pixs_h;

    src.getProperty(QString("id"),id);
    getProperty(QString("pixs_w"),pixs_w);
    getProperty(QString("pixs_h"),pixs_h);

    int currW = 0;
    int currH = 0;
    if(origSize.isNull())
    {
        QString str = CP.GalWinSys_o.GetInputSize(WinId_t(id.toInt()));

        if (sendRequest(str.toAscii().data(),str.count())) return true;

        if (!checkRespForSucc())  {
            qDebug("FC1000::remakeCrops failed");
            return false;
        }
        QString str1 = m_resp.remove(0,11);
        str1 = str1.remove(QString("\r\n"));
        str1 = str1.remove(QString("\""));
        str1 = str1.remove(QString("}"));
        str1 = str1.trimmed();

        currW = (str1.section(" ", 0, 0)).toInt();
        currH = (str1.section(" ", 1, 1)).toInt();

//        qDebug()<< "N2 str=="<< str1;
//        qDebug()<<"currW"<<currW;
//        qDebug()<<"currH"<<currH;

        origSize.setWidth(currW);
        origSize.setHeight(currH);
    }
    else
    {
        currW = origSize.width();
        currH = origSize.height();
    }

    Pos pos;
    VPosition src_pos = src.getPosition();

    pos.cx = qRound( currW * src_pos.getCropLxW() );
    pos.cy = qRound( currH * src_pos.getCropTyH() );
    pos.cw = qRound( currW * src_pos.getCropSowOnWiw() );
    pos.ch = qRound( currH * src_pos.getCropSohOnWih() );

    if(pos.cx % 2 != 0) // must be even
        pos.cx++;
    if(pos.cw % 2 != 0) // must be even
        pos.cw++;

    QString str = CP.GalWinSys_o.SetCrop(id.toInt(), pos);

    if (sendRequest(str.toAscii().data(),str.count())) return true;

    return false;
}

bool FC1000::replaceSource2(VSource &src,SubSystemKind kind,int old_id)
{
    QVariant id;
    QVariant pixs_w;
    QVariant pixs_h;

    src.getProperty(QString("id"),id);
    getProperty(QString("pixs_w"),pixs_w);
    getProperty(QString("pixs_h"),pixs_h);

    Pos pos;
    VPosition src_pos = src.getPosition();

    pos.x = (src_pos.lt.x() * pixs_w.toInt())/1000;
    pos.y = (src_pos.lt.y() * pixs_h.toInt())/1000;
    pos.w = ((src_pos.rb.x()  - src_pos.lt.x()) * pixs_w.toInt())/1000;
    pos.h = ((src_pos.rb.y()  - src_pos.lt.y()) * pixs_h.toInt())/1000;

    TWindowState state(id.toInt(),kind,1,NSTATECHANGE_ALL_Z,pos,old_id);

    QString str = CP.Window_o.SetState(state);

    if (sendRequest(str.toAscii().data(),str.count())) return true;

    return false;
}

bool FC1000::enableSource(VSource &src, SubSystemKind kind)
{
    QVariant id;
    QVariant pixs_w;
    QVariant pixs_h;

    src.getProperty(QString("id"),id);
    getProperty(QString("pixs_w"),pixs_w);
    getProperty(QString("pixs_h"),pixs_h);

    Pos pos;
    VPosition src_pos = src.getPosition();

    pos.x = (src_pos.lt.x() * pixs_w.toInt())/1000;
    pos.y = (src_pos.lt.y() * pixs_h.toInt())/1000;
    pos.w = ((src_pos.rb.x()  - src_pos.lt.x()) * pixs_w.toInt())/1000;
    pos.h = ((src_pos.rb.y()  - src_pos.lt.y()) * pixs_h.toInt())/1000;

    TWindowState state(id.toInt(),kind,1,NSTATECHANGE_,pos,-1);

    QString str = CP.Window_o.SetState(state);

    if (sendRequest(str.toAscii().data(),str.count())) return true;

    return false;
}

bool FC1000::enableSourceFlag(VSource &src, bool f)
{
    Q_UNUSED(f);
    QVariant id;
    src.getProperty(QString("id"),id);
    WinId_t id_(id.toInt());
    closeWindow(id_);
    return false;
}

void FC1000::getFrames(void)
{
	uint32_t framesNumber;
	m_framesTmp.clear();
	QString str = CP.WinServer_o.QueryAllWindows();

    if (sendRequest(str.toAscii().data(),str.count())) return ;
	/* here parse answers from server */
	if (checkRespForSucc())  {
	m_resp.remove(0,CODE_LENGHT+4); 
	m_resp.remove(m_resp.count()-3,1);
	QString length_str;
	for (int i = 0; i < m_resp.count();i++) {
	if (m_resp[i] == ' ') {
		m_resp.remove(0,i);
		break;
	}
	length_str +=m_resp[i];
	}

	framesNumber = length_str.toUInt(); 

	QStringList lst = m_resp.split(QString("} }"));
    if (lst.count()!= (int)(framesNumber+1)) {
		qDebug() << QString("Error: FC1000::getFrames");
	}
	m_frames.clear();
	for(int i = 0 ; i <lst.count()-1;i++) {
	QString str = lst[i];
	str.remove(0,3);
	str.append(QString("}"));
    m_framesTmp.append(TWindowState(str));
    //qDebug() << str;
	}
	}

    foreach(TWindowState state,m_framesTmp) {
       // qDebug() << state.id() << state.kind();
    }

}

bool FC1000::getResponse(bool packet)
{
    Q_UNUSED(packet);

    if (m_socket->state() != QAbstractSocket::ConnectedState) {
          m_busy = false;
          return true;
      }

    m_resp.clear();
    bool ret = false;
    if (!m_respFlag) {
        ret = m_socket->waitForReadyRead(m_lag);
    }
        QByteArray buffer;
        bool flag = true;
        int my = 0;
        while (flag) {
            if (!m_socket->isValid()) {
                m_busy = false;
                return true;
            }
            buffer += m_socket->readAll();
            if ((QString(buffer).contains("\r\n"))) {
                flag = false; } else {

                m_socket->waitForReadyRead(100);
                my++;
                if (my > 1000) flag = false;
            }
        }
        m_resp = QString(buffer);
        if (!m_resp.count()){
            qDebug() << "Empty responce";
            ret = false;
        }
    int mark = m_resp.indexOf("\r\n");
    //qDebug() << "mark " << mark;
    m_resp.remove(mark+2,m_resp.count()-mark-2);
    //qDebug() << "After all " << ret;
    return ret;
}

bool FC1000::compareFrames(void)
{
	return true;
}

bool FC1000::checkRespForSucc(void)
{	
    return (m_resp.startsWith(m_success));
}

void FC1000::readFinished()
{
    qDebug() << " Read finished";
}

bool FC1000::createApplication(VSource &src)
{
	QVariant var;
	bool ret = src.getProperty(QString("application"),var);
	if (!ret) return  true;
	QString str = CP.WinServer_o.InvokeAppWindow(var.toString());
	if (sendRequest(str.toAscii().data(),str.count())) return true;
    m_resp.remove(QString("{"));
    m_resp.remove(QString("}"));
    m_resp.remove(QString(" "));
    bool ok = false;
    m_resp = m_resp.remove(0,9);
    int id_int = m_resp.toInt(&ok);
    //if(ok) src.setProperty(QString("id"),QVariant(id_int));
    src.setProperty(QString("id"),QVariant(id_int));

    replaceSource(src,SubSystemKind_SystemWindow);
    return false;
}

bool FC1000::createWebPage(VSource &src)
{
	qDebug() << "createWebPage";
	QVariant var;
	bool ret = src.getProperty(QString("host"),var);
	if (!ret) return  true;
	QString str = CP.CPWebSys_o.NewWindow();

	if (sendRequest(str.toAscii().data(),str.count())) return true;

    m_resp.remove(QString("{"));
    m_resp.remove(QString("}"));
    m_resp.remove(QString(" "));
    m_resp.remove(0,9);

    bool ok = false;
    int id_int = m_resp.toInt(&ok);
    if(ok) src.setProperty(QString("id"),QVariant(id_int));

	WinId_t id(id_int);
	str = CP.CPWebSys_o.SetURL(id,var.toString());
	if (sendRequest(str.toAscii().data(),str.count())) return true;
    replaceSource(src,SubSystemKind_CPWeb);
    return false;
}

bool FC1000::setScene(const Sources &lst)
{
    if (m_busy)  {
        return -1;
    }
    m_busy = true;
    m_sources.clear();
    m_sources_.clear();
    foreach(QSharedPointer<VSource> src, lst) {
        m_sources.push_back(src);
        m_sources_.push_back(src);
    }
    emit setSceneSig();
    return 0;
}

void FC1000::setSceneSlot()
{
    m_busy = true;
    qDebug("FC1000::setSceneSlot()");
	getFrames();
	bool find = false;
    QSharedPointer<VSource> src;
	/* close all windows with no such type */
    for (int j = 0; j < m_framesTmp.count(); j++) {
        TWindowState state = m_framesTmp.at(j);
		find = false;
		for (int i = 0; i < m_sources.count();i++) {
			src = m_sources.at(i);
			QVariant var;
			if (!src->getProperty(F1000_TYPE_PROP,var)) return;
            if (var.toInt() == state.kind()) {
				if (compareFrames(state,src)){
                    if (!src->getEnabledFlag()) continue;
                if (!compareSize(state,src)) {
                    src->setProperty(QString("id"),(int)state.id());
					replaceSource(*src,state.kind());
                }
                QSize origSize(0,0);
                if (!compareCropSize(state,src,origSize)) {
                    src->setProperty(QString("id"),(int)state.id());
                    remakeCrop(*src,origSize);
                }
				m_sources.removeAt(i);		
				find = true;
                continue;
				}
            }
		}
            if (!find) {
				WinId_t id_(state.id());
                closeWindow(id_);
            }
	}

    foreach(QSharedPointer<VSource> src,m_sources) {
		m_src = src;
        if (!src->getEnabledFlag()) continue;
		createSourceSlot();
	}
	m_sources.clear();
    int old_id = -1;
    for (int i = m_sources_.count()-1; i >=0 ; i--) {
        QSharedPointer<VSource> src = m_sources_.at(i);
        QVariant var;
        QVariant var2;
        if (!src->getEnabledFlag()) continue;
        src->getProperty("id",var);
        if (!src->getProperty(F1000_TYPE_PROP,var2)) return;
        replaceSource2(*src,(SubSystemKind)var2.toInt(),old_id);
        remakeCrop(*src,QSize(0,0));
        old_id =var.toInt();
    }
    m_sources_.clear();
    m_busy = false;
    //emit sceneFinished(0);
}

bool FC1000::closeWindow(WinId_t &id)
{	
    if (!id._id()) return true;
	QString str = CP.WinServer_o.DeleteWindow(id);
	if (sendRequest(str.toAscii().data(),str.count())) return true;

/*	m_socket.write(str.toAscii().data(),str.count());
	if ((!getResponse())) {
    	QString str = QString("FC1000 ERROR: No response for "\
                             "Window_o.SetState ;") + m_resp;
   		emit errorSig(str);
	}*/
	/* here parse answers from server */
//	if (checkRespForSucc())  {
//		qDebug("FC1000::closeWindow Successed");
//		return false;
//	}
	
	qDebug("FC1000::closeWindow Rejected");
	return true;
}

bool FC1000::compareFrames(TWindowState &state, QSharedPointer<VSource> src)
{
	QVariant var;
	src->getProperty(F1000_TYPE_PROP,var);
    bool ret = false;
	if (var.toInt() != state.kind()) return false;

	switch(state.kind()) {
	case SubSystemKind_SystemWindow:
        ret = compareSystemWindow(state,src);
	break;
	case SubSystemKind_None:
	case SubSystemKind_Galileo:
	case SubSystemKind_LiveVideo:
	case SubSystemKind_RGBCapture:
        ret = compareNamed(state,src);
        break;
	case SubSystemKind_CPShare:
	case SubSystemKind_VidStream:
	case SubSystemKind_CPWeb:
        ret = compareWebPage(state,src);
        break;
	case SubSystemKind_PictureViewer:
        ret = comparePictureWindow(state,src);
        break;
	case SubSystemKind_CatalistLink:
	case SubSystemKind_IPStream:
	break;
	default:
	break;
	}
    return ret;
}

QString FC1000::getApplication(TWindowState &state) 
{
    Q_UNUSED(state);
    return QString("");
}

/**
* @brief get application path for TWindow state and compare to the src CP_Path
*
* @param state - [in] TWindowState &
* @param src - [in] VSource *
*
* @return  true on equals
*/
bool FC1000::compareSystemWindow(TWindowState &state,QSharedPointer<VSource> src)
{
	QVariant var;
    WinId_t id(state.id());
	QStringList lst_tmp;
	QString str = CP.WinServer_o.GetAppWinInfo(id);
	if (sendRequest(str.toAscii().data(),str.count())) return true;
    /*		
   	m_socket.write(str.toAscii().data(),str.count());
	if ((!getResponse())) {
   		QString str = QString("FC1000 ERROR: No response for "\
                            "WinServer.GetAppWinInfo ;") + m_resp;
   		emit errorSig(str);
     	} */
	if (!checkRespForSucc())  {
		qDebug("FC1000::compareFrames failed");
		return false;
	}

	lst_tmp = m_resp.split(QString("\""));
	if (lst_tmp.count() > 1) m_resp = lst_tmp.at(1);/* 3 just order in response */
    if (!src->getProperty(QString("CP_Path"),var)) return false;
	QString path = var.toString();


    m_resp.replace(QString("\\\\"),QString("\\"));
    bool ret = (path == m_resp);
    if (ret) {
        qDebug() << "find equals windows" << path <<"==" << m_resp;
        src->setProperty("id",(int)id._id());
    } else {
        qDebug() << "windows not equal!!!" << path <<"!=" << m_resp;
	}
    return (ret);

}

/**
* @brief get url for TWindow state and compare to the path
*
* @param state - [in] TWindowState &
* @param src - [in] VSource *
*
* @return  true on equals
*/
bool FC1000::compareWebPage(TWindowState &state,QSharedPointer<VSource> src)
{
	qDebug() << "CompareWebPage";
	QVariant var;
    WinId_t id(state.id());
	QStringList lst_tmp;
	QString str = CP.CPWebSys_o.GetURL(id);
	if (sendRequest(str.toAscii().data(),str.count())) return true;
    /*		
   	m_socket.write(str.toAscii().data(),str.count());
	if ((!getResponse())) {
   		QString str = QString("FC1000 ERROR: No response for "\
                            "WinServer.GetAppWinInfo ;") + m_resp;
   		emit errorSig(str);
     	} */
	if (!checkRespForSucc())  {
		qDebug("FC1000::compareFrames failed");
		return false;
	}
	QString str1 = m_resp.remove(0,11);
    str1 = str1.remove(QString("\"\r\n"));
    if (!src->getProperty(QString("host"),var)) return false;
	QString path = var.toString();

	bool res = (str1 == path) ;
    if (res) {
        src->setProperty("id",(int)id._id());
   }
	return res;
}

bool FC1000::compareNamed(TWindowState &state,QSharedPointer<VSource> src)
{
    qDebug() << "CompareNames";
    QVariant var;
    WinId_t id(state.id());
    QStringList lst_tmp;
    QString str = CP.GalWinSys_o.GetInput(id);

    if (sendRequest(str.toAscii().data(),str.count())) return true;

    if (!checkRespForSucc())  {
        qDebug("FC1000::compareFrames failed");
        return false;
    }
    QString str1 = m_resp.remove(0,11);
    str1 = str1.remove(QString("\"\r\n"));
    if (!src->getProperty(QString("named"),var)) return false;
    QString path = var.toString();
    qDebug()<< "path  atr" ;
    qDebug() << path <<" "<< str1;

    bool res = (str1 == path) ;
    if (res) {
     //   src->setProperty("id",(qlonglong)id._id());
        qDebug() << "EQuals";
    }
    return res;
}

bool FC1000::comparePictureWindow(TWindowState &state,QSharedPointer<VSource> src)
{
    qDebug() << "ComparePictureWindow";
    QVariant var;
    WinId_t id(state.id());
    QStringList lst_tmp;
    QString str = CP.PictureViewerSys_o.GetFileName(id);
    if (sendRequest(str.toAscii().data(),str.count())) return true;
    /*
    m_socket.write(str.toAscii().data(),str.count());
    if ((!getResponse())) {
        QString str = QString("FC1000 ERROR: No response for "\
                            "WinServer.GetAppWinInfo ;") + m_resp;
        emit errorSig(str);
        } */
    if (!checkRespForSucc())  {
        qDebug("FC1000::compareFrames failed");
        return false;
    }
    QString str1 = m_resp.remove(0,11);
    str1 = str1.remove(QString("\"\r\n"));
    if (!src->getProperty(QString("host"),var)) return false;
    QString path = var.toString();
    qDebug()<< "FILE NAME" ;
    qDebug() << path << str1;

    bool res = (str1 == path) ;
    if (res) {
        src->setProperty("id",(int)id._id());
        qDebug() << "EQuals";
    }
    return res;



}

/**
* @brief compare position and size for VSource and WindowState
*
* @param state - [in] TWindowState0 } &
* @param src - [in] VSource *
*
* @return   true if equal
*/
bool FC1000::compareSize(TWindowState &state,QSharedPointer<VSource> src)
{
	QVariant pixs_w;
	QVariant pixs_h;
    int w;
    int h;

	getProperty(QString("pixs_w"),pixs_w);
	getProperty(QString("pixs_h"),pixs_h);
    w = pixs_w.toInt()/1000;
    h = pixs_h.toInt()/1000;

	Pos pos = state.position();
	VPosition src_pos = src->getPosition();
//	return (
//    (pos.x == (src_pos.lt.x() * pixs_w.toInt()))/1000 &&
//    (pos.y == (src_pos.lt.y() * pixs_h.toInt()))/1000 &&
//    (pos.w == ((src_pos.rb.x()  - src_pos.lt.x()) * pixs_w.toInt()))/1000 &&
//    (pos.h == ((src_pos.rb.y()  - src_pos.lt.y()) * pixs_h.toInt()))/1000 );
    //return (
    int dx = (pos.x - ((src_pos.lt.x() * pixs_w.toInt()))/1000);
    int dy = pos.y - (src_pos.lt.y() * pixs_h.toInt())/1000;
    int dw = pos.w - (((src_pos.rb.x()  - src_pos.lt.x()) * pixs_w.toInt()))/1000;
    int dh = pos.h - (((src_pos.rb.y()  - src_pos.lt.y()) * pixs_h.toInt()))/1000;
//    w -= 1;
//    h -= 1;

    return (!
    (((dx <= -w) || (dx >= w)) ||
    ((dy <= -h) || (dy >= h)) ||
    ((dw <= -w) || (dw >= w)) ||
    ((dh <= -h) || (dh >= h))));
}

/**
* @brief compare crop position and crop size for VSource and WindowState
*
* @param state - [in] TWindowState0 } &
* @param src - [in] VSource *
*
* @return   true if equal
*/
bool FC1000::compareCropSize(TWindowState &state,QSharedPointer<VSource> src, QSize &sz)
{
    qDebug() << "CompareCrops";
    QVariant pixs_w;
    QVariant pixs_h;

    getProperty(QString("pixs_w"),pixs_w);
    getProperty(QString("pixs_h"),pixs_h);

    QVariant var;
    WinId_t id(state.id());

    QString str = CP.GalWinSys_o.GetCrop(id);

    if (sendRequest(str.toAscii().data(),str.count())) return true;

    if (!checkRespForSucc())  {
        qDebug("FC1000::compareCrops failed");
        return false;
    }
    QString str1 = m_resp.remove(0,11);
    str1 = str1.remove(QString("\r\n"));
    str1 = str1.remove(QString("\""));
    str1 = str1.remove(QString("}"));
    str1 = str1.trimmed();

    Pos pos;
    pos.cx = (str1.section(" ", 0, 0)).toInt();
    pos.cy = (str1.section(" ", 1, 1)).toInt();
    pos.cw = (str1.section(" ", 2, 2)).toInt();
    pos.ch = (str1.section(" ", 3, 3)).toInt();

    str = CP.GalWinSys_o.GetInputSize(id);

    if (sendRequest(str.toAscii().data(),str.count())) return true;

    if (!checkRespForSucc())  {
        qDebug("FC1000::compareCrops failed");
        return false;
    }
    str1 = m_resp.remove(0,11);
    str1 = str1.remove(QString("\r\n"));
    str1 = str1.remove(QString("\""));
    str1 = str1.remove(QString("}"));
    str1 = str1.trimmed();

    int currW = (str1.section(" ", 0, 0)).toInt();
    int currH = (str1.section(" ", 1, 1)).toInt();



    sz.setWidth(currW);
    sz.setHeight(currH);

    VPosition src_pos = src->getPosition();

    int cx = qRound( currW * src_pos.getCropLxW() );
    int cy = qRound( currH * src_pos.getCropTyH() );
    int cw = qRound( currW * src_pos.getCropSowOnWiw() );
    int ch = qRound( currH * src_pos.getCropSohOnWih() );

    if(cx%2 != 0) // must be even
        cx++;
    if(cw%2 != 0) // must be even
        cw++;

    return ( (cx == pos.cx)  &&
             (cy == pos.cy)  &&
             (cw == pos.cw)  &&
             (ch == pos.ch) );
}

bool FC1000::sendRequest(char *data, uint32_t count, int32_t del)
{
    if (!m_socket->isValid()) {
        return true;
    }
    if (m_socket->state() != QAbstractSocket::ConnectedState) {
        m_busy = false;
        return true;
    }
    if (m_status != status_t::connected)
        return true;
    m_respFlag =false;
    int len = m_socket->write(data,count);
    bool ret =	m_socket->waitForBytesWritten(1000);
    if (!ret) {
        qDebug() << "sendRequest timeout elapsed";
    }

    if (!len) return true;
    qDebug() << "senRequest";
    qDebug() << QString(data);
    usleep(del);
    m_resp.clear();
    if ((!getResponse())) {
        qDebug() << Q_FUNC_INFO << "Error!!!!in FC1000 set Request";
        return false;
    }
    return false;
}

bool FC1000::createNamed(VSource &src)
{
    QString str = CP.GalWinSys_o.NewWindow();
    qDebug() << str;
    QVariant path;

    if (!src.getProperty(QString("named"),path)) {
    return true;
    }

    if (sendRequest(str.toAscii().data(),str.count())) return true;
    QString str1 = m_resp.remove(0,12);
    m_resp.remove(5,4);
    qDebug() << m_resp;
    int id_int = m_resp.toInt();
    src.setProperty(QString("id"),id_int);
    str = CP.GalWinSys_o.SelectInput(WinId_t(m_resp.toInt()),path.toString());
    qDebug() << str;
    if (sendRequest(str.toAscii().data(),str.count())) return true;

    qDebug () << m_resp;
    str = CP.GalWinSys_o.Start(WinId_t(id_int));
    if (sendRequest(str.toAscii().data(),str.count())) return true;
    qDebug () << m_resp;
    replaceSource(src,SubSystemKind_RGBCapture);
    enableSource(src,SubSystemKind_RGBCapture);
    return false;
}

Q_EXPORT_PLUGIN2(FC1000, FC1000)

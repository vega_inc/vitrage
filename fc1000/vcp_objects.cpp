#include "vcp_objects.h"


using namespace ControlPoint;


/* Object */ 
QString Object::setRequest(QString func) 
{
	QString ret(QString("+") + name() 
	+ QString(" ")+ func + QString("\r\n"));
	return ret;
}


/* Window */
Window::Window()
{
	setName("Window");
}


QString Window::GetState(WinId_t id) 
{
	QString str;
	str = QString("GetState ") + id.toString();
	return setRequest(str);
}

QString Window::SetState(TWindowState state)
{
	QString str;
	str = QString("SetState ") + state.toString();
	return setRequest(str);
}

/* Viewer */



QString Viewer::NewWindow()
{
	return setRequest(QString("NewWindow"));
}

QString Viewer::NewWindowWithId(WinId_t id)
{
	return setRequest(QString("NewWindowWithId ") + id.toString());
}


/* PictureViewerSys */


QString  PictureViewerSys::ShowPicture(WinId_t id, QString path)
{
	QString str = QString("ShowPicture ") + id.toString()
	+ QString(" \"") + path + QString("\"") ;
    return setRequest(str);
}

QString PictureViewerSys::GetFileName(WinId_t id)
{
    QString str = QString("GetFileName ") + id.toString();
    return setRequest(str);
}



/* GalWinSys */


QString GalWinSys::QueryAllWindows(void)
{
	QString str = QString("QueryAllWindows");
    return setRequest(str);
}

QString GalWinSys::SelectInput(WinId_t id, QString name)
{
    QString str = QString("SelectInput");
    return setRequest(str + id.toString() + QString(" \"")+
                      name + QString("\""));
    return setRequest(str);
}

QString GalWinSys::GetInput(WinId_t id)
{
    QString str = QString("GetInput ") + id.toString();
    return setRequest(str);
}

QString GalWinSys::Start(WinId_t id)
{
    QString str = QString("Start ") + id.toString();
    return setRequest(str);
}

QString GalWinSys::SetCrop(WinId_t id, Pos crop_pos)
{
    QString s(" ");
    QString str = QString("SetCrop ") + id.toString() + s
            + QString ("{ ") + QString::number(crop_pos.cx) + s +  QString::number(crop_pos.cy)
            + s + QString::number(crop_pos.cw) + s + QString::number(crop_pos.ch) + QString(" }");
    return setRequest(str);
}

QString GalWinSys::GetCrop(WinId_t id)
{
    QString str = QString("GetCrop ") + id.toString();
    return setRequest(str);
}

QString GalWinSys::GetInputSize(WinId_t id)
{
    QString str = QString("GetInputSize ") + id.toString();
    return setRequest(str);
}

/* WinServer */

QString WinServer::QueryAllWindows()
{
	QString str = QString("QueryAllWindows");
	return setRequest(str);
}



QString WinServer::InvokeAppWindow(QString name)
{
	QString str = QString("InvokeAppWindow");
	return setRequest(str + QString(" \"")+ name + QString("\""));
}



QString WinServer::GetAppWinInfo(WinId_t &id)
{
	QString str = QString("GetAppWinInfo") + id.toString();
	return setRequest(str);
}



QString WinServer::DeleteWindow(WinId_t &id)
{
    QString str = QString("DeleteWindow ") + id.toString();
	return setRequest(str);
}



/* CPWebSys */

QString CPWebSys::NewWindow()
{
	QString str = QString("NewWindow");
	return setRequest(str);
}


QString CPWebSys::NewWindowWithId(WinId_t id)
{
    Q_UNUSED(id);
	return QString("trtrt");
}


QString CPWebSys::SetURL(WinId_t id,QString url)
{
	QString tmp = QString ("SetURL") + QString(" ");
	tmp +=id.toString() + QString(" ");
	tmp +=QString("\"") + url + QString("\"");
	return setRequest(tmp);
}



QString CPWebSys::GetURL(ControlPoint::WinId_t id)
{
	QString tmp = QString("GetURL" + QString(" "));
	tmp +=id.toString() + QString(" ");
	return setRequest(tmp);
}











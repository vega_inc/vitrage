#ifndef FRAME_H
#define FRAME_H

#include <QtNetwork/QTcpSocket>

#include <vsource/vsource.h>
#include <vcontroller/vcontroller.h>
#include "vcontrolpoint.h"

#include "vcp_structs.h"
#include "vcp_objects.h"

class Frame  {

public:
    Frame(const QString & name, const QString & clonename, const QString & type, WinId_t id);

	void setName(QString );
	void setCloneName(QString );
	void setType(QString);
	void setId(WinId_t);
	void setId(uint64_t id);

	QString getName(void);
	QString getCloneName(void);
	QString getType(void);
	WinId_t getId(void);
	uint64_t getIdInt(void);

private:
	QString m_name;
	QString m_cloneName;
	QString m_type;
	WinId_t m_id;
};

#endif /* FRAME_H */

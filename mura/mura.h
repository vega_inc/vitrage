#ifndef MURA_H
#define MURA_H

#include <vcontroller/vcontroller.h>
#include <QTimer>
#include <QTcpSocket>

static const QString VEGALAYOUT = "VegaLayout";
typedef QMap<QString,QVariant> InnerMap;
class Mura : public VController {
    Q_OBJECT
    Q_INTERFACES(AbstractVController)
public:
    Mura();
    ~Mura();
    virtual bool conn(void);
	bool disconn(void);
	bool placeSource(VSource &);
    //bool replaceSource2(VSource &src,SubSystemKind kind,int old_id);
	bool createSource(VSource &);
	bool startSource(VSource &);
	bool stopSource(VSource &);
	bool setBrightness(uint32_t );
	void run(void);
    VController * clone();
	void setScene(QList<VSource *> &);

private slots:
    void newState(QAbstractSocket::SocketState);
    void conn_slot(void);
	void disconnected(void);
    void sock_err(QAbstractSocket::SocketError );
	void ready(void);
	void timer_timeout(void);
	bool checkRespForSucc(void);
    void readFinished(void);
	void setSceneSlot(void);
	void createSourceSlot(void);
    void disSlot();
        void conSlot();
private:
	static const uint32_t CODE_LENGHT = 8;
	bool sendRequest(char *data,uint32_t count);
	const QString m_success;
    QTcpSocket *m_socket;
    QThread * m_thread;
	uint32_t m_lag;
	QMutex m_mutex;
	uint32_t m_wpixs; /* pixels in small cell */
	uint32_t m_hpixs; /* pixels in big cell */
	VSource *m_src;
    QList<VSource *> m_sources; /*list for set Scene method */
    QList<VSource *> m_sources_; /*list for set Scene method (to set Z order)*/
    QMultiMap< QString , QMap < QString , QVariant >* > m_sourcesMap;
    QMultiMap< QString , QPair< int , int > > m_sourcesMapInputs;
    QString m_resp; /* response */
    bool m_respFlag;
	QUrl m_url;
	QTimer m_timer;
     bool m_busy;
    //VControlPoint CP;
	bool compareFrames(void);
    bool comparePos(VSource *src,InnerMap *map);
    bool replaceSource(VSource *Src,InnerMap *map);
    bool replaceSource(VSource *src);
    bool replaceZSource(QString name, int id, int z);
    //void getFrames(void);
	void startTimer(void);
    bool getResponse();
    void fillSources(int maxId);
    void clearSourcesMap(void);
    void fillSourceMap(int id);
    bool closeSources(void);
    bool createSourcesProto(void);
    bool deleteSource(QString name);
    void setSource(VSource *src);
    bool createPicture(VSource *src);
    bool createApplication(VSource *src);
    bool createNamed(VSource *src);
    void removeFromMap(QString str);
    bool applyLayout(void);
    bool layout(void);
    int getWindowCount(void);
    virtual bool enableSourceFlag(VSource &s,bool f) {Q_UNUSED(s);Q_UNUSED(f);return false;}
signals:
    void createSourceSig(void);
    void setSceneSig(void);
    void finishSetScene(void);
    void dissig();
    void consig();
};



#endif /* MURA_H */

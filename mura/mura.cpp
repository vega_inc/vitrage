#include <qplugin.h>
#include "mura.h"

#define DEF_LAG 10000         /* default lag in milliseconds */
#define DEF_HOST "localhost" /* default host */
#define DEF_PORT 25456       /* default port */
#define DEF_DIF 5            /* default difference between sizes */

#define ESC "\r\n"


Mura::Mura():m_success("OK")
{

    m_socket  = new QTcpSocket();

    connect(m_socket,SIGNAL(connected()),this,SLOT(conn_slot()));
    connect(m_socket,SIGNAL(disconnected()),this,SLOT(disconnected()));
    qRegisterMetaType<QAbstractSocket::SocketError>("QAbstractSocket::SocketError") ;
    connect(m_socket,SIGNAL(error(QAbstractSocket::SocketError)),
            this,SLOT(sock_err(QAbstractSocket::SocketError)));
    connect(m_socket,SIGNAL(readyRead()),this,SLOT(ready()));
    //connect(&m_timer,SIGNAL(timeout()),this,SLOT(timer_timeout()));
    qRegisterMetaType<QAbstractSocket::SocketState>("QAbstractSocket::SocketState");
    connect(m_socket,SIGNAL(stateChanged(QAbstractSocket::SocketState)),this
            ,SLOT(newState(QAbstractSocket::SocketState)));
    connect(m_socket,SIGNAL(readChannelFinished()),this,SLOT(readFinished()));

	connect(this,SIGNAL(createSourceSig()),this,
                                        SLOT(createSourceSlot()),Qt::QueuedConnection);
    connect(this,SIGNAL(setSceneSig()),this,SLOT(setSceneSlot()),Qt::QueuedConnection );
    connect(this,SIGNAL(dissig()),this,SLOT(disSlot()));
        connect(this,SIGNAL(consig()),this,SLOT(conSlot()));
    setProperty(QString("model"),QString("Mura"));
    setProperty(CONTR_NAME,"");
    setProperty(CONTR_HOST,"localhost");
    setProperty(CONTR_PORT,"23");
    setProperty(CONTR_PASSWD,"");
    m_errorMsg = QString("Not started");
    m_busy = false;
	setStatus(stopped);
}


void Mura::newState(QAbstractSocket::SocketState state)
{
    qDebug() << "State changed" << state;
}



void Mura::run()
{
    exec();
}

VController *Mura::clone()
{
    return new Mura();
}




Mura::~Mura()
{
    this->exit();
    this->quit();
}
bool Mura::conn(){

    emit consig();
}


void Mura::conSlot()
{
    m_socket->flush();
    m_socket->close();

//    if (m_socket->isOpen()){
//       m_socket->close();
//   }

	QVariant host_var , port_var,lag;
	if(getProperty(QString("host"),host_var)) {
	m_url.setHost(host_var.toString());
	}else m_url.setHost(DEF_HOST);

	if(getProperty(QString("port"),port_var)) {
	m_url.setPort(port_var.toInt());
	} else m_url.setPort(DEF_PORT);

	if (!getProperty("timeout",lag)) {
        m_lag = DEF_LAG;
	} else m_lag = lag.toUInt();

    startTimer();

    if (!m_socket->isOpen()) {
    m_socket->connectToHost(m_url.host(),m_url.port());
    m_socket->waitForConnected(2000);
    if (!isRunning()) {
        moveToThread(this);
        m_socket->moveToThread(this);
 //      m_socket->moveToThread(this);
        start();
        qDebug() << "Mode to Thread";
        }
    }
 //   return true;
}



void Mura::timer_timeout()
{
	m_timer.stop();
    QString str("Mura ERROR:Timout error");
    setStatus(error);
        m_busy = false;
    //m_socket->abort();
    emit statusChanged();
    emit errorSig(str);
}


void Mura::startTimer(void)
{	
	QVariant lag;
	quint32 lag_int;
    //if (!getProperty("timeout",lag)) {
	lag_int = DEF_LAG;
//	} else lag_int = lag.toUInt();
	m_timer.start(lag_int);
}



bool Mura::disconn(void)
{
//    QString dummy("dlENameummy");
////    if (!m_socket->isWritable()) return false;
//    m_socket->write(dummy.toAscii().data());
//    m_socket->waitForBytesWritten(1000);
//    m_socket->disconnectFromHost();

//    setStatus(disc);
//    m_socket->close();
//    //m_socket.abort();
//    m_errorMsg = QString(tr("Disconnected"));
//    emit statusChanged();
//        m_busy = false;
    emit dissig();
    return false;
}




void Mura::disSlot()
{
    // if (m_busy) return true;
    QString dummy("dlENameummy\r\n");
     if (m_socket->state() == QAbstractSocket::UnconnectedState) {
           setStatus(disc);
         emit statusChanged();

         m_busy = false;
         return;
     }
     m_socket->write(dummy.toAscii().data());
     m_socket->waitForBytesWritten(1000);

     m_socket->disconnectFromHost();
     if (m_socket->state() == QAbstractSocket::UnconnectedState ||
             m_socket->waitForDisconnected(1000))
     setStatus(disc);
     //m_socket->close();
    // m_socket->abort();

     m_errorMsg = QString(tr("Disconnected"));
     emit statusChanged();
     m_busy = false;
     //return false;
}




bool Mura::placeSource(VSource &src)
{
    Q_UNUSED(src);
    return false;
}

void Mura::createSourceSlot(void)
{
	qDebug() << "createSourceSlot";
}



bool Mura::createSource(VSource & src)
{
	m_src = &src;
	emit createSourceSig();
	return false;
}



bool Mura::startSource(VSource &)
{
	return false;
}
bool Mura::stopSource(VSource &)
{
    return false;
}

bool Mura::setBrightness(uint32_t )
{
	return false;
}


void Mura::conn_slot()
{
    QString er_str = QString(tr("Controller connected"));
    m_timer.stop();
    m_busy = true;
    m_errorMsg = er_str;
    if (layout()) {

    }
    applyLayout();
    messSig(er_str);
    setStatus(connected);
    m_busy = false;
    emit statusChanged();
}


void Mura::disconnected()
{
    qDebug() << "Disconnnected signal;";
}

void Mura::sock_err(QAbstractSocket::SocketError err)
{

    QString str = QString("Mura ERROR: Socker Error ") + QString::number(err);
    qDebug() << str;
    bool flag  = false;
    QString msg;
    switch (err) {
    case (QAbstractSocket::ConnectionRefusedError) :
        flag = true;
        msg = QString("ConnectionRefusedError");
    break;
    case (QAbstractSocket::NetworkError) :
        flag = true;
        msg = QString("NetworkError");
    break;
    default:
    break;

    }
    if (flag) {
        setStatus(error);
        m_errorMsg = msg;
        emit statusChanged();
        //emit err
    }

}

void Mura::ready()
{
    qDebug() << "Ready slot";
    m_respFlag = true;
}




bool Mura::getResponse()
{
    qDebug() << "getResponse";
    qDebug() << m_lag;
    m_resp.clear();
    bool ret = true;
  //  if (!m_respFlag) {

    if (m_socket->state() != QAbstractSocket::ConnectedState) {
          m_busy = false;
          return true;
      }
        ret = m_socket->waitForReadyRead(3000);
    //}
        if (!m_socket->isValid()) {
            m_busy = false;
            return true;
        }
        QByteArray buffer;
        bool flag = true;
        int my = 0;
        while (flag) {
            qDebug() << "Iter";
            if (!m_socket->isValid()) {
                m_busy = false;
                return true;
            }
            qDebug("Before");
            //buffer += m_socket->readAll();
            buffer += m_socket->read(99999);

            qDebug("After");
            if (((QString(buffer).contains("\r\n"))) || (buffer.isEmpty())) {
                flag = false; } else {

                m_socket->waitForReadyRead(100);
                my++;
                if (my > 100) flag = false;
            }
        }
        qDebug() << ">Response";
        qDebug() << m_resp;
        m_resp = QString(buffer);
        if (!m_resp.count()){
            qDebug() << "Empty responce";
            ret = false;
        }
        int pos = m_resp.indexOf("\r\n");
        m_resp.remove(0,pos+2);
        return ret;
}

void Mura::fillSources(int maxId)
{
    clearSourcesMap();
    m_sourcesMapInputs.clear();
    for (int i = 0; i <= maxId; i++) {
        fillSourceMap(i);
   }
}

void Mura::clearSourcesMap()
{
    foreach(QString str,m_sourcesMap.keys()) {
        QMultiMap< QString , QMap < QString , QVariant >* >::iterator it =  m_sourcesMap.find(str);
        int size = m_sourcesMap.count(str);
        for (int i = 0; i < size; i++) {
            QMap<QString, QVariant> * innerMap = *it++;
            if (innerMap)
            delete innerMap; /* remove inner map */
            m_sourcesMap.remove(str,innerMap);
        }
    }
    m_sourcesMap.clear();
}

void Mura::fillSourceMap(int id)
{
      QString  send = "Window " + QString("\"")+ VEGALAYOUT + QString("\" ") + QString::number(id) + ESC;
      if (sendRequest(send.toAscii().data(),send.count())) return ;
      qDebug() << m_resp;
      if (m_resp.contains("Invalid")){
          return;
      }
      QMultiMap<QString,QVariant> * map = new QMultiMap<QString,QVariant>;
      int index,pos1;
      index =   m_resp.indexOf("Source");
      QString name;
      pos1 = m_resp.indexOf(ESC,index);
      for (int k = index+20 ; k <pos1 ; k++) {
         name += m_resp[k];
      }

      index =   m_resp.indexOf("Z");
      QString Z;
      pos1 = m_resp.indexOf(ESC,index);
      for (int k = index+20 ; k <pos1 ; k++) {
         Z += m_resp[k];
      }

      index =   m_resp.indexOf("Position");
      QString position;
      pos1 = m_resp.indexOf(ESC,index);
      for (int k = index+20 ; k <pos1 ; k++) {
         position += m_resp[k];
      }

      QStringList list1 = position.split(",");
      if (list1.count()!= 4) {
       delete  map;
       return;
      }
      map->insert("PosX",list1.at(0));
      map->insert("PosY",list1.at(1));
      map->insert("PosW",list1.at(2));
      map->insert("PosH",list1.at(3));
      map->insert("SourceName",name);
      map->insert("SourceZ",Z);
      map->insert("SourceId",id);
      qDebug() << name;
      //m_sourcesMap.insertMulti(name,map);
      m_sourcesMap.insertMulti(name,map);
    //  if (name.contains("Input")) {
        m_sourcesMapInputs.insert(name,QPair<int,int>(id,Z.toInt()));
    //  }

}

bool Mura::closeSources(void)
{
    QVariant id;
    QList<InnerMap *> lst = m_sourcesMap.values();
    QMap<int,InnerMap*> mmap;
    for( int i = 0 ; i < lst.count() ; i++)  {
         InnerMap * mp = lst.at(i);
        id = mp->value("SourceId");
        mmap.insert(id.toInt(),mp);
    }
    for (QMap<int,InnerMap*>::Iterator it = mmap.end()-1;it !=mmap.begin()-1;it--) {
        InnerMap * map = it.value();
        id = map->value("SourceId");
        QString  send = "DeleteWindow " + QString("\"")+ VEGALAYOUT + QString("\" ")
                                                        + id.toString() + ESC;
        sendRequest(send.toAscii().data(),send.count());
    }
    return true;
}

bool Mura::createSourcesProto()
{
   foreach(QString name,m_sourcesMap.keys()) {
    deleteSource(name);
   }
   foreach(VSource * src,m_sources) {
       if (!src->getEnabledFlag()) continue;
    // deleteSource(src);
     setSource(src);
   }
   return false;
   //applyLayout();
}

bool Mura::deleteSource(QString name)
{
    QString  send = "DeleteSource " + QString("\"")+ name + QString("\" ") + ESC;
     if (sendRequest(send.toAscii().data(),send.count())) return true;
     if (!m_resp.contains("Ok")) {
         return true; }
     return false;
}

void Mura::setSource(VSource *src)
{
       QVariant type;
        if(!src->getProperty(QString("type"),type)) {
        }
        QString type_str =  type.toString();
        if (type_str == PIC_TYPE) {
            createPicture(src);
           // createApplication(src);
        }
        if (src->getPhysName() == "Input 1") {
            createNamed(src);
            return;
        }


        if ((type_str == VNC_TYPE) ||
            (type_str == IPCAMERA_TYPE)) {
            createApplication(src);
        }
        if (type_str == WBPAGE_TYPE) {
            createApplication(src);
        }

        if (type_str == NAMED_TYPE) {
            createNamed(src);
        }
}

bool Mura::createPicture(VSource *src)
{
    QString    send = "CreateSource " + QString("\"")+ src->getPhysName() + QString("\" ")
            + QString("Image ") + ESC;
    if (sendRequest(send.toAscii().data(),send.count())) return -1;
    QVariant path;
    src->getProperty(QString("muraHost"),path);

    send = "SetSource " + QString("\"")+ src->getPhysName() + QString("\"")
           + " Image " + "/P:\"" +   path.toString()+ QString("\" ")+ ESC;
    if (sendRequest(send.toAscii().data(),send.count())) return true;
    if (!m_resp.contains("Ok")) {
        return true; }

    QVariant pixs_w;
    QVariant pixs_h;
    getProperty(QString("pixs_w"),pixs_w);
    getProperty(QString("pixs_h"),pixs_h);
    int x,y,w,h;
    VPosition src_pos = src->getPosition();

    x = (src_pos.lt.x() * pixs_w.toInt())/1000;
    y = (src_pos.lt.y() * pixs_h.toInt())/1000;
    w = ((src_pos.rb.x()  - src_pos.lt.x()) * pixs_w.toInt())/1000;
    h = ((src_pos.rb.y()  - src_pos.lt.y()) * pixs_h.toInt())/1000;

    w+=x;
    h+=y;

    send = "AddWindow " + QString("\"")+ VEGALAYOUT +  QString("\" ")
    + QString::number(x) + "," + QString::number(y) + "," +
    QString::number(w) + ","  + QString::number(h) +QString(" \"")
//    + src->getname() + QString("\" ") + ESC;
      + src->getPhysName() + QString("\" ") + ESC;
    if (sendRequest(send.toAscii().data(),send.count())) return true;
   // if (!m_resp.contains("Ok")) {
    //    return true; }
    return false;
}

bool Mura::createApplication(VSource *src)
{
    QString    send = "CreateSource " + QString("\"")+ src->getPhysName() + QString("\" ")
            + QString("Application ") + ESC;
    if (sendRequest(send.toAscii().data(),send.count())) return -1;
    QVariant path;
    src->getProperty(QString("command"),path);

    send = "SetSource " + QString("\"")+ src->getPhysName() + QString("\"")
           + " Application " + "/CMD:\"" +   path.toString()+ QString("\" ")+ ESC;
    if (sendRequest(send.toAscii().data(),send.count())) return true;
    if (!m_resp.contains("Ok")) {
        return true; }

    QVariant pixs_w;
    QVariant pixs_h;
    getProperty(QString("pixs_w"),pixs_w);
    getProperty(QString("pixs_h"),pixs_h);
    int x,y,w,h;
    VPosition src_pos = src->getPosition();

    x = (src_pos.lt.x() * pixs_w.toInt())/1000;
    y = (src_pos.lt.y() * pixs_h.toInt())/1000;
    w = ((src_pos.rb.x()  - src_pos.lt.x()) * pixs_w.toInt())/1000;
    h = ((src_pos.rb.y()  - src_pos.lt.y()) * pixs_h.toInt())/1000;
    w+=x;
    h+=y;
    send = "AddWindow " + QString("\"")+ VEGALAYOUT +  QString("\" ")
    + QString::number(x) + "," + QString::number(y) + "," +
    QString::number(w) + ","  + QString::number(h) +QString(" \"")
    + src->getPhysName() + QString("\" ") + ESC;
    if (sendRequest(send.toAscii().data(),send.count())) return true;
   // if (!m_resp.contains("Ok")) {
    //    return true; }
    return false;
}

bool Mura::createNamed(VSource *src)
{
    QVariant pixs_w;
    QVariant pixs_h;
    getProperty(QString("pixs_w"),pixs_w);
    getProperty(QString("pixs_h"),pixs_h);
    int x,y,w,h;
    VPosition src_pos = src->getPosition();

    x = (src_pos.lt.x() * pixs_w.toInt())/1000;
    y = (src_pos.lt.y() * pixs_h.toInt())/1000;
    w = ((src_pos.rb.x()  - src_pos.lt.x()) * pixs_w.toInt())/1000;
    h = ((src_pos.rb.y()  - src_pos.lt.y()) * pixs_h.toInt())/1000;
    w+=x;
    h+=y;
    QString send = "AddWindow " + QString("\"")+ VEGALAYOUT +  QString("\" ")
    + QString::number(x) + "," + QString::number(y) + "," +
    QString::number(w) + ","  + QString::number(h) +QString(" \"")
    + src->getPhysName() + QString("\" ") + ESC;
    if (sendRequest(send.toAscii().data(),send.count())) return true;
   // if (!m_resp.contains("Ok")) {
    //    return true; }
    return false;
}

void Mura::removeFromMap(QString str)
{
    QMap<QString, QVariant> * innerMap = m_sourcesMap.value(str);
    if (innerMap)
    delete innerMap; /* remove inner map */
    m_sourcesMap.remove(str,innerMap);
}

bool Mura::applyLayout(void)
{
    QString send = "ApplyLayout " + QString("\"")+ VEGALAYOUT + QString("\"") + ESC;
    if (sendRequest(send.toAscii().data(),send.count())) return true;
    if (m_resp.contains("Ok"))  return false;
    return true;
}



bool Mura::layout()
{
    QString send = "start" + QString("\r\n");
    if (sendRequest(send.toAscii().data(),send.count())) return true;
    send = "Layout" + QString("\r\n");
    if (sendRequest(send.toAscii().data(),send.count())) return true;
    if (m_resp.contains(VEGALAYOUT)) { return false; } else {
       send = "CreateLayout " + QString("\"")+ VEGALAYOUT + QString("\"") + ESC;
       if (sendRequest(send.toAscii().data(),send.count())) return true;
       if (m_resp.contains("OK"))  return false;

    };
    return true;
}

int Mura::getWindowCount()
{
    QString    send = "Window " + QString("\"")+ VEGALAYOUT + QString("\"") + ESC;
    if (sendRequest(send.toAscii().data(),send.count())) return -1;
    if (m_resp.contains("NoWindow")) {
        return 0;
    }
    int  index = m_resp.lastIndexOf(ESC);
//    m_resp.remove(0,index-1);
    m_resp.remove("0-");
    m_resp.remove(index,2);
    //m_resp.remove(QString(ESC) + ">");
    return m_resp.toInt();
}



bool Mura::compareFrames(void)
{
    return true;
}

bool Mura::comparePos(VSource *src, InnerMap *map)
{
    QVariant pixs_w;
    QVariant pixs_h;
    int w;
    int h;

    QVariant x,y,w_,h_;
    x = map->value("PosX");
    y = map->value("PosY");
//    w_ = map->value("PosW");
//    h_ = map->value("PosH");
  h_ = map->value("PosW");
  w_ = map->value("PosH");
    getProperty(QString("pixs_w"),pixs_w);
    getProperty(QString("pixs_h"),pixs_h);
    w = pixs_w.toInt()/1000;
    h = pixs_h.toInt()/1000;

    VPosition src_pos = src->getPosition();
    int dx = (x.toInt() - ((src_pos.lt.x() * pixs_w.toInt()))/1000);
    int dy = y.toInt() - (src_pos.lt.y() * pixs_h.toInt())/1000;
    int dw = w_.toInt() - (((src_pos.rb.x()  - src_pos.lt.x()) * pixs_w.toInt()))/1000;
    int dh = h_.toInt() - (((src_pos.rb.y()  - src_pos.lt.y()) * pixs_h.toInt()))/1000;
    w -= 1;
    h -= 1;
    return (!
    (((dx < -w) || (dx > w)) ||
    ((dy < -h) || (dy > h)) ||
    ((dw < -w) || (dw > w)) ||
     ((dh < -h) || (dh > h))));
}

bool Mura::replaceSource(VSource *src, InnerMap *map)
{
    QVariant id;
    int  x,y,w,h;
    id = map->value("SourceId");
    /*x = map->value("PosX");
    y = map->value("PosY");
    w = map->value("PosW");
    h = map->value("PosH");*/
    QVariant pixs_w;
    QVariant pixs_h;
    getProperty(QString("pixs_w"),pixs_w);
    getProperty(QString("pixs_h"),pixs_h);
    VPosition src_pos = src->getPosition();
    x = (src_pos.lt.x() * pixs_w.toInt())/1000;
    y = (src_pos.lt.y() * pixs_h.toInt())/1000;
    w = ((src_pos.rb.x()  - src_pos.lt.x()) * pixs_w.toInt())/1000;
    h = ((src_pos.rb.y()  - src_pos.lt.y()) * pixs_h.toInt())/1000;

    QString  send = "SetWindow " + QString("\"")+ VEGALAYOUT + QString("\" ")
           + id.toString() + " /P:" +QString::number(x) + "," + QString::number(y) + "," +
            QString::number(w+x) + "," + QString::number(h+y) + " /S:\"" + src->getPhysName() + ("\"") + ESC;
    if (sendRequest(send.toAscii().data(),send.count())) return true;
    if (m_resp.contains("Ok")) {
       return false; }
    return true;
}


bool Mura::replaceZSource(QString name, int id, int z)
{
    Q_UNUSED(z);
    QString  send = "SetWindow " + QString("\"")+ VEGALAYOUT + QString("\" ")
           + QString::number(id) + " /Z:" + QString::number(0) + " /S:\"" + name + ("\"") + ESC;
    if (sendRequest(send.toAscii().data(),send.count())) return true;
    if (m_resp.contains("Ok")) {
       return false; }
    return true;
}




bool Mura::replaceSource(VSource *src)
{
    Q_UNUSED(src);
    return true;
}


bool Mura::checkRespForSucc(void)
{	
    return (m_resp.startsWith(m_success));
}

void Mura::readFinished()
{
    qDebug() << " Read finished";
}



void Mura::setScene(QList<VSource *> &lst)
{
    if (m_busy)  {
        return;
    }
    m_busy = true;
    m_sources.clear();
    m_sources_.clear();
    foreach(VSource *src, lst) {
        m_sources.push_back(src);
        m_sources_.push_back(src);
    }
    emit setSceneSig();
}


void Mura::setSceneSlot(void)
{
    m_busy = true;
    if (m_socket->state() != QAbstractSocket::ConnectedState) {
          m_busy = false;
         emit sceneFinished();
          return;
      }
    int size = getWindowCount();
    fillSources(size);

    foreach (VSource* src, m_sources) {
        if (!src->getEnabledFlag()) continue;
        foreach(InnerMap *map,m_sourcesMap.values()) {
           QString name = map->value("SourceName").toString();
         //  if (name == src->getPhysName()) {
           if (name == src->getPhysName()) {
               if (!comparePos(src,map)) {
                     replaceSource(src,map);
                     m_sources.removeOne(src);
                     removeFromMap(name);
                     //continue;
                     break;
               }
                 m_sources.removeOne(src);
                 removeFromMap(name);
                 //continue;
                 break;
           }
    }
    }
    closeSources();
    createSourcesProto();

//    for (int i = 0; i < m_sources_.count();i++) {
//        VSource *src = m_sources_.at(i);
//        QList<QString> ls = m_sourcesMapInputs.keys();
//        if (ls.count()) {
//           for(int j = ls.count()-1; j >= 0 ; j--) {
//                QString str = ls.at(j);
//          //   if (str == src->getname()) {
//                if (str == src->getPhysName()) {
//                 QPair<int,int> pair = m_sourcesMapInputs.value(str);
//                // if (pair.second != (m_sources_.count() - i))
//          //      replaceZSource(src->getname(),pair.first,m_sources_.count() - i);
//                  replaceZSource(src->getPhysName(),pair.first,m_sources_.count() - i);
//            }
//            }
//         }
//    }

    size = getWindowCount();
    fillSources(size);
        for (int i = m_sources_.count()-1; i >= 0;i--) {
            VSource *src = m_sources_.at(i);
            QList<QString> ls = m_sourcesMapInputs.keys();
            if (ls.count()) {
               for(int j = ls.count()-1; j >= 0 ; j--) {
                    QString str = ls.at(j);
              //   if (str == src->getname()) {
                    if (str == src->getPhysName()) {
                     QPair<int,int> pair = m_sourcesMapInputs.value(str);
                    // if (pair.second != (m_sources_.count() - i))
              //      replaceZSource(src->getname(),pair.first,m_sources_.count() - i);
                      replaceZSource(src->getPhysName(),pair.first,m_sources_.count() - i);
                }
                }
             }
        }

     m_sources_.clear();
     m_busy = false;
     emit sceneFinished();
 }


bool Mura::sendRequest(char *data, uint32_t count)
{
    m_busy  = true;
    qDebug()  << "Send Request";
    m_respFlag =false;
    qDebug()  << "Send Request 1";
    m_socket->readAll();
    qDebug()  << "Send Request 2";
    int len = m_socket->write(data,count);
    qDebug()  << "Send Request 3";
    bool ret =	m_socket->waitForBytesWritten(10000);
    qDebug()  << "Send Request 4";
    if (!ret) {
        qDebug() << "sendRequest timeout elapsed";
    }
    qDebug()  << "Send Request 5";
  //  m_socket->flush();
    if (!len) return true;
    qDebug() << "senRequest";
//    qDebug() << QString(data);
    m_resp.clear();
    if (!getResponse()) {
            return true;
    }
//    qDebug() << "getResponse:" << m_resp;QString
    return false;
}

Q_EXPORT_PLUGIN2(Mura,Mura)

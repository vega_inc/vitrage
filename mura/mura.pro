include(../common.pri)

TARGET = mura 
CONFIG +=   rtti \
            debug \
            plugin

QT += network

DESTDIR = $$INSTALL_PATH_CTRL_PLUGINS 

TEMPLATE = lib

DEFINES += VCONTROLLER_LIBRARY

INCLUDEPATH += $$INSTALL_PATH_INCLUDE \
		/usr/include/

SOURCES += mura.cpp                 
HEADERS += mura.h


unix {
       target.path = /$(DESTDIR)
       INSTALLS += target
     }


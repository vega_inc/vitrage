#include "vipcamthread.h"
#include <QDebug>

VIpCamThread::VIpCamThread():
    VSourceThread(),
    w(110),
	h(110)
{
    timer =new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(timerSlot()));

	
    downloadManager = new QNetworkAccessManager();
    connect(downloadManager, SIGNAL(finished(QNetworkReply*)), this, 
                                	SLOT(downloadFinished(QNetworkReply*)));
    connect(downloadManager,SIGNAL(
	networkAccessibleChanged(QNetworkAccessManager::NetworkAccessibility)),this,
	SLOT(networkAccessible(QNetworkAccessManager::NetworkAccessibility)));
	downloadManager->moveToThread(this);									
    request = new QNetworkRequest();
    request->setRawHeader("User-Agent", "");
    reply = NULL;
	imageReader = new QImageReader();
	imageReader->setAutoDetectImageFormat(true);

	currentImage = new QImage(640,480,QImage::Format_RGB888);

    imageBuffer = new QBuffer();
	imageBuffer->open(QBuffer::ReadWrite);

	imageReader->setDevice(imageBuffer);
    moveToThread(this);

}

VIpCamThread::~VIpCamThread()
{
    delete timer;
}

void VIpCamThread::setUrl(QUrl url)
{
	currentUrl = url;
    QString str = url.toString();
    qDebug() << str;
}



void VIpCamThread::downloadFinished(QNetworkReply *reply)
{
    imageReader->setDevice(reply);
    qDebug() << "start reading image";
    imageReader->read(currentImage);
    qDebug() << "reading image done";
    newImageGrabbed(currentImage);
    timer->start(140);
}




void VIpCamThread::run() 
{
    timer->start(100);
	exec();
}





void VIpCamThread::timerSlot(void)
{
    qDebug("Timer slot");
	timer->stop();
	sendRequest();
}

void VIpCamThread::newImageGrabbed(QImage *img)
{
	Q_UNUSED(img);
    setImages(*currentImage);
    emit updatedImage();
}

void VIpCamThread::downloadErrorSlot(QNetworkReply::NetworkError error )
{
	qDebug("dowloadErrorSlot");
    qDebug() << error ;
    if (reply != NULL) {
        qDebug() << reply->errorString();
    }
        emit statusChanged(VSource::ErrorStatus);
}



void VIpCamThread::sendRequest()
{
    request->setUrl(currentUrl);
    QString str = currentUrl.toString();
    reply = downloadManager->get(*request);
    connect(reply , SIGNAL(error(QNetworkReply::NetworkError)), this,
                         SLOT(downloadErrorSlot(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(readyRead()), this, SLOT(replyDataAvailable()));
    currentImageSize = 0;
    mjpgState = MjpgBoundary;
    streamType = StreamTypeUnknown;
    imageBuffer->seek(0);
}

void VIpCamThread::replyDataAvailable()
{
   QString cLine;
    if (mjpgState == MjpgBoundary) {
        if (streamType == StreamTypeUnknown) {
            if (reply->bytesAvailable() >= 50) {
                cLine = reply->readLine(51);
                if (cLine.startsWith("mjpeg")) {
                    streamType = StreamTypeWebcamXP;
                    bool ok = false;
                    currentImageSize = cLine.mid(5,8).toInt(&ok);
                    if (ok) {
                        imageBuffer->seek(0);
                        mjpgState = MjpgJpg;
                        qWarning() << currentImageSize << "CI" << cLine;
                    } else {
                        qWarning() << QString("Could not convert %1 to number").arg(cLine.mid(5,7));
                    }
                    // we need to seek a bit
                } else {
                    streamType = StreamTypeMjpgStreamer;
                }
            } else { 
                return;
            }
        }

        if (mjpgState == MjpgBoundary) {
            if (streamType == StreamTypeWebcamXP) {
                if (reply->bytesAvailable() >= 50) {
                    QByteArray borderArray = reply->read(51);
                    if (!borderArray.startsWith("mjpeg")) {
                        qWarning() << "invalid border" << borderArray;
                        return;
                    } else {
                        bool ok = false;
                        currentImageSize = borderArray.mid(5,8).toInt(&ok);
                        if (ok) {
                            imageBuffer->seek(0);
                            mjpgState = MjpgJpg;
                            qWarning() << currentImageSize << "CI" << borderArray;
                        } 
                    }
                } else {  
                    return;
                }
            } else if(streamType == StreamTypeMjpgStreamer) {
                bool quitNext = false;
                while(reply->canReadLine()) {
                    QString cLine = reply->readLine();
                    //qDebug()<< cLine;
                    if(quitNext)
                        break;
                    if (cLine.startsWith("Content-Length:")) {
                        bool ok = false;
                        currentImageSize = cLine.mid(16).toInt(&ok);
                        if (!ok) {
                            return;
                        }
                        mjpgState = MjpgJpg;
                        quitNext = true;
                    } else if (cLine.startsWith("X-Timestamp:")) {
                        if (m_timestampRegexp.indexIn(cLine) > -1) {
                            m_timestampInMs =
                                    m_timestampRegexp.cap(1).toLong() * 1000 +
                                    m_timestampRegexp.cap(2).toLong();
                        }
                        mjpgState = MjpgJpg;
                        quitNext = true;
                    }
                }
            }
        }
    }

    if (mjpgState == MjpgJpg) {
        imageBuffer->write(reply->read(currentImageSize - imageBuffer->pos()));
        if (imageBuffer->pos() == currentImageSize) {
            bool ok = false;
            imageReader->setDevice(imageBuffer);
            imageBuffer->seek(0);
            ok = imageReader->read(currentImage);
            QString str = imageReader->errorString();
            imageBuffer->seek(0);
            if (ok == true) {
                newImageGrabbed(currentImage);
                //calcFPS(requestTime.msecsTo(QTime::currentTime()));
//                requestTime = QTime::currentTime();
            } else {
                qWarning() << "Image read fail" << imageReader->errorString();
            }
            mjpgState = MjpgBoundary;
        }
    }
}

void VIpCamThread::networkAccessible(QNetworkAccessManager::NetworkAccessibility  status)
{
	qDebug("NEW NETWORK STATUS");
	qDebug() << status;
}


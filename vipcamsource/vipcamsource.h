#ifndef VIPCAMSOURCE_H
#define VIPCAMSOURCE_H
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QBuffer>
#include <QtNetwork/QNetworkRequest>
#include <QtCore/qplugin.h>

#include <vsource/vsource.h>

#define IPCAM_SOURCE "IpCameraMJpeg"

/**
* @brief class backend source device for ip camera
* 		 worked by simble jpeg frames	
*/
class VIpCamSource : public VSource {
    Q_OBJECT
public:
    VIpCamSource(VIpCamSource *p = 0);
    QSharedPointer<VSource> clone() override;
    ~VIpCamSource();

    void resize_src(uint32_t w,uint32_t h) override;
    void start() override;
    void stop() override;
    void setUrl(QUrl) {}
    void finalize();
    void resizeEvent(QResizeEvent* event);
    VSourceOptions * getOptionsWindow(void);
    bool isInitialized();
    void setname(QString name) {
        this->name = name;
    }

private:
    QTimer * timer;
    QNetworkRequest request;
    QPixmap pixmap_image;
    QImage image;
    void createOptions(void);
    void newImage();

protected:
   void	paintEvent(QPaintEvent *event);

public slots:
	void saveParams(void);
	void updateImage(void);

};


/**
* @brief class Creator for IpCamSource
*/
class VIpCamCreator : public QObject , public VSourceCreator {
/* Builder for IpCamSource */
    Q_OBJECT
    Q_INTERFACES(VSourceCreator)
public:
    VIpCamCreator() {}
   ~VIpCamCreator() {}
    QString getSourceType() {
        return QString(IPCAM_SOURCE);
    }

    QList<VIpCamCreator::Param> getParamsList()
    {
        QList<Param> lst;
        lst.push_back(Param("host",tr("TCP/IP address or hostname of VNC server")));
        lst.push_back(Param("icon",(":/cam2.png"),VSourceCreator::ICON));
        return lst;
    }
    VSource *createVSource() {
        qDebug("Create VIpCam");
        return (new VIpCamSource()); /* returned prototype of source */
    }
};




#endif /* VIPCAMSOURCE_H */

#ifndef VIPCAMTHREAD_H
#define VIPCAMTHREAD_H
#include <QBuffer>
#include <QImageReader>
#include <QTimer>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>

#include <vsource/vsource.h>
#include <opencv2/opencv.hpp>

class  VIpCamThread : public VSourceThread {
    Q_OBJECT
public:
    typedef  enum {
        MjpgBoundary,
        MjpgJpg
    } MjpgState;

    typedef enum {
        StreamTypeUnknown,
        StreamTypeWebcamXP,
        StreamTypeMjpgStreamer
    } StreamType;

    VIpCamThread();
    ~VIpCamThread();
    void setUrl(QUrl  url);
    void startGrabbing();
    void stopGrabbing();
    bool isGrabbing();

    QString getDefaultSource() {return "http://127.0.0.1:8080/?action=stream";}

    QString grabberName() {return "HTTP mjpg image grabber";}

    void setFps(double ) {}
    void setSource(QString str);
    QString currentSource() {return currentUrl.toString();}
    QStringList enumerateSources() {return QStringList();}



signals:
 
//void image_updated(void);
public slots:
	void networkAccessible(QNetworkAccessManager::NetworkAccessibility);

private slots:
    void timerSlot(void);
	void downloadErrorSlot(QNetworkReply::NetworkError);
    void downloadFinished(QNetworkReply *reply);
    void replyDataAvailable();
    void newImageTimeOut();
    void ImageUpdated(QImage *img);
protected:
    void run();

private:
    QNetworkAccessManager *downloadManager;
    QNetworkRequest *request;
    QNetworkReply *reply;
    int lastState;
    QUrl currentUrl;
	void newImageGrabbed(QImage *img);
    void sendRequest();
    MjpgState mjpgState;

    StreamType streamType;

    int currentImageSize;

    quint64 m_timestampInMs;
    QRegExp m_timestampRegexp;
    QBuffer *imageBuffer;
    QImageReader *imageReader;
    QImage *currentImage;


	QTimer *timer;
	int w;
	int h;

	void  setImage(IplImage *image);
};

#endif /* WIPCAMTHREAD */

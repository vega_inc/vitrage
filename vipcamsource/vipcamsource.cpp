#include "vipcamsource.h"
#include "vipcamthread.h"

#include <QPainter>
#include <QResizeEvent>
#include <QDebug>

#define SZ_POLICY QSizePolicy::Preferred



/**
* @brief default ctor for IpCamSource
*/
VIpCamSource::VIpCamSource(VIpCamSource * p)
{
	if(!p) {
	VIpCamThread *th = new VIpCamThread();
    setThread(th); 
	//setname(IPCAM_SOURCE);
	setStarted(true);
    } else {
	setThread(p->getThread());
    p->copyProperty(this->m_propMap);
    setStatus(p->getStatus());
    setVisible(p->visualise());
    setVisualise(p->visualise());
    setEnabledFlag(p->getEnabledFlag());
    setPrototype(p);
    setPhysName(p->getPhysName());
    m_images = p->m_images;
	}

    type = IPCAM_SOURCE;
    m_propMap.insert("type",IPCAM_SOURCE);
    m_propMap.insert(F1000_TYPE_PROP,VSource::SubSystemKind_SystemWindow);
}


void VIpCamSource::start()
{
	QVariant var;
	getProperty(QString("host"),var);
	VIpCamThread *th = static_cast<VIpCamThread *>(m_thread);
	th->setUrl(QUrl(var.toString()));
	setStarted(true);
    if (!m_thread->isRunning()) {
        m_thread->start();
    }
}	


/**
* @brief dest
*/
VIpCamSource::~VIpCamSource()
{
}


/**
* @brief stop main cycle
*/
void VIpCamSource::stop()
{
if (isStarted()) {
	setStarted(false);
	}
}

/**
* @brief resize event function
*
* @param event -[in] QResizeEvent
*/
void VIpCamSource::resizeEvent(QResizeEvent *event)
{
	emit this->setThreadSize(event->size(),event->oldSize());
}



/**
* @brief resize source image
*
* @param w - width uint32_t
* @param h - height uint32_t
*/
void VIpCamSource::resize_src(uint32_t w,uint32_t h) 
{
	resize(w,h);
	setThreadSize(QSize(w,h),QSize(0,0));
}



void VIpCamSource::updateImage()
{
	image = m_thread->Image(size());
	update();
}

void VIpCamSource::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    if (image.isNull() || image.format() == QImage::Format_Invalid) {
        qDebug() << "no valid image to paint";
//        m_bare.scaled(size());
//        painter.drawImage(QPoint(0,0),m_bare);
        //paintAll(&painter);
        paintIcons(&painter);
        QWidget::paintEvent(event);
        painter.end();
        return;
    }
    event->accept();
    painter.drawImage(QPoint(0,0),image);
    //paintAll(&painter);
    paintIcons(&painter);
    QWidget::paintEvent(event);
    painter.end();
}

QSharedPointer<VSource> VIpCamSource::clone()
{
    QSharedPointer<VSource> ip(new VIpCamSource(this));
	return ip;
}

void VIpCamSource::finalize()
{

}

Q_EXPORT_PLUGIN2(VIpCamSource, VIpCamCreator)



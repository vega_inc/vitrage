#include "vshape.h"

#include <QtPlugin>

/// VArrow
VShape::VShape(VShape *src) :
    VSource(nullptr)
{
    if (src) {
        type = src->gettype();
        m_physName = src->getPhysName();
        shapeCmd = src->getShapeCmd();
        Q_ASSERT(!shapeCmd.isEmpty());
    }
}

QSharedPointer<VSource> VShape::clone()
{
    return QSharedPointer<VSource>(new VShape(this));
}

void VShape::setShapeCmd(const QVariantMap &ba)
{
    shapeCmd = ba;
}

Q_EXPORT_PLUGIN2(VShape,VShapeCreator)

include(../../common.pri)

TEMPLATE = lib
CONFIG += dll plugin
TARGET = vshape

DESTDIR = ../$$INSTALL_PATH_PLUGINS
INCLUDEPATH += ../$$INSTALL_PATH_INCLUDE

LIBS += -L../$${INSTALL_PATH_LIB} \
    -lvsource

HEADERS += vshape.h
SOURCES += vshape.cpp

unix {
       target.path = /$(DESTDIR)
       INSTALLS += target
     }

#ifndef VSHAPE_H
#define VSHAPE_H

#include "vsource/vsource.h"
#include <QStyleOptionGraphicsItem>

const QString VSHAPE = "VSHAPE";

class VShape : public VSource {
    Q_OBJECT

public:
    VShape() {
        type = VSHAPE;
    }
    VShape(VShape &src) : VSource(&src) {}
    VShape(VShape *src);
    void resize_src(uint32_t w, uint32_t h) { Q_UNUSED(w) Q_UNUSED(h) }
    void start() {}
    void stop() {}
    QSharedPointer<VSource>  clone();
    void finalize(void) {}
    void setShapeCmd(const QVariantMap &ba);
    QVariantMap getShapeCmd() const { return shapeCmd; }
    QRectF boundingRect() const { return QRect(); }
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0)
    {}
private:
    QVariantMap shapeCmd;
};


class VShapeCreator : public QObject , public VSourceCreator {
    Q_OBJECT
    Q_INTERFACES(VSourceCreator)
public:
    QString getSourceType() { return VSHAPE; }
    VSource *createVSource() { return (new VShape()); }
    QList<VShapeCreator::Param> getParamsList() { return QList<Param>(); }
};

#endif  /* VARROW_H */

#include <QTextCodec>
#include <QImageReader>
#include <qplugin.h>
#include "vega.h"
#include "proto.h"
#include <qjson/qjson_export.h>
#include <qjson/serializer.h>
#include <qjson/parser.h>

#define DEF_LAG 10000        /* default lag in milliseconds */
#define DEF_HOST "localhost" /* default host */
#define DEF_PORT 25456       /* default port */
#define DEF_DIF 5            /* default difference between sizes */

using namespace proto;

Vega::Vega() : m_success("OK")
{
    m_name = "vega";
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForCStrings(codec);
    QTextCodec::setCodecForTr(codec);
    QTextCodec::setCodecForLocale(codec);



    setProperty("model", "Mura");
    setProperty(CONTR_NAME, "");
    setProperty(CONTR_PASSWD, "");
    if (!isRunning()) {
     ;
        moveToThread(this);
        m_socket  = new QTcpSocket();
        m_socket->moveToThread(this);

        start();
    }
}

void Vega::newState(QAbstractSocket::SocketState state)
{
}

void Vega::run()
{

    qRegisterMetaType<QAbstractSocket::SocketError>("QAbstractSocket::SocketError");
    qRegisterMetaType<QAbstractSocket::SocketState>("QAbstractSocket::SocketState");
    connect(m_socket, SIGNAL(connected()), this, SLOT(conn_slot()), Qt::DirectConnection);
    connect(m_socket, SIGNAL(disconnected()), this, SLOT(disconnected()), Qt::DirectConnection);
    connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(sock_err(QAbstractSocket::SocketError)), Qt::DirectConnection);
    connect(m_socket, SIGNAL(readyRead()),this,SLOT(ready()), Qt::DirectConnection);
    connect(m_socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(newState(QAbstractSocket::SocketState)), Qt::DirectConnection);
    connect(m_socket, SIGNAL(readChannelFinished()), this, SLOT(readFinished()), Qt::DirectConnection);

    connect(this,SIGNAL(createSourceSig()),this, SLOT(createSourceSlot()), Qt::DirectConnection);
    connect(this,SIGNAL(setSceneSig()),this,SLOT(setSceneSlot()),Qt::QueuedConnection );

    connect(this,SIGNAL(dissig()),this,SLOT(disSlot()), Qt::QueuedConnection);
    connect(this,SIGNAL(consig()),this,SLOT(conSlot()), Qt::QueuedConnection );

    exec();
}

Vega::~Vega()
{

    this->exit();
    this->quit();
}

void Vega::conn()
{


    emit consig();
}

void Vega::conSlot()
{

    m_socket->flush();
    m_socket->close();

    startTimer();




    QVariant host_var , port_var,lag;
    if(getProperty(QString("host"),host_var)) {
        m_url.setHost(host_var.toString());
    } else m_url.setHost(DEF_HOST);

    if(getProperty(QString("port"),port_var)) {
        m_url.setPort(port_var.toInt());
    } else m_url.setPort(DEF_PORT);



    if (!m_socket->isOpen()) {
        qDebug() << Q_FUNC_INFO << m_url.host() << "port:" << m_url.port();
        if (m_address.isNull() || !m_port) {
            m_socket->connectToHost(m_url.host(), m_url.port());
        } else {
            m_socket->connectToHost(m_address, m_port);
        }
        if (!m_socket->waitForConnected(2000)) {
            QString str("HUUUUIII");
            setStatus(status_t::error);
            m_busy = false;
            emit statusChanged();
            emit errorSig(str);
            return;

        }

    }
    emit connectedS();
}

void Vega::timer_timeout()
{

    m_timer.stop();
    QString str("Mura ERROR:Timout error");
    setStatus(status_t::error);
    m_busy = false;
    emit statusChanged();
    emit errorSig(str);
}


void Vega::startTimer()
{


    m_timer.start(DEF_LAG);
}

void Vega::disconn()
{

    emit dissig();
}

bool Vega::createSource(VSource &src)
{

    Q_UNUSED(src);
    return true;
}

void Vega::disSlot()
{

    if (m_socket->state() == QAbstractSocket::UnconnectedState) {
        setStatus(status_t::disc);
        emit statusChanged();
        m_busy = false;
        return;
    }
    m_socket->disconnectFromHost();
    if (m_socket->state() == QAbstractSocket::UnconnectedState ||
            m_socket->waitForDisconnected(1000))
        setStatus(status_t::disc);
    m_errorMsg = "Контроллер не подключен";
    emit statusChanged();
    m_busy = false;
}

bool Vega::startSource(VSource &)
{
    return false;
}

bool Vega::stopSource(VSource &)
{
    return false;
}

bool Vega::enableSourceFlag(VSource &, bool)
{
    return false;
}

VController *Vega::clone()
{

    return new Vega();
}

void Vega::conn_slot()
{

    QString er_str("Контроллер подключен");
    m_timer.stop();
    m_busy = true;
    m_errorMsg = er_str;
    bool ok = applyLayout();
    messSig(er_str);
    m_status = status_t::connected;
    m_busy = false;
    //emit statusChanged();
    emit sceneFinished(ok);
}

void Vega::disconnected()
{

    m_timer.stop();
    m_busy = true;
    m_errorMsg = "Контроллер не подключен";
    applyLayout();
    messSig(m_errorMsg);
    m_status = status_t::disc;
    m_busy = false;
    emit statusChanged();
}

void Vega::sock_err(QAbstractSocket::SocketError err)
{

    setStatus(status_t::error);
    QString str = "Mura ERROR: Socker Error " + QString::number(err);
    qDebug() << Q_FUNC_INFO << str;
    QString msg;
    switch (err) {
    case (QAbstractSocket::ConnectionRefusedError) :
        msg = "ConnectionRefusedError";
        break;
    case (QAbstractSocket::NetworkError) :
        msg = "NetworkError";
        break;
    default:
        msg = "Another socket error";
        break;
    }
    m_errorMsg = msg;
    emit statusChanged();
//=======
//    if (flag) {
//        setStatus(status_t::error);
//        m_errorMsg = msg;
//        emit statusChanged();
//    }
//>>>>>>> 85dbf0718f31d2fe3dc845cf6f0d3a9a5e9811af
}

void Vega::ready()
{
    //qDebug() << Q_FUNC_INFO << "Ready slot";
    m_respFlag = true;
}

bool Vega::getResponse()
{

    m_resp.clear();
    bool ret = true;
    if (m_socket->state() != QAbstractSocket::ConnectedState) {
        m_busy = false;
        return true;
    }
    ret = m_socket->waitForReadyRead(13000);
    if (!m_socket->isValid()) {
        m_busy = false;
        return true;
    }
    QByteArray buffer = m_socket->readAll();
    m_resp = QString(buffer);
    qDebug() << Q_FUNC_INFO << m_resp;
    if (!m_resp.count()){
        qDebug() << "Empty responce";
        ret = false;
    }
    return ret;
}

void Vega::fillSources(const QList<int> &lst)
{

    m_sourcesMap.clear();
    for (auto i : lst) {
        fillSourceMap(i);
    }
}

int Vega::getWindowProp(QString prop, int id)
{

    QVariantMap package;
    QVariantMap arg;

    package.insert(field::command, commands::get_window_info);
    package.insert(field::type, type::command);
    arg.insert(args::window_id,id);
    arg.insert(args::scene_id,(int)m_currentSceneId);
    QVariantList lst;
    lst.push_back(prop);
    arg.insert(args::items, lst);
    package.insert(field::args, arg);
    QJson::Serializer serializer;
    QByteArray json = serializer.serialize(package);
    qDebug() << "###" << commands::get_window_info;
    sendRequest(json.data(),json.size());

    if (m_result.value(field::status).toInt()) return -1 ;
    QVariantMap m = m_result.value(field::response).toMap();
    return m.value(prop).toInt();
}

void Vega::fillSourceMap(int id)
{


    QMultiMap<QString,QVariant> map;
    map.insert("PosX",getWindowProp(args::left,id));
    map.insert("PosY",getWindowProp(args::top,id));
    map.insert("PosW",getWindowProp(args::width,id));
    map.insert("PosH",getWindowProp(args::height,id));

    map.insert("CroX",getWindowProp(args::crop_left,id));
    map.insert("CroY",getWindowProp(args::crop_top,id));
    map.insert("CroW",getWindowProp(args::crop_width,id));
    map.insert("CroH",getWindowProp(args::crop_height,id));

    map.insert("SourceZ",getWindowProp(args::z,id));
    int srcId = getWindowProp(args::source_id,id);
    map.insert("SourceId",srcId);
    map.insert("SourceName",getSourceItem(args::name,srcId));
    map.insert("WindowId",id);
    m_sourcesMap.insertMulti(id, map);
}

bool Vega::closeSources()
{

    QList<InnerMap> lst = m_sourcesMap.values();
    for( int i = 0 ; i < lst.count() ; i++)  {
        InnerMap map = lst.at(i);
        QVariant id = map.value("WindowId");
        deleteWindow(id.toInt());
        bool deleteS = true;
        foreach(QSharedPointer<VSource> src, m_sources) {
            if (src->getCloneName() == map.value("SourceName").toString())
                deleteS = false;
        }
        if (deleteS) {
            removeSource(map.value("SourceName").toString());
        }
        m_sourcesMap.remove(id.toInt());
    }
    return true;
}

bool Vega::deleteWindow(int id)
{

    QVariantMap package;
    package.insert(field::command, commands::delete_window);
    package.insert(field::type, type::command);

    QVariantMap arg;    
    arg.insert(args::window_id,id);
    package.insert(field::args,arg);

    QJson::Serializer serializer;
    QByteArray json = serializer.serialize(package);
    qDebug() << "###" << commands::delete_window;
    sendRequest(json.data(),json.size());

    if (m_result.value(field::status).toInt()) return true ;
    return false;
}

bool Vega::createSources()
{

    foreach(QSharedPointer<VSource> src, m_sources) {
        auto enabled = src->getEnabledFlag();
        if (enabled) {
            setSource(src);
        }
    }
    return false;
}

void Vega::setSource(QSharedPointer<VSource> src, bool window)
{

    QVariant type;
    if(!src->getProperty("type", type))
        return;
    QString type_str = type.toString();
    if (type_str == PIC_TYPE) {
        createPicture(src, window);
    }
    else if (type_str == VNC_TYPE || type_str == IPCAMERA_TYPE || type_str == MIXER_TYPE || type_str == EXTRONMIXER_TYPE) {
        createVNC(src, window);
    }
    else if (type_str == WBPAGE_TYPE) {
        createApplication(src, window);
    }
    else if (type_str == NAMED_TYPE) {
        createNamed(src, window);
    }
}

bool Vega::createPicture(QSharedPointer<VSource> src, bool widow)
{

    QVariant var;
    src->getProperty("muraHost", var);
    QString path = var.toString();
    if (path.isEmpty())
        return false;
    QVariantMap package;
    package.insert(field::command, commands::create_source);
    package.insert(field::type, type::command);
    QVariantMap arg;
    auto formats = QImageReader::supportedImageFormats();
    bool isPicture = false;
    for (auto format : formats) {
        if (path.contains(format, Qt::CaseInsensitive)) {
            arg.insert(args::src_type, src_type::picture);
            isPicture = true;
            break;
        }
    }
    if (!isPicture) {
        arg.insert(args::src_type, src_type::video);
    }
    arg.insert(args::path, path);
    arg.insert(args::name, src->getPhysName());
    package.insert(field::args, arg);

    QJson::Serializer serializer;
    QByteArray json = serializer.serialize(package);
    qDebug() << "###" << commands::create_source << arg.value(args::src_type);
    auto srcId = isHaveSource(src->getPhysName());
    if (srcId == -1) {
        sendRequest(json.data(), json.size());
        if (m_result.value(field::status).toInt())
            return true;
        QVariantMap m = m_result.value(field::response).toMap();
        srcId = m.value(response::source_id).toInt();
        qDebug() << Q_FUNC_INFO << "source id =" << srcId;
    }
    if (widow)
        createWindow(src, srcId);
    return false;
}

bool Vega::createApplication(QSharedPointer<VSource> src,bool widow)
{
    Q_UNUSED(src);
    Q_UNUSED(widow);
    return false;
}

bool Vega::createNamed(QSharedPointer<VSource> src, bool widow)
{

    QVariantMap package;
    QVariantMap arg;
    QVariant path,wi,he,bou,fo,server;

    int srcId;
    src->getProperty(QString("v4lpath"),path);
    src->getProperty(QString("v4lwidth"),wi);
    src->getProperty(QString("v4lheight"),he);
    src->getProperty(QString("v4lfps"),bou);
    src->getProperty(QString("v4lformat"),fo);
    src->getProperty(QString("server"),server);

    package.insert(field::command,commands::create_source);
    package.insert(field::type,type::command);
    arg.insert(args::src_type,src_type::v4l);
    arg.insert(args::dev,path.toString());
    arg.insert(args::width,wi.toInt());
    arg.insert(args::height,he.toInt());
    arg.insert(args::fps,bou.toInt());
    arg.insert(args::format,fo.toString());
    arg.insert(args::name,src->getPhysName());
    //arg.insert(args::server,server.toString());
    arg.insert(args::server,"127.0.0.1:5970");
    //removeSource(src->getPhysName());
    package.insert(field::args,arg);
    QJson::Serializer serializer;
    QByteArray json = serializer.serialize(package);
    qDebug() << QString(json) ;
    srcId = isHaveSource(src->getPhysName());
    if (srcId == -1 ) {
        sendRequest(json.data(),json.size());
        if (m_result.value(field::status).toInt()) return true;
        QVariantMap m = m_result.value(field::response).toMap();
        srcId = m.value(response::source_id).toInt();
        createSourceStream(src,srcId);
        qDebug() << "get Sourceid " << srcId;
    }
    // } else createSourceStream(src,srcId);

    if (widow) createWindow(src, srcId);
    return false;
}

bool Vega::createVNC(QSharedPointer<VSource> src, bool window)
{

    if (!window)
        return false;
//    if (src->getStatus() != VSource::ExecStatus::RunStatus)
//        return false;

    QVariant path;
    QVariant port;

    src->getProperty("location", path);
    src->getProperty("port", port);
    QString server = path.toString() + ":" + port.toString();

    QVariantMap arg;
    arg.insert(args::src_type, src_type::vnc);
    arg.insert(args::server, server);
    arg.insert("port", port.toInt());
    arg.insert(args::quality, "9");
    arg.insert(args::compression, "0");
    arg.insert(args::enable_jpeg, "true");
    arg.insert(args::force_true_colour, "true");
    arg.insert(args::name, src->getPhysName());

    QVariantMap package;
    package.insert(field::command, commands::create_source);
    package.insert(field::type, type::command);
    package.insert(field::args, arg);

    QJson::Serializer serializer;
    QByteArray json = serializer.serialize(package);
    qDebug() << QString(json) ;
    auto srcId = isHaveSource(src->getPhysName());
    if (srcId == -1 ) {
        sendRequest(json.data(), json.size());

        if (m_result.value(field::status).toInt()) {
            //removeSource()
            return true;
        }
        QVariantMap m = m_result.value(field::response).toMap();
        srcId = m.value(response::source_id).toInt();
        qDebug() << "get Sourceid " << srcId;
    }
    bool ok = createWindow(src, srcId);
    return !ok;
}

bool Vega::applyLayout()
{

    QVariantMap package;
    package.insert(field::command, commands::create_scene);
    package.insert(field::type, type::command);

    QJson::Serializer serializer;
    QByteArray json = serializer.serialize(package);
    qDebug() << "###" << commands::create_scene;
    bool ok = sendRequest(json.data(),json.size());

    if (m_result.value(field::status).toInt()) return true;
    QVariantMap m = m_result.value(field::response).toMap();
    m_currentSceneId = m.value(response::scene_id).toInt();
    qDebug() << "get Scene id " << m_currentSceneId;
    return true;
}

bool Vega::compareSources(VSource *src, int id)
{

    return src->getPhysName() == getSourceName(id);
}

QString Vega::getSourceName(int id)
{

    return getSourceItem(args::name, id);
}

bool Vega::comparePicture(VSource* src, int id)
{

    QString path = getSourceItem(args::path,id);
    QVariant srcPath;
    src->getProperty(QString("muraHost"),srcPath);
    return (srcPath.toString() == path) ;
}

QString Vega::getSourceItem(const QString &item, int id)
{

    QVariantMap package;
    package.insert(field::command, commands::get_source_info);
    package.insert(field::type, type::command);

    QVariantMap arg;
    arg.insert(args::source_id, id);

    QVariantList lst;
    lst.push_back(item);
    arg.insert(args::items, lst);
    package.insert(field::args, arg);

    QJson::Serializer serializer;
    QByteArray json = serializer.serialize(package);
    qDebug() << "###" << commands::get_source_info;
    sendRequest(json.data(),json.size());

    if (m_result.value(field::status).toInt())
        return "";
    QVariantMap m = m_result.value(field::response).toMap();
    QString value = m.value(item).toString();
    qDebug() << Q_FUNC_INFO << value;
    return value;
}

bool Vega::comparePos(QSharedPointer<VSource> src, const InnerMap &map)
{

    QVariant pixs_w;
    QVariant pixs_h;
    int w;
    int h;

    QVariant x,y,w_,h_,cr_x,cr_y,cr_w,cr_h,zz;
    x = map.value("PosX");
    y = map.value("PosY");
    h_ = map.value("PosH");
    w_ = map.value("PosW");
    cr_x = map.value("CroX");
    cr_y = map.value("CroY");
    cr_w = map.value("CroW");
    cr_h = map.value("CroH");
    zz = map.value("SourceZ");

    QVariant zzz;
    src->getProperty("z",zzz);
    if (zzz != zz) return false;

    getProperty(QString("pixs_w"),pixs_w);
    getProperty(QString("pixs_h"),pixs_h);
    w = pixs_w.toInt()/1000;
    h = pixs_h.toInt()/1000;

    VPosition src_pos = src->getPosition();
    int dx = (x.toInt() - ((src_pos.lt.x() * pixs_w.toInt()))/1000);
    int dy = y.toInt() - (src_pos.lt.y() * pixs_h.toInt())/1000;
    int dw = w_.toInt() - (((src_pos.rb.x()  - src_pos.lt.x()) * pixs_w.toInt()))/1000;
    int dh = h_.toInt() - (((src_pos.rb.y()  - src_pos.lt.y()) * pixs_h.toInt()))/1000;

    w -= 1;
    h -= 1;


    VPosition s_pos = src->getPosition();

    int cx = qRound( w * s_pos.getCropLxW() );
    int cy = qRound( h * s_pos.getCropTyH() );
    int cw = qRound( w * s_pos.getCropSowOnWiw() );
    int ch = qRound( h * s_pos.getCropSohOnWih() );

    if(cx % 2 != 0) // must be even
        cx++;
    if(cw % 2 != 0) // must be even
        cw++;
    if ((cx != cr_x) ||(cy != cr_y) ||(cw != cr_w) || (ch != cr_h)) return false;


    return (!
            (((dx < -w) || (dx > w)) ||
             ((dy < -h) || (dy > h)) ||
             ((dw < -w) || (dw > w)) ||
             ((dh < -h) || (dh > h))));
}

bool Vega::replaceSource(QSharedPointer<VSource> src, const InnerMap &map)
{

    QVariant id;
    int  x,y,w,h;
    int sourceId = map.value("SourceId").toInt();
    id = map.value("WindowId");

    QVariant pixs_w;
    QVariant pixs_h;
    getProperty(QString("pixs_w"),pixs_w);
    getProperty(QString("pixs_h"),pixs_h);
    VPosition src_pos = src->getPosition();
    x = (src_pos.lt.x() * pixs_w.toInt())/1000;
    y = (src_pos.lt.y() * pixs_h.toInt())/1000;
    w = ((src_pos.rb.x()  - src_pos.lt.x()) * pixs_w.toInt())/1000;
    h = ((src_pos.rb.y()  - src_pos.lt.y()) * pixs_h.toInt())/1000;


    VPosition s_pos = src->getPosition();

    int cx = qRound( w * s_pos.getCropLxW() );
    int cy = qRound( h * s_pos.getCropTyH() );
    int cw = qRound( w * s_pos.getCropSowOnWiw() );
    int ch = qRound( h * s_pos.getCropSohOnWih() );
    if ((src->gettype() == "MixerExtronVNCClient") ||
            (src->gettype() == "MixerVNCClient")) {
        QVariant na;
        src->getProperty("named",na);
        int delt = na.toInt() -1 ;
        x = mixerW*delt;
        y = 0;
        w = mixerW;
        h = mixerH;
        cx = cy = cw  = ch =0;
    }
    if(cx % 2 != 0) // must be even
        cx++;
    if(cw % 2 != 0) // must be even
        cw++;

    QVariantMap package;
    QVariantMap arg;

    package.insert(field::command,commands::update_window);
    package.insert(field::type,type::command);

    arg.insert(args::window_id,id);
    arg.insert(args::top,y);
    arg.insert(args::left,x);
    arg.insert(args::width,w);
    arg.insert(args::height,h);
    QVariant z;
    src->getProperty("z",z);
    arg.insert(args::z,z.toInt());
    if ((cx + cy + cw + ch)) {
        int ww = getSourceItem(args::width,sourceId).toInt();
        int hh = getSourceItem(args::height,sourceId).toInt();
        double www = (double)ww/(double)w;
        double hhh = (double)hh/(double)h;
        arg.insert(args::crop_left,cx*www);
        arg.insert(args::crop_top,cy*hhh);
        arg.insert(args::crop_width,cw*www);
        arg.insert(args::crop_height,ch*hhh);
    } else {
        int ww = getSourceItem(args::width,sourceId).toInt();
        int hh = getSourceItem(args::height,sourceId).toInt();
        arg.insert(args::crop_left,0);
        arg.insert(args::crop_top,0);
        arg.insert(args::crop_width,ww);
        arg.insert(args::crop_height,hh);
    }
    package.insert(field::args,arg);
    QJson::Serializer serializer;
    QByteArray json = serializer.serialize(package);
    qDebug() << QString(json);
    sendRequest(json.data(),json.size());

    if (m_result.value(field::status).toInt()) return true;
    return false;
}


void Vega::readFinished()
{

    qDebug() << " Read finished";
}

bool Vega::setScene(const Sources &lst)
{

    if (m_busy) {
        return true;
    }

    m_busy = true;
    m_sources.clear();
    int nam1 = 1;
    int nam2 = 1;
    int ExtrInp = 1;
    foreach(QSharedPointer<VSource> src, lst) {
        if (src->gettype() == "MixerExtronVNCClient") {
            src->setProperty("named", nam2++);
            src->setProperty("ExtronInput", ExtrInp++);
        }
        if (src->gettype() == "MixerVNCClient") {
            src->setProperty("named", nam1++);
        }
        if (src->getEnabledFlag())
            m_sources.push_back(src);
    }
    qDebug() << Q_FUNC_INFO << "sources size:" << m_sources.size();
    emit setSceneSig();
    return true;
}

void Vega::setSceneSlot(void)
{

    m_busy = true;
    if (m_socket->state() != QAbstractSocket::ConnectedState) {
        m_busy = false;
        emit sceneFinished(false);
        return;
    }
    deactivateScene((int)m_currentSceneId);
    QList<int> windows = getWindowList();
    fillSources(windows);
    foreach (QSharedPointer<VSource> src, m_sources) {
//        if (!src->getEnabledFlag()) continue;
        foreach(InnerMap map, m_sourcesMap.values()) {
            if (src->getPhysName() == map.value("SourceName").toString()) {
                if (!comparePos(src,map)) {
                    replaceSource(src,map);
                    m_sources.removeOne(src);
                    m_sourcesMap.remove(map.value("WindowId").toInt());
                    continue;
                }
                m_sources.removeOne(src);
                m_sourcesMap.remove(map.value("WindowId").toInt());
                continue;
            }
        }
    }
    closeSources();
    createSources();
    m_sources.clear();
    activateScene((int)m_currentSceneId);
    m_busy = false;
    if (m_bridgeController) {
        m_bridgeController->setScene(m_bridgeLst);
        m_bridgeLst.clear();
    }
    emit sceneFinished(true);
}


bool Vega::sendRequest(char *data, uint32_t count)
{

    m_resp.clear();
    m_busy  = true;
    m_respFlag =false;
    qDebug() << m_socket->peerAddress();
    qDebug() << m_socket->peerPort();
    int len = m_socket->write(data, count);
    bool ret =	m_socket->waitForBytesWritten(10000);
    if (!ret) {
        qDebug() << Q_FUNC_INFO << "sendRequest timeout elapsed";
    }
    if (!len) return true;
    if (!getResponse()) {
        return true;
    }
    QJson::Parser parser;
    bool ok;
    m_result = parser.parse(m_resp.toAscii(), &ok).toMap();
    return ok;
}

QList<int> Vega::getWindowList()
{

    QVariantMap package;
    QVariantMap arg;
    package.insert(field::command, commands::get_window_list);
    package.insert(field::type,type::command);
    arg.insert(args::scene_id,m_currentSceneId);
    package.insert(field::args,arg);

    QJson::Serializer serializer;
    QByteArray json = serializer.serialize(package);
    qDebug() << "###" << commands::get_window_list;
    sendRequest(json.data(),json.size());
    QList<int> lst;
    QVariantMap m = m_result.value(field::response).toMap();
    QVariantList list = m.value(response::windows).toList();
    foreach (QVariant c, list) {
        lst.append(c.toInt());
    }
    return lst;
}

bool Vega::createWindow(QSharedPointer<VSource> src, unsigned long sourceid)
{

    QVariantMap package;
    package.insert(field::command, commands::create_window);
    package.insert(field::type, type::command);
    QVariantMap arg;    
    arg.insert(args::source_id,(int)sourceid);
    VPosition src_pos = src->getPosition();
    QVariant pixs_w;
    QVariant pixs_h;
    getProperty("pixs_w", pixs_w);
    getProperty("pixs_h", pixs_h);
    auto wpx = pixs_w.toInt();
    auto hpx = pixs_h.toInt();
    auto x = src_pos.lt.x()*wpx/1000;
    auto y = src_pos.lt.y()*hpx/1000;
    auto w = ((src_pos.rb.x()  - src_pos.lt.x())*wpx)/1000;
    auto h = ((src_pos.rb.y()  - src_pos.lt.y())*hpx)/1000;

    if (src->gettype() == "MixerExtronVNCClient" || src->gettype() == "MixerVNCClient") {
        QVariant na;
        src->getProperty("named", na);
        int delt = na.toInt() -1 ;
        x = mixerW*delt;
        y = 0;
        w = mixerW;
        h = mixerH;
    }
    int cx = qRound( w * src_pos.getCropLxW() );
    int cy = qRound( h * src_pos.getCropTyH() );
    int cw = qRound( w * src_pos.getCropSowOnWiw() );
    int ch = qRound( h * src_pos.getCropSohOnWih() );
    if(cx % 2 != 0) // must be even
        cx++;
    if(cw % 2 != 0) // must be even
        cw++;
    auto type = src->gettype();
    if (type == "MixerExtronVNCClient" || type == "MixerVNCClient") {
        QVariant na;
        src->getProperty("named",na);
        int delt = na.toInt() -1 ;
        x = mixerW*delt;
        y = 0;
        w = mixerW;
        h = mixerH;
        cx = cy = cw  = ch =0;
    }
    arg.insert(args::top,y);
    arg.insert(args::left,x);
    arg.insert(args::width,w);
    arg.insert(args::height,h);
    if (cx + cy + cw + ch) {
        int ww = getSourceItem(args::width,sourceid).toInt();
        int hh = getSourceItem(args::height,sourceid).toInt();
        double www = (double)ww/(double)w;
        double hhh = (double)hh/(double)h;
        arg.insert(args::crop_left,cx*www);
        arg.insert(args::crop_top,cy*hhh);
        arg.insert(args::crop_width,cw*www);
        arg.insert(args::crop_height,ch*hhh);
    } else {
        int ww = getSourceItem(args::width,sourceid).toInt();
        int hh = getSourceItem(args::height,sourceid).toInt();
        arg.insert(args::crop_left,0);
        arg.insert(args::crop_top,0);
        arg.insert(args::crop_width,ww);
        arg.insert(args::crop_height,hh);
    }
    QVariant z;
    src->getProperty("z",z);
    arg.insert(args::z,z.toInt());
    arg.insert(args::scene_id,(int)m_currentSceneId);
    package.insert(field::args,arg);
    QJson::Serializer serializer;
    QByteArray json = serializer.serialize(package);
    qDebug() << "###" << commands::create_window;
    qDebug() << "###" << json;
    sendRequest(json.data(),json.size());

    if (m_result.value(field::status).toInt())
        return true;
    QVariantMap m = m_result.value(field::response).toMap();
    unsigned long srcId = m.value(response::window_id).toInt();
    qDebug() << Q_FUNC_INFO << "id =" << srcId;
    return true;
}

bool Vega::createSourceStream(QSharedPointer<VSource> src, unsigned long sourceid)
{
    QVariantMap package;
    QVariantMap arg;
    QVariant server;
    QVariant port;
    src->getProperty(QString("server"),server);
    src->getProperty(QString("port"),port);
    package.insert(field::command,commands::create_source_stream);
    package.insert(field::type,type::command);
    arg.insert(args::source_id,(uint)sourceid);
    //arg.insert(args::ipv4_host,server);

    /*arg.insert(args::listen_host,"10.1.0.45");
    arg.insert(args::listen_port,"10");*/
    arg.insert(args::ipv4_host,"10.1.0.45");
    arg.insert(args::tcp_port,port.toInt());
    //arg.insert(args::listen_port,port.toString());
    arg.insert(args::listen_port,port.toInt());
    /*arg.insert(args::tcp_port,"5970");
    arg.insert(args::listen_port,"5970");*/

    arg.insert(args::stream_type,"vnc");
    arg.insert(args::fps,10);
    //arg.insert(args::scale,"[1,0.5]");


    package.insert(field::args,arg);
    QJson::Serializer serializer;
    QByteArray json = serializer.serialize(package);
    qDebug() << QString(json) ;
    sendRequest(json.data(),json.size());

    if (m_result.value(field::status).toInt()) return true;
    QVariantMap m = m_result.value(field::response).toMap();
    unsigned long srcId = m.value(response::window_id).toInt();
    m_busy = false;
    qDebug() << "get Sourceid " << srcId;
    return true;
}

bool Vega::activateScene(int id)
{
    QVariantMap package;
    package.insert(field::command, commands::activate_scene);
    package.insert(field::type, type::command);

    QVariantMap arg;
    arg.insert(args::scene_id, id);
    package.insert(field::args, arg);

    QJson::Serializer serializer;
    QByteArray json = serializer.serialize(package);
    qDebug() << "###" << commands::activate_scene;
    sendRequest(json.data(), json.size());

    if (m_result.value(field::status).toInt()) return true;
    return false;
}

bool Vega::deactivateScene(int sceneId)
{
    QVariantMap package;
    QVariantMap arg;

    package.insert(field::command, commands::deactivate_scene);
    package.insert(field::type, type::command);
    arg.insert(args::scene_id,sceneId);
    package.insert(field::args,arg);
    QJson::Serializer serializer;
    QByteArray json = serializer.serialize(package);
    qDebug() << "###" << commands::deactivate_scene;
    sendRequest(json.data(),json.size());

    if (m_result.value(field::status).toInt()) return true;
    return false;
}

bool Vega::createSourcesProto(QList<QSharedPointer<VSource> > srcs)
{
    foreach(QSharedPointer<VSource>  src,srcs) {
//        if (!src->getEnabledFlag()) continue;
        setSource(src,false);
    }
    emit srcsCreated();
    return false;
}

QList<int> Vega::getSourceList()
{

    QVariantMap package;
    package.insert(field::command, commands::get_source_list);
    package.insert(field::type, type::command);
    QVariantMap arg;
    package.insert(field::args, arg);

    QJson::Serializer serializer;
    QByteArray json = serializer.serialize(package);
    qDebug() << "###" << commands::get_source_list;
    sendRequest(json.data(),json.size());
    QList<int> lst;
    QVariantMap m = m_result.value(field::response).toMap();
    QVariantList list = m.value(response::sources).toList();
    foreach (QVariant c, list) {
        lst.append(c.toInt());
    }
    return lst;
}

void Vega::bridge(VController *v, const QList<QSharedPointer<VSource> > &l)
{

    m_bridgeController = v;
    m_bridgeLst = l;
}

int Vega::isHaveSource(QString str)
{

    QList<int>  lst = getSourceList();
    foreach (int id, lst) {
        if (str == getSourceName(id)) {
            return id;
        }
    }
    return -1;
}

bool Vega::removeAllSources()
{

    QList<int> lst = getSourceList();
    foreach (int id, lst) {
        removeSource(id);
    }
    return true;
}

bool Vega::removeSource(const QString &str)
{

    QList<int>  lst = getSourceList();
    foreach (int id, lst) {
        if (str == getSourceName(id)) {
            removeSource(id);
        }
    }
    return true;
}

bool Vega::removeSource(int id)
{

    QVariantMap package;
    package.insert(field::command, commands::delete_source);
    package.insert(field::type, type::command);

    QVariantMap arg;
    arg.insert(args::source_id,id);
    arg.insert(args::remove_window,0);
    package.insert(field::args,arg);

    QJson::Serializer serializer;
    QByteArray json = serializer.serialize(package);
    qDebug() << "###" << commands::delete_source;
    sendRequest(json.data(), json.size());

    if (m_result.value(field::status).toInt()) return true ;
    return false;
}




Q_EXPORT_PLUGIN2(Vega,Vega)

include(../common.pri)

TARGET = vega
CONFIG +=   rtti \
            debug \
            plugin

QT += network

DESTDIR = $$INSTALL_PATH_CTRL_PLUGINS

TEMPLATE = lib

DEFINES += VCONTROLLER_LIBRARY

INCLUDEPATH += $$INSTALL_PATH_INCLUDE \
		/usr/include/

SOURCES += vega.cpp
HEADERS += vega.h \
    proto.h
#LIBS += -ljsoncpp


unix {
       target.path = /$(DESTDIR)
       INSTALLS += target
     }

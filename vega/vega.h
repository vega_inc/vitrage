#ifndef VEGA_H
#define VEGA_H

#include <vcontroller/vcontroller.h>
#include <QTimer>
#include <QTime>
#include <QTcpSocket>

static const QString VEGALAYOUT = "VegaLayout";
typedef QMap<QString,QVariant> InnerMap;


class Vega : public VController {
    Q_OBJECT
    Q_INTERFACES(AbstractVController)
public:
    Vega();
    ~Vega();
    void conn() override;
    void disconn() override;
    bool createSource(VSource &) override;
    bool startSource(VSource &) override;
    bool stopSource(VSource &) override;
    bool enableSourceFlag(VSource &,bool) override;
    bool setScene(const Sources &) override;
    VController *clone() override;
    void run(void);
    //void setScene(QList<QSharedPointer<VSource> > &);
    void setMixed(QList<VSource *> mixed, QList<VSource *> extroned);
    int getWindowProp(QString prop, int id);
    QString getSourceItem(const QString &item, int id);
    bool comparePicture(VSource *src, int id);
    bool deleteWindow(int id);
    bool activateScene(int id);
    bool createVNC(QSharedPointer<VSource> src, bool window = false);
    QString getSourceName(int id);
    QList<int> getSourceList();
    bool removeSource(int id);
    bool removeSource(const QString &str);
    bool removeAllSources();
    void bridge(VController *v, const QList<QSharedPointer<VSource> > &l) override;
    int isHaveSource(QString str);
    bool deactivateScene(int sceneId);
    virtual bool createSourcesProto(QList<QSharedPointer<VSource> >);
private slots:
    void newState(QAbstractSocket::SocketState);
    void conn_slot(void);
    void disconnected(void);
    void sock_err(QAbstractSocket::SocketError );
    void ready(void);
    void timer_timeout(void);
    bool checkRespForSucc(void);
    void readFinished(void);
    void setSceneSlot(void);
    void createSourceSlot(void);
    void disSlot();
    void conSlot();
private:
    int mixerW = 1920;
    int mixerH = 1080;
    int m_currentSceneId = 0;
    QVariantMap m_result;

    static const uint32_t CODE_LENGHT = 8;
    bool sendRequest(char *data,uint32_t count);
    const QString m_success;
    QTcpSocket *m_socket;
    QThread *m_thread;
    QMutex m_mutex;
    uint32_t m_wpixs; /* pixels in small cell */
    uint32_t m_hpixs; /* pixels in big cell */
    VSource *m_src;
    QList<QSharedPointer<VSource> > m_sources;
    QMap<int, QMap<QString, QVariant>> m_sourcesMap;
    QMultiMap<QString, QPair<int, int>> m_sourcesMapInputs;
    QString m_resp; /* response */
    bool m_rxespFlag;
    QUrl m_url;
    QTimer m_timer;
    bool m_busy = false;
    bool m_respFlag;
    QList<QSharedPointer<VSource> >  m_bridgeLst;
    VController *m_bridgeController = nullptr;

    QList<int> getWindowList();

    bool createWindow(QSharedPointer<VSource> src,unsigned long sourceid);

    bool comparePos(QSharedPointer<VSource> src, const InnerMap &map);
    bool replaceSource(QSharedPointer<VSource> Src, const InnerMap &map);
    bool replaceSource(VSource *src);
    void startTimer();
    bool getResponse();
    void fillSources(const QList<int> &lst);
    void fillSourceMap(int id);
    bool closeSources();
    bool createSources();
    void setSource(QSharedPointer<VSource> src, bool window = true);
    bool createPicture(QSharedPointer<VSource> src, bool widow = false);
    bool createApplication(QSharedPointer<VSource> src,bool widow = false);
    bool createNamed(QSharedPointer<VSource> src, bool widow = false);
    bool applyLayout();
    bool createSourceStream(QSharedPointer<VSource> src, unsigned long sourceid);
    bool compareSources(VSource *src,int id);

signals:
    void createSourceSig();
    void setSceneSig();
    void dissig();
    void consig();
};



#endif /* VEGA_H */

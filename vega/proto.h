#ifndef PROTO_H
#define PROTO_H

namespace proto {

namespace field {
    static const char * type    =      "type";
    static const char * command =      "command";
    static const char * args    =      "args";
    static const char * status  =      "status";
    static const char * response=      "response";
}

namespace type {
    static const char * command =  "command";
}

namespace response {
    static const char * scene_id =  "scene_id";
    static const char * source_id = "source_id";
    static const char * window_id = "window_id";
    static const char * windows =   "windows";
    static const char * sources =   "sources";

}

namespace args {
    static const char * src_type =    "src_type";
    static const char * path =        "path";
    static const char * dev =         "dev";
    static const char * server =      "server";
    static const char * width =       "width";
    static const char * height =      "height";
    static const char * source_id =   "source_id";
    static const char * remove_window ="remove_window";
    static const char * window_id =   "window_id";
    static const char * scene_id =    "scene_id";
    static const char * top =         "top";
    static const char * left =        "left";
    static const char * z =           "z";
    static const char * items =       "items";
    static const char * fps =         "fps";
    static const char * format =      "format";
    static const char * name =        "source_name";
    static const char * ipv4_host =   "ipv4_host";
    static const char * tcp_port =    "tcp_port";
    static const char * listen_port =   "listen_port";
    static const char * stream_type = "stream_type";

    static const char * crop_left =    "crop_left";
    static const char * crop_top =     "crop_top";
    static const char * crop_width =   "crop_width";
    static const char * crop_height =  "crop_height";

    static const char * quality =      "quality";
    static const char * compression =  "compression";
    static const char * enable_jpeg =  "enable_jpeg";
    static const char * force_true_colour =  "force_true_colour";
}

namespace commands {
    static const char * create_scene =     "create_scene";
    static const char * activate_scene =   "activate_scene";
    static const char * deactivate_scene = "deactivate_scene";
    static const char * create_window =    "create_window";
    static const char * delete_window =    "delete_window";
    static const char * update_window =    "update_window";
    static const char * create_source =    "create_source";
    static const char * create_source_stream = "create_source_stream";
    static const char * delete_source =    "delete_source";
    static const char * get_window_list =  "get_window_list";
    static const char * get_window_info =  "get_window_info";
    static const char * get_source_info =  "get_source_info";
    static const char * get_source_list =  "get_source_list";
}

namespace src_type {
    static const char * picture = "pic";
    static const char * video = "video";
    static const char * v4l =     "v4l";
    static const char * vnc =     "vnc";
}
}

#endif // PROTO_H

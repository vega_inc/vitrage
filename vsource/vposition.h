#ifndef VPOSITION_H
#define VPOSITION_H

#include <QVector>

#include "stdint.h"

class QPoint;

class VPosition
{
public:
    VPosition(){}
    VPosition(const QPoint &p1, const QPoint &p2) :lt(p1), rb(p2) {}
    VPosition(const VPosition&);

    VPosition& operator=(const VPosition &);
    bool operator!=(const VPosition  &);

    QPoint lt;   /* left top*/
    QPoint rb;   /* right bottom */
    void setWidth(uint32_t);
    uint32_t getWidth()  const;

    void setHeight(uint32_t);
    uint32_t getHeight() const;

    void setCropLxW(double);
    double getCropLxW() const;

    void setCropTyH(double);
    double getCropTyH() const;

    void setCropSowOnWiw(double);
    double getCropSowOnWiw() const;

    void setCropSohOnWih(double);
    double getCropSohOnWih() const;

    QVector<int> b_grid;

private:
    uint32_t z = 0;
    uint32_t width = 0; /* size of small cell in pixels */
    uint32_t height = 0;/* size of small cell in pixels */

    double cropLxW = 0;      // const (leftX/width) , may be zero, not devide on it without checking
    double cropTyH = 0;      // const (topY/height) , may be zero, not devide on it without checking
    double cropSowOnWiw = 0; // const (sourceWidth/widgetWidth) , may be zero, not devide on it without checking
    double cropSohOnWih = 0; // const (sourceHeigh/widgetHeight) , may be zero, not devide on it without checking
};

#endif/* VPOSITION_H*/

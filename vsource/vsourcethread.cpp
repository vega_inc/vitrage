#include "vsourcethread.h"

SizedImage::SizedImage() 
{
    m_bare = QImage(QSize(100,100),QImage::Format_RGB32);
    m_image = QImage(QSize(100,100),QImage::Format_RGB32);
}

void SizedImage::setImage(const QImage& image)
{
    m_image = image;
}

QSize SizedImage::getSize()
{
    return m_image.size();
}

QImage &SizedImage::getImage(void)
{
    //  QMutexLocker(&this->m_mutex);
//    if(!m_mutex.tryLock()) {
//        return m_bare;
//    }
//    // QImage im = m_image;
//    m_mutex.unlock();
    //return im;
    return m_image;

}

void SizedImage::setCount(uint32_t c)
{
    m_count = c;
}

uint32_t SizedImage::count(void)
{
    return m_count;
}

VSourceThread::VSourceThread()
{
}

VSourceThread::~VSourceThread()
{

}

void VSourceThread::setSize(QSize ns,QSize os)
{	
    QMutexLocker lo(&m_mutex);
    bool find = false;
    foreach(SizedImage *im,m_images) {
        if (im->getImage().size() == ns) {
            if (os.width() && os.height())    	im->setCount(im->count()+1);
            find = true;
        }
    }
    /* delete old size */
    for (int tj = 0; tj < m_images.count();tj++) {
        SizedImage *im  = m_images[tj];
        if (im->getImage().size() == os) {
            im->setCount(im->count()-1);
            if (!im->count()) {
                m_images.removeAt(tj);
                delete im; /* check for double free */
            }
        }
    }
    if (!find) {
        SizedImage *im = new SizedImage();
        QImage img(ns,QImage::Format_RGB32);
        if (!m_images.count()) {
            for (int i  = 0 ; i < ns.width(); i++) {
                for(int j = 0; j < ns.height(); j++) {
                    img.setPixel(QPoint(i,j),0);
                }
            }
            Q_ASSERT(!img.isNull()) ;
        } else {
            img = m_image.scaled(ns,Qt::IgnoreAspectRatio,Qt::FastTransformation);
        }
        im->setImage(img);
        im->setCount(1);
        m_images.append(im);
    }
}

void VSourceThread::setImages(QImage &img)
{
    QMutexLocker lo(&m_mutex);
   // img.detach();
//    foreach(SizedImage *im,m_images)
//    {
//        if(im->getImage().isNull())
//        {
//            // abort();
//            continue;
//            //return;
//        }
////        QImage im1 = img.scaled(im->getImage().size(),Qt::IgnoreAspectRatio,Qt::FastTransformation);
//        im->setImage(img.scaled(im->getImage().size(),Qt::IgnoreAspectRatio,Qt::FastTransformation));
//    }
    m_image = img.scaled(1920,1080);
   //  m_image = img;
}

QMutex *VSourceThread::mutex()
{
    return &m_mutex;
}

QImage &VSourceThread::Image(QSize s)
{
    Q_UNUSED(s);
    QMutexLocker lo(&m_mutex);
    //bool find = true;
    //if (m_mutex.tryLock(10)) {
//        foreach(SizedImage *sz,m_images) {
//            QImage im = sz->getImage();
//            if (im.size() == s) {
//               // m_mutex.unlock();
//                return sz->getImage();
//            }
//        }
        return m_image;
        /*
     //   find = false;
   // } else {

        if (!m_images.count()) {
        } else {
           // m_mutex.unlock();
            //return m_images.at(m_images.count()-1)->getImage();
            m_image = QImage(s,QImage::Format_RGB32);
            if (!find) {
                m_mutex.unlock();
            }
            return m_image;
        }
    }
//    m_image = QImage(s,QImage::Format_RGB32);
//    for (int i  = 0 ; i < s.width(); i++) {
//        for(int j = 0; j < s.height(); j++) {
//            m_image.setPixel(QPoint(i,j),5);
//        }
//    }
    //m_mutex.unlock();
    if (!find) {
        m_mutex.unlock();
    }
    return m_image;*/
}

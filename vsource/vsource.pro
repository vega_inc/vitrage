#-------------------------------------------------
#
# Project created by QtCreator 2012-12-13T14:59:03
#
#-------------------------------------------------
include(../common.pri)
CONFIG -= release
CONFIG += debug

TARGET = vsource

DESTDIR = $$INSTALL_PATH_LIB

TEMPLATE = lib

DEFINES += VSOURCE_LIBRARY

QMAKE_CXXFLAGS += -Werror
INCLUDEPATH += $$INSTALL_PATH_INCLUDE \
                /usr/include/
SOURCES += vsource.cpp \
    vposition.cpp \
    vsourcethread.cpp \
    vsourceoptions.cpp

HEADERS += vsource.h\
        vsource_global.h \
    vposition.h \
    vsourcethread.h \
    vsourceoptions.h

unix {

    target.path = /$(DESTDIR)
    INSTALLS += target
    header_files.files = $$HEADERS
    header_files.path = /$(INCLUDEDIR)
    INSTALLS += header_files

}



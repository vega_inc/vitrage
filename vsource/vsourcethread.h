#ifndef VSOURCETHREAD_H
#define VSOURCETHREAD_H

#include <QThread>
#include <QImage>
#include <QMutex>
#include "stdint.h"

class SizedImage {
public:
    SizedImage();
    QImage m_image;
    QImage m_bare;
    uint32_t m_count;
    QMutex m_mutex;

    void setImage(const QImage &);
    QSize getSize(void);
    QImage &getImage(void);
    uint32_t count(void);
    void setCount(uint32_t c);
};

class VSourceThread : public QThread {
    Q_OBJECT
public: 
    VSourceThread();
    VSourceThread(VSourceThread& );
    virtual ~VSourceThread();
    void setImages(QImage &img);
    QMutex* mutex();
    QImage &Image(QSize sz);

public slots:
    void setSize(QSize n,QSize o);

signals:
    void updatedImage();
    void statusChanged(int status);

protected:
    QList<SizedImage *> m_images;
    QMutex m_mutex;
    QImage m_image;
    QTimer *m_ptimer;
};

#endif /* VSOURCETHREAD */

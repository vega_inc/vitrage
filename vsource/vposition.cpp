#include <QPoint>

#include "vposition.h"

VPosition::VPosition(const VPosition &p)
{
    this->setWidth(p.getWidth());

    this->setHeight(p.getHeight());
    this->lt = p.lt;
    this->rb = p.rb;

    this->setCropLxW(p.getCropLxW());
    this->setCropTyH(p.getCropTyH());
    this->setCropSowOnWiw(p.getCropSowOnWiw());
    this->setCropSohOnWih(p.getCropSohOnWih());
}

VPosition & VPosition::operator=(const VPosition & pos)
{
    this->lt = pos.lt;
    this->rb = pos.rb;
    this->setHeight(pos.getHeight());
    this->setWidth(pos.getWidth());
    this->z = pos.z;

    this->setCropLxW(pos.getCropLxW());
    this->setCropTyH(pos.getCropTyH());
    this->setCropSowOnWiw(pos.getCropSowOnWiw());
    this->setCropSohOnWih(pos.getCropSohOnWih());

    return *this;
}

bool VPosition::operator!=(const VPosition & p)
{
    return  ((this->lt != p.lt) || (this->rb != p.rb));
}

void VPosition::setWidth(uint32_t pixs) {
    this->width = pixs;
}

uint32_t VPosition::getWidth() const {
    return this->width;
}

void VPosition::setHeight(uint32_t h) {
    this->height = h;
}

uint32_t VPosition::getHeight() const {
    return this->height;
}

void VPosition::setCropLxW(double lxOnW) {
    cropLxW = lxOnW;
}

double VPosition::getCropLxW() const {
    return cropLxW;
}

void VPosition::setCropTyH(double lyOnH) {
    cropTyH = lyOnH;
}

double VPosition::getCropTyH() const {
    return cropTyH;
}

void VPosition::setCropSowOnWiw(double sowOnWiw) {
    cropSowOnWiw = sowOnWiw;
}

double VPosition::getCropSowOnWiw() const {
    return cropSowOnWiw;
}

void VPosition::setCropSohOnWih(double sohOnWih) {
    cropSohOnWih = sohOnWih;
}

double VPosition::getCropSohOnWih() const {
    return cropSohOnWih;
}

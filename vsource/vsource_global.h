#ifndef VSOURCE_GLOBAL_H
#define VSOURCE_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(VSOURCE_LIBRARY)
#  define VSOURCESHARED_EXPORT Q_DECL_EXPORT
#else
#  define VSOURCESHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // VSOURCE_GLOBAL_H

#ifndef VSOURCEOPTIONS_H
#define VSOURCEOPTIONS_H

#include <QWidget>

class VSourceOptions : public QWidget {
    Q_OBJECT
public:
    VSourceOptions() {}
    ~VSourceOptions() {
    }
signals:
    void	save_clicked(void);
    void	cancel_clicked(void);
};


#endif /* VSOURCEOPTIONS_H */

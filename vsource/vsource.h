#ifndef VSOURCE_H
#define VSOURCE_H

#include <QWidget>
#include <QUrl>
#include <QThread>
#include <QRubberBand>
#include <QSharedPointer>
#include <QMap>
#include <QVariant>

#include "stdint.h"

#include "vsourceoptions.h"
#include "vsourcethread.h"
#include "vposition.h"

const QString F1000_TYPE_PROP = "F1000Type";
typedef enum resize{
    LEFT_H,
    LEFT_L,
    RIGHT_H,
    RIGHT_L,
    LEFT,
    RIGHT,
    HIGH,
    LOW,
    NONE
} resize_t;

class VSource;

class AbstractVSource {
public:
    virtual void resize_src(uint32_t w, uint32_t h)       = 0;
    virtual void start()                                  = 0;
    virtual void stop()                                   = 0;
    virtual QSharedPointer<VSource>  clone()              = 0;
    virtual void finalize()                               = 0;
};

Q_DECLARE_INTERFACE(AbstractVSource,"com.Vega.Vitrage.AbstractVSource/1.0")


class VSource : public QWidget , public AbstractVSource {
    Q_OBJECT
    Q_ENUMS(ExecStatus)
    Q_INTERFACES(AbstractVSource)
public:
    enum SubSystemKind {
        SubSystemKind_None = 0,
        SubSystemKind_Galileo,
        SubSystemKind_LiveVideo,
        SubSystemKind_RGBCapture,
        SubSystemKind_SystemWindow,
        SubSystemKind_CPShare,
        SubSystemKind_VidStream,
        SubSystemKind_CPWeb,
        SubSystemKind_PictureViewer,
        SubSystemKind_CatalistLink,
        SubSystemKind_IPStream
    };
    static constexpr float OPAC  = 0.4; /* default opacity for statuses */
    enum ExecStatus {
        StopStatus, /* source created, but not started, or stop (no info) */
        RunStatus,  /* source not in error status */
        ErrorStatus, /* any error */
        None
    };
    enum class StatusIcon {
        EnabledIcon,
        DisabledIcon,
        ErrorIcon,
        DisabledErrorIcon
    };
    typedef enum {
        noDraw,
        noSignal,
        disabled
    } draw_status;

    static const int BORDER_W = 2;
    static const int BORDER_PERCENT = 10;
    static const int MIN_RUBBER_BAND_WIDTH = 2;  // desirably equal
    static const int MIN_RUBBER_BAND_HEIGHT = 2; // equal or odd

    VSource(QWidget *parent = NULL);
    VSource(const VSource&);

    virtual QString gettype(void);
    virtual ~VSource();
    void setThread(VSourceThread *);
    virtual bool srcConnected() {
        return true;
    }
    virtual void connectSrc(bool c) {
        Q_UNUSED(c);
    }
    VSourceThread* getThread() const;
    VPosition getPosition();
    void setCellSize(uint32_t, uint32_t);

    virtual void setPosition(VPosition posn);
    void resizeEvent(QResizeEvent *event);
    resize_t resizetype(void);
    void  setResizeType(resize_t);


    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
    VPosition  pstn;  /* position in Canva */
    void setname(const QString &str);
    void setPhysName(const QString &str);
    QString getPhysName() const;
    void setCloneName(const QString &str);
    QString getCloneName() const;

    void setLogicName(const QString &str);
    QString logicName();

    void setProperty(const QString&, const QVariant&);
    bool getProperty(const QString&, QVariant&);
    QVariant getProperty(const QString &key) const;
    const QMap<QString, QVariant> getMapProperty() const { return m_propMap;}
    void copyProperty(QMap<QString,QVariant> &map);
    void setVisualise(bool);
    bool visualise(void) const;
    void setStarted(bool);
    bool isStarted(void);
    void enableMeta(bool meta);
    void setSelected(bool);
    bool getSelected() const;
    void setEnabledFlag(bool);
    bool getEnabledFlag(void) const;
    bool activated() const;
    void setActivated(bool act);

    void setCropped(bool);
    bool isCropped() const;
    bool metaEnabled() const;
    ExecStatus getStatus() const;
    QString statusText() const;
    void setDrawStatus(draw_status);
    draw_status drawStatus(void);
    void setStatus(ExecStatus);
    void paintSelected(QPainter *, int w, int h,int d_w = 0,int d_h = 0);
    void paintStatusIcon(QPainter *painter, VSource::StatusIcon icon, int w, int h, int d_w = 0, int d_h = 0);
    void paintIcons(QPainter * painter, int d_w = 0, int d_h = 0);
    void setPrototype(VSource *src);
    VSource *prototype();
    void paintAll(QPainter *, int d_w = 0, int d_h =0 );
    void paintStatusError(QPainter *painter, int x, int y, int w, int h);
    void paintStatusErrorOff(QPainter *painter, int x, int y, int w, int h);
    void paintStatusOff(QPainter *painter, int x, int y, int w, int h);
    void paintStatusOn(QPainter *painter,int x,int y,int w,int h);
    void changeCursor();

    bool isValidCropRect(void);
    bool isCropRubberRectShown(void);
    void setVisibleCropRubberRect(bool);
    QRect getRubberCropRect(void);
    void setRubberCropRect(QRect);
    virtual void reconnectd();

private:
    void updateCropRubberBandRegion();

public slots:
    void statusFromThread(int);
    void cell_size_changed(uint32_t w,uint32_t h);
    void global_mouse_move();
    QString getname();
    void prototypeChanged();

signals:
    void setThreadSize(QSize news,QSize olds);
    void save_clicked();
    void updateModelFromSource();
    void mouse_move(QPoint,VSource *);
    void mouse_press(QPoint,VSource*);
    void mouseRightPress(QPoint point);
    void resize_sig(QPoint,VSource*);
    void mouse_release(QPoint);
    void drop_sig(QString,QPoint);
    void statusForTable(QString name);
    void sourceDoubleClicked();
    void signalForClones();
    void sourceSeleced();
    void inited();

protected:
    VSource *m_pPrototype;
    QMap<QString, QVariant> m_propMap;
    QList<SizedImage*> *m_images;
    bool m_visualise;
    QString name;
    QString clone_name;
    QString m_physName;
    QString type;
    resize_t resize_type;
    draw_status m_drawStatus;
    QString host;
    VSourceThread *m_thread; /* thread for capture and resize */
    ExecStatus m_status = ExecStatus::None;
    bool pressed = false;
    bool resize_f = false;   /* flag for resizing */
    bool m_meta;     /* flag for paint event */
    bool m_selected; /* flag for paint event */
    bool m_enabled = true;   /* flag for enable flag in source view */
    bool m_started;
    bool m_activated; /* flag for view (to set editable source or widget */
    int m_smallCircle;
    int m_bigCircle;
    int m_selectedLineWidth;
    QPoint press_point;

    bool m_cropped = false;
    bool m_cropRubberBandIsShown;
    QRect m_cropRubberBandRect;
    volatile bool m_mouseOnBorderRubberRect;
    enum::resize rubberBorderMousePlace;

    bool dot_in_canva(QPoint p);
    void paintMeta(QPainter *p);
    void paintStatus(QPainter *painter);
    void paintBorder(QPainter *painter, int d_w = 0, int d_h = 0);
    void drawDoubleLine(int,int,int,int, QPainter *painter);
    void paintCroppedBox(QPainter *painter, int d_w = 0, int d_h = 0);
    void paintCropRubberBand(QPainter *painter);

public:
    volatile bool m_cropMode;
    bool m_isCroppedClone;
    volatile bool blinkCrop;
};

class VSourceCreator {
public:
    enum TYPE {
        STRING,
        INT,
        FLOAT,
        ICON
    };

    struct Param {
        Param(const QString &nm, const QString &d = "", TYPE t = TYPE::STRING, bool m = true)
            : name(nm),
              desc(d),
              type(t),
              mandatory(m)
        { }
        Param() {}
        QString name;
        QString desc;
        TYPE type;
        bool mandatory;
    };

    virtual VSource* createVSource()     = 0;
    virtual QString getSourceType()      = 0;
    virtual QList<Param> getParamsList() = 0;
};

Q_DECLARE_INTERFACE(VSourceCreator,"com.Vega.Vitrage.VSourceCreator/1.0")


#endif /* VSOURCE_H */

#include <QEvent>
#include <QDropEvent>
#include <QDragEnterEvent>
#include <QPainter>
#include <QApplication>
#include <QTextCodec>
#include <QDebug>

#include "vsource.h"
#define COS45 0.7071

constexpr int MARG = 8; // margines for resize cursor in pixels
constexpr int BIG_MARG = 25;


#define SMALL_CICRLE_DEF 5
#define BIG_CIRCLE_DEF   7
#define SELECTED_LINE    2 

#define RUNTIME_COLOR QColor(51,204,51)
#define SELECTED_COLOR QColor(153,230,153)
#define ON_COLOR QColor(153,230,153)

#define ERROR_COLOR QColor(255,0,0)


#define SMALL_D 2/3  /* r for small circle delta */

#define SZ_POLICY QSizePolicy::Preferred

VSource::VSource(QWidget *parent): QWidget(parent),
    m_drawStatus(VSource::noDraw),
    m_smallCircle(SMALL_CICRLE_DEF),
    m_bigCircle(BIG_CIRCLE_DEF),
    m_selectedLineWidth(SELECTED_LINE),
    m_cropped(false),
    m_cropRubberBandIsShown(m_cropped),
    m_mouseOnBorderRubberRect(false),
    rubberBorderMousePlace(NONE),
    m_isCroppedClone(false),
    blinkCrop(true)
{
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(codec);
    QTextCodec::setCodecForCStrings(codec);

    setMouseTracking(true);
    setAcceptDrops(true);
    setAutoFillBackground(false);
    setAttribute(Qt::WA_NoSystemBackground);
    setSizePolicy(SZ_POLICY,SZ_POLICY);
    setContentsMargins(0,0,0,0);
    setStarted(true);
    setEnabledFlag(true);
    setVisualise(true);
    setSelected(false);
    setPrototype(NULL);
    m_activated = false;
    setAttribute(Qt::WA_TransparentForMouseEvents);
    m_cropRubberBandRect.setTopLeft(QPoint(0,0));
    m_cropRubberBandRect.setBottomRight(QPoint(0,0));
    m_cropMode = true;
    setMouseTracking(true);
}

void VSource::setThread(VSourceThread * th)
{
    m_thread = th;
    connect(m_thread,SIGNAL(updatedImage()),this,SLOT(updateImage()),Qt::QueuedConnection);
    connect(this,SIGNAL(setThreadSize(QSize,QSize)),m_thread,
                              SLOT(setSize(QSize,QSize)),Qt::DirectConnection);
    connect(m_thread,SIGNAL(statusChanged(int)),this,SLOT(statusFromThread(int)));
    connect(this,SIGNAL(startSignal()),th,SLOT(start_()));
    connect(this,SIGNAL(stopSignal()),th,SLOT(stop_()));
}

VSourceThread* VSource::getThread() const
{
	return m_thread;
}	

VSource::VSource(const VSource &S): QWidget(NULL)
{
    m_propMap = S.getMapProperty();
    setMouseTracking(true);
    setAcceptDrops(true);
    pressed = false;
    setAutoFillBackground(false);
    setAttribute(Qt::WA_NoSystemBackground);
    setEnabledFlag(true);
    m_cropRubberBandIsShown=m_cropped;
    m_mouseOnBorderRubberRect=false;
    rubberBorderMousePlace=NONE;
    m_isCroppedClone=false;
    blinkCrop=true;
    m_cropRubberBandRect.setTopLeft(QPoint(0,0));
    m_cropRubberBandRect.setBottomRight(QPoint(0,0));
    m_cropMode = true;
}

QString VSource::getname()
{
    return name;
}

void VSource::prototypeChanged()
{
    if (!prototype()) return;
    setStatus(prototype()->getStatus());
    setEnabledFlag(prototype()->getEnabledFlag());
    update();
}

void VSource::reconnectd()
{
}

void VSource::setname(const QString &str)
{
    name = str;
}

void VSource::setPhysName(const QString &str)
{
    m_physName = str;
}

QString VSource::getPhysName() const
{
    return m_physName;
}

void VSource::setCloneName(const QString &str)
{
    clone_name = str;
}

QString VSource::getCloneName() const
{
    return clone_name;
}

void VSource::setLogicName(const QString &str)
{
    setProperty("logicName",str);
}

QString VSource::logicName()
{
    QVariant var;
    getProperty("logicName",var);
    return var.toString();
}

QVariant VSource::getProperty(const QString &key) const
{
    return m_propMap.value(key);
}

void VSource::setCellSize(uint32_t w, uint32_t h) 
{
    pstn.setWidth(w);
    pstn.setHeight(h);
}

void VSource::dragEnterEvent(QDragEnterEvent *event)
{
    event->acceptProposedAction();
}

void VSource::dropEvent(QDropEvent *event)
{
    QPoint point = QCursor::pos();
    if (event->mimeData()->hasText()) {
        emit drop_sig(event->mimeData()->text(),point);
        event->acceptProposedAction();
    }
}

void VSource::cell_size_changed(uint32_t w, uint32_t h)
{
    setCellSize(w,h);
    VPosition posn = getPosition();
    int32_t width = (posn.rb.x()-  posn.lt.x());
    int32_t height = (posn.rb.y() - posn.lt.y());
    QSize s(w*width,h*height);
    resize(s);
}

void VSource::updateCropRubberBandRegion()
{
    QRect rect = m_cropRubberBandRect.normalized();
    update(rect.left(), rect.top(), rect.width(), 1);
    update(rect.left(), rect.top(), 1, rect.height());
    update(rect.left(), rect.bottom(), rect.width(), 1);
    update(rect.right(), rect.top(), 1, rect.height());
}

void VSource::mouseMoveEvent(QMouseEvent *event)
{
    changeCursor();
    if (!m_isCroppedClone) {
        if (!m_selected)
            return;
        if (pressed && !resize_f) {
            setCursor(Qt::ClosedHandCursor);
            emit mouse_move(QCursor::pos(), this);
        }
        else if (resize_f) {
            emit resize_sig(QCursor::pos(), this);
        }
        return;
    }
    else {
        if ( (event->buttons() & Qt::LeftButton) && isCropRubberRectShown()) {
            QPoint tmpPoint = mapFromGlobal(event->globalPos());
            if(tmpPoint.x()>=0 && tmpPoint.y()>=0 && tmpPoint.x()<=width() && tmpPoint.y()<=height() ) {
                switch(rubberBorderMousePlace)
                {
                case NONE:
                {
                    updateCropRubberBandRegion();
                    m_cropRubberBandRect.setBottomRight(mapFromGlobal(event->globalPos()));
                    updateCropRubberBandRegion();
                } break;
                case LEFT_H:
                {
                    updateCropRubberBandRegion();
                    m_cropRubberBandRect.setTopLeft(mapFromGlobal(event->globalPos()));
                    updateCropRubberBandRegion();
                } break;
                case RIGHT_H:
                {
                    updateCropRubberBandRegion();
                    m_cropRubberBandRect.setTopRight(mapFromGlobal(event->globalPos()));
                    updateCropRubberBandRegion();
                } break;
                case LEFT_L:
                {
                    updateCropRubberBandRegion();
                    m_cropRubberBandRect.setBottomLeft(mapFromGlobal(event->globalPos()));
                    updateCropRubberBandRegion();
                } break;
                case RIGHT_L:
                {
                    updateCropRubberBandRegion();
                    m_cropRubberBandRect.setBottomRight(mapFromGlobal(event->globalPos()));
                    updateCropRubberBandRegion();
                } break;
                default: break;
                }

            }
            event->ignore();
        }
        else if(m_cropRubberBandIsShown) {
            QPoint tmpPoint = mapFromGlobal(event->globalPos());
            QRect tmpRect = m_cropRubberBandRect.normalized();
            if( tmpPoint.x()>=tmpRect.left()-10 && tmpPoint.x()<=tmpRect.left()+10 &&
                    tmpPoint.y()>=tmpRect.top()-10 && tmpPoint.y()<=tmpRect.top()+10 )
            {
                rubberBorderMousePlace = LEFT_H;
                m_mouseOnBorderRubberRect=true;
                QApplication::setOverrideCursor(QCursor(Qt::SizeFDiagCursor));
            }
            else if( tmpPoint.x()>=tmpRect.right()-10 && tmpPoint.x()<=tmpRect.right()+10 &&
                     tmpPoint.y()>=tmpRect.top()-10 && tmpPoint.y()<=tmpRect.top()+10 )
            {
                rubberBorderMousePlace = RIGHT_H;
                m_mouseOnBorderRubberRect=true;
                QApplication::setOverrideCursor(QCursor(Qt::SizeBDiagCursor));
            }
            else if( tmpPoint.x()>=tmpRect.left()-10 && tmpPoint.x()<=tmpRect.left()+10 &&
                     tmpPoint.y()>=tmpRect.bottom()-10 && tmpPoint.y()<=tmpRect.bottom()+10 )
            {
                rubberBorderMousePlace = LEFT_L;
                m_mouseOnBorderRubberRect=true;
                QApplication::setOverrideCursor(QCursor(Qt::SizeBDiagCursor));
            }
            else if( tmpPoint.x()>=tmpRect.right()-10 && tmpPoint.x()<=tmpRect.right()+10 &&
                     tmpPoint.y()>=tmpRect.bottom()-10 && tmpPoint.y()<=tmpRect.bottom()+10 )
            {
                rubberBorderMousePlace = RIGHT_L;
                m_mouseOnBorderRubberRect=true;
                QApplication::setOverrideCursor(QCursor(Qt::SizeFDiagCursor));
            }
            else
            {
                rubberBorderMousePlace = NONE;
                QApplication::restoreOverrideCursor();
                m_mouseOnBorderRubberRect=false;
            }
            event->ignore();
        }
    }
}

void VSource::mouseReleaseEvent(QMouseEvent *event)
{
    pressed = false;
    resize_f = false;
    if( !m_isCroppedClone ) {
        QPoint p = QCursor::pos();
        emit mouse_release(p);
    }
    else {
        if (event->button() == Qt::LeftButton && m_cropRubberBandIsShown) {
            updateCropRubberBandRegion();
            QApplication::restoreOverrideCursor();
            rubberBorderMousePlace = NONE;
            m_mouseOnBorderRubberRect = false;
            event->ignore();
        }
    }
}

void VSource::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::RightButton) {
        emit mouseRightPress(mapToGlobal(event->pos()));
        return;
    }
    if (event->button() == Qt::LeftButton)
        pressed = true;
    if (!m_isCroppedClone) {
        QCursor c = cursor();
        Qt::CursorShape shape = c.shape();
        if (shape != Qt::ArrowCursor) {
            resize_f = true;
            return;
        }
        press_point = QCursor::pos();
        emit mouse_press(press_point,this);
        emit sourceDoubleClicked();
    }
    else {
        if (event->button() == Qt::LeftButton) {
            QPoint tmpPoint = mapFromGlobal(event->globalPos());
            if( tmpPoint.x() >= 0 && tmpPoint.y() >= 0 && tmpPoint.x() <= width() && tmpPoint.y() <= height())
            {
                setVisibleCropRubberRect(true);
                if(m_cropRubberBandIsShown)
                {
                    switch(rubberBorderMousePlace)
                    {
                    case NONE:
                    {
                        m_cropRubberBandRect.setTopLeft(mapFromGlobal(event->globalPos()));//event->pos()-pos()-parentWidget()->pos());
                        m_cropRubberBandRect.setBottomRight(mapFromGlobal(event->globalPos()));//event->pos()-pos()-parentWidget()->pos());
                        updateCropRubberBandRegion();
                        QApplication::setOverrideCursor(QCursor(Qt::CrossCursor));
                    } break;
                    case LEFT_H:
                    {
                        updateCropRubberBandRegion();
                        m_cropRubberBandRect.setTopLeft(mapFromGlobal(event->globalPos()));
                        updateCropRubberBandRegion();
                    } break;
                    case RIGHT_H:
                    {
                        updateCropRubberBandRegion();
                        m_cropRubberBandRect.setTopRight(mapFromGlobal(event->globalPos()));
                        updateCropRubberBandRegion();
                    } break;
                    case LEFT_L:
                    {
                        updateCropRubberBandRegion();
                        m_cropRubberBandRect.setBottomLeft(mapFromGlobal(event->globalPos()));
                        updateCropRubberBandRegion();
                    } break;
                    case RIGHT_L:
                    {
                        updateCropRubberBandRegion();
                        m_cropRubberBandRect.setBottomRight(mapFromGlobal(event->globalPos()));
                        updateCropRubberBandRegion();
                    } break;
                    default: break;
                    }
                }
            }
            event->ignore();
        }
    }
}

void VSource::changeCursor()
{
    if (resize_f)
        return;
    QPoint pt =	QWidget::mapFromGlobal(QCursor::pos());
    int x = pt.x();
    int y = pt.y();
    if (x > this->width() - MARG) {
        this->setCursor(Qt::SizeHorCursor);
        this->setResizeType(RIGHT);
        if ((y >= 0) && (y <= BIG_MARG)) {
            setCursor(Qt::SizeBDiagCursor);
            setResizeType(RIGHT_H);
            return;
        }
        if (y >= (this->height() - BIG_MARG))   {
            setCursor(Qt::SizeFDiagCursor);
            setResizeType(RIGHT_L);
            return;
        }
        return;
    }
    if (x <= MARG) {
        setResizeType(LEFT);
        setCursor(Qt::SizeHorCursor);
        if ((y >= 0) && (y <= BIG_MARG)) {
            setResizeType(LEFT_H);
            setCursor(Qt::SizeFDiagCursor);
            return;
        }
        if (y >= (this->height() - BIG_MARG))   {
            setResizeType(LEFT_L);
            setCursor(Qt::SizeBDiagCursor);
            return;
        }
        return;
    }

    if (y > this->height() - MARG) {
        this->setResizeType(LOW);

        this->setCursor(Qt::SizeVerCursor);
        if ((x >= 0) && (x <= BIG_MARG)) {
            setResizeType(RIGHT_L);
            setCursor(Qt::SizeBDiagCursor);
            return;
        }
        if (x >= (this->width() - BIG_MARG))   {
            setResizeType(RIGHT_L);
            setCursor(Qt::SizeFDiagCursor);
            return;
        }
        return;
    }

    if (y <= MARG) {
        setResizeType(HIGH);
        setCursor(Qt::SizeVerCursor);
        if ((x >= 0) && (x <= BIG_MARG)) {
            setCursor(Qt::SizeFDiagCursor);
            setResizeType(LEFT_H);
            return;
        }
        if (x >= (this->width() - BIG_MARG))   {
            setResizeType(RIGHT_H);
            setCursor(Qt::SizeBDiagCursor);
            return;
        }
        return;
    }

    setCursor(Qt::ArrowCursor);
    setResizeType(NONE);
}

void VSource::global_mouse_move()
{
    if (!pressed) {
        changeCursor();
    }
}

QString VSource::gettype(void)
{
    return type;
}

VPosition VSource::getPosition()
{
    return pstn;
}

void VSource::setPosition(VPosition posn)
{
    pstn = posn;
}

resize_t VSource::resizetype(void)
{
    return resize_type;
}

void  VSource::setResizeType(resize_t t)
{
    resize_type = t;
}

void VSource::setProperty(const QString& name, const QVariant& val)
{
    m_propMap.insert(name, val);
}

bool VSource::getProperty(const QString& name, QVariant& value)
{
    bool ret_val = m_propMap.contains(name);
    if(ret_val)
        value = m_propMap.value(name);
    return ret_val;
}

void VSource::setStarted(bool state)
{
	m_started = state;
}

bool VSource::isStarted(void) 
{
	return m_started;
}

void VSource::resizeEvent(QResizeEvent* event)
{
	emit setThreadSize(event->size(),event->oldSize());
}

void VSource::copyProperty(QMap<QString,QVariant> &map)
{
    map.clear();
    foreach(QString str,m_propMap.keys()) {
        map.insert(str,m_propMap.value(str));
    }
}

VSource::ExecStatus VSource::getStatus() const
{
    return m_status;
}

QString VSource::statusText() const
{
    switch (m_status) {
    case ExecStatus::StopStatus:
        return "Не запущен";
        break;
    case ExecStatus::RunStatus:
        return "Работает";
        break;
    case ExecStatus::ErrorStatus:
        return "Ошибка";
        break;
    case ExecStatus::None:
        return "Нет статуса";
        break;
    default:
        break;
    }
    return "-";
}

void VSource::setStatus(ExecStatus st)
{
 	m_status = st;
    emit signalForClones();
    emit updateModelFromSource();
}

void VSource::statusFromThread(int st)
{
    setStatus((VSource::ExecStatus)st);
	emit statusForTable(name);
    emit signalForClones();
}

VSource::~VSource()
{
    //disconnect(m_thread,SIGNAL(updatedImage()),this,SLOT(updateImage()));
    //disconnect(this,SIGNAL(setThreadSize(QSize,QSize)),m_thread,SLOT(setSize(QSize,QSize)));
    //disconnect(m_thread,SIGNAL(statusChanged(int)),this,SLOT(statusFromThread(int)));
}

void VSource::setVisualise(bool started)
{
    m_visualise = started;
}
/**
* @brief flag for checkboxes in table
*
* @return bool 
*/
bool VSource::visualise() const
{
    return m_visualise;
}

void VSource::paintMeta(QPainter *painter)
{
    if (!m_meta) return;
    int w = width();
    int h = height();

    painter->drawLine(QPoint(0,0),QPoint(w,h));
}

void VSource::paintSelected(QPainter *painter, int w, int h, int d_w, int d_h)
{
    painter->save();
    int dif = (SELECTED_LINE+2)/2;
    drawDoubleLine(0+dif+d_w,0+dif+d_h,w-dif+d_w,0+dif+d_h,painter);
    drawDoubleLine(w-dif+d_w,0+dif+d_h,w-dif+d_w,h-dif+d_h,painter);
    drawDoubleLine(0+dif+d_w,0+dif+d_h,0+dif+d_w,h-dif+d_h,painter);
    drawDoubleLine(0+dif+d_w,h-dif+d_h,w-dif+d_w,h-dif+d_h,painter);

    painter->setPen(QPen((Qt::white),1));
    painter->setBrush(QBrush(Qt::red));
    painter->drawEllipse(QPoint(0+dif+d_w,0+dif+d_h),SMALL_CICRLE_DEF,SMALL_CICRLE_DEF);
    painter->drawEllipse(QPoint(0+dif+d_w,h/2+d_h),SMALL_CICRLE_DEF,SMALL_CICRLE_DEF);
    painter->drawEllipse(QPoint(0+dif+d_w,h-dif+d_h),SMALL_CICRLE_DEF,SMALL_CICRLE_DEF);
    painter->drawEllipse(QPoint(w/2+d_w,0+dif+d_h),SMALL_CICRLE_DEF,SMALL_CICRLE_DEF);
    painter->drawEllipse(QPoint(w-dif+d_w,0+dif+d_h),SMALL_CICRLE_DEF,SMALL_CICRLE_DEF);
    painter->drawEllipse(QPoint(w-dif+d_w,h/2+d_h),SMALL_CICRLE_DEF,SMALL_CICRLE_DEF);
    painter->drawEllipse(QPoint(w-dif+d_w,h-dif+d_h),SMALL_CICRLE_DEF,SMALL_CICRLE_DEF);
    painter->drawEllipse(QPoint(w/2+d_w,h-dif+d_h),SMALL_CICRLE_DEF,SMALL_CICRLE_DEF);
    painter->restore();
}

void VSource::paintStatus(QPainter *painter)
{
    painter->save();
    painter->setPen(QPen((Qt::white),1));
    painter->setBrush(QBrush(Qt::red));
    int w = width();
    int h = height();
    switch (m_drawStatus) {
    case (VSource::noDraw) :
        break;
    case (VSource::disabled) :
        painter->drawText(QPoint(w/2,h/2),tr("Unvisible"));
        break;
    case (VSource::noSignal) :
        break;
    }
    painter->restore();
}

void VSource::paintBorder(QPainter *painter, int d_w, int d_h)
{
    QPen pen = painter->pen();
    QPen pen_ = pen;
    pen.setWidth(1);
    pen.setColor(Qt::white);
    painter->setPen(pen);
    painter->drawRect(0+d_w,0+d_h,width()-2,height()-2);
    pen.setColor(Qt::black);
    painter->setPen(pen);
    painter->drawRect(1+d_w,1+d_h,width()-4,height()-4);
    painter->setPen(pen_);
}
/**
 * @brief VSource::paintAll
 * @param painter - [in] QPainter*
 * @param d_w - [in] int input delta for paintin in canva
 * @param d_h - [in] int input delta for paintin in canva
 */
void VSource::paintAll(QPainter *painter, int d_w, int d_h)
{
    paintBorder(painter, d_w, d_h);
    if (m_selected) {
        paintSelected(painter,width(),height(),d_w,d_h);
    }
    //paintStatus(painter);
    //paintIcons(painter,d_w,d_h);

    if(m_isCroppedClone && m_cropRubberBandIsShown) {
        paintCropRubberBand(painter);
        update();
    }
}

void VSource::paintCropRubberBand(QPainter *painter)
{
    painter->save();
    QPen pen = painter->pen();
    QBrush brush = painter->brush();

    pen.setColor(palette().light().color());
    int cropLineOffset = pen.dashOffset();
    QVector<qreal> dashes;
    qreal space = 4;
    dashes << 4 << space;
    pen.setDashPattern(dashes);
    if(blinkCrop) {
       pen.setDashOffset(cropLineOffset + 4);
    }

    if(m_mouseOnBorderRubberRect)
        pen.setWidth(2);
    else
        pen.setWidthF(qreal(1.5));
    painter->setPen(pen);

    if(m_cropMode)
    {
        brush.setColor(QColor(40, 90, 255, 127).dark());
        brush.setStyle(Qt::Dense4Pattern);
        painter->setBrush(brush);
    }
    else
    {
        auto w = width();
        auto h = height();
        brush.setStyle(Qt::Dense2Pattern);
        painter->fillRect(QRect(0,
                                0,
                                w,
                                m_cropRubberBandRect.normalized().top()
                                ),brush);
        painter->fillRect(QRect(0,
                                m_cropRubberBandRect.normalized().top(),
                                m_cropRubberBandRect.normalized().left(),
                                h
                                ),brush);
        painter->fillRect(QRect(m_cropRubberBandRect.normalized().left(),
                                m_cropRubberBandRect.normalized().top() + m_cropRubberBandRect.normalized().height(),
                                w,
                                h
                                ),brush);
        painter->fillRect(QRect(m_cropRubberBandRect.normalized().left() + m_cropRubberBandRect.normalized().width(),
                                m_cropRubberBandRect.normalized().top(),
                                w,
                                m_cropRubberBandRect.normalized().height()
                                ),brush);
    }
    painter->drawRect(m_cropRubberBandRect.normalized());
    painter->restore();
}

bool VSource::isValidCropRect(void)
{
    if( m_cropRubberBandRect.normalized().width()>=MIN_RUBBER_BAND_WIDTH &&
        m_cropRubberBandRect.normalized().height()>=MIN_RUBBER_BAND_HEIGHT)
        return true;
    return false;
}

bool VSource::isCropRubberRectShown(void)
{
    return m_cropRubberBandIsShown;
}

void VSource::setVisibleCropRubberRect(bool b)
{
    m_cropRubberBandIsShown=b;
}

QRect VSource::getRubberCropRect(void)
{
    return m_cropRubberBandRect;
}

void VSource::setRubberCropRect(QRect cropRect)
{
    m_cropRubberBandRect.setTopLeft(cropRect.topLeft());
    m_cropRubberBandRect.setWidth(cropRect.width());
    m_cropRubberBandRect.setHeight(cropRect.height());
}

void VSource::paintCroppedBox(QPainter *painter, int d_w, int d_h)
{
    painter->save();
    QPen pen = painter->pen();
    QBrush brush = painter->brush();

    pen.setWidth(1);
    pen.setColor(Qt::white);
    pen.setStyle(Qt::DashLine);
    painter->setPen(pen);

    QRect tmpRect(QPoint(0,0),QSize(0,0));

    VPosition tmpPosition = getPosition();

    QPoint tmpRubberPointTL( qRound(  width()* tmpPosition.getCropLxW() )+d_w ,
                             qRound(  height()* tmpPosition.getCropTyH() )+d_h );


    int rubberWidth = qRound(width()* tmpPosition.getCropSowOnWiw());
    int rubberHeight = qRound( height()* tmpPosition.getCropSohOnWih());

    tmpRect.setTopLeft(tmpRubberPointTL);
    tmpRect.setWidth(rubberWidth);
    tmpRect.setHeight(rubberHeight);

    brush.setColor(QColor(255, 255, 255, 127));
    brush.setStyle(Qt::Dense4Pattern);
    painter->fillRect(QRect(0 + d_w, 0 + d_h,
                            width(), tmpRect.top()
                            ),brush);
    painter->fillRect(QRect(0 + d_w, tmpRect.top() + d_h,
                            tmpRect.left(), height()
                            ),brush);
    painter->fillRect(QRect(tmpRect.left() + d_w, tmpRect.top() + tmpRect.height() + d_h,
                            width(), height()
                            ),brush);
    painter->fillRect(QRect(tmpRect.left() + tmpRect.width() + d_w, tmpRect.top() + d_h,
                            width(), tmpRect.height()
                            ),brush);

    painter->drawRect(tmpRect);
    painter->restore();
}

void VSource::paintIcons(QPainter *painter, int d_w, int d_h)
{
    int w = width();
    int h = height();
    VSource::ExecStatus status = getStatus();
    switch(status)
    {
    case (ExecStatus::None):
        break;
    case (ExecStatus::RunStatus) :
        if (!m_enabled) {
            paintStatusOff(painter,0,0,w,h);
            paintStatusIcon(painter, StatusIcon::DisabledIcon,w,h,d_w,d_h);
        }
        break;
    case (ExecStatus::StopStatus) :
    case (ExecStatus::ErrorStatus) :
        painter->fillRect(QRect(0,0,width(),height()),QBrush(Qt::gray));
        if (m_enabled) {
            paintStatusError(painter,0,0,w,h);
            paintStatusIcon(painter,StatusIcon::ErrorIcon,w,h,d_w,d_h);
        } else {
            paintStatusErrorOff(painter,0,0,w,h);
            paintStatusIcon(painter,StatusIcon::DisabledErrorIcon,w,h,d_w,d_h);
        }
        break;
    }
    if(m_cropped && !m_isCroppedClone) {
        if( getPosition().getCropSowOnWiw()>0 && getPosition().getCropSohOnWih()>0 ) {
            paintCroppedBox(painter,d_w,d_h);
        }
    }
}

void VSource::setPrototype(VSource *src)
{
    m_pPrototype = src;
    if (src) {
        connect(src,SIGNAL(signalForClones()),this,SLOT(prototypeChanged()));
    }
}

VSource* VSource::prototype(void)
{
    return m_pPrototype;
}

void VSource::drawDoubleLine(int x1,int y1,int x2,int  y2, QPainter *painter) 
{
    painter->setPen(QPen((Qt::black),SELECTED_LINE+2));
    painter->setBrush(QBrush(Qt::black));
    painter->drawLine(x1,y1,x2,y2);

    painter->setPen(QPen((Qt::white),SELECTED_LINE));
    painter->setBrush(QBrush(Qt::white));
    painter->drawLine(x1,y1,x2,y2);
}

void VSource::enableMeta(bool enable) 
{
    m_meta = enable;
}

bool VSource::metaEnabled() const
{
    return m_meta;
}

void VSource::setSelected(bool flag) 
{
    m_selected = flag;
    emit updateModelFromSource();
    emit signalForClones();
    if (flag) {
        emit sourceSeleced();
    }
    setAttribute(Qt::WA_TransparentForMouseEvents,!flag);
    update();
}

bool VSource::getSelected() const
{
    return m_selected;
}

void VSource::setEnabledFlag(bool flag)
{
    m_enabled = flag;
    emit updateModelFromSource();
    emit signalForClones();
    update();
}

bool VSource::getEnabledFlag() const
{
    return m_enabled;
}

bool VSource::activated() const
{
    return m_activated;
}

void VSource::setDrawStatus(VSource::draw_status status)
{
    m_drawStatus = status;
    update();
}

VSource::draw_status VSource::drawStatus(void)
{
    return m_drawStatus;
}

void VSource::paintStatusIcon(QPainter *painter, VSource::StatusIcon icon, int w, int h, int d_w, int d_h)
{
    painter->save();
    QPen pen = painter->pen();
    pen.setBrush(Qt::white);
    pen.setWidth(1);
    painter->setPen(pen);

    QFont ft = painter->font();
    ft.setPixelSize(((w>h)?h:w)/8);
    painter->setFont(ft);
    auto radius = ((w>h)?h:w)/6;
    auto delta = radius * COS45;

    switch (icon) {
    case (StatusIcon::EnabledIcon) :
        painter->drawEllipse(QPoint(d_w+w/2,d_h+h*2/5),radius,radius);
        painter->drawLine(QLine(d_w+w/2,d_w+h*2/5 - radius*2/3,
                                d_w+w/2,d_w+h*2/5 + radius*2/3));
        painter->drawText(QRect(d_w+w/4,d_w+h*3/5,w*2/3,h*3/5),tr("Enabled"));
        break;
    case (StatusIcon::DisabledIcon) :
    {
        painter->drawEllipse(QPoint(w/2,h*2/5),radius,radius);
        painter->drawEllipse(QPoint(w/2,h*2/5),radius/2,radius/2);
        QRect rectText = rect();
        rectText.setTop(h*3/5);
        painter->drawText(rectText, Qt::AlignCenter, "Выключен");
        break;
    }
    case (StatusIcon::ErrorIcon):
    {
        painter->drawEllipse(QPoint(w/2,h*2/5),radius,radius);
        auto leftTop = QPoint(w/2 - delta/2,h*2/5 - delta/2);
        auto rightButtom = QPoint(w/2 + delta/2,h*2/5 + delta/2);
        auto rightTop = QPoint(w/2 - delta/2,h*2/5 + delta/2);
        auto leftButtom = QPoint(w/2 + delta/2,h*2/5 - delta/2);

        painter->drawLine(leftTop,rightButtom);
        painter->drawLine(rightTop,leftButtom);
        painter->drawText(QRect(0,h*2/5,w,h*3/5), Qt::AlignCenter, "Нет сигнала");
        break;
    }
    case (StatusIcon::DisabledErrorIcon):
    {
        painter->drawEllipse(QPoint(w/4,h/4),radius*2/3,radius*2/3);
        painter->drawEllipse(QPoint(w/4,h/4),radius/3,radius/3);
        delta = delta/2;
        painter->drawEllipse(QPoint(w*3/4,h*3/4),
                             radius/2,radius/2);
        auto leftTop = QPoint(w*3/4 - delta*SMALL_D,h*3/4 - delta*SMALL_D);
        auto rightButtom = QPoint(w*3/4 + delta*SMALL_D,h*3/4 + delta*SMALL_D);
        auto rightTop = QPoint(w*3/4- delta*SMALL_D,h*3/4 + delta*SMALL_D);
        auto leftButtom = QPoint(w*3/4 + delta*SMALL_D,h*3/4 - delta*SMALL_D);

        painter->drawLine(leftTop,rightButtom);
        painter->drawLine(rightTop,leftButtom);
        break;
    }
    default:
        break;
    }
    painter->restore();
}

void VSource::paintStatusError(QPainter *painter,int x,int y,int w,int h)
{
    painter->setOpacity(OPAC);
    painter->fillRect(QRect(x,y,w-2*x,h-2*x),QBrush(Qt::red));
    painter->setOpacity(1);
}

void VSource::paintStatusErrorOff(QPainter *painter,int x,int y,int w,int h)
{
    painter->setOpacity(OPAC);
    /* black triangle */
    QVector<QPoint> points;
    points.push_back(QPoint(x,y));
    points.push_back(QPoint(x,h-y));
    points.push_back(QPoint(w-x,y));
    points.push_back(QPoint(x,y));

    QPolygon pol(points);
    QPainterPath path;
    path.addPolygon(pol);
    /* red triangle */
    QVector<QPoint> points1;
    points1.push_back(QPoint(x,h-y));
    points1.push_back(QPoint(w-x,y));
    points1.push_back(QPoint(w-x,h-y));
    points1.push_back(QPoint(x,h-y));
    QPolygon pol1(points1);
    QPainterPath path1;
    path1.addPolygon(pol1);

    painter->fillPath(path,QBrush(Qt::black));
    painter->fillPath(path1,QBrush(Qt::red));
    painter->setOpacity(1);
}

void VSource::paintStatusOff(QPainter *painter,int x,int y,int w,int h)
{
    painter->setOpacity(OPAC);
    painter->fillRect(QRect(x,y,w-2*x,h-2*y),QBrush(Qt::black));
    painter->setOpacity(1);
}

void VSource::paintStatusOn(QPainter *painter,int x,int y,int w,int h)
{
    painter->setOpacity(OPAC);
    painter->fillRect(QRect(x,y,w-2*x,h-2*y),QBrush(RUNTIME_COLOR));
    painter->setOpacity(1);
}

void VSource::setActivated(bool act)
{
    m_activated = act;
    emit updateModelFromSource();
    emit signalForClones();
}

void VSource::setCropped(bool state)
{
    if (m_cropped != state) {
        m_cropped = state;
        update();
    }
}

bool VSource::isCropped() const
{
    return m_cropped;
}

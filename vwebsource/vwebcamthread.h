#ifndef VWEBCAMTHREAD_H
#define VWEBCAMTHREAD_H

#include <QTimer>

#include <vsource/vsource.h>
#include <opencv2/opencv.hpp>

class  VWebCamThread : public VSourceThread {
    Q_OBJECT
public:
    //void stop(void);
   VWebCamThread();
   ~VWebCamThread();
    void stopCamera(void);

signals:
 
//void image_updated(void);

public slots:
    void timerSlot(void);
protected:
    void run();

private:
	QTimer *timer;
	CvCapture *camera ;
	int w;
	int h;
	void  setImage(IplImage *image);
};

#endif /* WWEBCAMTHREAD */

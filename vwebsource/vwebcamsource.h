#ifndef VWEBCAMSOURCE_H
#define VWEBCAMSOURCE_H

#include <QVBoxLayout>
#include <QLabel>
#include <QtCore/qplugin.h>
#include <QResizeEvent>
#include <QPainter>
#include <vsource/vsource.h>
#include "vwebcamthread.h"

#define WEBCAM_SOURCE "WebCamera"

class VWebCamSource : public VSource   {
    Q_OBJECT
public:
    void resize_src(uint32_t width, uint32_t heigth);
    void start() override;
    void stop() override;
    void resizeEvent(QResizeEvent* event);
    void setUrl(QUrl) {}
    void finalize() override;

    QSharedPointer<VSource> clone() override;
    QPixmap toPixmap(IplImage *Image);

    VWebCamSource(VWebCamSource *p = 0);

    ~VWebCamSource(); 

    void setPosition(VPosition pos);

    VSourceOptions * getOptionsWindow(void);
    void stopCamera(void);
    void setname(QString name) {
        this->name = name;
    }
    static QString static_type;
signals:
	void setSmallGrid(int w,int h,int pix);
private:
    QLabel *imagelabel;
    QVBoxLayout *layout;
    QPixmap pixmap_image;
    QImage image;
    void createOptions(void);
    /* Options window */

protected:
    void paintEvent(QPaintEvent *event);

public slots:
    void saveParams(void);
	void updateImage(void);
};

/* Set static type of source */

class VWebCamCreator : public QObject , public VSourceCreator {
    Q_OBJECT
    Q_INTERFACES(VSourceCreator)
public:
    VWebCamCreator() { }
    ~VWebCamCreator() {}
    QString getSourceType() {
        return QString(WEBCAM_SOURCE);
    }
    QList<VWebCamCreator::Param> getParamsList()
    {
        QList<Param> lst;
//        lst.push_back(Param("location",tr("TCP/IP address or hostname of VNC server")));
//        lst.push_back(Param("port",tr("TCP/IP port of VNC server")));
//        lst.push_back(Param("password",tr("password of VNC server")));
//        lst.push_back(Param("CP_Path",tr("Window Identifier for Juter contoller"),STRING,false));
//        lst.push_back(Param("application",tr("Application source name for Jupiter contoller"),STRING,false));
//        lst.push_back(Param("command",tr("Application executive line for Jupiter controller"),STRING,false));
//        lst.push_back(Param("icon",(":/vnc-server.png"),ICON));
        return lst;
    }

    VSource *createVSource() {
        qDebug("Create VWebCam");
        return (new VWebCamSource());
    }
};










#endif /* VWEBCAMSOURCE_H */

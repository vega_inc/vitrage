#include "vwebcamsource.h"
#include <QElapsedTimer>
#include <QDebug>
#include <QPainter>


CvCapture *camera ;
#define SZ_POLICY QSizePolicy::Preferred

VWebCamSource::VWebCamSource(VWebCamSource *p)
{
    qDebug("VWebCamSource ctor");

    if (!p) { VSourceThread * th = new VWebCamThread();
		th->start();
		setStarted(true);
        setThread(th);
        setStarted(true);
	} else {
        setThread(p->getThread());
        p->copyProperty(m_propMap);
        this->m_images = p->m_images;
        setStarted(p->isStarted());
        setStatus(p->getStatus());
        setEnabledFlag(p->getEnabledFlag());
        setPrototype(p);
        setPhysName(p->getPhysName());
		}
    m_propMap.insert("type",WEBCAM_SOURCE);
    this->type = WEBCAM_SOURCE;
    qDebug("VWebCamSource ctor");
}



void VWebCamSource::start()
{
    setStarted(true);
}


VWebCamSource::~VWebCamSource()
{
    //delete this->thread;
}

void VWebCamSource::resizeEvent(QResizeEvent* event)
{
	qDebug() << event->size().width()<< event->oldSize().width();
	emit this->setThreadSize(event->size(),event->oldSize());
}

QSharedPointer<VSource> VWebCamSource::clone()
{
    QSharedPointer<VSource> web(new VWebCamSource(this));
    return web;
}

void  VWebCamSource::resize_src(uint32_t width,uint32_t height)
{
	resize(width,height);
	emit setThreadSize(QSize(width,height),QSize(0,0));
}


void VWebCamSource::updateImage(void)
{
	image =m_thread->Image(size());
    update();
}


void VWebCamSource::setPosition(VPosition pos)
{
	emit setSmallGrid(pos.rb.x() - pos.lt.x(),pos.rb.y() - pos.lt.y(),2);
	VSource::setPosition(pos);
}


void VWebCamSource::paintEvent(QPaintEvent *event)
{
	QElapsedTimer timer;
	timer.start();
    if (image.isNull() || image.format() == QImage::Format_Invalid) {
        qDebug() << "no valid image to paint";
        QWidget::paintEvent(event);
        return;
    }
    event->accept();
	
    QPainter painter(this);
    painter.drawImage(QPoint(0,0),image);
    QWidget::paintEvent(event);
    paintIcons(&painter);
    paintAll(&painter);
    painter.end();
}


void VWebCamSource::stop()
{
//    if (isStarted()) {
//        delete m_thread;
//        setStarted(false);
//	}
}


void VWebCamSource::finalize()
{
    delete m_thread;
}

Q_EXPORT_PLUGIN2(VWebCamSource, VWebCamCreator)

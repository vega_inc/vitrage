#include "vwebcamthread.h"
#include <QMessageBox>
#include <QMutexLocker>
#include <QPainter>
#include <QElapsedTimer>
#include <QtCore/QCoreApplication>

VWebCamThread::VWebCamThread():
    VSourceThread(),
    w(110),
	h(110)
{
    moveToThread(this);
    timer =new QTimer();
    camera = cvCreateCameraCapture(0) ;
    if (!camera) {
    QMessageBox::critical(0, "", "No any web cameras");
    assert(camera);
    } else {
    cvSetCaptureProperty(camera, CV_CAP_PROP_FRAME_WIDTH, 1240);
    cvSetCaptureProperty(camera, CV_CAP_PROP_FRAME_HEIGHT, 1080);

    connect(timer,SIGNAL(timeout()),this,SLOT(timerSlot()));
    }
}


VWebCamThread::~VWebCamThread()
{
    timer->stop();
    cvReleaseCapture(&this->camera);
  //  exit();
    terminate();
    usleep(1000);
}

void VWebCamThread::run() 
{
    timer->start(100);
	exec();
}


void VWebCamThread::timerSlot(void)
{
    QMutexLocker locker(&m_mutex);
	QElapsedTimer timer_l;
	timer_l.start();
	timer->stop();
    assert(camera);
    IplImage *image = NULL;
    image = cvQueryFrame(camera);

    if (image) {
    setImage(image);
    emit updatedImage();
    }
    timer->start(140);
}



void VWebCamThread::setImage(IplImage *Ipimage)
{
	int height = Ipimage->height;
	int width = Ipimage->width;
	QElapsedTimer timer_l;
	timer_l.start();

	QImage image = QImage((uchar*)Ipimage->imageData,width,
            height, QImage::Format_RGB888).scaled(1280,1024,
            Qt::IgnoreAspectRatio,Qt::SmoothTransformation);

	this->setImages(image);
}


//void VWebCamThread::stop(void)
//{

//}

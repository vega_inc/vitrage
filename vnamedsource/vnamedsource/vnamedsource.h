#ifndef VNAMEDSOURCE_H
#define VNAMEDSOURCE_H

#include "../vsource/vsource.h"

const char NAMED_SOURCE[] = "NamedSource";

class VNamedSource : public VSource {
    Q_OBJECT
public:
    VNamedSource(QWidget *, QUrl &url);
    VNamedSource();
    VNamedSource(VNamedSource &src);

    void startQuitting();
    QSize framebufferSize();

    void resize_src(uint32_t width, uint32_t height) final;
    void start() final {}
    void stop() final {}
    QSharedPointer<VSource> clone() final;
    void finalize() final {}
    void setUrl(QUrl r){Q_UNUSED(r)}

public slots:
    void scaleResize(int w, int h);

signals: 
    void setFrameSize(QSize);

protected:
    void paintEvent(QPaintEvent *);

private:
    QString m_fileName = "./config/backImage.jpg";
    bool m_initDone;
    int m_buttonMask;
    QMap<unsigned int, bool> m_mods;
    int m_x, m_y, m_w, m_h;
    bool m_repaint;
    bool m_quitFlag;
    bool m_firstPasswordTry;
    bool m_dontSendClipboard;
    qreal m_horizontalFactor;
    qreal m_verticalFactor;

    QImage m_frame;

    bool isQuitting();
    void loadImage();

private slots:
    void outputErrorMessage(const QString &message);
};


class VFileCreator : public QObject , public VSourceCreator {
    Q_OBJECT
    Q_INTERFACES(VSourceCreator)
public:

    VFileCreator();
    ~VFileCreator();
    QString getSourceType() final;
    VSource *createVSource() final;
    QList<VFileCreator::Param> getParamsList() final;
};

#endif  /* VNAMEDSOURCE_H */

include(../../common.pri)
TEMPLATE = lib

CONFIG += dll \
    plugin

TARGET = vnamedsource


DESTDIR = ../$$INSTALL_PATH_PLUGINS

INCLUDEPATH += ../$$INSTALL_PATH_INCLUDE \
                          /usr/include/

LIBS += -L../$${INSTALL_PATH_LIB} \
    -lvsource \


# Input
HEADERS += vnamedsource.h
SOURCES += vnamedsource.cpp

unix {
       target.path = /$(DESTDIR)
       INSTALLS += target
     }


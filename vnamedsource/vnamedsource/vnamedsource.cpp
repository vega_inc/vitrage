#include "vnamedsource.h"
#include <QPainter>
#include <QPaintEvent>
#include <QImageReader>
#include <QDebug>

VNamedSource::VNamedSource(QWidget *, QUrl &url)
{
    host = url.host();
    setAutoFillBackground(false);
    loadImage();
    setMouseTracking(true);
    pressed = false;
    m_propMap.insert(F1000_TYPE_PROP,VSource::SubSystemKind_RGBCapture);
    setStatus(VSource::ExecStatus::None);
}

VNamedSource::VNamedSource()
{
    setAutoFillBackground(true);
    loadImage();
    setMouseTracking(true);
    pressed = false;
    type = NAMED_SOURCE;
    setStatus(VSource::ExecStatus::None);
    setSelected(false);
}

VNamedSource::VNamedSource( VNamedSource &src)
    : VSource(src)
{
    setAutoFillBackground(true);
    loadImage();
    setMouseTracking(true);
    pressed = false;
    m_propMap = src.m_propMap;
//    setVisible(src.visualise());
    setVisualise(src.visualise());
    m_propMap.insert(F1000_TYPE_PROP,VSource::SubSystemKind_RGBCapture);
    type = src.gettype();
    setStarted(src.isStarted());
    setStatus(src.getStatus());
    setSelected(src.getSelected());
    setEnabledFlag(src.getEnabledFlag());
    setPrototype(&src);
    setPhysName(src.getPhysName());
}

QSize VNamedSource::framebufferSize()
{
    return m_frame.size();
}

void VNamedSource::startQuitting()
{
    m_quitFlag = true;
}

bool VNamedSource::isQuitting()
{
    return m_quitFlag;
}

void VNamedSource::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    const int w = width();
    const int h = height();
    QRect rect(0, 0, w, h);

    if (m_frame.format() == QImage::Format_Invalid) {
        paintAll(&painter);
        painter.fillRect(rect, QBrush(Qt::gray));
        QVariant var;
        getProperty("icon",var);
        QPixmap  px;
        px.loadFromData(var.toByteArray(),"PNG");
        painter.drawPixmap(10,10,42,42,px);
        paintIcons(&painter);
        painter.drawText(w/3,h/3,clone_name);
        return;
    }

    painter.drawImage(rect, m_frame);
    QBrush br = painter.brush();
    br.setColor(Qt::white);
    painter.setBrush(br);
    painter.fillRect(rect, QBrush(Qt::gray));
    QVariant var;
    getProperty("icon",var);
    QPixmap px;
    px.loadFromData(var.toByteArray(),"PNG");
    painter.drawPixmap(0,0,32,32,px);
    paintIcons(&painter);
    paintAll(&painter);
    painter.setFont(QFont("OpenSans Mono", 14));
    painter.setPen(Qt::white);
    painter.drawText(QRect(5,5,w,h), Qt::AlignLeft | Qt::AlignTop, clone_name);

}

void VNamedSource::resize_src(uint32_t width, uint32_t height)
{
    m_verticalFactor = (qreal) height / m_frame.height();
    m_horizontalFactor = (qreal) width / m_frame.width();
}

void VNamedSource::loadImage()
{
    QVariant var;

    if(getProperty("linuxhost", var)) {
        //        qDebug() <<  QImageReader::supportedImageFormats();
    }
    if(m_frame.isNull()) {
        bool ok = m_frame.load(m_fileName);
        if(m_frame.isNull() && !ok) {
            QImage dummy(1024,1080,QImage::Format_RGB32);
            m_frame = dummy;
        }
    }
}

void VNamedSource::outputErrorMessage(const QString &str)
{
    qDebug() << str;
}

QSharedPointer<VSource> VNamedSource::clone(void)
{
    return (QSharedPointer<VSource>(new VNamedSource(*this)));
}

VFileCreator::VFileCreator()
{
}

VFileCreator::~VFileCreator()
{
}

QList<VFileCreator::Param> VFileCreator::getParamsList()
{
    QList<Param> lst;
    lst.push_back(Param("named",tr("Named source name for Jupiter contoller"),TYPE::STRING));
    lst.push_back(Param("linuxhost",tr("Static picture for source view"),TYPE::STRING,false));
    lst.push_back(Param("v4lpath",tr("Path to v4l device  (for Vega contorller)"),TYPE::STRING,false));
    lst.push_back(Param("v4lformat",tr("Color format (for Vega controller)"),TYPE::STRING,false));
    lst.push_back(Param("icon",(":/dvi_port.png"),TYPE::ICON));
    return lst;
}

QString VFileCreator::getSourceType()
{
    return NAMED_SOURCE;
}

VSource *VFileCreator::createVSource()
{
    return (new VNamedSource());
}

Q_EXPORT_PLUGIN2(VNamedSource,VFileCreator)

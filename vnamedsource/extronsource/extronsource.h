#ifndef EXTRONSOURCE_H
#define EXTRONSOURCE_H

#include "vnamedsource.h"
#include "vsource/vsource.h"
#define Extron_SOURCE "ExtronInput"

class VExtronSource : public VNamedSource {
public :
    VExtronSource() {
        type = Extron_SOURCE;
         m_propMap.insert(F1000_TYPE_PROP,VSource::SubSystemKind_RGBCapture);
    }
};

class VExtronCreator : public QObject , public VSourceCreator {
    Q_OBJECT
    Q_INTERFACES(VSourceCreator)
public:

    VExtronCreator();
    ~VExtronCreator();
    QString getSourceType();
    VSource *createVSource();
    virtual QList<struct VExtronCreator::Param> getParamsList();
};







#endif  /* VNAMEDSOURCE_H */

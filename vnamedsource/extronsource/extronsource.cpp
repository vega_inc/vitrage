#include "extronsource.h"
#include <QPushButton>
#include <QPaintEvent>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QElapsedTimer>
#include <QPainter>
#include <qplugin.h>
#include <QDebug>
#include <QPaintEvent>


VExtronCreator::VExtronCreator()
{

}

VExtronCreator::~VExtronCreator()
{
}


QString VExtronCreator::getSourceType()
{
    return QString(Extron_SOURCE);
}

VSource *VExtronCreator::createVSource()
{
    return (new VExtronSource());
}

QList<VExtronCreator::Param> VExtronCreator::getParamsList()
{
    QList<Param> lst;
    lst.push_back(Param("ExtronInput",tr("Input Number for Extron")));
//    lst.push_back(Param("port",tr("TCP/IP port of VNC server")));
//    lst.push_back(Param("password",tr("password of VNC server")));
    lst.push_back(Param("icon",(":/vnc-server.png"),ICON));
    return lst;
}

Q_EXPORT_PLUGIN2(VExtronSource,VExtronCreator)

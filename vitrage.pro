TEMPLATE = subdirs

SUBDIRS += \
    vsource \
    vcontroller \
    extron \
    vnamedsource \
    vvncsource \
    vfilesource \
    vappsource \
    vitrage \
    fc1000 \
    vega \
    datapath \
    tests \
    #vwebpagesource \
    #vipcamsource \
    #vfilesource \
    #vwebsource

vitrage.depends = vsource
vitrage.depends = vcontroller

vappsource.depends = vsource
vfilesource.depends = vsource
vnamedsource.depends = vsource
vvncsource.depends = vsource

datapath.depends = vcontroller

tests.depends = vsource
tests.depends = vcontroller

#!/bin/bash

ARCH="alt7-64"
PROJECT="vitrage"

#set tarball name 
#TAR_NAME=`date +"$PROJECT"_%Y_%m_%d_%H_%M`
TAR_NAME="vitrage_devel"

# enter to the bin dir
cd bin

# tar only needed files

tar zcf "$TAR_NAME.tar.gz" ./*

mv "$TAR_NAME.tar.gz" ~/share/bark/projects/$PROJECT/binary/$ARCH/

cd ~/share/bark/projects/$PROJECT/binary/$ARCH/

mv -f "$TAR_NAME".tar.gz "$PROJECT"_devel.tar.gz

cd -

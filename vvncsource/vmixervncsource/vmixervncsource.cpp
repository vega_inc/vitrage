#include "vmixervncsource.h"
#include <QPushButton>
#include <QPaintEvent>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QElapsedTimer>
#include <QPainter>
#include <QPaintEvent>

VMixerVNCCreator::VMixerVNCCreator()
{
}

VMixerVNCCreator::~VMixerVNCCreator()
{
}

QString VMixerVNCCreator::getSourceType()
{
    return QString(VNCMIXER_SOURCE);
}

VSource *VMixerVNCCreator::createVSource()
{
    return (new VMixerVNCSource());
}

QList<VMixerVNCCreator::Param> VMixerVNCCreator::getParamsList()
{
    QList<Param> lst;
    lst.push_back(Param("location",tr("TCP/IP address or hostname of VNC server")));
    lst.push_back(Param("port",tr("TCP/IP port of VNC server")));
    lst.push_back(Param("password",tr("password of VNC server"),STRING,false));
    lst.push_back(Param("icon",(":/vnc-server.png"),ICON));
    return lst;
}

Q_EXPORT_PLUGIN2(VMixerVNCSource,VMixerVNCCreator)


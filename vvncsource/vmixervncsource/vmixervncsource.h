#ifndef VMIXERVNCSOURCE_H
#define VMIXERVNCSOURCE_H

#include "vvncsource.h"

#define VNCMIXER_SOURCE "MixerVNCClient"

class VMixerVNCSource : public VVNCSource {
public:
    VMixerVNCSource() {
        type = VNCMIXER_SOURCE;
        m_propMap.insert(F1000_TYPE_PROP,VSource::SubSystemKind_RGBCapture);
    }
};

class VMixerVNCCreator : public QObject , public VSourceCreator {
    Q_OBJECT
    Q_INTERFACES(VSourceCreator)
public:
	
    VMixerVNCCreator();
    ~VMixerVNCCreator();
    QString getSourceType();
    VSource *createVSource();
    virtual QList<struct VMixerVNCCreator::Param> getParamsList();
};

#endif  /* VMIXERVNCSOURCE_H */

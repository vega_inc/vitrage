include(../../common.pri)
TEMPLATE = lib


CONFIG += dll \
    plugin
CONFIG += debug
TARGET = vmixervncsource

DESTDIR = ../$$INSTALL_PATH_PLUGINS

INCLUDEPATH += ../$$INSTALL_PATH_INCLUDE \
                /usr/include/ \
                ../../../include/ \
                ../vvncsource/
message($$INCLUDEPATH)
LIBS += -L../$${INSTALL_PATH_LIB} \
    -L../$${INSTALL_PATH_PLUGINS}     \
    -lvsource \
    -lvncclient \
    -lvvncsource

# Input
HEADERS += vmixervncsource.h
SOURCES += vmixervncsource.cpp

unix {
       target.path = /$(DESTDIR)
       INSTALLS += target
     }

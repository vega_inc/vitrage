#ifndef VMIXERVNCSOURCE_H
#define VMIXERVNCSOURCE_H

#include "vvncsource.h"

#define VNCMIXEREXTRON_SOURCE "MixerExtronVNCClient"

class VMixerExtronVNCSource : public VVNCSource {
public:
    VMixerExtronVNCSource() {
        type = VNCMIXEREXTRON_SOURCE;
        m_propMap.insert(F1000_TYPE_PROP,VSource::SubSystemKind_RGBCapture);
    }
};

class VMixerExtronVNCCreator : public QObject , public VSourceCreator {
    Q_OBJECT
    Q_INTERFACES(VSourceCreator)
public:
	
    VMixerExtronVNCCreator();
    ~VMixerExtronVNCCreator();
    QString getSourceType();
    VSource *createVSource();
    virtual QList<struct VMixerExtronVNCCreator::Param> getParamsList();
};

#endif  /* VMIXERVNCSOURCE_H */

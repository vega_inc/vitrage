#include "vmixerExtronVncsource.h"
#include <QPushButton>
#include <QPaintEvent>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QElapsedTimer>
#include <QPainter>
#include <QPaintEvent>


VMixerExtronVNCCreator::VMixerExtronVNCCreator()
{
}

VMixerExtronVNCCreator::~VMixerExtronVNCCreator()
{
}


QString VMixerExtronVNCCreator::getSourceType()
{
    return QString(VNCMIXEREXTRON_SOURCE);
}

VSource *VMixerExtronVNCCreator::createVSource()
{
    return (new VMixerExtronVNCSource());
}

QList<VMixerExtronVNCCreator::Param> VMixerExtronVNCCreator::getParamsList()
{
    QList<Param> lst;
    lst.push_back(Param("location",tr("TCP/IP address or hostname of VNC server")));
    lst.push_back(Param("port",tr("TCP/IP port of VNC server")));
    lst.push_back(Param("password",tr("password of VNC server"),STRING,false));
    lst.push_back(Param("icon",(":/vnc-server.png"),ICON));
    return lst;
}

Q_EXPORT_PLUGIN2(VMixerExtronVNCSource,VMixerExtronVNCCreator)


######################################################################
# Automatically generated by qmake (2.01a) ?? ???. 13 15:20:00 2012
######################################################################
include(../../common.pri)
TEMPLATE = lib


CONFIG += dll \
    plugin \
    debug

TARGET = vmixerextronvncsource

DESTDIR = ../$$INSTALL_PATH_PLUGINS

INCLUDEPATH += ../$$INSTALL_PATH_INCLUDE \
                /usr/include/ \
                ../../../include/ \
                ../vvncsource/
message($$INCLUDEPATH)
LIBS += -L../$${INSTALL_PATH_LIB} \
    -L../$${INSTALL_PATH_PLUGINS}     \
    -lvsource \
    -lvncclient \
    -lvvncsource
# Input
HEADERS += vmixerExtronVncsource.h
SOURCES += vmixerExtronVncsource.cpp

unix {
       target.path = /$(DESTDIR)
       INSTALLS += target
     }


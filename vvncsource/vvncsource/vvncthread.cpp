#include "vvncthread.h"

#include <QMutexLocker>
#include <QTimer>
#include <QTcpSocket>

static QString outputErrorMessageString;
#define TIMER_l 200

//#define  INTEL_AMT_KVM_STRING

QVector<QRgb> VVNCThread::m_colorTable;


void VVNCThread::setClientColorDepth(rfbClient* cl, VVNCThread::ColorDepth cd)
{
    switch (cd) {
    case bpp8:
        if (m_colorTable.isEmpty()) {
            m_colorTable.resize(256);
            int r,g,b;
            for (int i = 0; i < 256; ++i) {
                r= (i & 0x07) << 5;
                g= (i & 0x38) << 2;
                b= i & 0xc0;
                m_colorTable[i] = qRgb(r, g, b);
            }
        }
        cl->format.depth = 8;
        cl->format.bitsPerPixel = 8;
        cl->format.redShift = 0;
        cl->format.greenShift = 3;
        cl->format.blueShift = 6;
        cl->format.redMax = 7;
        cl->format.greenMax = 7;
        cl->format.blueMax = 3;
        break;
    case bpp16:
        cl->format.depth = 16;
        cl->format.bitsPerPixel = 16;
        cl->format.redShift = 11;
        cl->format.greenShift = 5;
        cl->format.blueShift = 0;
        cl->format.redMax = 0x1f;
        cl->format.greenMax = 0x3f;
        cl->format.blueMax = 0x1f;
        break;
    case bpp32:
    default:
        cl->format.depth = 24;
        cl->format.bitsPerPixel = 32;
        cl->format.redShift = 16;
        cl->format.greenShift = 8;
        cl->format.blueShift = 0;
        cl->format.redMax = 0xff;
        cl->format.greenMax = 0xff;
        cl->format.blueMax = 0xff;
    }
}


rfbBool VVNCThread::newclient(rfbClient *ncl)
{
    qDebug() << "new client";
    VVNCThread *t = (VVNCThread*)rfbClientGetClientData(ncl, 0);
    Q_ASSERT(t);
    setClientColorDepth(ncl, t->colorDepth());
    //isConn = true;
    const int width = ncl->width, height = ncl->height,
            depth = ncl->format.bitsPerPixel;
    int size = width * height * (depth / 8);
    //size = size*100;
    if (t->frameBuffer)
        delete [] t->frameBuffer;
    t->frameBuffer = new uint8_t[size];
    ncl->frameBuffer = t->frameBuffer;
    memset(ncl->frameBuffer, '\0', size);
    switch (t->quality()) {
    case High:
        ncl->appData.encodingsString = "tight zrle ultra copyrect";
        //   ncl->appData.encodingsString = NULL;
        ncl->appData.compressLevel = 7;
        ncl->appData.qualityLevel = 3;
        break;
    case Medium:
        ncl->appData.encodingsString = "tight zrle ultra copyrect";
        //   ncl->appData.encodingsString = NULL;
        ncl->appData.compressLevel = 5;
        ncl->appData.qualityLevel = 7;
        break;
    case Low:
    case Unknown:
    default:
        ncl->appData.encodingsString = "tight zrle ultra copyrect";
        //  ncl->appData.encodingsString = NULL;
        ncl->appData.compressLevel = 9;
        ncl->appData.qualityLevel = 1;
    }

    if (ncl)    SetFormatAndEncodings(ncl);
    //    if (ncl)    SetFormatAndEncodings(t);
    // qDebug(5011) << "Client created";
    return true;
}

void VVNCThread::updateFB(rfbClient *cl,int x,int y,int w,int h)
{
    Q_UNUSED(x);
    Q_UNUSED(y);
    Q_UNUSED(w);
    Q_UNUSED(h);
    static QMutex mtx;
    QMutexLocker  m(&mtx);

    VVNCThread *t = (VVNCThread*)rfbClientGetClientData(cl, 0);
    if(t->m_timerSh.isActive()) return;
    t->m_timerSh.setSingleShot(true);
    t->m_timerSh.start(1000);

    if (!t->getRenew()) return;
    //  t->buffer_mutex.lock();
    Q_ASSERT(t);
    int width = cl->width;
    int height = cl->height;
    QImage img;
    switch (t->colorDepth()) {
    case bpp8:
        img = QImage((uchar*)cl->frameBuffer, width, height, QImage::Format_Indexed8);
        img.setColorTable(m_colorTable);
        break;
    case bpp16:
        img = QImage((uchar*)cl->frameBuffer, width, height, QImage::Format_RGB16);
        break;
    case bpp32:
        img = QImage((uchar*)cl->frameBuffer, width, height, QImage::Format_RGB32);
        break;
    }
    img = img.scaled(width,height);
    if (img.isNull()) {
        qDebug("image not loaded");
    }
    //t->m_bufferImage = img;

    t->setImages(img);
    t->emitUp();
    t->setRenew(false);
}

void VVNCThread::timerSlt(void)
{
    m_timer->stop();
    m_renuw = true;
    if (mcl) {
        int num = WaitForMessage(mcl,500);
        if (num) {
            bool ret = HandleRFBServerMessage(mcl);
            if(!ret) {
                emit statusChanged(static_cast<int>(VSource::ExecStatus::ErrorStatus));
                isConn = false;
                reconnect();
            }
            setRenew(true);
            m_timer->start(TIMER_l);
            return;
        } else {
            setRenew(false);
            m_timer->start(TIMER_l);
            return;
        }
    }
    if (!isConn && m_reconnect) {
        reconnect();
    }
    emitUp();
    setRenew(true);
    m_timer->start(TIMER_l);
}

void VVNCThread::filnalizeSlot()
{
    //  stop_();
}


void VVNCThread::emitUp()
{
    emit updatedImage();
    m_timer->start(230);
}



char *VVNCThread::passwdHandler(rfbClient *cl)
{
    VVNCThread *t = (VVNCThread*)rfbClientGetClientData(cl, 0);
    Q_ASSERT(t);

    //emit t->passwordRequest();
    //   t->m_passwordError = true;

    return strdup(t->password().toLocal8Bit());
}

void VVNCThread::outputHandler(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    QString message;
    message.vsprintf(format, args);
    va_end(args);
    //qDebug() << Q_FUNC_INFO << "Output Heandler +" << message;
    message = message.trimmed();
    if ((message.contains("Couldn't convert ")) ||
            (message.contains("Unable to connect to VNC server")) ||
            (message.contains("VNC connection failed: Authentication failed, too many tries")) ||
            (message.contains("VNC connection failed: Too many authentication failures")) ||
            (message.contains("VNC connection failed: Authentication failed")) ||
            (message.contains("VNC server closed connection"))) {
        //qDebug() << Q_FUNC_INFO << "Some vnc error!!!";
    }
}

VVNCThread::VVNCThread()
    :VSourceThread()
{
    externalConnect = true;
    clientCounter = 0;
    m_timer = new QTimer(this);
    m_ReconTimer = new QTimer(this);
    delTimer = new QTimer(this);
    initTimer = new QTimer(this);
    m_bufferImage = QImage(800, 600, QImage::Format_Mono);
    m_bufferImage.fill(Qt::red);

    frameBuffer = NULL;
    mcl = NULL;
    connect(m_ReconTimer,SIGNAL(timeout()),this,SLOT(reconnect()));
    connect(initTimer,SIGNAL(timeout()),this,SLOT(initTimed()));
    connect(delTimer,SIGNAL(timeout()),this,SLOT(timedDiscon()));
    connect(m_timer,SIGNAL(timeout()),this,SLOT(timerSlt()));
    connect(this,SIGNAL(startCycleSelfS()),this,SLOT(startCycle()));
    moveToThread(this);
    isConn = false;
    m_reconnect = true;
    exitF = false;
}

VVNCThread::~VVNCThread()
{
    //rfbClientCleanup(mcl);
    qDebug() << "Lock2";
    QMutexLocker m(&m_initialMutex);
    m_timer->stop();
    m_ReconTimer->stop();
    delTimer->stop();
    initTimer->stop();
    m_timerSh.stop();
    wait(100);
    if (isRunning()) {
        //   exit();
        // terminate();
        //  wait(1000);
    }
    if (isConn) {

        delete [] frameBuffer;
        if (mcl) {
            rfbClientCleanup(mcl); }
    }
    delete m_timer;
}

void VVNCThread::checkOutputErrorMessage()
{
    if (!outputErrorMessageString.isEmpty()) {
        QString errorMessage = outputErrorMessageString;
        outputErrorMessageString.clear();
    }
}

void VVNCThread::setHost(const QString &host)
{
    // QMutexLocker locker(&mutex);
    m_host = host;
}

void VVNCThread::setPort(int port)
{
    //  QMutexLocker locker(&mutex);
    m_port = port;
}

void VVNCThread::setQuality(Quality quality)
{
    m_quality = quality;
    switch (quality) {
    case Low:
        setColorDepth(bpp8);
        break;
    case High:
        setColorDepth(bpp32);
        break;
    case Medium:
    default:
        setColorDepth(bpp16);
    }
}

void VVNCThread::setColorDepth(ColorDepth colorDepth)
{
    m_colorDepth= colorDepth;
}

VVNCThread::Quality VVNCThread::quality()
{
    return m_quality;
}

void VVNCThread::initTimed()
{
    checkAndSet();
    initTimer->stop();
    emit initDone();
}

VVNCThread::ColorDepth VVNCThread::colorDepth() const
{
    return m_colorDepth;
}

void VVNCThread::reconnectT()
{
    checkAndSet();
    //    reconnect();
}

void VVNCThread::timedDiscon()
{
    if (clientCounter == 0)  {
        stop_();
    }
}

void VVNCThread::setImage(QImage &img)
{
    m_image =  img;
}

void VVNCThread::run()
{
    exec();
}

void VVNCThread::cuttext(rfbClient* cl, const char *text, int textlen)
{
    Q_UNUSED(cl);
    QString cutText = QString::fromAscii(text, textlen);
    qDebug() << cutText;
}

void VVNCThread::setPassword(QString password)
{
    m_password = password;
}
const QString VVNCThread::password()
{
    return m_password;
}

void VVNCThread::setRenew(bool d)
{
    m_renuw = d;
}
bool VVNCThread::getRenew(void)
{
    return m_renuw;
}

void VVNCThread::startCycleM() {
    emit startCycleSelfS();
}

void VVNCThread::initiateInit()
{
    initTimer->setSingleShot(true);
    initTimer->start(300);
}

void VVNCThread::startCycle() {
    start_();
}

void VVNCThread::checkAndSet() {
    if (isConn)
    {
        emit statusChanged(VSource::ExecStatus::RunStatus);
        return;
    }
    if (checkForVNC())
    {
        emit statusChanged(VSource::ExecStatus::RunStatus);
    }
    else
    {
        emit statusChanged(VSource::ExecStatus::ErrorStatus);
    }
}

bool VVNCThread::checkForVNC()
{
    if (!m_initialMutex.tryLock()) return true;
    QScopedPointer<QTcpSocket>  socket(new QTcpSocket());
    socket->connectToHost(m_host.toUtf8().constData(),m_port,QTcpSocket::ReadOnly);
    //    usleep(100);
    //    if (socket->state() != QAbstractSocket::ConnectingState) {
    //        usleep(30000);
    //    }
    //    QAbstractSocket::SocketState ss = socket->state();
    //    if (socket->state() != QAbstractSocket::ConnectingState) {
    //      return false;
    //    }
    if (!socket->waitForConnected(1000)) {
        m_initialMutex.unlock();
        return false;
    }
//    qDebug() << "Connect from test";
    if (!socket->waitForReadyRead(1000))  {
        socket->disconnectFromHost();
        if (socket->state() == QAbstractSocket::UnconnectedState ||
                socket->waitForDisconnected(1000))
//            qDebug() << "Disconnect from test";
        m_initialMutex.unlock();
        return false;
    }
    QByteArray buffer;
    buffer += socket->readAll();
    QString resp = QString(buffer);
    qDebug() << resp;
    if (!resp.count()){
//        qDebug() << "Empty responce";
        socket->disconnectFromHost();
        if (socket->state() == QAbstractSocket::UnconnectedState ||
                socket->waitForDisconnected(1000))
//            qDebug() << "Disconnect from test";
        m_initialMutex.unlock();
        return false;
    }

    socket->disconnectFromHost();
    if (socket->state() == QAbstractSocket::UnconnectedState ||
            socket->waitForDisconnected(1000))
//        qDebug() << "Disconnect from test";
    if (resp.contains("RFB"))
    {
        m_initialMutex.unlock();
        return true;
    }
    m_initialMutex.unlock();
    return false;

}

void VVNCThread::start_()
{
//    static long long call_counter_start = 0;
//    qDebug() << Q_FUNC_INFO << this << " calls " << ++call_counter_start;
    QMutexLocker  lock(&m_initialMutex);
    mcl = NULL;
    rfbClientErr = outputHandler;
    rfbClientLog = outputHandler;
    mcl = rfbGetClient(8, 3, 4);
    if (!mcl)
    {
    //    m_initialMutex.unlock();
        return;
    }
    setClientColorDepth(mcl, this->colorDepth());
    mcl->MallocFrameBuffer = newclient;
    mcl->canHandleNewFBSize = true;
    mcl->GetPassword = passwdHandler;
    mcl->GotXCutText =cuttext;
    mcl->GetCredential = credentialHandler;
    mcl->authScheme = 0;

    mcl->GotFrameBufferUpdate = updateFB;
    rfbClientSetClientData(mcl, 0, this);
    mcl->serverHost = strdup(m_host.toUtf8().constData());
    if (m_port < 0 || !m_port) // port is invalid or empty...
        m_port = 5900; // fallback: try an often used VNC port
    if (m_port >= 0 && m_port < 100)
        m_port += 5900;
    mcl->serverPort = m_port;
    bool ret =   rfbInitClient(mcl, 0,0);
    if (!ret) {
        mcl = NULL;
        isConn = false;
        emit statusChanged(VSource::ExecStatus::ErrorStatus);
      //  m_initialMutex.unlock();
        if (exitF)
        {
//            qDebug() << "VVNCThread::start_()"<< this<<" RETURN";
            return ;
        }
        m_ReconTimer->setSingleShot(true);
        m_ReconTimer->start(1000);
        //reconnect();

//        qDebug() << "VVNCThread::start_()"<< this<<" RETURN";

        return;
    } else {
        emit statusChanged(VSource::ExecStatus::RunStatus);
        isConn = true;
        m_stopped = false;
        m_timer->start(TIMER_l);
      //  m_initialMutex.unlock();
    }
//    qDebug() << "VVNCThread::start_()"<< this<<" RETURN";
    return;
}

void VVNCThread::stop_()
{
    if (mcl != NULL) {
        delTimer->stop();
        m_timer->stop();
        shutdown(mcl->sock,SHUT_RDWR);
        close(mcl->sock);
        rfbClientCleanup(mcl);
        usleep(20000);
        isConn = false;
        mcl = NULL;
    }
}

void VVNCThread::setSource(VVNCSource *src)
{
    m_src = src;
}

void VVNCThread::reconnect()
{
    static long long call_counter_reconnect = 0;
    qDebug() << "***********************  VVNCThread::reconnect()"<< this<<" calls " << ++call_counter_reconnect;

    if (isConn) {
//        qDebug() << "VVNCThread::reconnect()"<< this<<" RETURN";
        return;
    }
    stop_();
    usleep(2000000);
    start_();

    qDebug() << "VVNCThread::reconnect()"<< this<<" RETURN";

}
void VVNCThread::emitDiscon()
{
    delTimer->setSingleShot(true);
    delTimer->start(3000);
}

rfbCredential *VVNCThread::credentialHandler(rfbClient *cl, int credentialType)
{
    //  qDebug() << "credential request" << credentialType;

    VVNCThread *t = (VVNCThread*)rfbClientGetClientData(cl, 0);
    Q_ASSERT(t);

    rfbCredential *cred = 0;

    switch (credentialType) {
    case rfbCredentialTypeUser:
        // t->passwordRequest(true);
        //    t->m_passwordError = true;

        //     cred = new rfbCredential;
        //      cred->userCredential.username = strdup(t->username().toUtf8());
        //      cred->userCredential.password = strdup(t->password().toUtf8());
        break;
    default:
        qDebug() << "credential request failed, unspported credentialType:" << credentialType;
        qDebug("VNC authentication type is not supported.");
        break;
    }
    return cred;
}

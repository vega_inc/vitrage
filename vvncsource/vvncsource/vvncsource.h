#ifndef VVNCSOURCE_H
#define VVNCSOURCE_H

#include <rfb/rfbclient.h>
#include <vsource/vsource.h>
#include <qurl.h>

#include "vvncthread.h"

#define VNC_SOURCE "VNCClient"

class VVNCSource : public VSource {
    Q_OBJECT
public:

    VVNCSource(VVNCSource *src = NULL);
    ~VVNCSource();

	Q_ENUMS(RemoteStatus)

    enum RemoteStatus {
        Connecting     = 0,
        Authenticating = 1,
        Preparing      = 2,
        Connected      = 3,
        Disconnecting  = -1,
        Disconnected   = -2
    };

    Q_ENUMS(ErrorCode)

    enum ErrorCode {
        None = 0,
        Internal,
        Connection,
        Protocol,
        IO,
        Name,
        NoServer,
        ServerBlocked,
        Authentication
    };
	void resize_src(uint32_t width, uint32_t height);
    void startQuitting();
    QSize framebufferSize();
    void resizeEvent(QResizeEvent* event);
    void finalize();
    void start();
    void stop(){}
    void setUrl(QUrl u){url = u;}
    bool srcConnected();
    void connectSrc(bool c);
    QSharedPointer<VSource> clone();
    VVNCThread *m_vncthread;

public slots:
    void scaleResize(int w, int h);
    void updateImage(void);
    void recon();

signals: 
    void startCycleS();
    void reconnectTo();
	void setFrameSize(QSize);
    void timedDisconS();
    void startInitTh();
    void finalizeSignal();
    void startSignal();
    void stopSignal();

protected:
    void paintEvent(QPaintEvent *event);

protected:
    QRegion reg;
    QTimer* m_recon;
    QImage image;
	QUrl url;
    bool m_initDone;
    int m_buttonMask;
    QMap<unsigned int, bool> m_mods;
    int m_x, m_y, m_w, m_h;
    bool m_repaint;
    bool m_quitFlag;
    bool m_firstPasswordTry;
    bool m_dontSendClipboard;
    bool finalized;
    qreal m_horizontalFactor;
    qreal m_verticalFactor;
    QImage m_frame;
    RemoteStatus m_status;
    bool isQuitting();

private slots:
    void updateImage(int x, int y, int w, int h);
    void setCut(const QString &text);
    void requestPassword();
    void outputErrorMessage(const QString &message);
    void initDoneSlot();
};

class VVNCCreator : public QObject , public VSourceCreator {
    Q_OBJECT
    Q_INTERFACES(VSourceCreator)
public:
	
    VVNCCreator();
    ~VVNCCreator();
    QString getSourceType(); 
    VSource *createVSource(); 
    virtual QList<VVNCCreator::Param> getParamsList();
};

#endif  /* VVNCSOURCE_H */

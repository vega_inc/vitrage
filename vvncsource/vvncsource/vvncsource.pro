include(../../common.pri)
TEMPLATE = lib
CONFIG += dll \
            plugin
QT += network
CONFIG += debug
TARGET = vvncsource
DESTDIR = ../$$INSTALL_PATH_PLUGINS
INCLUDEPATH += ../$$INSTALL_PATH_INCLUDE \
		/usr/include/
message($$INCLUDEPATH)
LIBS += -L../$${INSTALL_PATH_LIB} \
    -lvsource \
    -lvncclient

# Input
HEADERS += vvncsource.h vvncthread.h
SOURCES += vvncsource.cpp vvncthread.cpp

unix {
       target.path = /$(DESTDIR)
       INSTALLS += target
     }

#ifndef VVNCTHREAD_H
#define VVNCTHREAD_H

#include <QImage>
#include <QVBoxLayout>
#include <QtCore/qplugin.h>
#include <QObject>
#include <QResizeEvent>
#include <QSize>
#include <QMutex>
#include <QTimer>

//extern "C" {
#include <rfb/rfbclient.h>
//}

#include <vsource/vsource.h>
#include <vsource/vsourcethread.h>

class VVNCSource;

class VVNCThread : public VSourceThread {
    Q_OBJECT
public:
	
    Q_ENUMS(Quality)
    enum Quality_ {
        Unknown,
        High,
        Medium,
        Low,
    };
	typedef enum Quality_ Quality;
    Q_ENUMS(ColorDepth)
	enum  ColorDepth {
		bpp32,
		bpp16,
		bpp8
	};

   VVNCThread();
   ~VVNCThread();

    const QImage image(int x = 0, int y = 0, int w = 0, int h = 0);
    void setImage( QImage &img);
    void setHost(const QString &host);
    void setPort(int port);
    void setQuality(Quality quality);
    void emitUp(void);
    void setPassword(QString password);
    const QString password() ;
    void setRenew(bool d) ;
    bool getRenew(void);
    void setExit(bool b) {
        exitF = b;
    }

    void setSource(VVNCSource *src);

    void emitDiscon();
    bool isConn;
    ColorDepth colorDepth() const;
    uint8_t *frameBuffer;
    int clientCounter;
    QImage m_bufferImage;
    bool checkForVNC();
    void checkAndSet();
    void startCycleM();

signals:
    void initDone();
    void disconS();
    void startCycleSelfS();
    void passwordRequest();
 public slots:
    void reconnect();
    void reconnectT();
    void mouseEvent(int x, int y, int buttonMask);
    void keyEvent(int key, bool pressed);
    void clientCut(const QString &text);
	void setFrmSize(QSize size);
    void timedDiscon();
    void startCycle();
    void initiateInit();
    void start_();
    void stop_();

protected:
    void run();

private:
    static void setClientColorDepth(rfbClient *cl, ColorDepth cd);
    void setColorDepth(ColorDepth colorDepth);
    //these static methods are callback functions for libvncclient
    static rfbBool newclient(rfbClient *cl);
    static char* passwdHandler(rfbClient *cl);
    static void outputHandler(const char *format, ...);
    static void updateFB(rfbClient *cl,int x,int y,int w,int h);
    static  void cuttext(rfbClient* cl, const char *text, int textlen);
    static rfbCredential *credentialHandler(rfbClient *cl, int credentialType);
    static bool getFErr(void) { return f_err; }

    Quality quality();
    static bool flag;
    QTimer *delTimer;
    QTimer *initTimer;
    static QTimer static_timer;
    QImage m_image;
    QTimer *m_timer;
    QTimer *m_ReconTimer;
    VVNCSource *m_src;
    rfbClient *mcl;
    QString m_host;
    QTimer m_timerSh;
    QString m_password;
    bool m_renuw;
    bool externalConnect;
    bool exitF;
    int m_port;
    QMutex buffer_mutex;
    QMutex m_initialMutex;

    Quality m_quality;
    ColorDepth m_colorDepth;
	QSize m_frame_size;
	qreal hor_factor;
	qreal ver_factor;
  //  color table for 8bit indexed colors
    static QVector<QRgb> m_colorTable;
    static bool f_err;
    volatile bool m_stopped;
    volatile bool m_reconnect;
    volatile bool m_passwordError;

private slots:
    void initTimed();
    void checkOutputErrorMessage();
    void timerSlt(void);
	void newImageTimeOut();
	void ImageUpdated(QImage *img);
    void filnalizeSlot();
};

#endif  /* VVNCTHREAD_H */

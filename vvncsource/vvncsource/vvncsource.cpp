#include "vvncsource.h"
#include "vvncthread.h"
#include <QPushButton>
#include <QPaintEvent>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QElapsedTimer>
#include <QCoreApplication>
#include <QPainter>
#include <QPaintEvent>

VVNCSource::~VVNCSource()
{
    if (isStarted()) {
        setStarted(false);
    }
    if (m_vncthread) {
        m_vncthread->clientCounter--;
        if (!m_vncthread->clientCounter) {
            if (!m_vncthread->clientCounter) {
                m_vncthread->emitDiscon();
            }
        }
    }
}

QSize VVNCSource::framebufferSize()
{
    return m_frame.size();
}

void VVNCSource::startQuitting()
{
    m_quitFlag = true;

    disconnect(m_thread, SIGNAL(passwordRequest()), this, SLOT(requestPassword()));
    m_thread->quit();
    m_thread->wait(1000);
}

bool VVNCSource::isQuitting()
{
    return m_quitFlag;
}

void VVNCSource::start()
{
    finalized = false;
    VVNCThread *th = m_vncthread;
    QVariant location;
    QVariant port;
    QVariant password;

    getProperty(QString("location"),location);
    getProperty(QString("port"),port);
    getProperty(QString("password"),password);
    QString prt = port.toString();
    url = QUrl(location.toString());
	
    th->setHost(url.path());
    th->setPort(prt.toInt());
    th->setPassword(password.toString());
    VVNCThread::Quality quality;
    quality = VVNCThread::High;
    th->setQuality(quality);
    if (!th->isRunning()) {
          th->start();
        emit startInitTh();
    } else {
        if (!th->isConn)
            th->startCycleM();
    }
}

bool VVNCSource::srcConnected()
{
    if (!m_vncthread) return false;
    return m_vncthread->isConn;
}

void VVNCSource::connectSrc(bool c)
{
    if (!m_vncthread) return;
    if(c) {
        emit startSignal();
    } else {
        emit stopSignal();
    }
}

void VVNCSource::requestPassword()
{

////    setStatus(Authenticating);
//	VVNCThread *th = static_cast<VVNCThread *>(m_thread);
//    if (m_firstPasswordTry && !url.password().isNull()) {
//      //  th->setPassword("1234");
//        //th->setPassword(url.password());
//        m_firstPasswordTry = false;(
//        return;
//    m_firstPasswordTry = false;
    //    }
    //   // QString password  = QString("VikingVega");
//    //th->setPassword(password);
    //startQuitting();
}

void VVNCSource::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    QSize  sz = size();
    QImage im = m_thread->Image(sz);
    auto w = width();
    auto h = height();

    if (!im.isNull()) {
        painter.drawImage(QPoint(0,0), im.scaled(w,h,Qt::IgnoreAspectRatio,Qt::FastTransformation));
    }
    paintIcons(&painter);
    paintAll(&painter);
    painter.setFont(QFont("OpenSans Mono", 14));
    painter.setPen(Qt::white);
    painter.drawText(QRect(5,5,w,h), Qt::AlignLeft | Qt::AlignTop, clone_name);
    event->accept();
}

void VVNCSource::resizeEvent(QResizeEvent *event)
{
    emit this->setThreadSize(event->size(),event->oldSize());
}

void VVNCSource::resize_src(uint32_t w, uint32_t h)
{
	resize(w,h);
	setThreadSize(QSize(w,h),QSize(0,0));
}

void VVNCSource::updateImage()
{
    update();
}

void VVNCSource::recon()
{
    m_recon->stop();
    emit reconnectTo();
    int  t = 6000 + ((float)qrand()/(float)RAND_MAX)*3000;
    m_recon->start(t);
}

void VVNCSource::outputErrorMessage(const QString &str)
{
    Q_UNUSED(str);
    //qDebug() << str;
}

void VVNCSource::initDoneSlot()
{
    m_recon->start(3000);
    emit inited();
}

QSharedPointer<VSource> VVNCSource::clone(void)
{
    return (QSharedPointer<VSource>(new VVNCSource(this)));
}

VVNCSource::VVNCSource(VVNCSource *src) :VSource(NULL)
{
    if (src) {
        setThread(src->getThread());
        src->copyProperty(m_propMap);
        this->m_images = src->m_images;
        setStarted(src->isStarted());
        setStatus(src->getStatus());
        setEnabledFlag(src->getEnabledFlag());
        setPrototype(src);
        type = src->gettype();
        setPhysName(src->getPhysName());
        m_vncthread  = src->m_vncthread;
        m_vncthread->clientCounter++;
    } else {
        VVNCThread *th = new VVNCThread;
        setThread(th);
        m_recon = new QTimer(this);
        connect(this,SIGNAL(reconnectTo()),th,SLOT(reconnectT()));
        connect(this,SIGNAL(timedDisconS()),th,SLOT(timedDiscon()));
        connect(this,SIGNAL(startInitTh()),th,SLOT(initiateInit()));
        connect(this,SIGNAL(finalizeSignal()),th,SLOT(filnalizeSlot()));
        connect(m_recon,SIGNAL(timeout()),this,SLOT(recon()));
        connect(this,SIGNAL(startSignal()),th,SLOT(start_()));
        connect(this,SIGNAL(stopSignal()),th,SLOT(stop_()));
        connect(th,SIGNAL(initDone()),this,SLOT(initDoneSlot()));

        th->setSource(this);
        m_vncthread = th;
    }
    connect(m_thread, SIGNAL(passwordRequest()), this, SLOT(requestPassword()), Qt::BlockingQueuedConnection);
    if (!src) {
        type = "VNCClient";
        m_propMap.insert(F1000_TYPE_PROP,VSource::SubSystemKind_SystemWindow);
        setStatus(ExecStatus::StopStatus);
    }
    finalized = false;
}

VVNCCreator::VVNCCreator()
{
}

VVNCCreator::~VVNCCreator()
{
}

QString VVNCCreator::getSourceType()
{
    return VNC_SOURCE;
}

VSource *VVNCCreator::createVSource()
{
    return (new VVNCSource());
}

QList<VVNCCreator::Param> VVNCCreator::getParamsList()
{
    QList<Param> lst;
    lst.push_back(Param("location",tr("TCP/IP address or hostname of VNC server")));
    lst.push_back(Param("port",tr("TCP/IP port of VNC server")));
    lst.push_back(Param("password",tr("password of VNC server"),TYPE::STRING,false));
    lst.push_back(Param("CP_Path",tr("Window Identifier for Juter contoller"),TYPE::STRING,false));
    lst.push_back(Param("application",tr("Application source name for Jupiter contoller"),TYPE::STRING,false));
    lst.push_back(Param("command",tr("Application executive line for Jupiter controller"),TYPE::STRING,false));
    lst.push_back(Param("icon",(":/vnc-server.png"),TYPE::ICON));
    return lst;
}

void VVNCSource::finalize()
{
    disconnect(this);
    this->blockSignals(true);
    m_thread->blockSignals(true);
    QCoreApplication::removePostedEvents(this);
    if (m_thread)
        QCoreApplication::removePostedEvents(m_thread);
    if (m_thread){
        m_vncthread->setExit(true);
        m_vncthread->quit();
        disconnect(m_thread);
        if (!m_vncthread->wait(50000)) {
            qDebug("Thread is alive");
            return;
        }
        disconnect(m_thread);
        m_vncthread->exit();;
        /*delete */m_vncthread->deleteLater();
        m_thread = NULL;
        m_vncthread = NULL;
        finalized = true;
    }
}

Q_EXPORT_PLUGIN2(VVNCSource,VVNCCreator)

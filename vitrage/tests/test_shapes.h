#ifndef TESTSSHAPES_H
#define TESTSSHAPES_H

#include <QtTest>
#include "controlleroptions.h"

class TestsShapes : public QObject
{
    Q_OBJECT
private:
    VController *m_controller;
private slots:
    void initTestCase();
    void addText();
    void addArrows();
    void addOrderArrows();
    void addRect();
    void addComplexData();
    void addVSourceWithShapes();
    void cleanupTestCase();
};

#endif // TESTSSHAPES_H

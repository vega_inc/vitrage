#ifndef TEST_VITRAGE_MARKERS
#define TEST_VITRAGE_MARKERS

#include <QtTest>
#include "vcontroller/vcontroller.h"
#include "vsource/vsource.h"

class TestsVitrageMarkers : public QObject
{
    Q_OBJECT

    void fillSourceProperty(VSource *source, const QString &name);

public:
    VController *m_controller;
    QList<QSharedPointer<VSource>> m_sources;

private slots:
    void initTestCase();
    void markersTestCase();
    void cleanupTestCase();
};

#endif // TEST_VITRAGE_MARKERS


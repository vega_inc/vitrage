#include <QtTest/QTest>

#include "test_vitrage.h"
#include "test_shapes.h"
#include "test_vitrage_markers.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    TestsVitrage test;
    TestsShapes testShapes;
    TestsVitrageMarkers testMarkers;

    return QTest::qExec(&testShapes, argc, argv);// ||
           // QTest::qExec(&testMarkers);
            //QTest::qExec(&test, argc, argv) ||
            //QTest::qExec(&testControllers, argc, argv);
}

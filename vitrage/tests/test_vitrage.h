#ifndef TESTSVITRAGE_H
#define TESTSVITRAGE_H

#include <QtTest>

class TestsVitrage : public QObject
{
    Q_OBJECT
private slots:
    void initTestCase();
    void testAddSource();
    void clearScene();
    void deleteScene();
};

#endif // TESTSVITRAGE_H

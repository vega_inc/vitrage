#include "test_vitrage.h"
#include "vscene.h"
#include "vcanva.h"
#include "vsourcemanager.h"
#include "support.h"

void TestsVitrage::initTestCase()
{
    QVERIFY2(VSrcManager::Instance().loadPlugins(), "Plugins is not loaded");
}

void TestsVitrage::testAddSource()
{
    // create source
    const QString name = "eims";
    const QString type = "VNCClient";
    VSource *source = VSrcManager::Instance().createVSource(type);
    if(source) {
        test::setSourceProperty(source, "eims");
        source->start();
        source->setStatus(VSource::RunStatus);
    }

    // create canva
    VCanva canva(nullptr);
    VAction action("test");
    VScene scene(0,0,0,0,tr("Empty Scene"));

    // tune vaction
    action.addSource(name, source);

    // tune vcanva
    canva.setAction(&action);
    canva.setScene(&scene);
    canva.addSource(name);
    QTime t;
    t.start();
    int minutes = 1;
    bool connected;
    while(t.elapsed() < minutes*1000) {
        if (source->getStatus() == VSource::RunStatus && source->getThread()->isRunning()) {
            connected = true;
            break;
        }
    }

    QVERIFY2(connected, QString("Source is not connected in %1 min")
             .arg(minutes).toLatin1());
    source->stop();
    source->deleteLater();
}

void TestsVitrage::clearScene()
{
    //QSKIP("Skip clear scene", SkipSingle);

    // create source
    const QString name = "eims";
    const QString type = "VNCClient";
    VSource *source = VSrcManager::Instance().createVSource(type);
    if(source) {
        test::setSourceProperty(source, name);
        source->start();
        source->setStatus(VSource::RunStatus);
    }

    // create canva
    VCanva canva(nullptr);
    VAction action("test");
    VScene scene(0,0,0,0,tr("Empty Scene"));

    // tune vaction
    action.addSource(name, source);

    // tune vcanva
    canva.setAction(&action);
    canva.setScene(&scene);
    canva.addSource(name);

    // clear
    canva.clearCanva();
    canva.clearScene();

    // stop sources
    source->stop();
    source->deleteLater();
}

void TestsVitrage::deleteScene()
{
    //QSKIP("* move some where", SkipSingle);

    // create source
    const QString name = "eims";
    const QString type = "VNCClient";
    VSource *source = VSrcManager::Instance().createVSource(type);
    if(source) {
        test::setSourceProperty(source, name);
        source->start();
        source->setStatus(VSource::RunStatus);
    }

    // create canva
    VCanva canva(nullptr);
    VAction action("test");
    VScene scene(0,0,0,0,tr("Empty Scene"));

    // tune vaction
    action.addSource(name, source);

    // tune vcanva
    canva.setAction(&action);
    canva.setScene(&scene);
    canva.addSource(name);

    // clear
    //canva.();
    canva.clearScene();

    // stop sources
    source->stop();
    source->deleteLater();
}

#ifndef SUPPORT
#define SUPPORT

#include "vsource/vsource.h"
namespace test {

static void setSourceProperty(VSource* source, const QString &name)
{
    QString logicName = name;
    QString physName = name;

    source->setProperty("logicName", logicName);
    source->setname(logicName);
    source->setProperty("physName",physName);
    source->setPhysName(physName);

    source->setProperty("location","10.1.1.158");
    source->setProperty("port",1);
    source->setProperty("password","");
}
}

#endif // SUPPORT

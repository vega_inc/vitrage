#include <thread>
#include <QLineF>

#include "test_shapes.h"
#include "vsourcemanager.h"
#include "vscene.h"
#include "vcanva.h"
#include "../vshapes/varrow/varrow.h"
#include "markers/arrow.h"
#include "markers/diagramitem.h"
#include "markers/diagramtextitem.h"
#include "support.h"

/// load plugins and connect to controller
void TestsShapes::initTestCase()
{
    QVERIFY2(VSrcManager::Instance().loadPlugins(), "Plugins is not loaded");

    ControllerOptions m_contrOptions;
    auto controllers = m_contrOptions.getControllers();
    m_controller = controllers.value("libvega.so");
    m_controller->setProperty("host", "10.1.1.4");
    m_controller->setProperty("port", "44444");

    QTime t;
    t.start();
    while(!m_controller->conn() && t.elapsed() < 5000) {
        auto error = m_controller->errorMsg();
        if (!error.isEmpty()) {
            QFAIL("Controller network error");
        }
        if (t.elapsed() > 5000) {
            QFAIL("Not started in 5 second");
        }
    }
}

void TestsShapes::addText()
{
    QSKIP("No add text test", SkipSingle);

    VSource *source = VSrcManager::Instance().createVSource(VARROW_SOURCE);
    if (!source)
        QFAIL("VSource is not created");
    static uint text_number = 0;
    auto name = QString("rect_%1").arg(text_number);
    source->setname(name);
    source->setVisible(false);

    auto textItem = new DiagramTextItem();
    textItem->setZValue(1000.0);
    textItem->setColor(Qt::blue);
    textItem->setSource(source);
    textItem->createEntity();

    auto copy = source->clone();
    QList<QSharedPointer<VSource>> m_sources;
    m_sources.push_back(copy);

    delete textItem;
    delete source;

    std::thread thr(&VController::setScene, m_controller, std::ref(m_sources));
    thr.join();
}

void TestsShapes::addArrows()
{
    //QSKIP("No add arrows test", SkipSingle);

    QPointF pos(0,0);
    QLineF line1{QPointF(pos.x(), pos.y()),     QPointF(pos.x()+500, pos.y()+500)};
    QLineF line2{QPointF(pos.x(), pos.y()+250), QPointF(pos.x()+500, pos.y()+250)};
    QLineF line3{QPointF(pos.x(), pos.y()+500), QPointF(pos.x()+500, pos.y())};
    QLineF line4{QPointF(pos.x()+250, pos.y()), QPointF(pos.x()+250, pos.y()+500)};
    QLineF line5{QPointF(pos.x()+250, pos.y()), QPointF(pos.x(), pos.y()+250)};
    Arrow *arrow1 = new Arrow(line1);
    Arrow *arrow2 = new Arrow(line2);
    Arrow *arrow3 = new Arrow(line3);
    Arrow *arrow4 = new Arrow(line4);
    Arrow *arrow5 = new Arrow(line5);
    QList<QSharedPointer<VSource>> m_sources;
    for (Arrow *arr : {arrow1, arrow2, arrow3, arrow4, arrow5}) {
        VSource *source = VSrcManager::Instance().createVSource(VARROW_SOURCE);
        if (!source)
            QFAIL("VSource is not created");
        static uint arr_number = 0;
        auto name = QString("arrow_%1").arg(arr_number);
        source->setname(name);
        source->setVisible(false);

        arr->setColor(Qt::red);
        arr->setSource(source);
        arr->createEntity();
        auto copy = source->clone();
        m_sources.push_back(copy);

        delete arr;
        delete source;
    }
    std::thread thr(&VController::setScene, m_controller, std::ref(m_sources));
    thr.join();
    QTest::qWait(1000);
}

void TestsShapes::addOrderArrows()
{
    QSKIP("No addOrderArrows test", SkipSingle);

    QLineF line1{300,   600,    300, 0};
    QLineF line2{0,     300,    600, 300};
    QLineF line3{300,   0,      300, 600};
    QLineF line4{600,   300,    0,   300};

    Arrow *arrow1 = new Arrow(line1);
    Arrow *arrow2 = new Arrow(line2);
    Arrow *arrow3 = new Arrow(line3);
    Arrow *arrow4 = new Arrow(line4);

    for (Arrow *arr : {arrow1, arrow2, arrow3, arrow4}) {
        VSource *source = VSrcManager::Instance().createVSource(VARROW_SOURCE);
        if (!source)
            QFAIL("VSource is not created");
        static uint arr_number = 0;
        auto name = QString("arrow_%1").arg(arr_number);
        source->setname(name);
        source->setVisible(false);

        arr->setColor(Qt::red);
        arr->setSource(source);
        arr->createEntity();
        auto copy = source->clone();
        QList<QSharedPointer<VSource>> m_sources;
        m_sources.push_back(copy);

        delete arr;
        delete source;
        std::thread thr(&VController::setScene, m_controller, std::ref(m_sources));
        thr.join();
        QTest::qWait(3000);
        m_sources.clear();
    }
}

void TestsShapes::addRect()
{
    QSKIP("No add rect test", SkipSingle);

    VSource *source = VSrcManager::Instance().createVSource(VARROW_SOURCE);
    if (!source)
        QFAIL("VSource is not created");
    static uint rect_number = 0;
    auto name = QString("rect_%1").arg(rect_number);
    source->setname(name);
    source->setVisible(false);

    QMenu menu;
    DiagramItem *rect = new DiagramItem(DiagramItem::Rectangle, &menu);
    QPolygonF myPolygon = QPolygonF() << QPointF(0, 0) << QPointF(400, 0)
                                      << QPointF(400, 400) << QPointF(0, 400)
                                      << QPointF(0, 0);
    rect->setMyPolygon(myPolygon);
    rect->setColor(Qt::blue);
    rect->setSource(source);
    rect->createEntity();

    auto copy = source->clone();
    QList<QSharedPointer<VSource>> m_sources;
    m_sources.push_back(copy);

    delete rect;
    delete source;

    std::thread thr(&VController::setScene, m_controller, std::ref(m_sources));
    thr.join();
    QTest::qWait(1000);
}

void TestsShapes::addComplexData()
{
    QSKIP("No add complex data test", SkipSingle);

    QList<QSharedPointer<VSource>> m_sources;
    // add rect
    {
        VSource *source = VSrcManager::Instance().createVSource(VARROW_SOURCE);
        if (!source)
            QFAIL("VSource is not created");
        static uint rect_number = 0;
        auto name = QString("rect_%1").arg(rect_number);
        source->setname(name);
        source->setVisible(false);

        QMenu menu;
        DiagramItem *rect = new DiagramItem(DiagramItem::Rectangle, &menu);
        QPolygonF myPolygon = QPolygonF() << QPointF(0, 0) << QPointF(200, 0)
                                          << QPointF(200, 200) << QPointF(0, 200)
                                          << QPointF(0, 0);
        rect->setMyPolygon(myPolygon);
        rect->setColor(Qt::blue);
        rect->setSource(source);
        rect->createEntity();

        auto copy = source->clone();
        m_sources.push_back(copy);

        delete rect;
        delete source;
    }
    // add arrow
    {
        QLineF line(500, 500, 250, 250);
        Arrow *arrow = new Arrow(line);
        VSource *source = VSrcManager::Instance().createVSource(VARROW_SOURCE);
        if (!source)
            QFAIL("VSource is not created");
        static uint arr_number = 0;
        auto name = QString("arrow_%1").arg(arr_number);
        source->setname(name);
        source->setVisible(false);

        arrow->setColor(Qt::red);
        arrow->setSource(source);
        arrow->createEntity();
        auto copy = source->clone();
        m_sources.push_back(copy);

        delete arrow;
        delete source;
    }
    // add text
    {
        VSource *source = VSrcManager::Instance().createVSource(VARROW_SOURCE);
        if (!source)
            QFAIL("VSource is not created");
        static uint text_number = 0;
        auto name = QString("text_%1").arg(text_number);
        source->setname(name);
        source->setVisible(false);

        auto textItem = new DiagramTextItem();
        textItem->setZValue(1000.0);
        textItem->setColor(Qt::yellow);
        textItem->setSource(source);
        textItem->createEntity();

        auto copy = source->clone();

        m_sources.push_back(copy);

        delete textItem;
        delete source;
    }
    std::thread thr(&VController::setScene, m_controller, std::ref(m_sources));
    thr.join();
}

void TestsShapes::addVSourceWithShapes()
{
    QSKIP("No add addVSourceWithShapes test", SkipSingle);

    QList<QSharedPointer<VSource>> m_sources;
    /*
    {
        QLineF line(0, 0, 500, 500);
        Arrow *arrow = new Arrow(line);
        VSource *source = VSrcManager::Instance().createVSource(VARROW_SOURCE);
        if (!source)
            QFAIL("VSource is not created");
        static uint arr_number = 0;
        auto name = QString("arrow_%1").arg(arr_number);
        source->setname(name);
        source->setVisible(false);

        arrow->setColor(Qt::red);
        arrow->setSource(source);
        arrow->createEntity();
        auto copy = source->clone();

        m_sources.push_back(copy);

        delete arrow;
        delete source;
    }*/
    QSharedPointer<VSource> src;
    {
        const QString name = "eims";
        const QString type = "VNCClient";
        src = QSharedPointer<VSource>(VSrcManager::Instance().createVSource(type));
        auto source = src.data();
        if(source) {
            test::setSourceProperty(source, "eims");
            source->start();
            source->setStatus(VSource::RunStatus);
        }

        // create canva
        VCanva canva(nullptr);
        VAction action("test");
        VScene scene(0,0,0,0,tr("Empty Scene"));

        // tune vaction
        action.addSource(name, source);

        // tune vcanva
        canva.setAction(&action);
        canva.setScene(&scene);
        canva.addSource(name);
        QTime t;
        t.start();
        int minutes = 1;
        //bool connected;
        while(t.elapsed() < minutes*1000) {
            if (source->getStatus() == VSource::RunStatus && source->getThread()->isRunning()) {
          //      connected = true;
                break;
            }
        }
        source->start();
        m_sources.push_back(src);
    }
    std::thread thr(&VController::setScene, m_controller, std::ref(m_sources));
    thr.join();

    // wait to get image from vnc
    auto minutes = 10;
    QTime t;
    t.start();
    while (!src.data()->isUpdated && t.elapsed() < minutes*1000)
        QTest::qWait(250);
    qDebug() << src.data()->isUpdated;
    qDebug() << src.data()->isUpdated;
    QVERIFY2(src.data()->isUpdated, QString("Source is not updated in %1 min").arg(minutes).toLatin1());
}
/// disconnect controller
void TestsShapes::cleanupTestCase()
{
    QTime t;
    t.start();

    while(!m_controller->disconn() && t.elapsed() < 5000) {
        QTest::qWait(250);
    }
    if (!m_controller->disconn())
        QFAIL("Controller is not disconnected");
}

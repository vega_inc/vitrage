#include "vscene.h"
#include "vcanva.h"
#include "vsourcemanager.h"
#include "controlleroptions.h"

#include "test_vitrage_markers.h"

#include <thread>
#include <QSignalSpy>

void TestsVitrageMarkers::fillSourceProperty(VSource* source, const QString &name)
{
    QString logicName = name;
    QString physName = name;

    source->setProperty("logicName", logicName);
    source->setname(logicName);
    source->setProperty("physName",physName);
    source->setPhysName(physName);

    source->setProperty("location","10.1.1.4");
    source->setProperty("port",5900);
    source->setProperty("password","");
}
/// init
void TestsVitrageMarkers::initTestCase()
{
    QVERIFY2(VSrcManager::Instance().loadPlugins(), "Plugins is not loaded");
    ControllerOptions m_contrOptions;
    auto controllers = m_contrOptions.getControllers();
    m_controller = controllers.value("libvega.so");
    m_controller->setProperty("host", "10.1.1.4");
    m_controller->setProperty("port", "44444");
    m_controller->conn();
    QTime t;
    t.start();
    while(m_controller->conn() && t.elapsed() > 5000) {
        QString error = m_controller->errorMsg();
        if (!error.isEmpty()) {
            delete m_controller;
            QFAIL("Controller network error");
        }
        if (t.elapsed() > 5000) {
            delete m_controller;
            QFAIL("Not started in 5 second");
        }
    }
    if (!m_controller->conn())
        QFAIL("Controller not connected");
}
/// test markers
void TestsVitrageMarkers::markersTestCase()
{
    // create source
    const QString name = "local";
    const QString type = "VNCClient";
    auto src = QSharedPointer<VSource>(VSrcManager::Instance().createVSource(type));
    auto source = src.data();
    if(source) {
        fillSourceProperty(source, "local");
        source->start();
    }

    // create canva
    VCanva canva(nullptr);
    VAction action("test");
    VScene scene1(0,0,0,0,tr("Scene #1"));
    VScene scene2(0,0,0,0,tr("Scene #2"));
    // tune vaction
    action.addSource(name, source);

    // tune vcanva
    canva.setAction(&action);
    canva.setScene(&scene1);
    canva.setScene(&scene2);
    canva.addSource(name);
    //
    VPosition pos;
    auto h = 0;
    auto w = 0;
    auto height_cells = 300;
    auto width_cells = 300;
    pos.lt.setY(h);
    pos.lt.setX(w);
    pos.rb.setY(h + height_cells);
    pos.rb.setX(w + width_cells);
    source->setPosition(pos);

    // check running state
    QTime t; t.start();
    int minutes = 2;
    bool connected;
    while(t.elapsed() < minutes*1000) {
        QTest::qWait(250);
        if (source->getStatus() == VSource::RunStatus &&
                source->getThread()->isRunning()) {
            connected = true;
            break;
        }
    }
    QVERIFY2(connected, QString("Source is not connected in %1 min").arg(minutes).toLatin1());

    m_sources.push_back(src);
    std::thread thr(&VController::setScene, m_controller, std::ref(m_sources));
    thr.join();

//    QTest::mousePress(source, Qt::LeftButton, Qt::NoModifier, QPoint());

    // wait to get image from vnc
    minutes = 10;
    t.start();
    while (!source->isUpdated && t.elapsed() < minutes*1000)
        QTest::qWait(250);
    qDebug() << "source->isUpdated =" << source->isUpdated;
    QVERIFY2(source->isUpdated, QString("Source is not updated in %1 min").arg(minutes).toLatin1());
    m_sources.clear();
    QTest::qWaitForWindowShown(source);
    src.clear();

    /*
    //QSignalSpy spy(copy.data(), SIGNAL(clicked(bool)));

     // do something that triggers the signal
     //box->animateClick();

     QCOMPARE(spy.count(), 1); // make sure the signal was emitted exactly one time
     QList<QVariant> arguments = spy.takeFirst(); // take the first signal

     QVERIFY(arguments.at(0).toBool() == true); // verify the first argument
     */
}
/// cleanup
void TestsVitrageMarkers::cleanupTestCase()
{
    // delete source
    for (auto s : m_sources) {
        s->finalize();
        s->deleteLater();
    }
    m_sources.clear();

    // delete controller
    m_controller->disconn();
    QTime t; t.start();
    while(!m_controller->disconn() && t.elapsed() < 5000) {
        QTest::qWait(250);
    }
    QVERIFY2(m_controller->disconn(), "Controller error disconnected");
    delete m_controller;
}

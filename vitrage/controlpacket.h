#ifndef CONTROLPACKET
#define CONTROLPACKET

#include <QPoint>
#include <QDataStream>

struct control_packet {
    bool control;
    QPoint point;
    int width, height;
    int event;
};

QDataStream & operator<< (QDataStream& stream, const control_packet& packet)
{
    stream << packet.control;
    stream << packet.point;
    stream << packet.width << packet.height;
    stream << packet.event;
    return stream;
}

QDataStream & operator>> (QDataStream& stream, control_packet& packet)
{
    stream >> packet.control;
    stream >> packet.point;
    stream >> packet.width >> packet.height;
    stream >> packet.event;
    return stream;
}

#endif // CONTROLPACKET


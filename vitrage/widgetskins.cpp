#include "widgetskins.h"
#include "ui_widgetskins.h"
#include <QDir>
#include <QDebug>

WidgetSkins::WidgetSkins(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WidgetSkins)
{
    ui->setupUi(this);

}

WidgetSkins::WidgetSkins(QString path) :
    ui(new Ui::WidgetSkins)
{
    ui->setupUi(this);
    m_scene =  new QGraphicsScene;
    ui->graphicsView->setScene(m_scene);


    m_path = path;
    QDir dir(path);
    QString str("*.svg");
    QStringList tmp;
    tmp.append(str);
    int wi = 0, hi = 0;
    connect(m_scene,SIGNAL(selectionChanged()),this,SLOT(selectionCh()));
    QStringList list = dir.entryList(tmp);
    for (int i = 0; i < list.count(); i++) {
        QString file = dir.absoluteFilePath(list[i]);
        qDebug() << file;
        QSvgRenderer * rend = new QSvgRenderer(file);
        rend->setProperty("file",file);
        m_renders.append(rend);
        QGraphicsSvgItem * item = new QGraphicsSvgItem;
        item->setSharedRenderer(rend);
        item->setElementId("RunTime");
        item->setFlag(QGraphicsItem::ItemIsSelectable);
        m_items.append(item);
        item->setPos(wi,hi);
        hi += item->boundingRect().height() + 20;
        m_scene->addItem(item);
    }
}

WidgetSkins::~WidgetSkins()
{
    delete ui;
    foreach(QSvgRenderer *rnd,m_renders) {
        delete rnd;
    }
    foreach(QGraphicsSvgItem *itm,m_items) {
        delete itm;
    }
}

void WidgetSkins::selectionCh()
{
  QList<QGraphicsItem *> lst =  m_scene->selectedItems();
  if (lst.count() != 1) return;
  QGraphicsSvgItem *item = dynamic_cast<QGraphicsSvgItem *>(lst.at(0));
  if (!item) return;

  QVariant file = item->renderer()->property("file");
  emit selectedWidget(file.toString());
  qDebug()<< file;



}

#ifndef CROPPINGWIDGET_H
#define CROPPINGWIDGET_H

#include <QDialog>
#include <QAbstractButton>
#include <QPushButton>
#include "vsource/vsource.h"
#include "vscene.h"

namespace Ui {
class CroppingWidget;
}

class CroppingWidget : public QDialog
{
    Q_OBJECT

    static const int VALUE_MARGIN_LAYER = 5;
    
public:
    explicit CroppingWidget(QWidget *parent = 0, QSharedPointer<VSource> src = QSharedPointer<VSource>(), QSize srcSize = QSize(100,100),
                            VScene *pScene = 0, QSharedPointer<VSource> pSrc = QSharedPointer<VSource>());
    ~CroppingWidget();

protected:
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void resizeEvent(QResizeEvent *);

private:
    Ui::CroppingWidget *ui;
    QSharedPointer<VSource> m_vsrc;
    VScene *parentScene;
    QSharedPointer<VSource> parentSource;
    double m_hFW;
    QSize firstSrcSize;
    double stepXfromFirstSz = 1;
    double stepYfromFirstSz = 1;
    QRect firstRubberRect;
    QTimer *timer;
    int magicNumberHeight = 0; //for remove bug on resizing in big, when heigh>width in cropped image

public slots:
    void buttonsClicked(QAbstractButton*);
    void onOkClicked();
    void onButtonCancelCropClicked();
    void onButtonEnableCropClicked();
private slots:
    void updateCropShapeTimer();
};

#endif // CROPPINGWIDGET_H

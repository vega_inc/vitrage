#include "vscene.h"

#include <QObject>
#include <QDebug>

VScene::VScene(uint32_t id, const uint32_t& tF, const uint32_t& tD, const QString& n)
    : m_id(id),
      m_tFrom(tF),
      m_tDuration(tD),
      m_name(n)
{
    content.clear();
    m_extronInput.clear();
    m_extronMixer.clear();
    m_mixerVNC.clear();
}

/**
 * @brief VScene::removeSource  remove source from all content
 * @param name - [in] QString
 */
void VScene::removeSource(QString name)
{
    QVector<frame> vec;
    foreach(frame fr,content) {
        if (!(fr.source_name == name)) {
            vec.push_back(fr);
        }
    }
    content = vec;
}

bool VScene::contentContains(QString str)
{
    foreach(frame fr,content) {
        if (fr.source_name == str)
            return true;
    }
    return false;
}

bool VScene::validateSource2(QSharedPointer<VSource> src)
{
    for (auto ss : m_mixerVNC.keys()) {
        if (src->getCloneName() == ss) {
            m_mixerVNC[ss] = src;
        }
    }
    for (auto ss : m_extronMixer.keys()) {
        if (src->getCloneName() == ss) {
            m_extronMixer[ss] = src;
        }
    }
    for (auto ss : m_extronInput.keys()) {
        if (src->getCloneName() == ss) {
            m_extronInput[ss] = src;
        }
    }
    return false;
}

bool VScene::validateSource(QSharedPointer<VSource> src, QString &message)
{
    QString type = src->gettype();
    qDebug() << Q_FUNC_INFO << type;

    if ((type == MIXERVNC)  || type == EXTRONMIXER ||
            type == EXTRON  || type == "NamedInput") {
        if (!haveInput())
            return true;
    }
    if (type  == MIXERVNC) {
        if (m_mixerVNC.size() >= m_maxMixer) {
            message = tr("Have no empty inputs for MixerVNC");
            return true; }
            m_mixerVNC.insert(src->getCloneName(),src);
            return false;
    }
    if (type == EXTRONMIXER) {
        if ((m_extronMixer.size()+m_extronInput.size()) >= m_maxExtronMixer) {
            message = tr("Have no empty inputs for MixerVNC over Extron");
            return true; }
            m_extronMixer.insert(src->getCloneName(),src);
            return false;
    }

    if (type == EXTRON) {
        if ((m_extronMixer.size()+m_extronInput.size()) >= m_maxExtronMixer) {
        message = tr("Have no empty inputs for MixerVNC over Extron");
        return true; }
        m_extronInput.insert(src->getCloneName(),src);
        return false;
    }
    return false;
}

bool VScene::haveInput() const
{
    if (m_DVIInputs+1 > m_maxDVIInputs)
        return false;
    return true;
}

QList<QSharedPointer<VSource> > VScene::getExtronAll()
{
    QList<QSharedPointer<VSource>  > lst= m_extronMixer.values();
    lst += m_extronInput.values();
    return lst;
}

uint32_t VScene::id() const
{
    return m_id;
}

void VScene::addSource(VSource &src)
{
    frame fr;

    fr.source_name = src.getname();
    fr.clone_name = src.getCloneName();
    fr.pos = src.getPosition();
    fr.visible = src.visualise();
    content.append(fr);
}

void VScene::removeClone(QString name)
{
    for (int i = 0 ; i < content.count(); i++) {
        if (content[i].clone_name == name) {
            content.remove(i);
        }
    }
}

QVector<frame> &VScene::getContent()
{
    return content;
}

void VScene::setDuration(uint32_t dura)
{
    m_tDuration = dura;
}

void VScene::setName(const QString &n)
{
    m_name = n;
}

QString VScene::name() const {
    return m_name;
}

uint32_t VScene::timeFrom() const {
    return m_tFrom;
}

uint32_t VScene::duration() const
{
    return m_tDuration;
}

void VScene::setActive(bool active) {
    m_active = active;
}

bool VScene::isActive() const {
    return m_active;
}
/*
uint32_t VScene::number() const
{
    return m_number;
}
*/
void VScene::setPosition(VSource &src)
{
    for (int i = 0 ; i < content.count();i++) {
        if (content[i].clone_name == src.getCloneName()) {
            content[i].pos = src.getPosition();
        }
    }
}

void VScene::setVisible(const QString &cloneName, bool flag)
{
    for (auto c : content) {
        if (c.clone_name == cloneName) {
            c.visible = flag;
        }
    }
}

bool VScene::getVisible(QString clone) 
{
    for (int i = 0 ; i < this->content.count();i++) {
        if (content[i].clone_name == clone) {
            return content[i].visible;
        }
    }
	return false;
}

int VScene::getPosMargin(frame fr)
{
    QString end = fr.clone_name.remove(0, fr.source_name.count());
    return end.toInt();
}

QList<QSharedPointer<VSource> > VScene::getMixerVNCList() {
    return m_mixerVNC.values();
}

QList<QSharedPointer<VSource> > VScene::getMixerAll() {
    QList<QSharedPointer<VSource>  > lst= m_mixerVNC.values();
    lst + m_extronMixer.values();
    return lst;
}

QList< QSharedPointer<VSource> > VScene::getMixerAll(QList<QSharedPointer<VSource> > &ll) {
    QSharedPointer<VSource>  src;
    QList <QSharedPointer<VSource> > outputList;
    foreach (src, ll) {
        if ((src->gettype() == EXTRONMIXER) ||
                ((src->gettype() == MIXERVNC)))
            outputList.push_back(src);
    }
    return outputList;
}

QList<QSharedPointer <VSource> > VScene::getExtronAll(QList<QSharedPointer<VSource> > &ll)
{
    QSharedPointer<VSource>  src;
    QList <QSharedPointer<VSource> > outputList;
    foreach (src, ll) {
        if ((src->gettype() == EXTRONMIXER) ||
                ((src->gettype() == EXTRON)))
            outputList.push_back(src);
    }
    return outputList;
}

QList<QSharedPointer<VSource> > VScene::getExtronInput() {
    return m_extronInput.values();
}

QList<QSharedPointer<VSource> > VScene::getExtronMixer() {
    return m_extronMixer.values();
}

void VScene::removeSourceFromConts(const QString &name)
{
    m_extronInput.remove(name);
    m_extronMixer.remove(name);
    m_mixerVNC.remove(name);
}

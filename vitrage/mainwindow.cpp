#include <QSplitter>
#include <QDir>
#include <QMessageBox>
#include <QPluginLoader>
#include <QMimeData>
#include <QDrag>
#include <QSettings>
#include <QTranslator>
#include <QLabel>
#include <QPushButton>
#include <QTranslator>
#include <QTime>

#include <vsource/vsource.h>
#include "ui_mainwindow.h"
#include "mainwindow.h"
#include "configpath.h"
#include "logicsourcemanager.h"
#include "vcanva.h"
#include "vaction.h"
#include "vsourcemanager.h"
#include "fillaction.h"
#include "collapsibleframe.h"
#include "pluginsmanager.h"
#include "extron/extron.h"
#include "support.h"

#include "spdlog/spdlog.h"

#define SRC_DEFLT_SZ      100
#define SCN_DEFLT_SZ      100
#define CONTENT_DEFLT_SZ  70

#define DEF_HEIGHT          1024
#define DEF_WIDTH           1280
#define DEF_WALL_W          5
#define DEF_WALL_H          3
#define DEF_CELL_W          3
#define DEF_CELL_H          3
#define DEF_RESOLV_W        1280
#define DEF_RESILV_H        1024
#define DEF_CTR_PORT        12345
#define DEF_CTR_TIMEOUT     3000
const char temp_file_path[] = "/tmp/temp.act";

bool camera = false; /* FIXME */

/**
* @brief ctor for main window
*
* @param parent
*/
MainWindow::MainWindow(QWidget *parent):
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(codec);
    QTextCodec::setCodecForCStrings(codec);

    try {
        QDir().mkdir("logs");
        if (!QDir("logs").exists())
            throw spdlog::spdlog_ex("fail to create dir logs");
        auto logger = spdlog::daily_logger_mt("log", "logs/vitrage");
        logger->info() << "Запуск витража";
    }
    catch (const spdlog::spdlog_ex& ex) {
        qDebug() << ex.what();
    }

    m_sourceOldSize = SRC_DEFLT_SZ;
    m_scenesOldSize = SCN_DEFLT_SZ;
    m_contentOldSize = CONTENT_DEFLT_SZ;

    QString file_path = getTranslatePath();
    if(!file_path.endsWith("/"))
        file_path += "/";
    auto appName = QCoreApplication::arguments().at(0);
    if(tor.load( file_path + QFileInfo(appName).fileName() + ".qm"))
        QApplication::installTranslator(&tor);

    ui->setupUi(this);
    createCollapsibles();

    auto config_path = support::getSettingsPath();
    QSettings settings(config_path, QSettings::IniFormat);
    if (!settings.contains("MainWinW") ) {
        spdlog::get("log")->warn() << "set default settings, config not found from path: "
                                   << config_path.toStdString();
        setDefaultSettings(settings);
    }
    else {
        spdlog::get("log")->info() << "config is found from path: "
                                   << config_path.toStdString();
    }
    if (!settings.contains("mediaPath")) {
        settings.setValue("mediaPath", "/tmp");
    }
    resize(settings.value("MainWinW").toInt(), settings.value("MainWinH").toInt());
    move(settings.value("MainWinX").toInt(), settings.value("MainWinY").toInt());
    //qApp->setFont(QFont(settings.value("FontFamily").toString(), settings.value("FontSize").toInt()));
    auto w = settings.value("WallWidth").toInt();
    auto h = settings.value("WallHeight").toInt();
    auto cw = settings.value("CellWidth").toInt();
    auto ch = settings.value("CellHeight").toInt();
    auto rw = settings.value("ResolvWidth").toInt();
    auto rh = settings.value("ResolvHeight").toInt();
    setConfigs(w, h, cw, ch, rw, rh);

    connect(ui->actionModel,   SIGNAL(triggered()), this, SLOT(openCanvaOptions()));
    connect(ui->createAction,  SIGNAL(triggered()), this, SLOT(slotCreateAction()));
    connect(ui->createScenary, SIGNAL(triggered()), this, SLOT(slotCreateScenary()));
    m_pActiveAction = new VAction(tr("new action"),
                                  m_tableSources,
                                  m_tableScenes,
                                  m_tableSceneContent);
    m_tableSources->installEventFilter(this);
    m_tableScenes->installEventFilter(this);
    m_tableSceneContent->installEventFilter(this);

    m_tableScenes->setDragEnabled(false);
    m_tableScenes->viewport()->setAcceptDrops(false);
    m_tableScenes->setDropIndicatorShown(false);

    m_tableSceneContent->setDragEnabled(false);
    m_tableSceneContent->viewport()->setAcceptDrops(false);
    m_tableSceneContent->setDropIndicatorShown(false);

    connect(m_pActiveAction, SIGNAL(clearCanvas()),         this, SLOT(clearCanvas()));
    connect(m_pActiveAction, SIGNAL(clearCanvas(QString)),  this, SLOT(clearCanvas(QString)));
    connect(m_pActiveAction, SIGNAL(updateModel(VScene*)),  this, SIGNAL(setModelScene(VScene*)));
    connect(m_pActiveAction, SIGNAL(refillTimeline()),      this, SLOT(fillTimeline()));
    connect(m_pActiveAction, SIGNAL(setPresetScene(QString)),this, SLOT(sceneRadioSet(QString)));
    connect(m_pActiveAction, SIGNAL(setScenesItem(int,int)), this, SLOT(sceneCellClicked(int)));
    connect(m_pActiveAction, SIGNAL(somethingChanged()),     this, SLOT(actionModefied()));
    connect(m_pActiveAction, SIGNAL(loadStatus(int,int)),    this, SLOT(getLoadStatus(int,int)));
    auto is_admiralty = settings.value("IsAdmiralty", false).toBool();
    m_modelCanva = new VCanva(this,m_wallH,m_wallW,m_cellH,m_cellW,m_resolvW,m_resolvH, MODEL);

    connect(m_pActiveAction, SIGNAL(newScene(VScene*)), m_modelCanva, SLOT(setScene(VScene*)));
    connect(m_pActiveAction,SIGNAL(activateSource(QString)), m_modelCanva, SLOT(setActiveSource(QString)));
    connect(m_modelCanva,SIGNAL(sourceRightClicked(QPoint)), m_pActiveAction, SLOT(sourceRighrClick(QPoint)));

    setMinimumWidth(600);
    setMinimumHeight(600);

    m_runtimeCanva = new VCanva(this,m_wallH,m_wallW,m_cellH,m_cellW,m_resolvW,m_resolvH,RUNTIME,is_admiralty);

    connect(m_modelCanva,SIGNAL(sourceClicked()),this,SLOT(sourceClickedOnCanva()));

    m_canvaOptions = new CanvaOptions(this);
    connect(m_canvaOptions, SIGNAL(sendOpenHtml(QString)), this, SLOT(openHtml(QString)));
    optionsWidgetsSetup();
    connect(m_contrOptions, SIGNAL(conn(VController*)), SLOT(setController(VController*)));
    m_contrOptions->setControllerProperty(config_path);
    m_pMixerOptions->loadSettings(&settings);
    m_pExtronOptions->loadSettings(&settings);

    VSrcManager::Instance().loadPlugins();

    m_modelCanva->setAction(m_pActiveAction);
    m_runtimeCanva->setAction(m_pActiveAction);

    connect(this, SIGNAL(global_mouse_move(QPoint)), m_modelCanva, SIGNAL(global_mouse_move_sig(QPoint)));
    connect(m_canvaOptions,SIGNAL(setOptionsCanva(int,int,int,int,int,int)),
            m_modelCanva,SLOT(setNewOptions(int,int,int,int,int,int)));
    connect(m_canvaOptions,SIGNAL(setOptionsCanva(int,int,int,int,int,int)),
            m_runtimeCanva,SLOT(setNewOptions(int,int,int,int,int,int)));

    connect(m_canvaOptions,SIGNAL(setOptionsCanva(int,int,int,int,int,int)),
            this,SLOT(setConfigs2(int,int,int,int,int,int)));

    connect(this,SIGNAL(setModelScene(VScene *)), m_modelCanva,SLOT(setScene(VScene *)));
    connect(this,SIGNAL(setMinCanvaHeight(int)), m_modelCanva,SLOT(setMinHeight(int)));
    connect(this,SIGNAL(setMinCanvaHeight(int)), m_runtimeCanva,SLOT(setMinHeight(int)));
    connect(m_modelCanva,SIGNAL(setOptions(int,int,int,int,int,int)),
            m_canvaOptions,SLOT(setOptions(int,int,int,int,int,int)));

    connect(m_runtimeCanva,SIGNAL(setOptions(int,int,int,int,int,int)),
            m_canvaOptions,SLOT(setOptions(int,int,int,int,int,int)));
    connect(m_modelCanva,SIGNAL(sourceModeSignal()), this,SLOT(onSourceMoved()));
    m_modelCanva->setAcceptDrops(true);

    m_modelCanva->setMouseTracking(true);
    m_runtimeCanva->setMouseTracking(true);
    m_modelCanva->scrollArea()->setMouseTracking(true);
    m_runtimeCanva->scrollArea()->setMouseTracking(true);


    /* set Start options */
    m_modelCanva->setOptionsWindow();

    ui->createScenary->setEnabled(false);
    on_actionFit_triggered();
    ui->actionGrid->setChecked(true);

    ui->actionTimePlay->setEnabled(false);
    ui->actionPrev->setEnabled(false);
    ui->actionNext->setEnabled(false);
    ui->actionToRun->setEnabled(false);
    ui->actionTimeStop->setEnabled(false);

    m_pGraphicsEditor = new VGraphicsEditor;
    m_pGraphicsEditor->setAction(m_pActiveAction);

    m_lsmgr = new LogicSourceManager(m_pActiveAction,this);
    m_pGraphicsEditor->setSourceManager(m_lsmgr);

    connect(m_lsmgr,SIGNAL(openGEditorS()),this,SLOT(openGEditor()));
    connect(m_lsmgr,SIGNAL(sendOpenHtml(QString)), this, SLOT(openHtml(QString)));
    connect(m_lsmgr,SIGNAL(setDeviceToAction(bool)),SLOT(actionUpdatedInManager()));
    connect(m_lsmgr,SIGNAL(renameSourceSig(QString,QString)),SLOT(renameSource(QString,QString)));

    connect(m_pGraphicsEditor,SIGNAL(sourcesUpdated()),m_lsmgr,SLOT(actionUpdated()));
    connect(m_pGraphicsEditor,SIGNAL(schemeLoaded(QString)),this, SLOT(schemeLoadedInEditor()));
    connect(m_pGraphicsEditor,SIGNAL(closeSig()),this,SLOT(actionUpdatedInManager()));
    connect(m_pGraphicsEditor,SIGNAL(schemeMod()),this,SLOT(actionModefied()));
    connect(m_pGraphicsEditor,SIGNAL(hideMe()),this,SLOT(hideGraphicsEditor()));
    connect(m_pGraphicsEditor,SIGNAL(sendOpenHtml(QString)),this,SLOT(openHtml(QString)));

    initStartingMenu();
    m_modelCanva->installEventFilter(this);
    m_modelCanva->scrollWgt()->installEventFilter(this);
    initAllDockWidgets(settings);
    m_modelCanva->scrollArea()->setWidgetResizable(false);
    m_runtimeCanva->scrollArea()->setWidgetResizable(false);
    //m_runtimeCanva->scrollArea()->setBackgroundRole(QPalette::Dark);
    //m_runtimeCanva->setBackgroundRole(QPalette::Window);

    m_pStatusBar = new VStatusBar(ui->statusbar);
    m_pStatusBar->setMessage(tr("Waiting"));
    m_pStatusBar->setConnectStatus(tr("Not connected"));

    m_pGraphicsEditor->setSourceTable(m_lsmgr->getTableWidget());
    m_pGraphicsEditor->setLogicTable(m_lsmgr->getLogicTableWidget());

    ui->actionStandart->setChecked(!ui->toolBar->isHidden());
    ui->actionPlaying->setChecked(!ui->toolBarPlaying->isHidden());
    ui->actionConfigurations->setChecked(!ui->toolBarConfig->isHidden());

    m_bareScene = new VScene(0,0,0,tr("Empty Scene"));

    openResources();
    aboutWind = new AboutProgram(this);
    initPlugins();

    helpWebView.setWindowTitle("Справка");
    helpWidget = new QWidget(this, Qt::Window);
    helpWidget->setMinimumSize(800,480);
    QVBoxLayout *vboxLayout = new QVBoxLayout;
    vboxLayout->addWidget(&helpWebView);
    helpWidget->setLayout(vboxLayout);

    connect(m_timeline, SIGNAL(setScene(const QString&)), m_pActiveAction, SLOT(setScene(const QString&)));
    connect(m_timeline, SIGNAL(setRuntimeScene(QString)), this, SLOT(setRuntimeSceneFromTimeLine(QString)));
    connect(m_timeline, SIGNAL(finished()),this,SLOT(timelineFinished()));
    connect(m_timeline, SIGNAL(enableButtons(bool,bool)), this, SLOT(enableButtons(bool,bool)));
    connect(m_timeline, SIGNAL(enabToPlay(bool)),this,SLOT(enableToRun(bool)));
    setResizeModeTables();
    connect(&timerCheckSourceItems, SIGNAL(timeout()), this, SLOT(checkSourceItems()));
    restoreAfterCrash();
}// restore state before crash if file exist in /tmp
void MainWindow::restoreAfterCrash()
{
    if (!QFile(temp_file_path).exists())
        return;
    ui->save_action->setEnabled(true);
    QMessageBox box;
    box.setWindowTitle("Восстановление мероприятия");
    box.setText("Приложение было завершено не корректно.");
    box.setInformativeText("Хотите восстановить мероприятие?");
    box.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    box.setDefaultButton(QMessageBox::Yes);
    int ret = box.exec();
    switch (ret) {
    case QMessageBox::Yes:
        loadAction(temp_file_path);
        break;
    case QMessageBox::No:
        break;
    default:
        break;
    }
}

/**
* @brief event filter for mainwindow
*
* @param obj QObject *
* @param ev QEvent *
*1
* @return true - succ
*/
bool MainWindow::eventFilter(QObject *obj, QEvent *ev)
{
    if (obj == m_modelCanva->scrollWgt()) {
        if(ev->type() == QEvent::DragEnter) {
            QDragEnterEvent * drag_ev = static_cast<QDragEnterEvent*>(ev);
            drag_ev->acceptProposedAction();
            return false;
        }
//        if (ev->type() == QEvent::MouseMove) {
//            emit global_mouse_move(QCursor::pos());
//            return true;
//        }
        if(ev->type() == QEvent::Drop) {
            QDropEvent *dr_ev = static_cast<QDropEvent*>(ev);
            if (dr_ev->mimeData()->hasText()) {
                m_modelCanva->source_drop(dr_ev->mimeData()->text(), m_modelCanva->scrollWgt()->mapToGlobal(dr_ev->pos()));
                saveAction(temp_file_path);
                return true;
            }
        }
        return false;
    }

    if (obj == m_tableSources) {
        if (ev->type() == QEvent::KeyPress) {
            QKeyEvent *keyEvent = static_cast<QKeyEvent*>(ev);
            if (keyEvent->key() == Qt::Key_Delete) {
                auto row = m_tableSources->currentRow();
                auto text = m_tableSources->item(row,1)->text();
                int ret = QMessageBox::question(this, "Удаление ИИ", "Удалить ИИ " + text, QMessageBox::Yes | QMessageBox::No);
                if (ret == QMessageBox::Yes) {
                    m_pActiveAction->onRemoveSource();
                    m_pActiveAction->fill();
                }
                return true;
            }
            return false;
        }
        else if (ev->type()  == QEvent::ContextMenu) {
            m_pActiveAction->contextMenu(ev, VAction::TreeType::SourceWidget);
            return true;
        }
        return false;
    }
    else if (obj == m_tableScenes) {
        if (ev->type() == QEvent::KeyPress) {
            QKeyEvent *keyEvent = static_cast<QKeyEvent*>(ev);
            if (keyEvent->key() == Qt::Key_Delete) {
                auto row = m_tableScenes->currentRow();
                auto text = m_tableScenes->item(row,1)->text();
                int ret = QMessageBox::question(this, "Удаление сцены", "Удалить сцену " + text, QMessageBox::Yes | QMessageBox::No);
                if (ret == QMessageBox::Yes) {
                    m_pActiveAction->onRemoveScene();
                }
                return true;
            }
        }
        if (ev->type()  == QEvent::ContextMenu)  {
            m_pActiveAction->contextMenu(ev, VAction::TreeType::SceneWidget);
            return true;
        }
        return false;
    }
    else if (obj == m_tableSceneContent || obj == m_modelCanva) {
        if (ev->type() == QEvent::KeyPress) {
            QKeyEvent *keyEvent = static_cast<QKeyEvent*>(ev);
            if (keyEvent->key() == Qt::Key_Delete) {
                int row = -1;
                for (int r = 0; r < m_tableScenes->rowCount(); ++r) {
                    auto item = m_tableScenes->cellWidget(r,0);
                    auto w = qobject_cast<QRadioButton*>(item);
                    if (w->isChecked()) {
                        row = r;
                        break;
                    }
                }
                if (row < 0)
                    return false;
                auto item = m_tableScenes->item(row,1);
                if (!item)
                    return false;
                auto sceneName = item->text();
                auto content = m_tableSceneContent->item(m_tableSceneContent->currentRow(),1)->text();
                int ret = QMessageBox::question(this, "Удаление контента", "Удалить контент " + content, QMessageBox::Yes | QMessageBox::No);
                if (ret == QMessageBox::Yes) {
                    m_modelCanva->clearCanva();
//                    m_pActiveAction->removeSrc_s();
//                    m_pActiveAction->fillContentForScene(sceneName);
//                    sceneRadioSet(sceneName);
                }
                return true;
            }
            return false;
        }
        if (ev->type()  == QEvent::ContextMenu)  {
            m_pActiveAction->contextMenu(ev, VAction::TreeType::SceneContentWidget);
            return true;
        }
        return false;
    }
    return QMainWindow::eventFilter(obj, ev);
}

void MainWindow::mouseMoveEvent(QMouseEvent *)
{
    //emit global_mouse_move(QCursor::pos());
}

MainWindow::~MainWindow()
{
    delete m_pActiveAction;
    delete m_pGraphicsEditor;
    delete m_pController;//->deleteLater();
    delete ui;
    delete m_bareScene;
    spdlog::get("log")->info() << "Завершение работы";
    spdlog::drop_all();
}

void MainWindow::addLayoutToCanva(VCanva *canva,QLayout *lay,
                                  int row, int column,
                                  int rowspan,int colspan)
{
    static_cast<QGridLayout*>(canva->layout())->addLayout(lay,row,column,rowspan,colspan);
}

void MainWindow::setDefaultSettings(QSettings &settings)
{
    settings.setValue("MainWinX"            , 0);
    settings.setValue("MainWinY"            , 0);
    settings.setValue("MainWinW"            ,DEF_WIDTH);
    settings.setValue("MainWinH"            ,DEF_HEIGHT);
    settings.setValue("WallWidth"           ,DEF_WALL_W);
    settings.setValue("WallHeight"          ,DEF_WALL_H);
    settings.setValue("CellWidth"           ,DEF_CELL_W);
    settings.setValue("CellHeight"          ,DEF_CELL_H);
    settings.setValue("ResolvWidth"         ,DEF_RESOLV_W);
    settings.setValue("ResolvHeight"        ,DEF_RESILV_H);
    settings.setValue("ControllerHost"      ,"localhost");
    settings.setValue("ControllerPort"      ,DEF_CTR_PORT);
    settings.setValue("ControllerUsername"  ,"admin");
    settings.setValue("ControllerPasswd"    ,"");
    settings.setValue("ControllerTimeout"   ,DEF_CTR_TIMEOUT);
    settings.setValue("IsAdmiralty",         false);
    if (m_pController) settings.setValue("ControllerState",0);
}

void MainWindow::openCanvaOptions(void)
{
    m_canvaOptions->open();
    m_modelCanva->setOptionsWindow();
}

void MainWindow::slotCreateAction()
{
    if (m_timeline->isRunning()) {
        QMessageBox box;
        box.setText("Идет проигрывание, невозможно создать новое мероприятие.");
        box.exec();
        return;
    }
    //    if (ui->save_action->isEnabled())
    if (!m_pActiveAction->scenes(0).isEmpty())
        if(!saveActionMessageBox())
            return;

    /*
    m_pActiveAction->removeScene_s();
    m_lsmgr->on_btNewAction_clicked();
    m_tableSources->clear();
    m_tableSources->setRowCount(0);

    m_tableSceneContent->clear();
    m_tableSceneContent->setRowCount(0);
    m_timeline->reset();
    m_pGraphicsEditor->clearScheme();
    m_pGraphicsEditor->openForAction(m_pActiveAction);
    */
    on_actionCloseAction_triggered();
    m_pActiveAction->removeScene_s();
    m_lsmgr->show();
}

void MainWindow::slotCreateScenary()
{
    FillAction action(m_pActiveAction);
    connect(&action, SIGNAL(sendOpenHtml(QString)), this, SLOT(openHtml(QString)));
    action.exec();

    m_timeline->reset();
    if (m_pActiveAction->scenes().isEmpty())
        return;
    auto scenes0 = m_pActiveAction->scenes(0);
    for (int i = 0; i < scenes0.count(); i++) {
        auto scene = scenes0.at(i);
        m_timeline->addInterval(scene->id(), scene->duration(), scene->name());
    }
    m_pActiveAction->fill();
    setCanvasForAction();

    setWindowTitle();
}

void MainWindow::renameSource(QString name, QString oldName)
{
    m_pGraphicsEditor->renameSource(name,oldName);
}
/// save to last loaded file
void MainWindow::on_save_action_triggered()
{
    saveAction("");
    /*
    bool ok = m_pActiveAction->saveJson("");
    m_pGraphicsEditor->setSavePath(m_pActiveAction->loadPath());
    m_pGraphicsEditor->on_actionSave_triggered();
    ui->actionSave_Action->setEnabled(ok);
    ui->save_action->setEnabled(ok);*/
}

/**
 * @brief MainWindow::on_load_action_triggered
 * load action from selected file
 */
void MainWindow::on_load_action_triggered()
{
    if(ui->save_action->isEnabled()) {
        if(!saveActionMessageBox()) {
            return;
        }
    }
    auto fileName = QFileDialog::getOpenFileName(this,
                                                 "Открыть мероприятие",
                                                 QCoreApplication::applicationDirPath(),
                                                 tr("Act Files (*.act);;All files (*.*)"));
    if (fileName.isNull())
        return;
    loadAction(fileName);
    saveAction(temp_file_path);
}

void MainWindow::loadAction(const QString &fileName)
{
    m_modelCanva->clearScene();
    clearCanvas();
    setEnabled(false);
    if (!m_pActiveAction->loadJson(fileName)) {
        ui->createScenary->setEnabled(true);
        m_timeline->reset();
        if (!m_pActiveAction->scenes().isEmpty()) {
            auto scenes = m_pActiveAction->scenes(0);
            for (int i = 0; i < scenes.count(); i++)
                m_timeline->addInterval(
                            scenes.at(i)->id(),
                            scenes.at(i)->duration(),
                            scenes.at(i)->name());
        }
        m_pActiveAction->fill();
        setCanvasForAction();
        setWindowTitle();
        setLoadPath(fileName);
        ui->actionSave_As->setEnabled(true);
        m_pGraphicsEditor->setSavePath(fileName);
        m_pGraphicsEditor->setAction(m_pActiveAction);
        if (m_pGraphicsEditor->loadFromSaved()) {
            QMessageBox::critical(NULL, "FC1000", tr("No binded scheme"));
        }
        else {
            m_pGraphicsEditor->openForAction(m_pActiveAction);
            m_pActiveAction->setScheme(m_pGraphicsEditor->scheme());
            m_pGraphicsEditor->startAllWidgets();
        }
    }
    checkSourceItems();
    setEnabled(true);
    //    ui->actionSave_Action->setEnabled(true);
    //    ui->save_action->setEnabled(true);
}

void MainWindow::clearCanvas()
{
    m_modelCanva->clearCanva();
    m_modelCanva->clearScene();
    m_runtimeCanva->clearCanva();
}

void MainWindow::clearCanvas(QString str)
{
    if (!m_modelCanva->getScene())
        return;
    if(m_modelCanva->getScene()->name() == str) {
        m_modelCanva->clearCanva();
    }
}

void MainWindow::errorFromController(QString str)
{
    QMessageBox::critical(m_contrOptions, m_pController->getName(), str);
}


void MainWindow::messFromController(QString str)
{
    QMessageBox::information(m_contrOptions, m_pController->getName(), str);
    if (str.contains("disconnect"))
        m_runtimeCanva->clearCanva();
}
/**
* @brief set Parametrs for computing of absolute position in pixs 
*
* @param cubs_w
* @param cubs_h
* @param resol_w
* @param resol_h
* @param step_w
* @param step_h
*/
void MainWindow::setControllerParam(int cubs_w,int cubs_h,
                                    int resol_w, int resol_h,
                                    int step_w,int step_h)
{
    Q_UNUSED(cubs_w);
    Q_UNUSED(cubs_h);
    m_pController->setProperty(QString("pixs_w"),QVariant(resol_w*1000/step_w));
    m_pController->setProperty(QString("pixs_h"),QVariant(resol_h*1000/step_h));
}

/**
* @brief enable grid
*
* @param arg1 - [in] bool 
*/
void MainWindow::on_actionGrid_toggled(bool arg1)
{
    m_modelCanva->enableSmallGrid(arg1);
    m_runtimeCanva->enableSmallGrid(arg1);
    m_modelCanva->update();
    m_runtimeCanva->update();
}

/**
* @brief set optiomal fit for canvas
*/
void MainWindow::on_actionFit_triggered()
{
    int width = m_modelCanva->widthForHeight(height()/3.5);
    QSize size(width,height()/3.5);
    QResizeEvent evr(size,size);
    m_modelCanva->customResize(&evr);
    m_runtimeCanva->customResize(&evr);
}

void MainWindow::setRuntimeSceneFromTimeLine(QString str)
{
    VScene *scn = m_pActiveAction->getScene(str);
    if (scn) {
        for (int r = 0; r < m_tableScenes->rowCount(); r++) {
            auto item = m_tableScenes->item(r, 1);
            if (item->text() == str) {
                auto w = m_tableScenes->cellWidget(r, 0);
                auto radioButton = qobject_cast<QRadioButton*>(w);
                radioButton->setChecked(true);
            }
        }
        ui->actionActionClear->setEnabled(false);
        //        ui->actionPrev->setEnabled(false);
        //        ui->actionNext->setEnabled(false);
        ui->actionActionClear->setEnabled(false);
        m_runtimeCanva->setScene(scn);
    }
}

void MainWindow::onTimeLineStatus(bool status)
{
    ui->actionToRun->setEnabled(status);
}

void MainWindow::on_actionToRun_triggered()
{
    VScene *scene = m_modelCanva->getScene();
    if (scene) {
        ui->actionActionClear->setEnabled(false);
        ui->actionToRun->setEnabled(false);
        ui->actionTimePlay->setEnabled(false);
        //        ui->actionPrev->setEnabled(false);
        //        ui->actionNext->setEnabled(false);
        m_runtimeCanva->setScene(scene);
    }
}

void MainWindow::timelineChanged(QTimeLine::State state)
{
    switch (state) {
    //    case QTimeLine::NotRunning:
    //        ui->actionTimePlay->setIcon(QIcon(":/play.png"));
    //        ui->actionTimePlay->setText(tr("Пуск"));
    //        ui->actionTimePlay->setEnabled(true);
    //        ui->actionTimeStop->setEnabled(false);
    //        ui->actionNext->setEnabled(false);
    //        ui->actionPrev->setEnabled(false);
    //        ui->actionActionClear->setEnabled(true);
    //        ui->actionToRun->setEnabled(true);
    //        break;
    case QTimeLine::Running:
        ui->actionTimePlay->setIcon(QIcon(":/pause.png"));
        ui->actionTimePlay->setText(tr("Пауза"));
        //        ui->actionTimePlay->setEnabled(true);
        //        ui->actionTimeStop->setEnabled(true);
        //        ui->actionNext->setEnabled(false);
        //        ui->actionPrev->setEnabled(false);
        //        ui->actionActionClear->setEnabled(false);
        //        ui->actionToRun->setEnabled(false);
        break;
    case QTimeLine::NotRunning:
        ui->actionTimePlay->setIcon(QIcon(":/play.png"));
        ui->actionTimePlay->setText(tr("Пуск"));
        //        ui->actionTimePlay->setEnabled(true);
        ui->actionTimeStop->setEnabled(false);
        //        ui->actionNext->setEnabled(false);
        //        ui->actionPrev->setEnabled(false);
        break;
    case QTimeLine::Paused:
        ui->actionTimePlay->setIcon(QIcon(":/play.png"));
        ui->actionTimePlay->setText(tr("Пуск"));
        //        ui->actionTimePlay->setEnabled(true);
        ui->actionTimeStop->setEnabled(true);
        ui->actionNext->setEnabled(true);
        ui->actionPrev->setEnabled(true);
        break;
    default:
        break;
    }
    controllerStatusChanged();
}

void MainWindow::timelineFinished()
{
    timelineChanged(QTimeLine::NotRunning);
}

void MainWindow::on_actionTimePlay_triggered()
{
    if (!m_pActiveAction)
        return;

    if (ui->actionTimePlay->isEnabled())
        ui->actionTimeStop->setEnabled(true);
    if (!m_timeline->isRunning()) {
        ui->actionTimePlay->setText(tr("Pause"));
        ui->actionTimePlay->setIcon(QIcon(":/pause.png"));
        ui->actionNext->setEnabled(false);
        ui->actionPrev->setEnabled(false);
        m_timeline->play();
    } else {
        ui->actionTimePlay->setText(tr("Start"));
        ui->actionTimePlay->setIcon(QIcon(":/play.png"));
        ui->actionNext->setEnabled(true);
        ui->actionPrev->setEnabled(true);
        m_timeline->pause();
    }
    controllerStatusChanged();
}

void MainWindow::on_actionTimeStop_triggered()
{
    ui->actionTimeStop->setEnabled(false);
    m_timeline->stop();
    m_timeline->reset();
    for (int i = 0; i < m_pActiveAction->scenes(0).count(); i++) {
        auto scene = m_pActiveAction->scenes(0).at(i);
        m_timeline->addInterval(scene->id(),
                                scene->duration(),
                                scene->name());
    }
    QIcon icon(":/play.png");
    ui->actionTimePlay->setIcon(icon);
    ui->actionTimePlay->setText(tr("Start"));
    ui->actionStart->setIcon(icon);
    VScene *scn = m_modelCanva->getScene();
    if (scn) {
        ui->actionToRun->setEnabled(true);
    } else {
        ui->actionToRun->setEnabled(false);
    }
}

void MainWindow::fillTimeline()
{
    m_timeline->reset();
    if (m_pActiveAction->scenes().isEmpty())
        return;
    auto scenes = m_pActiveAction->scenes(0);
    for (auto scene : scenes) {
        m_timeline->addInterval(scene->id(), scene->duration(), scene->name());
    }
}

void MainWindow::setWindowTitle()
{
    auto name = m_pActiveAction->name();
    QWidget::setWindowTitle(name + " - " + "Витраж");
}

void MainWindow::setLoadPath(const QString &path)
{
    m_loadPath = path;
}

QString MainWindow::loadPath() const
{
    return m_loadPath;
}

void MainWindow::setConfigs(int wallW, int wallH, int cellw, int cellH, int resolvW, int resolvH)
{
    m_wallW = wallW;
    m_wallH = wallH;
    m_cellH = cellH;
    m_cellW = cellw;
    m_resolvW = resolvW;
    m_resolvH = resolvH;
}

void MainWindow::setConfigs2(int wallW, int wallH, int resolvW, int resolvH, int cellw, int cellH)
{
    m_wallW = wallW;
    m_wallH = wallH;
    m_cellH = cellH;
    m_cellW = cellw;
    m_resolvW = resolvW;
    m_resolvH = resolvH;
}

void MainWindow::actionModefied()
{
    ui->actionSave_As->setEnabled(true);
    ui->actionSave_Action->setEnabled(true);
    ui->save_action->setEnabled(true);
}

void MainWindow::schemeLoadedInEditor()
{
    actionModefied();
}

void MainWindow::enableButtons(bool previous, bool next)
{
    ui->actionPrev->setEnabled(previous);
    ui->actionNext->setEnabled(next);
}

void MainWindow::enableToRun(bool b)
{
    ui->actionToRun->setEnabled(b);
    ui->actionTimePlay->setEnabled(b);
}

void MainWindow::hideGraphicsEditor()
{
    on_actionGrahics_Editor_triggered(false);
}

void MainWindow::on_actionControllerParams_triggered()
{
    m_pOptionsDialog->open();
}

void MainWindow::createCollapsibles()
{
    m_tableSources = new SourceTableWidget;
    m_tableScenes = new QTableWidget;
    m_tableSceneContent = new QTableWidget;

    m_SourceManager = new CollapsibleFrame(this,tr("Sources Manager"));
    m_ScenesManager = new CollapsibleFrame(this,tr("Scenes Manager"));
    m_SceneContentManager = new CollapsibleFrame(this,tr("Scene's content"));

    m_SourceManager->hideHeader();
    m_SourceManager->setTopBackground(QColor(102,102,102));
    m_SourceManager->addWidget(m_tableSources);

    m_ScenesManager->addUpDown();
    m_ScenesManager->setMoveDownEnable(false);
    m_ScenesManager->setMoveUpEnable(false);
    m_ScenesManager->hideHeader();
    m_ScenesManager->setTopBackground(QColor(102,102,102));
    m_ScenesManager->addWidget(m_tableScenes);


    m_SceneContentManager->hideHeader();
    m_SceneContentManager->hideButton();
    m_SceneContentManager->setTopBackground(QColor(153,153,153));
    m_SceneContentManager->addWidget(m_tableSceneContent);
    m_SceneContentManager->setHeaderHeight(20);

    m_ScenesManager->addWidget(m_SceneContentManager);
    m_ScenesManager->addWidget(m_tableScenes);
    m_ScenesManager->addWidget(m_SceneContentManager);

    setOptionsForTable(m_tableSources);
    setOptionsForTable(m_tableScenes);
    setOptionsForTable(m_tableSceneContent);

    connect(m_tableScenes, SIGNAL(cellClicked(int,int)), this, SLOT(sceneCellClicked(int)));

    m_ScenesManager->changeState();
    m_SourceManager->changeState();
    m_SceneContentManager->changeState();
}
/**
* @brief fill options for table in collapsible frame
*
* @param table - [in] QTableWidget*
*/
void MainWindow::setOptionsForTable(QTableWidget *table)
{
    table->setSelectionMode(QAbstractItemView::SingleSelection);
    table->setSelectionBehavior(QAbstractItemView::SelectRows);
    table->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    table->setShowGrid(false);
    table->setDragEnabled(true);
    table->setMinimumHeight(30);
    table->setStyleSheet("QTableWidget { background:rgb(225,225,225); }");
}

void MainWindow::initStartingMenu()
{
    ui->actionSave_Action->setEnabled(false);
    ui->actionSave_As->setEnabled(false);
    ui->save_action->setEnabled(false);
}

void MainWindow::initAllDockWidgets(QSettings &settings)
{
    dockSources = new QDockWidget;
    dockScenes = new QDockWidget;
    dockContents = new QDockWidget("Контент сцены");
    dockSources->setObjectName("srcDock");
    dockScenes->setObjectName("scnDock");
    dockContents->setObjectName("dockContent");
    dockSources->setWindowTitle(tr("Sources Manager"));
    dockScenes->setWindowTitle(tr("Scenes Manager"));

    m_innerMW = new QMainWindow;
    centralWidget()->layout()->addWidget(m_innerMW);

    m_innerMW->addDockWidget(Qt::RightDockWidgetArea, m_pGraphicsEditor->dock());
    m_innerMW->addDockWidget(Qt::RightDockWidgetArea, m_runtimeCanva->dock());
    m_innerMW-> addDockWidget(Qt::RightDockWidgetArea, m_modelCanva->dock());

    m_runtimeCanva->dock()->setObjectName("RuntimeDock");
    m_modelCanva->dock()->setObjectName("ModelDock");
    dockSources->setWidget(m_SourceManager);
    dockScenes->setWidget(m_ScenesManager);
    dockContents->setWidget(m_SceneContentManager);
    addDockWidget(Qt::LeftDockWidgetArea, dockSources);
    addDockWidget(Qt::LeftDockWidgetArea, dockScenes);
    addDockWidget(Qt::LeftDockWidgetArea, dockContents);
    m_timeline = new TimelineWidget(this);
    m_innerMW->addDockWidget(Qt::RightDockWidgetArea,m_timeline->dock());
    restoreState(settings.value("MainWindowState").toByteArray());
    restoreGeometry(settings.value("MainWindowGeometry").toByteArray());
    m_innerMW->restoreState(settings.value("InnerMainWindowState").toByteArray());
    m_innerMW->restoreGeometry(settings.value("InnerMainWindowGeometry").toByteArray());
    m_pGraphicsEditor->restoreGeometry(settings.value("GEditorGeometry").toByteArray());
    m_pGraphicsEditor->restoreState(settings.value("GEditorState").toByteArray());

    /* checks for menu view */
    ui->actionViewSources->setChecked(!dockSources->isHidden());
    ui->actionViewScenes->setChecked(!dockScenes->isHidden());
    ui->actionViewContents->setChecked(!dockContents->isHidden());

    ui->actionGrahics_Editor->setChecked(!m_pGraphicsEditor->dock()->isHidden());
    ui->actionModelWall->setChecked(!m_modelCanva->dock()->isHidden());
    ui->actionRunTime_Wall->setChecked(!m_runtimeCanva->dock()->isHidden());
    ui->actionTimeline_Widget->setChecked(!m_timeline->dock()->isHidden());

    connect(dockSources,SIGNAL(visibilityChanged(bool)), ui->actionViewSources, SLOT(setChecked(bool)));
    connect(dockScenes,SIGNAL(visibilityChanged(bool)), ui->actionViewScenes, SLOT(setChecked(bool)));
    connect(dockContents,SIGNAL(visibilityChanged(bool)), ui->actionViewContents, SLOT(setChecked(bool)));

    connect(m_timeline->dock(),SIGNAL(visibilityChanged(bool)), ui->actionTimeline_Widget,SLOT(setChecked(bool)));
    connect(m_runtimeCanva->dock(),SIGNAL(visibilityChanged(bool)), ui->actionRunTime_Wall,SLOT(setChecked(bool)));
    connect(m_modelCanva->dock(),SIGNAL(visibilityChanged(bool)), ui->actionModelWall,SLOT(setChecked(bool)));
    connect(m_pGraphicsEditor->dock(),SIGNAL(visibilityChanged(bool)), ui->actionGrahics_Editor,SLOT(setChecked(bool)));
}

void MainWindow::saveSettings()
{
    QSettings settings(support::getSettingsPath(), QSettings::IniFormat);
    settings.setValue("MainWinX", x());
    settings.setValue("MainWinY", y());
    settings.setValue("MainWinW", width());
    settings.setValue("MainWinH", height());
    //settings.setValue("FontFamily", qApp->font().family());
    //settings.setValue("FontSize", qApp->font().pointSize());
    settings.setValue("WallWidth",m_wallW);
    settings.setValue("WallHeight",m_wallH);
    settings.setValue("CellWidth",m_cellW);
    settings.setValue("CellHeight",m_cellH);
    settings.setValue("ResolvWidth",m_resolvW);
    settings.setValue("ResolvHeight",m_resolvH);
    settings.setValue("MainWindowState",saveState());
    settings.setValue("MainWindowGeometry",saveGeometry());
    settings.setValue("InnerMainWindowState",m_innerMW->saveState());
    settings.setValue("InnerMainWindowGeometry",m_innerMW->saveGeometry());
    settings.setValue("GEditorGeometry",m_pGraphicsEditor->saveGeometry());
    settings.setValue("GEditorState",m_pGraphicsEditor->saveState());
    m_pMixerOptions->saveSettings(&settings);
    m_pExtronOptions->saveSettings(&settings);
    if (!m_pController)
        return;
    QVariant host,port,user,pass,timeout,file;
    m_pController->getProperty("host",host);
    settings.setValue("ControllerHost",host.toString());
    m_pController->getProperty("port",port);
    settings.setValue("ControllerPort",port.toString());
    m_pController->getProperty("username",user);
    settings.setValue("ControllerUsername",user.toString());
    m_pController->getProperty("passwd",pass);
    settings.setValue("ControllerPasswd",pass.toString());
    m_pController->getProperty("timeout",timeout);
    settings.setValue("ControllerTimeout",timeout.toString());
    m_pController->getProperty("fileName",file);
    settings.setValue("ControllerFileName",file.toString());
    settings.setValue("ControllerState",static_cast<int>(m_pController->status()));
}

void MainWindow::openResources()
{
    if(!QResource::registerResource(RES_PATH)) {
        if (!QResource::registerResource(RES_PATH_DEF)) {
            //  QMessageBox::critical(NULL,tr("Resources"),tr("Can,t load resources"));
            // exit(0);
        }
    }
}

void MainWindow::initPlugins()
{
    if (!PluginsManager::Instance().loadPlugins()) {
        qCritical("Fail to load plugins");
    }
    if (!PluginsManager::Instance().extronFlag()) {
        qCritical("No extron Loaded");
        m_pOptionsTW->removeTab(1);
    } else {
        connect(m_pExtronOptions,SIGNAL(switchOnStateChanged(bool)),this,SLOT(onOffExtron(bool)));
        m_pExtronOptions->setController(&PluginsManager::Instance().extron());
    }
    if (!PluginsManager::Instance().mixerFlag()) {
        qCritical("No mixer loaded");
        m_pOptionsTW->removeTab(2);
    } else {
        connect(m_pMixerOptions,SIGNAL(switchOnStateChanged(bool)),this,SLOT(onOffMixer(bool)));
        m_pMixerOptions->setController(&PluginsManager::Instance().mixer());
    }
}

void MainWindow::createDefaultScene()
{
    m_tableScenes->clear();
    m_tableScenes->setRowCount(0);
    auto scenes = m_pActiveAction->scenes();
    if (!scenes.isEmpty())
        return;
    FillAction action(m_pActiveAction);
    action.addNewScene();
    action.saveScenes();
    // init timeline widget
    m_timeline->stop();
    m_timeline->reset();
    for (int i = 0; i < m_pActiveAction->scenes(0).count(); i++) {
        auto scene = m_pActiveAction->scenes(0).at(i);
        m_timeline->addInterval(scene->id(), scene->duration(), scene->name());
    }
}

void MainWindow::setResizeModeTables()
{
    auto setResizeMode = [](QTableWidget *table, const std::vector<uint> &columns){
        table->horizontalHeader()->hide();
        table->verticalHeader()->hide();
        table->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
        for (auto col : columns) {
            table->horizontalHeader()->setResizeMode(col, QHeaderView::ResizeToContents);
        }
    };
    setResizeMode(m_tableSources, {0,2});
    setResizeMode(m_tableScenes, {0,3,4,5});
    setResizeMode(m_tableSceneContent, {0,2,3});
}

void MainWindow::checkSourceItems()
{
    if (!m_tableSources) {
        timerCheckSourceItems.stop();
        return;
    }
    auto rowCount = m_tableSources->rowCount();
    if (!rowCount) {
        timerCheckSourceItems.stop();
        return;
    }
    auto rowBegin = m_tableSources->rowAt(0);
    auto rowEnd = m_tableSources->rowAt(m_tableSources->height()+10);
    if (rowEnd < 0)
        rowEnd = rowCount;

    for (int r = 0; r < rowCount; r++) {
        auto item = m_tableSources->item(r, 1);
        if (item) {
            auto name = item->text();
            auto source = m_pActiveAction->getSource(name);
            if (source) {
                if (r >= rowBegin && r < rowEnd) {
                    if (source->gettype() == "VNCClient") {
                        auto status = source->getStatus();
                        if (status == VSource::ExecStatus::RunStatus || status == VSource::ExecStatus::ErrorStatus)
                            continue;
                        source->start();
                        source->setStatus(VSource::ExecStatus::RunStatus);
                    }
                }
                else {
                    source->stop();
                }
            }
        }
    }
    timerCheckSourceItems.start(3000);

    saveAction(temp_file_path);
}

void MainWindow::fucker() {
//    qDebug() << "MAINFUCKER" <<timeee.elapsed();
}


/*
void MainWindow::initServer()
{
    udpSocket.bind(QHostAddress("10.0.2.15"), 7755);
    //timer.start(500);
    connect(&timer, SIGNAL(timeout()), this, SLOT(broadcastDatagram()));
    connect(&udpSocket, SIGNAL(readyRead()), this, SLOT(readPendingDatagrams()));
}

void MainWindow::broadcastDatagram()
{
    QPixmap pixmap(size());
    render(&pixmap);

    QByteArray datagram;
    QDataStream stream(&datagram, QIODevice::WriteOnly);
    stream << pixmap;
    udpSocket.writeDatagram(datagram.data(), datagram.size(), QHostAddress("10.0.2.15"), 60100);
}

QDebug operator<<(QDebug dbg, const vitrage_data &data)
{
    dbg.nospace() << "-- " << data.state << " " << data.event << " ";
    dbg.nospace() << "-- " << data.point;

    return dbg.space();
}

void MainWindow::readPendingDatagrams()
{
    while (udpSocket.hasPendingDatagrams()) {
        QByteArray datagram;
        datagram.resize(udpSocket.pendingDatagramSize());
        QHostAddress sender;
        quint16 senderPort;

        udpSocket.readDatagram(datagram.data(), datagram.size(), &sender, &senderPort);

        QDataStream stream(datagram);
        vitrage_data data;
        stream >> data;

        qDebug() << data;

        if (!data.state) {
            QMessageBox::information(this, "Info", "Administrator take off control");
        }
        if (data.state && !control) {
            QMessageBox::information(this, "Info", "Administrator take in control");
        }
        control = data.state;
        QMouseEvent event(QEvent::MouseMove, data.point, Qt::LeftButton, 0, 0);
        QApplication::sendEvent(this, &event);
    }
}
*/
void MainWindow::onOffMixer(bool flag)
{
    if (!m_pController) return;
    if (flag) {
        PluginsManager::Instance().mixer().conn();
    } else {
        PluginsManager::Instance().mixer().disconn();
    }
}

void MainWindow::onOffExtron(bool flag)
{
    if (!m_pController) return;
    if (flag) {
        PluginsManager::Instance().extron().conn();
    } else PluginsManager::Instance().extron().disconn();
}

bool MainWindow::saveActionMessageBox()
{
    if (!m_pActiveAction->activeScene())
        return true;
    QString str = tr("Save action ") + "\"" + m_pActiveAction->name() + "\"" + tr(" before close");
    QMessageBox box;
    box.setWindowTitle("Сохранить мероприятие");
    box.setText(str);
    QPushButton *yes = box.addButton(tr("Yes"), QMessageBox::ActionRole);
    QPushButton *no = box.addButton(tr("No"), QMessageBox::ActionRole);
    QPushButton *cancel = box.addButton(tr("Cencel"), QMessageBox::ActionRole);
    box.exec();
    if (box.clickedButton() == yes) {
        saveAction("");
        /*
        if (m_pActiveAction->saveJson("")) {
            return true;
        }
        m_pGraphicsEditor->setSavePath(m_pActiveAction->loadPath());
        m_pGraphicsEditor->on_actionSave_triggered();*/
        return true;
    }
    else if (box.clickedButton() == no) {
        return true;
    }
    else if (box.clickedButton() ==  cancel) {
        return false;
    }
    return true;
}

void MainWindow::optionsWidgetsSetup()
{
    m_pOptionsDialog = new QDialog(this);
    m_pOptionsDialog->resize(200,200);
    m_pOptionsDialog->setLayout(new QVBoxLayout(m_pOptionsDialog));
    m_pOptionsDialog->setWindowTitle(tr("Hardware options"));
    m_pOptionsTW = new QTabWidget(this);
    m_pOptionsDialog->layout()->addWidget(m_pOptionsTW);
    m_contrOptions = new ControllerOptions(this);
    m_pExtronOptions = new ExtronOptions(this);
    m_pMixerOptions = new MixerOptions(this);
    m_pOptionsTW->addTab(m_contrOptions,tr("Controller"));
    m_pOptionsTW->addTab(m_pExtronOptions,tr("Extron"));
    m_pOptionsTW->addTab(m_pMixerOptions,tr("Mixer"));

    connect(m_contrOptions,     SIGNAL(sendOpenHtml(QString)), this, SLOT(openHtml(QString)));
    connect(m_pExtronOptions,   SIGNAL(sendOpenHtml(QString)), this, SLOT(openHtml(QString)));
    connect(m_pMixerOptions,    SIGNAL(sendOpenHtml(QString)), this, SLOT(openHtml(QString)));
}
/**
* @brief resize table with scene's content
*/
void MainWindow::showContent(void)
{
    m_ScenesManager->setInnerSplitter(m_contentOldSize);
}

void MainWindow::smallSplitterMoved(int pos,int index)
{
    Q_UNUSED(pos);
    Q_UNUSED(index);
    if (m_SceneContentManager->isCollapsed()) {
        QList<int> list = m_ScenesManager->splitter()->sizes();
        list[1] = 20;
        m_ScenesManager->setSplitterSizes(m_InnerSplitterSizes);
    }
    m_InnerSplitterSizes = m_ScenesManager->getSplitterSize();
}

void MainWindow::sourceClickedOnCanva()
{
    QString sceneName = m_modelCanva->getScene()->name();
    for (int i = 0; i < m_tableScenes->rowCount(); i++) {
        if (m_tableScenes->item(i,VAction::SCN_NAME)->text() == sceneName) {
            sceneCellClicked(i);
        }
    }
}

void MainWindow::setControllerConnects(VController *controller)
{
    if (controller) {
        connect(controller, SIGNAL(errorSig(QString)),this, SLOT(errorFromController(QString)),Qt::QueuedConnection);
        connect(controller, SIGNAL(messSig(QString)),this, SLOT(messFromController(QString)), Qt::QueuedConnection);
        connect(m_canvaOptions,SIGNAL(setOptionsCanva(int,int,int,int,int,int)),
                this,SLOT(setControllerParam(int,int,int,int,int,int)),Qt::QueuedConnection);
        connect(controller,SIGNAL(statusChanged()),this, SLOT(controllerStatusChanged()),Qt::QueuedConnection);
        connect(controller, SIGNAL(sceneFinished(bool)), this, SLOT(sceneFinishedCond(bool)), Qt::QueuedConnection);

        m_runtimeCanva->setController(controller);
        setControllerParam(m_wallW, m_wallH, m_resolvW, m_resolvH, m_cellW, m_cellH);
    }
}

/**
* @brief scene in scenetable clicked
*
* @param row - [in] int
* @param column - [in] int
*/
void MainWindow::sceneCellClicked(int row)
{
    QTableWidgetItem *wgt = m_tableScenes->item(row, VAction::SCN_NAME);
    if (!wgt)
        return;
    auto text = wgt->text();
    m_pActiveAction->fillContentForScene(text);
}

void MainWindow::controllerStatusChanged()
{
    if (!m_pController)
        return;
    auto enableControllerButtons = [this](bool on){
        m_contrOptions->stateChanged(on);
        if (m_timeline->isRunning()) {
            ui->actionToRun->setEnabled(false);
            ui->actionActionClear->setEnabled(false);
        }
        else {
            ui->actionToRun->setEnabled(on);
            ui->actionActionClear->setEnabled(on);
        }
        ui->actionTimePlay->setEnabled(on);

    };
    switch(m_pController->status()) {
    case (status_t::start) :
        m_pStatusBar->setConnectStatus(tr("Connection started"));
        m_pStatusBar->setMessage(m_pController->errorMsg());
        enableControllerButtons(false);
        break;
    case (status_t::connected) :
        m_pStatusBar->setConnectStatus(tr("Connected OK"));
        m_pStatusBar->setMessage("Контроллер подключен");
        enableControllerButtons(true);
        break;

    case (status_t::stopped)  :
        m_pStatusBar->setConnectStatus(tr("Not Connected"));
        m_pStatusBar->setMessage(tr("Controller Stopped"));
        enableControllerButtons(false);
        break;

    case (status_t::disc)     :
        m_pStatusBar->setConnectStatus(tr("Disconnected"));
        m_pStatusBar->setMessage(tr("Controller disconnected"));
        m_contrOptions->stateChanged(false);
        enableControllerButtons(false);
        break;

    case (status_t::error)    :
        m_pStatusBar->setConnectStatus(tr("Connection Error"));
        m_pStatusBar->setMessage(m_pController->errorMsg());
        enableControllerButtons(false);
        break;
    case status_t::busy:
        enableControllerButtons(false);
        break;
    case status_t::unbusy:
        enableControllerButtons(true);
        break;
    }
}

/**
* @brief set selected(by radiobutton) scene
*
* @param str - [in] QString 
*/
void MainWindow::sceneRadioSet(QString str)
{
    VScene *scene = m_pActiveAction->getScene(str);
    if (scene) {
        m_modelCanva->setScene(scene);
    }
}

void MainWindow::openGEditor()
{
    m_pGraphicsEditor->dock()->show();
}

void MainWindow::openGEditorForAction(VAction *act)
{
    m_pGraphicsEditor->openForAction(act);
}

void MainWindow::setCanvasForAction()
{
    m_pActiveAction->setCanva(VAction::CanvaType::Runtime, m_runtimeCanva);
    m_pActiveAction->setCanva(VAction::CanvaType::Model, m_modelCanva);
}

void MainWindow::on_actionOpenScheme_triggered()
{
    m_pGraphicsEditor->setMode(VGraphicsEditor::G_RUNTIME);
    openGEditor();
}

void MainWindow::actionUpdatedInManager()
{
    createDefaultScene();
    m_pActiveAction->fill();
    setFirstScene();
    setCanvasForAction();
    ui->createScenary->setEnabled(true);
    m_pGraphicsEditor->openForAction(m_pActiveAction);
    auto message = "Новое мероприятие " + m_pActiveAction->name();
    m_pStatusBar->setMessage(message);
    setWindowTitle();
    checkSourceItems();
    ui->save_action->setEnabled(true);
}

void MainWindow::setFirstScene()
{
    if (m_tableScenes->rowCount() > 0) {
        auto cell = m_tableScenes->cellWidget(0,0);
        auto radioButton = qobject_cast<QRadioButton*>(cell);
        radioButton->setChecked(true);
    }
}

void MainWindow::on_actionClose_triggered()
{
    close();
}

void MainWindow::on_actionLoad_Action_triggered()
{
    on_load_action_triggered();
}

void MainWindow::on_actionSave_As_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this, "Сохранить мероприятие",
                                                    QCoreApplication::applicationDirPath(),
                                                    tr("Act files (*.act);;All files (*.*)"));
    if (fileName.isNull()) {
        return;
    }
    saveAction(fileName);
}

void MainWindow::saveAction(const QString &fileName)
{
    if (m_pActiveAction->saveJson(fileName))
        return;
    m_pGraphicsEditor->setSavePath(fileName);
    m_pGraphicsEditor->on_actionSave_triggered();
    ui->actionSave_Action->setEnabled(false);
    ui->save_action->setEnabled(false);
}

void MainWindow::on_actionSave_Action_triggered()
{
    on_save_action_triggered();
}

void MainWindow::on_actionOpen_Graphics_triggered()
{
    on_actionOpenScheme_triggered();
}

void MainWindow::actionTimeChanged()
{
    ui->actionStop->setEnabled(ui->actionTimeStop->isEnabled());
    ui->actionStart->setEnabled(ui->actionTimePlay->isEnabled());
}

void MainWindow::on_actionStop_triggered()
{
    on_actionTimeStop_triggered();
}

void MainWindow::on_actionStart_triggered()
{
    on_actionTimePlay_triggered();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (ui->actionSave_Action->isEnabled()) {
        QMessageBox msgBox;
        msgBox.setText("Сохранить мероприятие перед закрытием");
        QPushButton *yes = msgBox.addButton(tr("Yes (Save) "), QMessageBox::ActionRole);
        QPushButton *no = msgBox.addButton(tr("No (Delete changes)"),QMessageBox::ActionRole);
        QPushButton *cancel = msgBox.addButton(tr("Cencel"),QMessageBox::ActionRole);
        msgBox.exec();
        if (msgBox.clickedButton() == yes) {
            saveAction("");
            /*
            if(m_pActiveAction->saveJson("")) {
                event->ignore();
                return;
            }
            m_pGraphicsEditor->setSavePath(m_pActiveAction->loadPath());
            m_pGraphicsEditor->on_actionSave_triggered();
            */
        } else if (msgBox.clickedButton() == no) {
            /* return to editor */
        } else if (msgBox.clickedButton() ==  cancel) {
            event->ignore();
            return;
        }
    }
    saveSettings();
    helpWidget->close();
    m_pGraphicsEditor->close();
    m_lsmgr->close();
    m_contrOptions->close();
    m_canvaOptions->close();
    event->accept();

    QFile(temp_file_path).remove();
}

void MainWindow::on_actionRunTime_Wall_triggered(bool checked)
{
    m_runtimeCanva->dock()->setShown(checked);
}

void MainWindow::on_actionGrahics_Editor_triggered(bool checked)
{
    m_pGraphicsEditor->dock()->setShown(checked);
}

void MainWindow::on_actionTimeline_Widget_triggered(bool checked)
{
    m_timeline->dock()->setShown(checked);
}

void MainWindow::on_actionViewSources_triggered(bool checked)
{
    dockSources->setShown(checked);
}

void MainWindow::on_actionViewScenes_triggered(bool checked)
{
    dockScenes->setShown(checked);
}

void MainWindow::on_actionViewContents_triggered(bool checked)
{
    dockContents->setVisible(checked);
}

void MainWindow::on_actionModelWall_triggered(bool checked)
{
    m_modelCanva->dock()->setShown(checked);
}

void MainWindow::on_actionZoomIn_triggered()
{
    m_modelCanva->zoomFor(100);
}

void MainWindow::on_actionZoomOut_triggered()
{
    m_modelCanva->zoomFor(-100);
}

void MainWindow::on_actionNext_triggered()
{
    m_timeline->setNext();
}

void MainWindow::on_actionPrev_triggered()
{
    m_timeline->setPrev();
}

void MainWindow::on_actionSources_triggered()
{
    m_lsmgr->show();
}

void MainWindow::on_actionEditSource_triggered()
{
    m_lsmgr->show();
    //    m_lsmgr->fillDevices(m_pActiveAction);
    //    m_lsmgr->showEditSource();
}

void MainWindow::on_actionPlay_active_scene_triggered()
{
    on_actionToRun_triggered();
}

void MainWindow::on_actionWall_options_triggered()
{
    openCanvaOptions();
}

void MainWindow::on_actionController_options_triggered()
{
    on_actionControllerParams_triggered();
}

void MainWindow::on_actionStatusBar_triggered(bool checked)
{
    ui->statusbar->setVisible(checked);
}

void MainWindow::on_actionStandart_triggered()
{
    ui->toolBar->setVisible(ui->actionStandart->isChecked());
}

void MainWindow::on_actionPlaying_triggered()
{
    ui->toolBarPlaying->setVisible(ui->actionPlaying->isChecked());
}

void MainWindow::on_actionConfigurations_triggered()
{
    ui->toolBarConfig->setVisible(ui->actionConfigurations->isChecked());
}

void MainWindow::on_actionScale_triggered()
{
    on_actionFit_triggered();
}

void MainWindow::setController(VController *ctr)
{
    m_pController = ctr;
    setControllerConnects(ctr);
    ctr->conn();
}

void MainWindow::on_actionCascade_triggered()
{
    int hc =  m_modelCanva->getheight_cells();
    int wc = m_modelCanva->getwidth_cells();
    int h = m_modelCanva->getheight();
    int w = m_modelCanva->getwidth();

    VScene *scn = m_modelCanva->getScene();
    if (!scn) return;
    QVector<frame> cnt = scn->getContent();
    if (!cnt.count() || (!(cnt.count()-1))) return;
    scn->getContent().clear();
    int h_step = ((h-1) * hc)    /(cnt.count()-1);
    int w_step = ((w-1) * wc) / (cnt.count()-1);
    if (!h_step ) h_step = 1;
    if (!w_step ) w_step = 1;

    int x = 0;
    int y = 0;
    foreach(frame fr, cnt) {
        fr.pos.lt = QPoint(x,y);
        fr.pos.rb = QPoint(x+wc,y+hc);
        if ((x+wc < w*wc) && (y + hc < h*hc)) {
            y+=h_step;
            x+=w_step;
        }
        scn->getContent().push_back(fr);
    }
    m_modelCanva->setScene(scn);
}

void MainWindow::on_actionActionTile_triggered()
{
    int hc = m_modelCanva->getheight_cells();
    int wc = m_modelCanva->getwidth_cells();
    int h = m_modelCanva->getheight();
    int w = m_modelCanva->getwidth();
    int x = 0;
    int y = 0;

    VScene *scn = m_modelCanva->getScene();
    if (!scn) return;
    QVector<frame> cnt = scn->getContent();
    QVector<frame>::iterator it = cnt.begin();
    if (!cnt.count())  return;
    scn->getContent().clear();

    int rows = sqrt(cnt.count());
    int columns  = cnt.count()/rows;

    int h_size = ((h) * hc) / (rows);
    int w_size = ((w) * wc) / (columns);
    if (!h_size ) h_size = 1;

    for (int i = 0; i < rows; i++) {

        if (i == rows-1) {
            if(cnt.count() > rows*columns) {
                columns+=cnt.count()- rows*columns;
                w_size = ((w) * wc) / (columns);
            }
        }

        if (!w_size ) w_size= 1;
        for (int j = 0; j < columns;j++) {

            it->pos.lt = QPoint(j*w_size,i*h_size);
            /* more than cells*/

            x = (j+1)*(w_size);
            y = (i+1)*(h_size);

            if (j == columns-1) {
                x = wc*w;
            }
            if (i == rows-1) {
                y = hc*h;
            }

            /* more than cells */
            if ((x > wc*w) || (y > hc*h)) {
                it->pos.lt = QPoint(w*wc-1,h*hc-1);
                x = w*wc;
                y = h*hc;
            }

            it->pos.rb = QPoint(x,y);

            if (it == cnt.end()){
                break;
            }
            scn->getContent().push_back(*it);
            it++;
        }
    }
    m_modelCanva->setScene(scn);
}

void MainWindow::on_actionActionClear_triggered()
{
    m_runtimeCanva->setScene(m_bareScene);
}

QString MainWindow::getTranslatePath()
{
    QString translateFilePath = FIRST_SHARE_PATH
            + QFileInfo(QString(qApp->argv()[0])).fileName()
            + TRANSLATE_DIR_NAME;
    QDir dir(translateFilePath);
    if(dir.exists() && dir.cd(translateFilePath))
    {
        return translateFilePath;
    }
    else
    {
        translateFilePath = QDir::current().canonicalPath() + TRANSLATE_DIR_NAME;
        if(!QDir(translateFilePath).exists())
        {
            translateFilePath = QFileInfo(QString(qApp->argv()[0])).path()+ TRANSLATE_DIR_NAME;
        }
    }

    return translateFilePath;
}

void MainWindow::sceneFinishedCond(bool ok)
{
    qDebug() << "MainWindow::sceneFinishedCond "<< ok;
    controllerStatusChanged();
    if (!ok) {
        QMessageBox::information(this, "!!!", "Controller fail to set scene");
        return;
    }
    m_runtimeCanva->setVisibleAll(true);
}

void MainWindow::getLoadStatus(int i, int c)
{
    QString l = tr("Loading sources ") + QString::number(i) + tr(" from ") + QString::number(c);
    if (i == c) {
        l = "Сценарий: " + m_pActiveAction->name();
    }
    m_pStatusBar->setMessage(l);
}

void MainWindow::onSourceMoved()
{
    m_pActiveAction->renew();
}

void MainWindow::on_actionAboutVitrage_triggered()
{
    aboutWind->setShown(false);
    aboutWind->show();
}

void MainWindow::on_actionHelp_triggered()
{
    openHtml("index.html");
}

void MainWindow::on_actionHelpMainWindow_triggered()
{
    openHtml("index.html?21.htm");
}

void MainWindow::on_actionCloseAction_triggered()
{
    QWidget::setWindowTitle("Новое мероприятие - Витраж");
    m_lsmgr->setActionName("Новое мероприятие");
    m_lsmgr->on_btRemAll_clicked();

    m_modelCanva->clearCanva();
    m_timeline->reset();

    m_pActiveAction->clearSources();
    m_pActiveAction->clearScenes();

    ui->actionSave_Action->setEnabled(false);
    ui->actionSave_As->setEnabled(false);
    ui->actionCloseAction->setEnabled(false);
}

void MainWindow::on_actionChangeUiMode_triggered(bool flag)
{
    auto docks = { dockSources, dockScenes, dockContents, m_modelCanva->dock(), m_runtimeCanva->dock(),
                   m_timeline->dock() };
    if (flag) {
        ui->menuCanva->setEnabled(false);
        auto f = [=]() { for (auto dock : docks) dock->setFeatures(QDockWidget::NoDockWidgetFeatures); };
        f();
    }
    else {
        ui->menuCanva->setEnabled(true);
        auto f = [=]() { for (auto dock : docks) dock->setFeatures(QDockWidget::AllDockWidgetFeatures); };
        f();
    }
}

void MainWindow::on_actionResetUi_triggered()
{
    resize(640,480);
    move(20,20);

    restoreDockWidget(dockSources);
    restoreDockWidget(dockScenes);
    restoreDockWidget(dockContents);
    m_innerMW->removeDockWidget(m_modelCanva->dock());
    m_innerMW->removeDockWidget(m_runtimeCanva->dock());
    m_innerMW->removeDockWidget(m_pGraphicsEditor->dock());
    m_innerMW->removeDockWidget(m_timeline->dock());

    addDockWidget(Qt::LeftDockWidgetArea, dockSources);
    addDockWidget(Qt::LeftDockWidgetArea, dockScenes);
    addDockWidget(Qt::LeftDockWidgetArea, dockContents);
    m_innerMW->addDockWidget(Qt::RightDockWidgetArea, m_pGraphicsEditor->dock());
    m_innerMW->addDockWidget(Qt::RightDockWidgetArea, m_runtimeCanva->dock());
    m_innerMW->addDockWidget(Qt::RightDockWidgetArea, m_modelCanva->dock());
    m_innerMW->addDockWidget(Qt::RightDockWidgetArea, m_timeline->dock());

    dockSources->show();
    dockScenes->show();
    dockContents->show();
    m_modelCanva->dock()->show();
    m_runtimeCanva->dock()->show();
    m_timeline->dock()->show();
}

void MainWindow::openHtml(const QString &page)
{
    QString url = "file:///" + QCoreApplication::applicationDirPath() + "/html/" + page;
    if (!url.isEmpty()) {
        helpWebView.load(QUrl(url));
        helpWidget->setWindowModality(Qt::WindowModal);
        helpWidget->show();
    }
    else {
        QMessageBox::information(this, "!!!", "Не удалось открыть ссылку: " + url);
    }
}

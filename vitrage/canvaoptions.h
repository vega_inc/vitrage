#ifndef CANVAOPTIONS_H
#define CANVAOPTIONS_H

#include <QDialog>

namespace Ui {
class CanvaOptions;
}

class CanvaOptions : public QDialog
{
    Q_OBJECT
    
public:
    explicit CanvaOptions(QWidget *parent = 0);
    ~CanvaOptions();

	void setCubsW(int cubs);
	void setCubsH(int h);

	void setResolvW(int w);
	void setResolvH(int h);

	void setStepW(int w);
	void setStepH(int h);

    int cubsW() const;
    int cubsH() const;

    int resolvW() const;
    int resolvH() const;

    int stepW() const;
    int stepH() const;
   
public slots:
	void setOptions(int cubs_w,int cubs_h,
     				int resol_w, int resol_h,
	    			int step_w,int step_h);
signals:
    void setOptionsCanva(int cubs_w, int cubs_h,
	        			int resol_w, int resol_h,
                        int step_w, int step_h);
    void sendOpenHtml(const QString &page);

private slots:
    void somethingChanged();
    void save();
    void on_close_clicked();
    void on_toolButtonHelp_clicked();

private:
    Ui::CanvaOptions *ui;
	int cubs_w; /* canva's size width in cubes*/
	int cubs_h; /* canva's size height in cubes*/

	int resolv_w; /*monitor resolution w in pixs*/
	int resolv_h; /*monitor resolution h in pixs*/
	
	int step_w; /* small cells in cube w */
	int step_h; /* small cells in cube h */
};

#endif // CANVAOPTIONS_H

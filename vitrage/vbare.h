#ifndef VBARE_H
#define VBARE_H


#include <QVBoxLayout>
#include <QLabel>
#include <QPainter>
#include <QPixmap>
#include <QWidget>
#include <QDialog>
#include "stdint.h"

#include <vsource/vsource.h>

class VBare : public VSource {
    Q_OBJECT
public:
    VBare(QWidget *parent = NULL,uint32_t width = 10,uint32_t height = 10);
    ~VBare();
    void resize_src(uint32_t width,uint32_t height) {Q_UNUSED(width);Q_UNUSED(height)}
    void start()       {}
    void stop()        {}
    void setUrl(QUrl)  {}
    QSharedPointer<VSource> clone() {
        return QSharedPointer<VSource>(this);
    }
protected:
    void mousePressEvent(QMouseEvent * event);
    void mouseMoveEvent(QMouseEvent *event);
    void resizeEvent ( QResizeEvent * event);
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);

signals:
    void drop_signal(QString str,uint32_t,uint32_t);

private:
    QVBoxLayout *layout ;
    QLabel *imagelabel ;
    QPixmap pixmap_image;
protected:
    void fillCanva(); /*fill canva with bare widgets*/
    void paintEvent(QPaintEvent *p);
};

#endif /* VCANVA_H */

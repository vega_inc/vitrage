#include "sourcetablewidget.h"
#include <QEvent>
#include <QDrag>
#include <QMimeData>
#include <QMouseEvent>
#include <QHeaderView>

SourceTableWidget::SourceTableWidget()
{
    setAcceptDrops(true);
    auto setResizeMode = [](QTableWidget *table, const std::vector<uint> &columns){
        table->horizontalHeader()->hide();
        table->verticalHeader()->hide();
        table->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
        for (auto col : columns) {
            table->horizontalHeader()->setResizeMode(col, QHeaderView::ResizeToContents);
        }
    };
    setResizeMode(this, {0,2}); // 0 - checkbox, 2 - info
}

void SourceTableWidget::mousePressEvent(QMouseEvent *event)
{
    QDrag *drag = new QDrag(this);
    QMimeData *mimeData = new QMimeData;
    auto row = rowAt(event->pos().y());
    if (row >= 0 && row < rowCount()) {
        selectRow(row);
        auto nameItem = item(row, 1); // 1 - source name
        if (nameItem) {
            mimeData->setText(nameItem->text());
            drag->setMimeData(mimeData);
            QPixmap pixmap = nameItem->icon().pixmap(32);
            drag->setPixmap(pixmap);
            drag->exec();
            /*
             * QPixmap tempPixmap = pixmap;
             * QPainter painter;
             * painter.begin(&tempPixmap);
             * painter.fillRect(pixmap.rect(), QColor(127, 127, 127, 127));
             * painter.end();*/
        }
    }
}

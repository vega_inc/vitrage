#include "vscheme.h"

#include "vgraphicswidget.h"
#include "QGraphicsItem"
#include "setsize.h"
#include "vgraphicstext.h"

#include <qjson/parser.h>
#include <qjson/serializer.h>

#define PIC_DIR "_pics"



VScheme::VScheme()
{
    m_pScene = new QGraphicsScene;
    setScene(m_pScene);
    this->show();
    m_image = new QPixmap;
    m_backSvg = new QGraphicsSvgItem;
    m_pScene->addItem(m_backSvg);
    m_back = new QGraphicsPixmapItem;
    m_pScene->addItem(m_back);
    m_backGrd = NULL;
    setRenderHint(QPainter::Antialiasing);
    setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
    m_pActionSetBack = new QAction(tr("Set Background"),this);
    m_pActionSetSvg = new QAction(tr("Set SVG file for background"),this);
    m_pActionSetSize = new QAction(tr("Set underlayer size"),this);
    setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    connect(m_pActionSetBack,SIGNAL(triggered()),this,SLOT(setBackSlot()));
    connect(m_pActionSetSvg,SIGNAL(triggered()),this,SLOT(setSvgSlot()));
    connect(m_pActionSetSize,SIGNAL(triggered()),this,SLOT(setSizeSlot()));
    m_backRenderer = NULL;
    setResizeAnchor(AnchorUnderMouse);
    m_wgtResized = false;
    m_backHeight = 1024;
    m_backWidth = 1024;

    m_backSvg->installEventFilter(this);
    m_underType = UNDER_NONE;
    m_empty = true;
    setSavedFile("");
    setPicturePath("");
}

void VScheme::setBackgroundImage(QString path)
{
    if (m_backRenderer)  {
        delete m_backRenderer;
        m_backRenderer = NULL;
    }
    bool done = m_image->load(path);
    if (!done) {
        QMessageBox::critical(this,NULL,tr("Can't set image"));
        return;
    }
    qreal ws = (qreal)m_image->width()/(qreal)m_backWidth;
    qreal hs = (qreal)m_image->height()/(qreal)m_backHeight;
    m_back->scale(ws,hs);
    m_back->setPixmap(*m_image);

    m_backHeight = m_back->boundingRect().height();
    m_backWidth  = m_back->boundingRect().width();
    m_underType  = UNDER_JPG;
    m_underFile  = path;
    m_pScene->addItem(m_back);
    m_backSvg->setZValue(0);
    m_back->setZValue(1);
    setBackgroundSize(m_backWidth,m_backHeight);
    emit modefied();
}

void VScheme::setBackgroundSvg(QString path)
{
    if (m_backRenderer)  {
        delete m_backRenderer;
        m_backRenderer = NULL;
    }
    //removeItem();
    m_backRenderer = new QSvgRenderer(path);
    m_underType = UNDER_SVG;
    m_underFile = path;
    m_backSvg->setSharedRenderer(m_backRenderer);
    m_pScene->addItem(m_backSvg);
    m_backHeight = m_backSvg->boundingRect().height();
    m_backWidth = m_backSvg->boundingRect().width();
    m_backGrd = m_backSvg;
    m_backSvg->setZValue(1);
    m_back->setZValue(0);
    emit modefied();
}

void VScheme::setAction(VAction *act)
{
    m_pAction = act;
}

void VScheme::addSourceWidget(VGraphicsWidget *widget)
{
    //VGraphicsWidget *w = widget;
    QString name = widget->logicName();
    m_Items.insert(name,widget);
    //m_Items[widget->logicName()] = widget;
    widget->setProperty("phisName",widget->phisName());
    m_pScene->addItem(widget);
    m_pScene->addItem(widget->textItem());
    widget->setZValue(2);
    widget->textItem()->setZValue(2);
    connect(this,SIGNAL(mouse_move_sig(QPoint)),widget,SLOT(mouseModeSlot(QPoint)));
    connect(widget,SIGNAL(updateParent()),this,SLOT(updatePicture()));
    connect(widget,SIGNAL(setSizeSignal(int,int)),this,SLOT(setWidgetsSizes(int,int)));
    connect(widget,SIGNAL(modefied()),this,SIGNAL(modefied()));
    connect(widget,SIGNAL(setResizeFlag(bool)),this,SLOT(resizeWgtEnabled(bool)));
    connect(widget,SIGNAL(sigDefPicturePath(QString)),this,SLOT(setWgtPicturePath(QString)));
    connect(widget,SIGNAL(setActivate()),this,SLOT(widgetActivated()));
    connect(widget,SIGNAL(widgetPressed(QString)),SLOT(widgetClicked(QString)));
}

VGraphicsWidget *VScheme::getWidget(QString name)
{
    if (m_Items.contains(name))
        return m_Items.value(name);
    return NULL;
}

void VScheme::contextMenuEvent(QContextMenuEvent *event)
{
    QGraphicsItem *item = itemAt(VScheme::mapFromParent(event->pos()));
    if (item) {
        if (VGraphicsWidget * wgt = dynamic_cast<VGraphicsWidget*>(item)) {
            Q_UNUSED(wgt);
            event->ignore();
            //event->
            wgt->contextMenuEvent(event);
            return;
        }
    }
    QMenu menu;
    //QAction act1;1
    switch (m_mode) {
    case (RUNTIME) :
        //menu.addAction(m_pActionSetBack);
        break;
    case (EDITING) :
        menu.addAction(m_pActionSetBack);
        // menu.addAction(m_pActionSetSvg);
        menu.addAction(m_pActionSetSize);
        break;
    }
    menu.exec(VScheme::mapToGlobal(event->pos()));
    event->accept();
}

void VScheme::wheelEvent(QWheelEvent *event)
{
    int delta = event->delta();

    qreal wdt = this->transform().m11();
    if (delta > 0) {
        if (wdt > MAX_SCALE) return;
        centerOn(this->mapFromGlobal(cursor().pos()));
        scale(1.1,1.1);
    } else {
        if (wdt < MIN_SCALE) return;
        centerOn(this->mapFromGlobal(cursor().pos()));
        scale(1.0/1.1,1.0/1.1);
    }
}

void VScheme::mouseMoveEvent(QMouseEvent *event)
{
    changeCursor(event->pos());
    static QPoint old(0,0);
    QCursor ct = this->viewport()->cursor();
    QCursor ct2 = this->cursor();
    if (m_wgtResized) {
        QGraphicsView::mouseMoveEvent(event);
        return ;}
    if (ct2.shape() == Qt::SizeFDiagCursor) {
    }
    if ((Qt::LeftButton == event->buttons()) && (ct.shape() == Qt::OpenHandCursor)) {
        QPointF point = mapToScene(viewport()->rect().center());
        emit modefied();
        // QPoitn newpoint = QPoint(point.x()-event->)
        if ((!old.x()) && (!old.y())) {
            old.setX(event->x());
            old.setY(event->y());
        } else {
            QPoint pos;
            pos.setX(point.x() -  event->x() + old.x());
            pos.setY(point.y() -  event->y() + old.y());
            centerOn(pos);
            old.setX(event->x());
            old.setY(event->y());
        }
    } else  {
        old.setX(0);
        old.setY(0);
    }
    QGraphicsView::mouseMoveEvent(event);
}

void VScheme::mouseReleaseEvent(QMouseEvent *event)
{
    m_wgtResized = false;
    QGraphicsView::mouseReleaseEvent(event);
}

void VScheme::setMode(VScheme::SCHEME_MODE mode)
{
    //bool movable = true;
    VGraphicsWidget::WGT_MODE w_mode = VGraphicsWidget::EDITING;
    switch(mode) {
    case EDITING:
        m_back->setAcceptDrops(true);
        m_backSvg->setAcceptDrops(true);
        this->setAcceptDrops(true);
        break;
    case RUNTIME:
        m_back->setAcceptDrops(false);
        m_backSvg->setAcceptDrops(false);
        //this->setAcceptDrops(false);
        //movable = false;
        w_mode = VGraphicsWidget::RUNTIME;
        foreach(QString str,m_Items.keys()) {
            VGraphicsWidget *wgt = m_Items.value(str);
            wgt->setSelected(false);
            wgt->textItem()->setSelected(false);
        }
        break;
    }
    foreach(QString name,m_Items.keys()) {
        VGraphicsWidget *wgt = m_Items.value(name);
        if (!wgt) break;
        wgt->setMode(w_mode);
        wgt->setSelected(false);
    }
    m_mode = mode;
}

void VScheme::setLightedWidgets(QList<QString> list)
{
    if (mode() == EDITING) return;
    foreach(QString key,m_Items.keys()) {
        VGraphicsWidget *wgt = m_Items.value(key);
        if(list.contains(wgt->logicName())) {
            wgt->getSource()->setSelected(true);
        } else {
            wgt->getSource()->setSelected(false);
        }
    }
}

void VScheme::renameItem(QString phisName, QString newname)
{
    foreach(QString key,m_Items.keys()) {
        VGraphicsWidget * wgt = m_Items.value(key);
        QString ph = wgt->phisName();
        if (ph == phisName) {
            m_Items.remove(key);
            m_Items.insert(newname,wgt);
            wgt->textItem()->setPlainText(newname);
            wgt->setLogicName(newname);
        }
    }
}

void VScheme::removeItem(QString str)
{
    VGraphicsWidget *wgt = m_Items.value(str);
    if (!wgt) return;
    this->m_pScene->removeItem(static_cast<QGraphicsItem *>(wgt));
    this->m_pScene->removeItem(wgt->textItem());
    m_Items.remove(str);
    emit modefied();
}

bool VScheme::isEmpty()
{
    return (!m_Items.count());
}


VScheme::SCHEME_MODE VScheme::mode(void)
{
    return m_mode;
}

void VScheme::setSavedFile(QString file)
{
    m_saveFile = file;
}

QString VScheme::savedFile()
{
    return m_saveFile;
}

QString VScheme::picturePath()
{
    return m_picturePath;
}

void VScheme::setPicturePath(QString path)
{
    m_picturePath = path;
}

void VScheme::setSelectedWidget(QString str)
{
    m_selectedWidget = str;
}

QString VScheme::selectedWidget()
{
    return m_selectedWidget;
}

void VScheme::setSelectedSource(QString source)
{
    foreach(QString str,m_Items.keys()) {
        VGraphicsWidget * wgt = m_Items.value(str);
        if (wgt->getSource())         {
            if (wgt->getSource()->getname() == source) {
                setSelectedWidget(source);
                wgt->setSelected(true);
                wgt->getSource()->setActivated(true);
            } else {
                wgt->getSource()->setActivated(false);
                wgt->setSelected(false);
            }
        }
    }
}

void VScheme::dropEvent(QDropEvent *event)
{
    QString src_name = event->mimeData()->text();
    emit sourceDroped(src_name,this->mapToScene(event->pos()).toPoint());
}

bool VScheme::eventFilter(QObject *obj, QEvent *ev)
{
    Q_UNUSED(ev);
    QGraphicsSvgItem * item = dynamic_cast<QGraphicsSvgItem*>(obj);
    if (item) {
        if (item == m_backSvg) {
            qDebug() << "EVENT FROM BACH";
            return false;
        }
    }
    return false;
}

void VScheme::setAllPictures(QString path)
{
    foreach (QString name, m_Items.keys()) {
        VGraphicsWidget * wgt = m_Items.value(name);
        if (!wgt) continue;
        QString picture = path + PIC_DIR + "/" + name;
        wgt->setPicture(picture);
        wgt->setType(VGraphicsWidget::PICTURE);
    }
}
/**
 * @brief VScheme::repaceAllWidgets
 * set positions and sizes for widgets
 */
void VScheme::replaceAllWidgets(QVariantList &lst)
{
    foreach(QString str,m_Items.keys()) {
        for (int i = 0 ; i < lst.count();i++) {
            QMap<QString,QVariant>  map = lst.at(i).toMap();
            //  QString name  =  map.value(ELEM_PHIS_NAME).toString();
            QString name2  =  map.value(ELEM_LOGIC_NAME).toString();
            if (name2 == str) {
                int w = map.value(ELEM_POS_W).toInt();
                int h = map.value(ELEM_POS_H).toInt();
                int lx = map.value(ELEM_LABEL_X).toInt();
                int ly = map.value(ELEM_LABEL_Y).toInt();
                VGraphicsWidget *wgt = m_Items.value(str);
                //    qreal ww = w;
                //     qreal hh = h;
                //    int wew = wgt->sceneBoundingRect().width();
                //   int heh = wgt->sceneBoundingRect().height();
                //   float x_m = ww/wew;
                //    float y_m = hh/heh;
                //                float x_m = wgt->getOriginImage().width()/ww;
                //                float y_m = wgt->getOriginImage().height()/hh;
                //  wgt->scale(x_m,y_m);
                //wgt->setPictureSize(ww,hh);
                wgt->textItem()->setPos(lx,ly);
                wgt->setPictureSize(w,h);
            }
        }
    }
}

void VScheme::paintSelectedWidget(QPainter *painter)
{
    foreach(QString name,m_Items.keys()) {
        if (selectedWidget() == name) {
            VGraphicsWidget * wgt = m_Items.value(name);
            if (!wgt) return;
            if (wgt->getSource()) {
                wgt->getSource()->paintSelected(painter,100,100,100,100);
            }
        }
    }
}

VScheme::~VScheme()
{
    delete m_pScene;
}

void VScheme::sourceStatusChanged(QString src_name)
{
    foreach(QString name,m_Items.keys()) {
        VGraphicsWidget *wgt = m_Items.value(name);
        if (!wgt) return;
        VSource *src = m_Items.value(name)->getSource();
        if (!src) return;
        if (src->getname() == src_name) {
            switch(src->getStatus()) {
            case VSource::ExecStatus::ErrorStatus:
                wgt->setStatus(ERROR_ST);
                break;
            case VSource::ExecStatus::RunStatus :
                wgt->setStatus(RUNTIME_ST);
                break;
            case VSource::ExecStatus::StopStatus:
                break;
            }
        }
    }
}

void VScheme::widgetClicked(QString str)
{
    emit setActiveWidget(str);
}

void VScheme::changeCursor(QPoint point)
{
    Q_UNUSED(point);
    QGraphicsItem * item = itemAt(point.x(),point.y());
    QGraphicsItem *pix = static_cast<QGraphicsItem *>(m_back);
    QGraphicsItem *svg = static_cast<QGraphicsItem *>(m_backSvg);
    QCursor cr = cursor();

    if(cr.shape() == Qt::SizeFDiagCursor)return;


    if ((item == pix) || (item == svg)) {
        cr.setShape(Qt::OpenHandCursor);




        //this->setCursor(cr);
    } else {
        cr.setShape(Qt::ArrowCursor);
    }
    this->viewport()->setCursor(cr);
    //setCursor(cr);
}



void VScheme::setBackSlot()
{
    QString fileName = QFileDialog::getOpenFileName(NULL,
                                                    tr("set background image"), getImagesPath(), "*"  );
    if (fileName.isEmpty()) return;

    if (fileName.endsWith(".svg")) {
        setBackgroundSvg(fileName);
        m_pScene->removeItem(m_back);
    } else {
        setBackgroundImage(fileName);
        m_pScene->removeItem(m_backSvg);
    }
}

void VScheme::setSvgSlot()
{
    QString fileName = QFileDialog::getOpenFileName(NULL, tr("set svg file"), "", "*.svg"  );
    if (fileName.isEmpty()) return;
    setBackgroundSvg(fileName);
    m_pScene->removeItem(m_back);
}
/**
 * @brief VScheme::setSizeSlot
 *  slot for setsize Acton
 */
void VScheme::setSizeSlot()
{
    SetSize sz(this,m_backWidth,m_backHeight);
    connect(&sz,SIGNAL(sizes(int,int)),this,SLOT(setBackgroundSize(int,int)));
    sz.exec();
}

void VScheme::setBackgroundSize(int w, int h)
{
    int new_w = m_backWidth;
    int new_h = m_backHeight;
    if (m_backRenderer) {
        qreal ws = (qreal)w/(qreal)m_backWidth;
        qreal hs = (qreal)h/(qreal)m_backHeight;
        m_backSvg->scale(ws,hs);
        new_w = w;
        new_h  = h;
    } else {
        qreal ws = (qreal)w/(qreal)m_backWidth;
        qreal hs = (qreal)h/(qreal)m_backHeight;
        m_back->scale(ws,hs);
        new_w = w;
        new_h  = h;
    }
    m_backWidth  = new_w;
    m_backHeight = new_h;
    emit modefied();
}

void VScheme::widgetActivated()
{
    VGraphicsWidget *wgt = static_cast<VGraphicsWidget*>(sender());
    if (!wgt) return;
    setSelectedSource(wgt->getSource()->getname());
}

void VScheme::setWidgetsSizes(int w, int h)
{
    QList<QString>  lst = m_Items.keys();
    for(int i = 0 ; i < lst.count() ; i++) {
        QString name = lst[i];
        VGraphicsWidget *wgt = m_Items.value(name);
        if (wgt->isSelected()) {
            wgt->setSize(w,h);
        }
    }
}

void VScheme::resizeWgtEnabled(bool enabled)
{
    m_wgtResized = enabled;
}

void VScheme::updatePicture()
{
    this->update();
    //m_back->update();
}

void VScheme::setWgtPicturePath(QString path)
{
    foreach(VGraphicsWidget *wgt,m_Items) {
        wgt->setDefPicPath(path);
    }
}


/**
 * @brief VScheme::saveJson save all objects from scheme
 * @return false - successed
 */
bool VScheme::saveJson(QString fileName)
{
    if (!fileName.endsWith(SCHEME_FILE)) {
        fileName.append(SCHEME_FILE);
    }
//    QFile under_file(m_underFile);
//    if (!under_file.open(QIODevice::ReadOnly))
//        return true;
//    auto under = under_file.readAll();
    QVariantMap config;
    config.insert(SCM_UNDER_TYPE,m_underType);
    QVariantList elements = getElements();
    config.insert(SCM_OBJECTS,elements);
    QSize backgrndSize(getBackgroundSize());
    QVariantList sizeList;
    sizeList.append(backgrndSize.width());
    sizeList.append(backgrndSize.height());
    config.insert(SCM_BGRND_SIZE,sizeList);
    QJson::Serializer serializer;
    bool ok;
    QByteArray json = serializer.serialize(config);
    if (!ok)
        return true;
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
        return true;
    file.write(json);
    file.close();
    savePictures(fileName);
    setSavedFile(fileName);
    //qDebug() << Q_FUNC_INFO << "json:" << json;
    return false;
}

/* save underlayer and pictures for widgets to selected dir */
bool VScheme::savePictures(const QString &dir)
{
    QDir direct;
    QString lower_dir = dir + "_pics";
    direct.mkdir(lower_dir);
    direct.cd(lower_dir);
    if (QFile::exists(m_underFile))
        QFile::copy(m_underFile, lower_dir+"/underLayer");

    foreach(VGraphicsWidget *wgt, m_Items) {
        QByteArray ba;
        QBuffer buffer(&ba);
        buffer.open(QIODevice::ReadWrite);
        QImage image = wgt->getOriginImage();
        if (image.isNull() && (wgt->getType() == VGraphicsWidget::ICON)) {
            image = wgt->getIconPicture();
            qDebug() << "Empty picture";
            // abort();
        }
        image.save(&buffer,"PNG"); // writes image into ba in PNG format
        QFile file(lower_dir +"/" + wgt->logicName());
        file.open(QIODevice::ReadWrite);
        file.write(ba);
        file.close();
    }
    //direct.mkdir(" ");
    return false;
}



/**
 * @brief VScheme::loadJson - load all object for scheme
 * @return  - false successed
 */
bool VScheme::loadJson(bool load)
{
    QString fileName;
    if (!load) {
        fileName = QFileDialog::getOpenFileName(this);
    } else {
        fileName = m_saveFile;
    }
    if (fileName.isEmpty())
        return true;
    /* clear current canva */
    fileName.append(SCHEME_FILE);
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly))
        return true;
    auto json = file.readAll();
    file.close();

    QJson::Parser parser;
    bool ok;
    QVariantMap result = parser.parse(json, &ok).toMap();
    if (!ok) {
        qDebug("VAction::loadJson - Error while parsing Action");
        abort();
        return true;
    }
    m_underType = static_cast<UNDERLAYER_TYPE>(result.value(SCM_UNDER_TYPE).toInt());
    if(m_underType == UNDER_SVG ) {
        qDebug() << file.fileName();
        QString under_file =file.fileName() + PIC_DIR + "/underLayer";
        setBackgroundSvg(under_file);
    } else if (m_underType == UNDER_JPG) {
        QString under_file =file.fileName() + PIC_DIR + "/underLayer";
        setBackgroundImage(under_file);
    }

    /* load all elements */
    auto elements = result.value(SCM_OBJECTS).toList();
    for (auto e : elements) {
        QMap<QString,QVariant> map = e.toMap();
        QString name  =  map.value(ELEM_LOGIC_NAME).toString();
        int x = map.value(ELEM_POS_X).toInt();
        int y = map.value(ELEM_POS_Y).toInt();
//        int w = map.value(ELEM_POS_W).toInt();
//        int h = map.value(ELEM_POS_H).toInt();
        emit sourceDroped(name, QPoint(x,y));
    }
    setAllPictures(file.fileName());
    replaceAllWidgets(elements);

    auto sizeList = result.value(SCM_BGRND_SIZE).toList();
    if (sizeList.empty()) {
        sizeList.push_back(1024);
        sizeList.push_back(1024);
    }
    QSize backgrndSize(sizeList.at(0).toInt(), sizeList.at(1).toInt());
    setBackgroundSize(backgrndSize.width(), backgrndSize.height());
    m_saveFile = fileName;
    return false;
}

QVariantList VScheme::getElements()
{
    QVariantList elements;
    QMap<QString,QVariant> map;
    foreach(VGraphicsWidget* wgt,m_Items) {
        wgt->getMap(map);
        elements.push_back(map);
    }
    return elements;
}

void VScheme::removeItems()
{
    foreach(VGraphicsWidget *item ,m_Items.values()) {
        disconnect(item);
        m_pScene->removeItem(item->textItem());
        m_pScene->removeItem(item);
    }
    m_Items.clear();

}

QSize VScheme::getBackgroundSize()
{
    return QSize(m_backWidth, m_backHeight);
}

QString VScheme::getImagesPath()
{
    QString imageFilesPath = FIRST_SHARE_PATH
            + QFileInfo(QString(qApp->argv()[0])).fileName()
            + IMAGES_DIR_NAME;
    QDir dir(imageFilesPath);
    if(dir.exists() && dir.cd(imageFilesPath))
        return imageFilesPath;
    else
    {
        imageFilesPath = QDir::current().canonicalPath() + IMAGES_DIR_NAME;
        QDir dir(imageFilesPath);
        if(dir.exists() && dir.cd(imageFilesPath))
            return imageFilesPath;
        else
            return "";
    }


}


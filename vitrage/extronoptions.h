#ifndef ExtronOPTIONS_H
#define ExtronOPTIONS_H

#include <QWidget>
#include "ui_controlleroptions.h"
#include "vcontroller/vcontroller.h"
#include <QSettings>
namespace Ui {
class ControllerOptions;
}

class ExtronOptions : public QWidget
{
    Q_OBJECT
    
public:
    explicit ExtronOptions(QWidget *parent = 0);
    ~ExtronOptions();
    void    setHost     (QString);
    void    setPort     (int    );
    void    setLogin    (QString);
    void    setPassword (QString);
    QString getHost     ();
    int     getPort     ();
    QString getLogin    ();
    QString getPassword ();
    /**
     * @brief setSettings Вызвав этот метод можно заполнить поля из ини файла.
     * @param st
     */
    void loadSettings(QSettings *st);//Установить настройки в форме
    void saveSettings(QSettings *st);//Считать настройки с формы

    void setController(VController *ctr);

    void fillStatus();
    void setPropEnabled(bool en);

signals:
    void switchOnStateChanged(bool);
    void saveClickedSignal();
    void sendOpenHtml(const QString &page);

private slots:
    void on_tBSave_clicked();
    void newStatus();

private slots:
    void someChanged(QString str);
    void on_toolButtonHelp_clicked();

private:
    bool m_connectFlag = false;
    Ui::ControllerOptions *ui;
    void hideUnused();
    VController *m_pController = nullptr;
};

#endif // ExtronOPTIONS_H

#ifndef COLLAPSIBLEFRAME_H
#define COLLAPSIBLEFRAME_H

#include <QPropertyAnimation>
#include <QPushButton>
#include "extendedqlabel.h"
#include "extendedqlabel.h"

class QHBoxLayout;
class QVBoxLayout;
class QSplitter;

class CollapsibleFrame : public QWidget
{
    Q_OBJECT
private:
    QLabel* m_headerArea;
    QPixmap m_collapseIcon;
    QString m_headerText;
    QWidget* m_widgetArea;
    ExtendedQLabel* icon;
    ExtendedQLabel * m_down;
    ExtendedQLabel * m_up;
    QLabel* headerTextLabel;
    QHBoxLayout* headerLayout;
    QVBoxLayout* widgetLayout;
	QPropertyAnimation *m_pOpenAnimation;
	QPropertyAnimation *m_pCloseAnimation;
	QSplitter *m_pSplitter;
	QFrame *m_frame;
    int m_frameState;
    void determineIcon();
    QObject* m_parent;

public:
    CollapsibleFrame(QObject *parent, QString headerText);
	void setTopBackground(QColor color);
    void addWidget(QWidget *widget);
	void addInnerSplitted(QWidget *);
    void addLayout(QLayout *layout);
	void setInnerSplitter(int sz);
	void setHeaderHeight(int h);
    void setMoveUpEnable(bool flag);
    void setMoveDownEnable(bool flag);
    void addUpDown(void);
    void hideHeader(void);
    void hideButton(void);
    bool isCollapsed(void);
    enum { OPEN, CLOSED };
	void setSplitterSizes(QList<int> list);
	QList<int> getSplitterSize(void);
	QSplitter *splitter(void);
signals:
	void hideMe(void);
	void showMe(void);
    void moveUp(void);
    void moveDown(void);
public slots:
    void hideWgt(void);
    void changeState();
};

#endif // COLLAPSIBLEFRAME_H

#include "setsize.h"
#include "ui_setsize.h"
#include <QPushButton>

SetSize::SetSize(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SetSize)
{
    ui->setupUi(this);
}

SetSize::SetSize(QWidget *parent, int w, int h):
    QDialog(parent),
    ui(new Ui::SetSize)
{
    ui->setupUi(this);

    ui->buttonBox->button(QDialogButtonBox::Ok)->setText(tr("Apply"));
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText(tr("Cancel"));

    ui->spinBoxHeight->setValue(h);
    ui->spinBoxWidth->setValue(w);
    m_oldw = w;
    m_oldh = h;
}

SetSize::~SetSize()
{
    delete ui;
}

void SetSize::on_buttonBox_accepted()
{
    emit sizes(ui->spinBoxWidth->value(), ui->spinBoxHeight->value());
}

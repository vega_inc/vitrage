#ifndef VCANVA_H
#define VCANVA_H

#include <QWidget>
#include <QDialog>
#include <QMap>
#include <QGridLayout>
#include <QStackedLayout>

#include "stdint.h"


#include <vsource/vsource.h>
#include "vgrid.h"
#include "vscene.h"
#include "vaction.h"
#include "../vcontroller/vcontroller.h"

enum Canva_type { MODEL, RUNTIME };

typedef struct CanvaConf {
	int cells_w;
	int cells_h;
	int resolv_w;
	int resolv_h;
	int small_w;
	int small_h; 
}CanvaConf;

class  VCanva : public QWidget {
    Q_OBJECT
public:
    VCanva(QWidget *parent,uint32_t height = 4,
           uint32_t width = 4,
           uint32_t height_cells = 1,
           uint32_t width_cells = 1,
           uint32_t resolv_width = 1024,
           uint32_t resolv_height = 800,
           Canva_type type = MODEL,
           bool isAdmiralty = false
          );

    void resizeEvent ( QResizeEvent * event );
    int heightForWidth ( int w );
    int widthForHeight ( int h );

	/* working with controller */


    void zoomFor(int zoom);
    QPoint dot_to_cell(uint32_t w,uint32_t  h);
    bool over_grid(const VPosition pos);/* check position for borders */
    QScrollArea *scrollArea();
    void placeByPos(VSource *src);
    void dragEnterEvent(QDragEnterEvent *event);
    void addSource(const QString &name, uint32_t w, uint32_t h);
    QPoint cell_to_dot(uint32_t x, uint32_t y); /*returns global point in the middle of cell */
    bool dot_in_canva(QPoint p);

    virtual ~VCanva(); 
    QWidget *scrollWgt(void);

    /* set all configs */
    void setAllConfigs(void);

    /* setters getters */
    void  setheight(unsigned int height);
    unsigned int  getheight(void);

    void setwidth(unsigned int width);
    unsigned int getwidth(void);

    void  setheight_cells(unsigned int height_cells);
    unsigned int  getheight_cells(void);

    void setwidth_cells(unsigned int width_cells);
    unsigned int getwidth_cells(void);

    void  setresolv_height(unsigned int resolv_height);
    unsigned int  getresolv_height(void);

	void setController(VController *);
	VController *getController(void);

    void setresolv_width(unsigned int resolv_width);
    unsigned int getresolv_width(void);
    void setCloneName(QSharedPointer<VSource> src);

    void setMinimumCell();

	CanvaConf getConfigs(void);

    void setParentWgt(QWidget *wgt);
	void setOptionsWindow(void);
	void customResize(QResizeEvent *event);
    void setAction(VAction * action);
	void clearCanva(); /* clear all bare widgets or all sources */ 
    void clearScene();
    QSharedPointer<VSource> getCloneByName(QString ); /* return VSource from elements */
    QSharedPointer<VSource> getCloneBySrcName(QString ); /* return VSource from elements */
    void setSelectedSource(const QString & );
    void paintAllSources(QPainter*);
    void updateAllElementsExcept(const QString &name);
    QDockWidget *dock(void);
    void setVisibleAll(bool flag);
    bool setEnabledCloneSource(const QString &clone_name, bool flag);

public slots:
	void setNewOptions(int,int,int,int,int,int);		
    void drop_bare(QString,uint32_t, uint32_t );
    void source_mouse_move(QPoint, VSource *);
    void source_mouse_press(QPoint,VSource*);
    void source_mouse_release(QPoint);
    void global_mouse_move(QPoint);
    void source_resize(QPoint, VSource*);
	void source_drop(QString,QPoint);
	void setMinHeight(int h);
	void setScrollSize(QSize sz);
    void setScene(VScene *scene_t);
    VScene *getScene();
	void enableSmallGrid(bool e);
    void slotSourceDoubleClicked(void);
    void setActiveSource(QString);

signals:
    void cell_size_changed(uint32_t w,uint32_t h);
    void global_mouse_move_sig(QPoint);
	void setOptions(int, int ,int ,int ,int ,int);
	void addSourceToScene(VSource &);
    void setSrcPosScene(VSource &);
    void sourceClicked(void);
    void sourceRightClicked(QPoint p);
    void sourceModeSignal(void);

protected:
    void wheelEvent(QWheelEvent * e);
    void mousePressEvent(QMouseEvent * event);
    void mouseMoveEvent(QMouseEvent *);
    bool eventFilter(QObject *obj, QEvent *event);

private:
    QGridLayout *grid;
	QStackedLayout *stack;
	QWidget *grid_wgt;
	QWidget *parent_wgt; /* resize */
    VGrid *d_grid; /*draw grid */
    QMap<QString, QSharedPointer<VSource>> elements;
    VSource * moved_source;
	VScene *scene;
	VAction *action;
	VController *m_controller; /* current controller */
    QPoint moved_dot;
    QPoint moved_head;
	QSize scrollsize;
    Canva_type type;
    QScrollArea *m_scroll;
    QWidget *m_pScrollWidget;
    QDockWidget *m_dock;
    QString m_title;
    uint32_t height_big_cell; /* big cells */
    uint32_t width_big_cell;  /* big cells */
    uint32_t height_cells; /* small cells in big cell */
    uint32_t width_cells;  /* small cells in big cell */
    uint32_t resolv_height; /* monitor resolv */
    uint32_t resolv_width;  /* monitor resolv */
    /*____________________________________*/
    uint32_t pixs_per_height; /*pixelgs per height big cell */
    uint32_t pixs_per_width; /*pixels per width big cell */
    uint32_t pixs_per_cell_height; /* ... per small cell */
    uint32_t pixs_per_cell_width;  /* ... per small cell */

//	static const int MIN_CANVA_HEIGHT = 150;
//    static const int MAX_CANVA_HEIGHT = 12024;

	int minheight;
    bool m_is_admiralty = false;
};

#endif /* VCANVA_H */

#include "vgraphicsviewer.h"
#include "ui_vgraphicsviewer.h"

VGraphicsViewer::VGraphicsViewer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::VGraphicsViewer)
{
    ui->setupUi(this);
    m_layout = new QHBoxLayout;
    ui->w_scene->setLayout(m_layout);
}

void VGraphicsViewer::setScheme(VScheme *scheme)
{
    if (!scheme) return;
    m_scheme = scheme;
    m_scheme->setMode(VScheme::RUNTIME);
    m_layout->addWidget(scheme);

}

VGraphicsViewer::~VGraphicsViewer()
{
    delete ui;
}

void VGraphicsViewer::editorClose()
{
    m_layout->addWidget(m_scheme);

}

void VGraphicsViewer::on_actionEditor_triggered()
{
    emit openEditor();
}

void VGraphicsViewer::on_actionClose_triggered()
{
    close();
}

#include <QGridLayout>
#include <QDebug>
#include <QLabel>
#include <QPainter>
#include <QPixmap>
#include <QResizeEvent>
#include <QMessageBox>
#include <QWheelEvent>

#include "vcanva.h"
#include "vsourcemanager.h"
#include "pluginsmanager.h"
#include "../vcontroller/vcontroller.h"
#include "vgrid.h"

#include "spdlog/spdlog.h"

#define SZ_POLICY QSizePolicy::Preferred

VCanva::VCanva(QWidget *parent,
               uint32_t height_t,
               uint32_t width_t,
               uint32_t  height_cells_t,
               uint32_t width_cells_t,
               uint32_t resolv_width,
               uint32_t resolv_height,
               Canva_type tp, bool isAdmiralty)
    : QWidget(parent), m_is_admiralty(isAdmiralty)
{
    type = tp;
    setAutoFillBackground(true);
    this->setMouseTracking(true);
//    QPalette p(palette());
//    p.setColor(QPalette::Background, Qt::white);
    stack = new QStackedLayout();
    grid = new QGridLayout();
    d_grid = new VGrid(this);
    d_grid->installEventFilter(this);
    grid->setSizeConstraint(QLayout::SetNoConstraint);
    d_grid->setMouseTracking(true);
    d_grid->setAttribute(Qt::WA_TransparentForMouseEvents);
    d_grid->setS_Grids(width_t*width_cells_t,height_t*height_cells_t);
    d_grid->setB_Grids(width_t,height_t);
    connect(d_grid, SIGNAL(updateParent()), this, SLOT(update()));

    setLayout(stack);
    grid_wgt = new QWidget();
    QSizePolicy qsp(QSizePolicy::Ignored,QSizePolicy::Ignored);
    grid_wgt->setSizePolicy(qsp);
    grid_wgt->setMouseTracking(true);

    setSizePolicy(qsp);

    grid_wgt->setLayout(grid);
    stack->addWidget(d_grid);
    stack->addWidget(grid_wgt);
    stack->setStackingMode(QStackedLayout::StackAll);
    stack->installEventFilter(this);

    grid->setSpacing(0);
    grid->setVerticalSpacing(0);
    grid->setHorizontalSpacing(0);
    grid->setContentsMargins(0,0,0,0);

//    setPalette(p);
    setheight(height_t);
    setwidth(width_t);
    setheight_cells(height_cells_t);
    setwidth_cells(width_cells_t);
    setresolv_width(resolv_width);
    setresolv_height(resolv_height);
    setScrollSize(QSize(200,200));
    setAllConfigs();
    moved_source = NULL;
    setMouseTracking(true);
    scene = NULL;
    setMinimumCell();
    elements.clear();
    m_scroll = new QScrollArea;
    m_scroll->setWidgetResizable(false);
    setAcceptDrops(true);
    m_pScrollWidget = new QWidget;
    setParentWgt(m_pScrollWidget);
    m_pScrollWidget->setAcceptDrops(true);
    m_scroll->setWidget(m_pScrollWidget);
    QVBoxLayout *l2 = new QVBoxLayout;
    m_pScrollWidget->setLayout(l2);
    m_pScrollWidget->layout()->addWidget(this);

    switch (type) {
    case MODEL :
        m_title =tr("Preset Wall");
        break;
    case RUNTIME :
        m_title = tr("Runtime Wall");
        break;
    }
    m_dock = new QDockWidget(m_title);
    m_dock->setWidget(m_scroll);
    m_scroll->setWidgetResizable(false);
    m_scroll->setAlignment(Qt::AlignCenter);
    m_dock->setAllowedAreas(Qt::RightDockWidgetArea);

    switch (type) {
    case MODEL :
        m_title =tr("Preset Wall");
        break;
    case RUNTIME :
        m_title = tr("Runtime Wall");
        break;
    }
    m_dock->setWindowTitle(m_title);
}

VCanva::~VCanva()
{
    delete grid;
    delete stack;
    delete grid_wgt;
}

QWidget *VCanva::scrollWgt()
{
    return m_pScrollWidget;
}

void VCanva::setOptionsWindow(void) 
{
    emit this->setOptions(width_big_cell,
                          height_big_cell,
                          resolv_width,
                          resolv_height,
                          width_cells,
                          height_cells);
}

void VCanva::mouseMoveEvent(QMouseEvent *)
{
    emit global_mouse_move_sig(QCursor::pos());
}

bool VCanva::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::Paint) {
        static_cast<VGrid*>(obj)->paintEvent(static_cast<QPaintEvent*>(event));
    }
    if (event->type() == QEvent::Drop) {
        dropEvent(static_cast<QDropEvent*>(event));
    }
    return true;
}

void VCanva::dragEnterEvent(QDragEnterEvent *event)
{
    QString name = event->mimeData()->text();
    if (!action->getSource(name)) {
        event->ignore();
        QCursor cr = cursor();
        cr.setShape(Qt::ForbiddenCursor);
        setCursor(cr);
        return;
    }
    QWidget::dragEnterEvent(event);
}

void VCanva::mousePressEvent(QMouseEvent * event )
{
    if (type == RUNTIME)
        return;
    QPoint pos = dot_to_cell(event->pos().x(),event->pos().y());
    auto x = pos.x();
    auto y = pos.y();
    foreach(QString name, elements.keys()) {
        QSharedPointer<VSource> src = elements.value(name);
        if (!src) continue;
        VPosition pos_ = src->getPosition();
        if (x >= pos_.lt.x() && x < pos_.rb.x() &&
            y >= pos_.lt.y() && y < pos_.rb.y()) {
            emit sourceClicked();
            setSelectedSource(name);
            if (event->button() == Qt::RightButton) {
                emit sourceRightClicked(mapToGlobal(event->pos()));
            }
            return;
        }
    }
}

void VCanva::wheelEvent(QWheelEvent *e)
{
    int delta = e->delta();
    QSize sz(parent_wgt->size());
    sz.setWidth(sz.width() + delta/2);
    sz.setHeight(sz.height() + delta/2);
    auto x = sz.width()+delta/2;
    auto y = sz.height()+delta/2;
    if (x < 150 || y < 150)
        return;
    QResizeEvent ev(sz, sz);
    setScrollSize(sz);
    customResize(&ev);
    update();
}

void VCanva::setVisibleAll(bool flag)
{
    if (grid) {
        for (int hi = 0; hi < 300; ++hi){
        for (int i = 0; i < grid->count(); ++i)
            grid->itemAt(i)->widget()->setVisible(flag);
    }
}
}

/**
* @brief remove all Sources and disconnect from them
*/
void VCanva::clearCanva(void)
{
    QLayoutItem *child;
    while ((child = grid->takeAt(0)) != 0) {
        child->widget()->setParent(NULL);
        QWidget *child_w = 	child->widget();
        disconnect(this,SIGNAL(cell_size_changed(uint32_t,uint32_t)),
                   child_w,SLOT(cell_size_changed(uint32_t,uint32_t)));
        disconnect(child_w,SIGNAL(mouse_move(QPoint,VSource*)),this
                   ,SLOT(source_mouse_move(QPoint,VSource *)));
        disconnect(child_w,SIGNAL(mouse_press(QPoint,VSource*)),this
                   ,SLOT(source_mouse_press(QPoint,VSource*)));
        disconnect(child_w,SIGNAL(mouse_release(QPoint)),this
                   ,SLOT(source_mouse_release(QPoint)));
        disconnect(this,SIGNAL(global_mouse_move_sig(QPoint)),child_w,
                   SLOT(global_mouse_move()));
        disconnect(child_w,SIGNAL(resize_sig(QPoint,VSource*)),this,
                   SLOT(source_resize(QPoint,VSource*)));
        disconnect(child_w,SIGNAL(drop_sig(QString,QPoint)),this,
                   SLOT(source_drop(QString,QPoint)));
    }
    elements.clear();
    grid->invalidate();
    grid->update();
//    d_grid->setText(tr("No selected scene"));
}
/**
* @brief return height for inserted width
*
* @param w - [in] int width
*
* @return int width 
*/
int  VCanva::heightForWidth (int w )
{
    pixs_per_width = w / width_big_cell;
    pixs_per_height =  (pixs_per_width * resolv_height) / resolv_width;

    pixs_per_cell_width = pixs_per_width / width_cells;
    pixs_per_cell_height = pixs_per_height / height_cells;
    return pixs_per_height * height_big_cell;

}

int VCanva::widthForHeight ( int h )
{
    int pixs_h =  h / height_big_cell;
    int pixs_w = (pixs_h * resolv_width) / resolv_height;
    return pixs_w * width_big_cell;
}

void VCanva::zoomFor(int zoom)
{
    int delta = zoom;
    QSize sz(this->parent_wgt->size());
    sz.setWidth(sz.width() + delta/2);
    QResizeEvent ev(sz,sz);
    setScrollSize(sz);
    customResize(&ev);
}

void VCanva::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);
}

void VCanva::customResize(QResizeEvent *event)
{
    /* qDebug() << QString("VCanva::customResize %2 %1")
    .arg(event->size().height()).arg(event->size().width());*/

    QSize sz_src = event->size();
    QSize sz(sz_src.width() , sz_src.height() ); /*Margins from parent widget*/
    this->setScrollSize(sz);
    int pixs_w =  sz.width() / width_big_cell;
    int pixs_h =  (pixs_w * resolv_height) / resolv_width;

    pixs_per_width = pixs_w;
    pixs_per_height = pixs_h ;
    pixs_per_cell_width = pixs_per_width / width_cells;
    pixs_per_cell_height = pixs_per_height / height_cells;
    QSize newsz = QSize(pixs_per_width * width_big_cell, pixs_per_height * height_big_cell);
    emit cell_size_changed(pixs_per_cell_width, pixs_per_cell_height);
    newsz.setWidth(newsz.width());
    newsz.setHeight(newsz.height());
    parent_wgt->resize(newsz);
    updateGeometry();
    setMinimumCell();
}

/**
* @brief calculate all params by scroll size
*/
void VCanva::setAllConfigs(void)
{
    int pixs_w =  scrollsize.width() / width_big_cell;
    int pixs_h =  (pixs_w * resolv_height) / resolv_width;

    if ((pixs_h * ((int)height_big_cell)) > scrollsize.height()){
        pixs_h =  scrollsize.height() / height_big_cell;;

        pixs_w =   (pixs_h * resolv_width) / resolv_height;
    }

    pixs_per_width = pixs_w;
    pixs_per_height = pixs_h ;
    pixs_per_cell_width = pixs_per_width / width_cells;
    pixs_per_cell_height = pixs_per_height /height_cells;
}

/**
* @brief addSource on canva 
*
* @param name - [in] QString Source clone name
* @param w - [in] int position in grid 
* @param h - [in] int position in grid
*/
void VCanva::addSource(const QString &name, uint32_t w, uint32_t h)
{
    if (VSource *src = action->getSource(name)) {
        QSharedPointer<VSource> copy = src->clone();
        copy->setname(name);

        unsigned int grid_h = grid->rowCount();
        unsigned int grid_w = grid->columnCount();

        if ((grid_h - h) < height_cells) {
            h = h - (height_cells - (grid_h - h));
        }

        if ((grid_w - w) < width_cells) {
            w = w - (width_cells - (grid_w - w));
        }
        setCloneName(copy);
        QString message;
        if (scene->validateSource(copy,message)) {
            QMessageBox::warning(0,"",message);
            return;
        }
        VPosition pos;
        pos.lt.setY(h);
        pos.lt.setX(w);
        pos.rb.setY(h + height_cells);
        pos.rb.setX(w + width_cells);
        connect(copy.data(),SIGNAL(sourceSeleced()),action,SLOT(updateContentTable()));
        copy->setPosition(pos);
        copy->setCellSize(pixs_per_cell_width,pixs_per_cell_height);
        elements.insert(copy->getCloneName(),copy);
        copy->start();

//        if (type == MODEL) {
//            auto source = copy.data();
//            connect(this,   SIGNAL(cell_size_changed(uint32_t,uint32_t)), source, SLOT(cell_size_changed(uint32_t,uint32_t)));
//            connect(source, SIGNAL(mouse_move(QPoint,VSource*)), this, SLOT(source_mouse_move(QPoint,VSource *)));
//            connect(source, SIGNAL(mouse_press(QPoint,VSource*)), this ,SLOT(source_mouse_press(QPoint,VSource*)));
//            connect(source, SIGNAL(mouseRightPress(QPoint)), this, SIGNAL(sourceRightClicked(QPoint)));
//            connect(source, SIGNAL(mouse_release(QPoint)), this, SLOT(source_mouse_release(QPoint)));
//            connect(this,   SIGNAL(global_mouse_move_sig(QPoint)), source, SLOT(global_mouse_move()));
//            connect(source, SIGNAL(resize_sig(QPoint,VSource*)), this, SLOT(source_resize(QPoint,VSource*)));
//            connect(source, SIGNAL(drop_sig(QString,QPoint)), this, SLOT(source_drop(QString,QPoint)));
//        }
        scene->addSource(*copy);
        placeByPos(copy.data());
        action->fill(true);
        action->fillContentForScene(scene->name());
    }
    else {
        qWarning("[Error] VCanva::AddSource - no such source");
    }
}

/**
 * @brief Slot for  source mouse move
 *
 * @param p - QPoint - ponint in grid
 * @param src - VSource * - pointer to moving source
 */
void VCanva::source_mouse_move(QPoint p,VSource*  src)
{
    QPoint po = mapFromGlobal(p);
    QPoint d = dot_to_cell(po.x(),po.y()); /* point in canva (not pixels)*/

    if (d != moved_dot) {
        int delta_w = d.x() - moved_dot.x();
        int delta_h = d.y() - moved_dot.y();

        VPosition pos = src->getPosition();

        pos.lt.setX(pos.lt.x()+delta_w);
        pos.lt.setY(pos.lt.y()+delta_h);
        pos.rb.setX(pos.rb.x()+delta_w);
        pos.rb.setY(pos.rb.y()+delta_h);

        if (over_grid(pos)) {
            QPoint newpt = cell_to_dot(moved_dot.x(),moved_dot.y());
            QCursor::setPos(newpt);
            return ;
        };
        moved_dot = d ;
        src->setPosition(pos);
        placeByPos(src);
    }
}

bool VCanva::over_grid(const VPosition pos)
{
    int32_t y_cells = height_big_cell*height_cells;
    int32_t x_cells = width_big_cell*width_cells;
    if ((pos.lt.x() < 0) || (pos.lt.y() < 0) ||
            (pos.rb.x() > x_cells) || (pos.rb.y() > y_cells)) {
        return true;
    }
    return false;
}

QScrollArea* VCanva::scrollArea()
{
    return m_scroll;
}

void VCanva::source_mouse_release(QPoint p)
{
    Q_UNUSED(p);
    moved_source = NULL;
}

QPoint VCanva::cell_to_dot(uint32_t x, uint32_t y)
{
    QPoint p;
    p.setX((x*pixs_per_cell_width)+(pixs_per_cell_width/2));
    p.setY((y*pixs_per_cell_height)+(pixs_per_cell_height/2));
    return QWidget::mapToGlobal(p);
}

void VCanva::source_mouse_press(QPoint p,VSource *source)
{
    QPoint po = mapFromGlobal(p);
    moved_source = source;

    QPoint d = dot_to_cell(po.x(),po.y());
    moved_dot = d;
    moved_head = source->getPosition().lt;
}

void VCanva::placeByPos(VSource *src)
{
    VPosition pos = src->getPosition();
    int delta_h =  pos.rb.y() - pos.lt.y();
    int delta_w = pos.rb.x() - pos.lt.x();
    if (!delta_h) delta_h = 1;
    if (!delta_w) delta_w = 1;
    src->resize_src((delta_w*pixs_per_cell_width)
                    ,(delta_h*pixs_per_cell_height));
    grid->removeWidget(src);
    grid->addWidget(src,pos.lt.y(),pos.lt.x(),
                    delta_h,
                    delta_w);
    scene->setPosition(*src);
    updateAllElementsExcept("");
    emit sourceModeSignal();
}

void VCanva::drop_bare(QString str,uint32_t w, uint32_t h)
{
    if (!scene) {
        QMessageBox::critical(0,"","Scene not selected");
        return;
    }
    addSource(str,w,h);
}

void VCanva::source_drop(QString str, QPoint p)
{
    if (!scene) {
        QMessageBox::critical(0,"",tr("Scene not selected"));
        return;
    }
    QPoint pt = mapFromGlobal(p);
    QPoint cell = dot_to_cell(pt.x(),pt.y());
    addSource(str, cell.x(), cell.y());
    spdlog::get("log")->info() << "Добавление ИИ на сцену";
}

void VCanva::global_mouse_move(QPoint p)
{
    emit global_mouse_move_sig(p);
}

void  VCanva::setheight(unsigned int height)
{
    height_big_cell = height;
}
unsigned int  VCanva::getheight(void) {
    return height_big_cell;
}

void VCanva::setwidth(unsigned int width) {
    width_big_cell = width;
}
unsigned int VCanva::getwidth(void) {
    return width_big_cell;
}

void  VCanva::setheight_cells(unsigned int height_cells) {
    this->height_cells = height_cells;
}

unsigned int  VCanva::getheight_cells(void) {
    return this->height_cells;
}

void VCanva::setwidth_cells(unsigned int width_cells) {
    this->width_cells = width_cells;
}

unsigned int VCanva::getwidth_cells(void) {
    return this->width_cells;
}

void  VCanva::setresolv_height(unsigned int resolv_height) {
    this->resolv_height=resolv_height;
}

unsigned int  VCanva::getresolv_height(void) {
    return resolv_height;
}

void VCanva::setresolv_width(unsigned int resolv_width) {
    this->resolv_width = resolv_width;
}
unsigned int VCanva::getresolv_width(void) {
    return this->resolv_width;
}

QPoint VCanva::dot_to_cell(uint32_t w, uint32_t h) {
    int cells_w = width_cells*width_big_cell;
    int cells_h = height_cells*height_big_cell;
    QPoint pos;

    if  (w > cells_w * pixs_per_cell_width) w = cells_w * pixs_per_cell_width;
    if  (h > cells_h * pixs_per_cell_height) h = cells_h * pixs_per_cell_height;
    if  (w <= 0) w =0;
    if  (h <=0 ) h =0;

    int i = 0, j = 0;
    for ( i = 0; i < cells_w; i++) {
        if ((w >= i * pixs_per_cell_width) && (w< ((i+1)*pixs_per_cell_width))) {
            break;
        }
    }
    pos.setX(i);
    for ( j = 0; j < cells_h; j++) {
        if ((h >= j * pixs_per_cell_height) && (h< ((j+1)*pixs_per_cell_height))) {
            break;
        }
    }
    pos.setY(j);
    return pos;

}

/**
 * @param pt - QPoint - new point
 * @param src - VSource * - pointer to new Source
 */
void VCanva::source_resize(QPoint pt, VSource *src) {

    QPoint p = this->mapFromGlobal(pt);

    int x = p.x(); /* new point x */
    int y = p.y(); /* new point y */
    VPosition pos = src->getPosition();
    if ((x) < 0)x = 0;
    if ((y) < 0)y = 0;
    QPoint new_cell = this->dot_to_cell(x,y);
    int src_l = pos.lt.x(); /* position of left line of source widget */
    int src_hight = pos.lt.y(); /* position of hight line of source widget */
    int src_r = pos.rb.x(); /* right line */
    int src_b = pos.rb.y();  /* butoom line */
    switch (src->resizetype()) {
    case (HIGH) :
        if (new_cell.y() < src_b ) {
            pos.lt.setY(new_cell.y());
        } else pos.lt.setY(src_b-1);
        break;
    case (LOW) :
        if (new_cell.y() > src_hight) {
            pos.rb.setY(new_cell.y());
        } else pos.rb.setY(src_hight +1);
        break;
    case (RIGHT) :
        if (new_cell.x() > src_l) {
            pos.rb.setX(new_cell.x());
        } else pos.rb.setX(src_l+1);
        break;
    case (LEFT) :
        if (new_cell.x() < src_r) {
            pos.lt.setX(new_cell.x());
        }	else  pos.lt.setX(src_r-1);
        break;
    case (LEFT_L) :
        if (new_cell.x() < src_r)	pos.lt.setX(new_cell.x());
        if (new_cell.y() > src_hight ) 	pos.rb.setY(new_cell.y());
        if (new_cell.x() >= src_r) 	pos.lt.setX(src_r-1);
        if (new_cell.y() <=src_hight)pos.rb.setY(src_hight+1);
        break;
    case (LEFT_H) :
        if (new_cell.x() < src_r) pos.lt.setX(new_cell.x());
        if (new_cell.y() < src_b) pos.lt.setY(new_cell.y());
        if (new_cell.x() > src_r) pos.lt.setX(src_r-1);
        if (new_cell.y() >= src_b) pos.lt.setY(src_b-1);
        break;
    case (RIGHT_L):
        if (new_cell.x() > src_l)	pos.rb.setX(new_cell.x());
        if (new_cell.y() > src_hight ) 	pos.rb.setY(new_cell.y());
        if (new_cell.x() <= src_l) 	pos.rb.setX(src_l+1);
        if (new_cell.y() <=src_hight) pos.rb.setY(src_hight+1);
        break;
    case (RIGHT_H) :
        if (new_cell.x() > src_l)	pos.rb.setX(new_cell.x());
        if (new_cell.y() < src_b) 	pos.lt.setY(new_cell.y());
        if (new_cell.x() <= src_l) 	pos.rb.setX(src_l+1);
        if (new_cell.y() >=src_b) pos.lt.setY(src_b-1);
        break;
    case(NONE) :
        break;
    }
    if (pos != src->getPosition()) {
        src->setPosition(pos);
        this->placeByPos(src);
    }

}

bool VCanva::dot_in_canva(QPoint p)
{
    QPoint np = QWidget::mapFromGlobal(p);
    if ((np.x() >static_cast<int>( this->getwidth())) ||
            (np.x() < 0) ||
            (np.y() > static_cast<int>(this->getheight())) ||
            (np.y() < 0)) return false;
    return true;

}

void VCanva::setMinHeight(int h)
{
    this->minheight = h;
}

void VCanva::setNewOptions(int cubsW, int cubsH, int resW, int resH, int stepW, int stepH)
{
    if (cubsW<1 || cubsH<1 || resW<100 || resH<100 || stepW<1 || stepH<1)
        return;
    setheight(cubsH);
    setwidth(cubsW);
    setheight_cells(stepH);
    setwidth_cells(stepW);
    setresolv_width(resW);
    setresolv_height(resH);
    setAllConfigs();
    emit this->cell_size_changed(pixs_per_cell_width,pixs_per_cell_height);
    d_grid->setS_Grids(cubsW*(stepW),cubsH*(stepH));
    d_grid->setB_Grids(cubsW,cubsH);
    QSize n(scrollsize.width(),scrollsize.height());
    QResizeEvent re(n,n);
    clearCanva();
    customResize(&re);
    setMinimumCell();
    setScene(scene);
}

void VCanva::setParentWgt(QWidget *prt)
{
    this->parent_wgt = prt;
}

void VCanva::setScrollSize(QSize sz)
{
    scrollsize = sz;
}

void VCanva::setScene(VScene *scene_t)
{
    if (!scene_t)
        return;
    qDebug() << Q_FUNC_INFO << scene_t->name();
    spdlog::get("log")->info() << "Set scene with name: " << scene_t->name().toStdString()
                               << " , number of content:" << scene_t->content.size();

    QList<QSharedPointer<VSource>> lst;
    scene = scene_t;
    clearCanva();
    elements.clear();
    dock()->setWindowTitle(m_title + " " + scene_t->name());
    int contentCount = scene_t->content.count();
    for (int i = contentCount-1; i >= 0; i--) {
        frame fr = scene_t->content[i];
        VSource *src = action->getSource(fr.source_name);
        if (!src)
            continue;
        QSharedPointer<VSource> copy = src->clone();
//        auto enabled = src->getEnabledFlag();
//        auto visible = fr.visible;
//        auto flag = enabled && visible;
//        copy->setEnabledFlag(flag);
        copy->setProperty("z", contentCount - i);
        copy->setname(fr.source_name);
        copy->setCloneName(fr.clone_name);
        if (fr.pos.getCropSowOnWiw()>0 && fr.pos.getCropSohOnWih()>0) {
            copy->setCropped(true);
        }
        copy->setPosition(fr.pos);
        copy->setCellSize(pixs_per_cell_width,pixs_per_cell_height);
        scene_t->validateSource2(copy);
        elements.insert(fr.clone_name,copy);
        copy->start();
        placeByPos(copy.data());
        if (type == MODEL) {
            copy->setSelected(false);
        }
        connect(copy.data(),SIGNAL(sourceSeleced()),action,SLOT(updateContentTable()));
        connect(this,SIGNAL(cell_size_changed(uint32_t,uint32_t)),
                copy.data(),SLOT(cell_size_changed(uint32_t,uint32_t)));
        connect(copy.data(),SIGNAL(resize_sig(QPoint,VSource*)),this,
                SLOT(source_resize(QPoint,VSource*)));
        if (type == MODEL) {

            connect(copy.data(),SIGNAL(mouse_move(QPoint,VSource*)),this
                    ,SLOT(source_mouse_move(QPoint,VSource *)));
            connect(copy.data(),SIGNAL(mouse_press(QPoint,VSource*)),this
                    ,SLOT(source_mouse_press(QPoint,VSource*)));
            connect(copy.data(),SIGNAL(mouse_release(QPoint)),this
                    ,SLOT(source_mouse_release(QPoint)));
            connect(this,SIGNAL(global_mouse_move_sig(QPoint)),copy.data(),
                    SLOT(global_mouse_move()));
            connect(copy.data(),SIGNAL(drop_sig(QString,QPoint)),this,
                    SLOT(source_drop(QString,QPoint)));
            connect(copy.data(),SIGNAL(sourceDoubleClicked(void)),this,
                    SLOT(slotSourceDoubleClicked(void)));
            connect(copy.data(),SIGNAL(mouseRightPress(QPoint)),this
                    ,SIGNAL(sourceRightClicked(QPoint)));

        }  else  {
            lst.push_back(copy);
        }
    }
    if (type == RUNTIME) {
        if (m_controller) {
            if (m_is_admiralty) {
                if (PluginsManager::Instance().mixerFlag()) {
                    QList<QSharedPointer<VSource>> ll = scene_t->getMixerAll(lst);
                    PluginsManager::Instance().mixer().setScene(ll);
                    PluginsManager::Instance().mixer().bridge(m_controller,lst);
                }
                if (PluginsManager::Instance().extronFlag()) {
                    QList<QSharedPointer<VSource> > ll = scene_t->getExtronAll(lst);
                    PluginsManager::Instance().extron().setScene(ll);
                }
            }
            if ((!PluginsManager::Instance().mixerFlag()) || !m_is_admiralty) {
                auto status = m_controller->status();
                if (status == status_t::connected) {
                    m_controller->setScene(lst);
                    setVisibleAll(false);
                }
                else {
                    QMessageBox::critical(this, "Ошибка контроллера", tr("Controller NOT connected"));
                }
            }
        }
    }
}

void VCanva::setCloneName(QSharedPointer<VSource>  src)
{
    int max_pos = 0;
    QString elem_name = src->getname();
    foreach(QString str,elements.keys()) {
        QSharedPointer<VSource> sc = elements.value(str);
        if (sc->getname() == src->getname()) {
            QString clone_name = elements.value(str)->getCloneName();
            QString end = clone_name.remove(0,elements.value
                                            (str)->getname().count());
            int pos = end.toInt();
            if (pos > max_pos) max_pos = pos;
        }
    }
    src->setCloneName(elem_name + QString::number(++max_pos));
}

void VCanva::setAction(VAction *action)
{
    this->action = action;
}

VScene *VCanva::getScene()
{
    return scene;
}

void VCanva::setMinimumCell()
{
    for (int i = 0; i < grid->columnCount();i++) {
        grid->setColumnMinimumWidth(i,0);
    }
    for (int i = 0; i < grid->rowCount();i++) {
        grid->setRowMinimumHeight(i,0);
    }
    auto h = height_big_cell * height_cells;
    for (unsigned int i = 0; i < h; i++) {
        grid->setRowMinimumHeight(i, pixs_per_cell_height+5); /* +1 margines */
    }
    auto w = width_big_cell * width_cells;
    for (unsigned int i = 0; i < w; i++) {
        grid->setColumnMinimumWidth(i, pixs_per_cell_width+5); /* +1 margines */
    }
}

void VCanva::setController(VController *ctr)
{
    m_controller = ctr;
}

VController* VCanva::getController(void)
{
    return m_controller;
}

void VCanva::enableSmallGrid(bool e)
{
    d_grid->enableSmall(e);
}

void VCanva::clearScene()
{
    this->scene = NULL;
}

QSharedPointer<VSource> VCanva::getCloneByName(QString clone_name)
{
    foreach(QString name,elements.keys()) {
        if (name == clone_name) {
            return	elements.value(clone_name);
        }
    }
    return {};
}

QSharedPointer<VSource> VCanva::getCloneBySrcName(QString clone_name)
{
    foreach(QString name,elements.keys()) {
        if (elements.value(name)->getname() == clone_name) {
            return elements.value(name);
        }
    }
    return {};
}

bool VCanva::setEnabledCloneSource(const QString &clone_name, bool flag)
{
    foreach(QString name, elements.keys()) {
        QSharedPointer<VSource> src = elements.value(name);
        if (!src)
            return false;
        if (name == clone_name) {
            src->setVisualise(flag);
            src->setEnabledFlag(flag);
            return true;
        }
    }
    return false;
}

void VCanva::setSelectedSource(const QString &clone_name)
{
    foreach(QString name, elements.keys()) {
        QSharedPointer<VSource> src = elements.value(name);
        if (!src)
            return;
        if (name == clone_name) {
            src->setSelected(true);
        } else {
            src->setSelected(false);
        }
    }
}

void VCanva::paintAllSources(QPainter *painter)
{
    for (auto src : elements) {
        if (!src) continue;
        auto pos = src->getPosition();

        int w = this->width();
        int h = this->height();

        int w_step = (w*1000)/(width_cells*width_big_cell);
        int h_step= (h*1000)/(height_cells*height_big_cell);

        int d_w = w_step * pos.lt.x();
        int d_h = h_step * pos.lt.y();

        src->paintAll(painter,d_w/1000,d_h/1000);
    }
}

void VCanva::updateAllElementsExcept(const QString &n)
{
    foreach(QString name, elements.keys()) {
        QSharedPointer<VSource> src = elements.value(name);
        if (src) {
            if (src->getname() != n) {
                src->update();
            }
        }
    }
}

QDockWidget *VCanva::dock()
{
    return m_dock;
}

void VCanva::slotSourceDoubleClicked(void)
{
    VSource *src = dynamic_cast<VSource *>(sender());
    if (!src) return;
    QString clone_name = src->getCloneName();
    setSelectedSource(clone_name);
}

void VCanva::setActiveSource(QString str)
{
    foreach(QString name,elements.keys()) {
        QSharedPointer<VSource> src = elements.value(name);
        if (!src) continue ;
        if (str == src->getname() ) {
            setSelectedSource(name);
        }
    }
}

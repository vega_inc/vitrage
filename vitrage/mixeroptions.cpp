#include "mixeroptions.h"
#include "ui_controlleroptions.h"

MixerOptions::MixerOptions(QWidget *parent) :
    QWidget(parent)

{
    m_pController  = NULL;
    m_connectFlag = false;
    ui = new Ui::ControllerOptions();
    ui->setupUi(this);
//    ui->lEMessage->setEnabled(false);
    hideUnused(); // скрываем неиспользуемые виджеты на форме
    connect(ui->pushButtonConnect,SIGNAL(clicked(bool)),this,SIGNAL(switchOnStateChanged(bool)));
    connect(ui->lEHost,SIGNAL(textChanged(const QString &)),this,SLOT(someChanged(const QString &)));
    connect(ui->lEName,SIGNAL(textChanged(const QString &)),this,SLOT(someChanged(const QString &)));
    connect(ui->lEPort,SIGNAL(textChanged(const QString &)),this,SLOT(someChanged(const QString &)));
    connect(ui->lEPassword,SIGNAL(textChanged(const QString &)),this,SLOT(someChanged(const QString &)));
}

MixerOptions::~MixerOptions()
{
    delete ui;
}

void MixerOptions::on_toolButtonHelp_clicked()
{
    emit sendOpenHtml("index.html?210.htm");
}

///Getters&Setters
void    MixerOptions::setHost(QString host)
{
    ui->lEHost->setText(host);
}
void    MixerOptions::setPort(int port)
{
    ui->lEPort->setText(QString::number(port));
}
void    MixerOptions::setLogin(QString name)
{
    ui->lEName->setText(name);
}
void    MixerOptions::setPassword(QString pass)
{
    ui->lEPassword->setText(pass);
}
QString MixerOptions::getHost()
{
    return ui->lEHost->text();
}
int     MixerOptions::getPort()
{
    return ui->lEPort->text().toInt();
}
QString MixerOptions::getLogin()
{
    return ui->lEName->text();
}
QString MixerOptions::getPassword()
{
    return ui->lEPassword->text();
}

void MixerOptions::loadSettings(QSettings *st)
{
    st->beginGroup("MixerOptions");
    ui->lEHost      ->setText(st->value("host","localhost"  ).toString());
    ui->lEPort      ->setText(st->value("port",""           ).toString());
    ui->lEName      ->setText(st->value("name","login"      ).toString());
    ui->lEPassword  ->setText(st->value("pass","pass"       ).toString());
    int conn = st->value("state","0"       ).toInt();
    if (conn == 1) m_connectFlag = true;
    st->endGroup();
}

void MixerOptions::saveSettings(QSettings *st)
{
 //   st->remove("MixerOptions");
    st->beginGroup("MixerOptions");
        st->setValue("host",QVariant::fromValue( ui->lEHost    ->text()));
        st->setValue("port",QVariant::fromValue( ui->lEPort    ->text()));
        st->setValue("name",QVariant::fromValue( ui->lEName    ->text()));
        st->setValue("pass",QVariant::fromValue( ui->lEPassword->text()));
        if (m_pController)
        st->setValue("state",QVariant::fromValue(static_cast<int>(m_pController->status())));
    st->endGroup();
}

void MixerOptions::hideUnused()
{
    ui->tBClose->hide();
    ui->label->hide();
    ui->cBModel->hide();
}


void MixerOptions::on_tBSave_clicked()
{
    m_pController->setProperty(CONTR_NAME,ui->lEName->text());
    m_pController->setProperty(CONTR_HOST,ui->lEHost->text());
    m_pController->setProperty(CONTR_PORT,ui->lEPort->text());
    m_pController->setProperty(CONTR_PASSWD,ui->lEPassword->text());
    ui->pushButtonConnect->setEnabled(true);
    ui->tBSave->setEnabled(false);
}

void MixerOptions::setController(VController *ctr)
{
    //if (!m_pController) return;
    m_pController = ctr;
    connect(m_pController,SIGNAL(statusChanged()),this,SLOT(newStatus()),Qt::QueuedConnection);
    m_pController->setProperty(CONTR_NAME,ui->lEName->text());
    m_pController->setProperty(CONTR_HOST,ui->lEHost->text());
    m_pController->setProperty(CONTR_PORT,ui->lEPort->text());
    m_pController->setProperty(CONTR_PASSWD,ui->lEPassword->text());
    if (m_connectFlag) m_pController->conn();

}
void MixerOptions::someChanged(const QString &str)
{
    Q_UNUSED(str);
    QVariant name, host, port, passwd;

    VController *ctr = m_pController;
    if (!ctr) {
        ui->tBSave->setEnabled(false);
        return;
    }

    if (!(ctr->getProperty(CONTR_NAME,name))||
        !(ctr->getProperty(CONTR_HOST,host))||
        !(ctr->getProperty(CONTR_PASSWD,passwd))||
        !(ctr->getProperty(CONTR_PORT,port))) {
        return;
    }

    if ((ui->lEHost->text() != host.toString()) ||
       (ui->lEName->text() != name.toString()) ||
       (ui->lEPassword->text() != passwd.toString()) ||
       (ui->lEPort->text() != port.toString())) {
    ui->tBSave->setEnabled(true);
    ui->pushButtonConnect->setEnabled(false);
    } else {
    ui->tBSave->setEnabled(false);
    ui->pushButtonConnect->setEnabled(true);
    }
}

void MixerOptions::fillStatus()
{
    if (!m_pController) return;
    status_t st = m_pController->status();
    switch (st) {
    case status_t::start:
        break;
    case status_t::connected:
        //        ui->lConnstatus->setText(tr("Connected"));
        ui->pushButtonConnect->setChecked(true);
        setPropEnabled(false);
        break;
    case status_t::stopped:
        //        ui->lConnstatus->setText(tr("Stopped"));
        ui->pushButtonConnect->setChecked(false);
        break;
    case status_t::disc:
        //        ui->lConnstatus->setText(tr("Disconnected"));
        ui->pushButtonConnect->setChecked(false);
        setPropEnabled(true);
        break;
    case status_t::error:
        //        ui->lConnstatus->setText(tr("Error"));
        ui->pushButtonConnect->setChecked(false);
        setPropEnabled(true);
        break;
    default:
        ui->pushButtonConnect->setText("Неизвестный статус");
    }
    ui->pushButtonConnect->setText(tr("On"));
}

void MixerOptions::setPropEnabled(bool en)
{
       ui->lEHost->setEnabled(en);
       ui->lEPort->setEnabled(en);
       ui->lEName->setEnabled(en);
       ui->lEPassword->setEnabled(en);
       ui->cBModel->setEnabled(en);

}
void MixerOptions::newStatus()
{
    fillStatus();
//    ui->lEMessage->setText(m_pController->errorMsg());
}

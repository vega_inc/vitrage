#ifndef VACTION_H
#define VACTION_H

#include <QObject>
#include <QList>
#include <QMap>
#include <QMenu>
#include <QFrame>

#include <stdint.h>
#include "vscene.h"
#include "vscheme.h"

class QTreeWidget;
class QTreeWidgetItem;
class QTableWidget;
class QTableWidgetItem;
class VCanva;
class FillAction;

const char SRCMAP[] = "sources";
#define SCNMAP 	    "scenes"
#define CANCAMAP	"canva"
#define ACTNAME		"actionName"
#define ICONPATH    "iconpath"
#define SCHEMEPATH  "schemePath"

#define ACTION_FILE ".act"



typedef QFrame *(*CREATOR)(QWidget * r,uint32_t duration, QTimeEdit **);
/**
 * @brief The VAction class Содержащий информацию о мероприятии
 */
class VAction : public QObject
{
    Q_OBJECT

public:
    enum class TreeType {
        NoType = 0,
        SourceWidget,
        SceneWidget,
        SceneContentWidget
    };

    enum class CanvaType {
        Runtime,
        Model
    };

    enum class SRC_TABLE { CHECK, NAME, INFO, TYPE, STATUS, COLUMN_COUNT};

    static const int SCN_TABLE_COLUMNS = 6;
    static const enum {
        SCN_RADIO,
        SCN_NAME,
        SCN_DURATION,
        SCN_COPY,
        SCN_UP,
        SCN_DOWN
    } SCN_TABLE;

    enum CNT_TABLE {
        CNT_CHECK,
        CNT_NAME,
        CNT_DOWN,
        CNT_UP
    };

private:
    bool m_running; // говорит о том запущено ли мероприятие в данный момент
    uint32_t m_duration = 0;
    uint32_t m_lastSceneDuration;
    QString m_loadPath; /* path to save and load action */
    QString m_IconPath;
    QString m_SchemePath;
    QList<VScene*> m_scenes;
    QMap<QString, VSource*> m_sourceMap;
    QList<QTreeWidgetItem*> m_treeList;
    VScene* m_pActiveScene;
    QString m_name;
    QTreeWidget *m_pSourceTree;
    QTreeWidget *m_pSceneTree;
    VScheme *m_pScheme;
    QTableWidget *m_pSourceTable;
    QTableWidget *m_pSceneTable;
    QTableWidget *m_pSceneContentTable;
    QMenu *m_pContextMenu;
    QMenu *menuSources;
    //
    QAction *removeSource;
    /* context menu actons */
    QAction *addScene;
    QAction *removeScene;
    QAction *sceneUp;
    QAction *sceneDown;
    QAction *separ;
    QAction *removeSrc;
    QAction *actionNameSort, *actionTypeSort, *actionStatusSort;
    QAction *srcBack;
    QAction *srcFront;
    QAction *srcCrop;
    QAction *srcCropDelete;
    QAction *connectAct;
    /* VCanvas */
    VCanva *mp_RunTimeCanva;
    VCanva *mp_ModelCanva;


    bool fillItemsForSource(VSource *);
    bool renameScene(QString old,QString newname);
    void setSchemeScene(VScene * scheme);

public:
    VAction(const QString& name, QTableWidget *t1, QTableWidget *t2, QTableWidget *t3);
    ~VAction();
    void fillItemsForScene(VScene *scene = nullptr);
    /**
          * Метод, предназначенный для создания сцены в мероприятии
          * @param tF время начала сцены
          * @param tD время конца сцены
          * @param name название сцены
          * @param stage этап на таймлайне, на котором будет располагаться
          */
    VScene *createScene(int count = 0, int id = 1, int tF = 0, int tD = 5,
                        const QString& name = "Сцена 1", int stage = 0);
    void renew();               // отправляет сигнал somethingChanged();
    void deleteScene(const QString &name);
    void setScheme(VScheme *scheme);
    void removeSourseComple(const QString &name);
    void renameSource(QString name, VSource *source);
    void renameSourceScene(VScene *scn,QString old, QString name);
    uint32_t getFreeSceneId(QList<uint32_t> lst = QList<uint32_t>());
    QMap <QString, VSource*> &returnSources(void);
    VScene *getScene(const QString &name) const;
    VSource* getSource(const QString &name) const;
    void addSource(const QString&, VSource*);
    void setSchemePath(const QString &path);
    QString schemePath(void);
    bool isEmpty(void);
    void setIconPath(QString path) { m_IconPath = path; }
    QString iconPath() const { return m_IconPath; }
    void start();
    void stop();
    bool hasScene(const QString &name);
    void setCanva(CanvaType type,VCanva *);
    bool isRunning() const { return m_running; }
    QString name() const { return m_name; }
    void setName(const QString& n) { m_name = n; }
    VScene* activeScene() const { return m_pActiveScene; }
    uint32_t duration() const { return m_duration; }
    QList<VSource*> sources() const { return m_sourceMap.values(); }
    QList<VScene *> scenes(uint32_t stage = 0) const;
    void fill(bool s = false);
    void fillScenes();
    void fillSourcesTable();
    void fillScenesTable();
    void fillContentForScene(const QString &str);
    void fillContentForScene(VScene *scene);
    void fillTable(QTableWidget *table, CREATOR create,FillAction * act);
    void toFront(uint32_t stage = 0);
    bool saveJson(QString path);
    bool loadJson(QString path);
    void clearSources();
    void clearScenes();
    void sendClearCanvas();
    void sendResetTimeline();
    QVariantList getScenes();
    QVariantList getSources();
    QVariantList getCanva();
    void setSources(QVariantList &lst);
    void setScenes(QVariantList &lst);
    void contextMenu(QEvent *event, TreeType treetype = TreeType::NoType);
    VScheme *scheme();
    void setLoadPath(QString path);
    QString loadPath();
    void clearAll(QList<VSource *> lst);
    void checkForActionConnect(bool &show, bool &connected);


public slots:
    void setScene(const QString& name);
    void slotNewScene(uint32_t id, uint32_t stage);
    void copyScene();
    void updateContentTable();
    void setActiveSource(QString);
    void removeScene_s();
    void sourceRighrClick(QPoint p);
    void addScene_s();
    void onRemoveSource();
    void onRemoveScene();
    void removeSrc_s();

private slots:
    void getSourceDetails();
    void changeSrcStatus(const QString &src_name);
    void sceneCell(int r,int c);
    void sceneUp_s();
    void sceneDown_s();    
    void srcBack_s();
    void srcFront_s();
    void srcCrop_s();
    void srcCropDelete_s();
    void radioToggled(bool);
    void checkToggledSources(bool);
    void checkToggledContent(bool);
    void cloneDoubleClicked(int row);
    void updateSourceTable(void);
    void actionTrigg();
    void srcConnect_s(bool b);
    void sceneTimeChanged(const QTime &time);
    void nameSort();
    void typeSort();
    void statusSort();

signals:
    void setScenesItem(int,int);
    void clearCanvas();
    void clearCanvas(QString scene);
    void openScenesWindow();
    void updateModel(VScene *);
    void newScene(VScene*);
    void resetTimeline();
    void refillTimeline();
    void setPresetScene(QString );
    void sourceStatusChanged(QString);
    void sourceRemoved(QString);
    void sourcesUpdated();
    void somethingChanged();
    void activateSource(QString);
    void sceneContentSelect(int r, int c);
    void loadStatus(int, int);
};

#endif // VACTION_H

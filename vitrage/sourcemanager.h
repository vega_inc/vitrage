#ifndef SOURCEMANAGER_H
#define SOURCEMANAGER_H

#include <QDialog>
#include <QWidget>
#include <QPair>
#include <QSpinBox>
#include <QToolButton>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QFile>
#include <QTableWidget>

#include "../vsource/vsource.h"
#include "vaction.h"
#include "vsourcemanager.h"
#include <qjson/serializer.h>
#include <qjson/parser.h>

const int MINIMAL_ROW_COUNT = 0;
const char PARAM_NAME[] = "param name";
const int TW_NAME_COLUMN = 1;

#define SETMODE_DEBUG


namespace Ui {
class SourceManager;
}

class SourceManager : public QDialog
{
    Q_OBJECT
    
public:
    explicit SourceManager(const QString &jsonPath, QWidget *parent = 0);
    ~SourceManager();
    QTableWidget * getTab();
    
private:
    QString currentSourceName;
    enum Mode
    {
        IDLE = 0,       //небыло никаких изменений
        EDIT_EXISTED,   //
        ADD_NEW
    };
    Mode mode;
    void setMode(Mode);
    QString pathToJsonFile;
    bool updating;
    QVector<QWidget*> wlist;//Содержит указатели на виджеты специфичных параметров

    QVariantList sourcesVariantList; //Список источников

    QList <QMap<QString,QVariant> > listOfParameterMap;
    Ui::SourceManager *ui;
    QMap<QString, VSourceCreator *> m_src;
    QVariantList getSourcesVariantListFromJson(QString,bool&);
    void fillTable(const QVariantList &);
    QVariantMap getSorceDataByName(const QString &, const QVariantList &, bool& ok);
    QList<VSourceCreator::Param> getParamListBySourceType(QString);

    ///работа с виджетом свойств источника
    void updateSourceTypeCB();
    void addParam(VSourceCreator::Param param, QVariant value);
    void clearParams(); /// очистка специфичных параметров
    void setIcon(QString type,QToolButton * tb);

    void removeSource(QString str);
    void addNewToSourcelist();
    void writeChangesToSourcelist();
    void writeSourceListToJson();

protected:
    void closeEvent(QCloseEvent *event);

signals:
    void sourcesChanged();

public slots:
    void open();
    void open(const QVariantList &list);

private slots:
    void on_twSources_cellClicked(int row);
    void deleteButtonClickedSlot();
    void on_pbAdd_clicked();
    void specOpsChangedSlot();
    void on_leName_textChanged (const QString &text);
    void on_cbType_currentIndexChanged ( const QString & text );

    void changedLineEdit(const QString &);
    void on_leName_editingFinished ();
    bool isCorrect();
    void on_buttonBox_clicked(QAbstractButton *button);

    QVariant getDataFromForm(); //Получение данных из формы
};

#endif // SOURCEMANAGER_H

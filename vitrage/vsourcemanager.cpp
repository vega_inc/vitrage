#include <QCoreApplication>
#include <QPluginLoader>
#include <QMessageBox>
#include <QDir>
#include <QDebug>

#include "vsourcemanager.h"

#include "spdlog/spdlog.h"

VSrcManager &VSrcManager::Instance()
{
    static VSrcManager self;
    return self;
}
/**
* @brief load all plugins for source
*
* @return true - OK
*/
bool VSrcManager::loadPlugins(void)
{
    auto path = getPluginsPath();
    QDir dir(path);
    if (!dir.exists()) {
        QString message = "Plugins directory does not exist in path, " + path;
        QMessageBox::critical(nullptr, "Plugins is not found", message);
        return false;
    }
    /* clear creators map and delete all creators */
    src_creators.clear();
    auto logger = spdlog::get("log");

    foreach (QString fileName, dir.entryList(QDir::Files)) {
        QPluginLoader loader(dir.absoluteFilePath(fileName));
        VSourceCreator *creator = qobject_cast<VSourceCreator*>(loader.instance());
        if (!creator) {
            qDebug() << loader.errorString();
            if (logger) logger->warn() << "Plugin error " << loader.errorString().toStdString();
        }
        QString type = creator->getSourceType();
        src_creators.insert(type, creator);
        if (!creator) {
            qDebug() << loader.errorString();
            if (logger) logger->warn() << "Plugin error " << loader.errorString().toStdString();
        }
        if (logger) logger->info() << "Plugin " << type.toStdString() << " is loaded";
    }
    return true;
}

VSource* VSrcManager::createVSource(const QString& type)
{
    if (src_creators.contains(type)) {
        if (VSourceCreator* crtr = src_creators.value(type)) {
            return crtr->createVSource();
        }
    }
    return nullptr;
}

QMap<QString, VSourceCreator*> VSrcManager::getSourceCreatorMap()
{
    return src_creators;
}

QString VSrcManager::getPluginsPath()
{
    auto args = QCoreApplication::arguments();
    QString appName;
    if (args.isEmpty()) {
        appName = "vitrage";
    }
    else {
        appName = QCoreApplication::arguments().at(0);
    }
    QString pluginsFilePath = FIRST_LIB_PATH
                              + QFileInfo(appName).fileName()
                              + PLUGINS_DIR_NAME;
    QDir dir(pluginsFilePath);
    if(dir.exists() && dir.cd(pluginsFilePath))
        return pluginsFilePath;
    else
    {
        return pluginsFilePath = QDir::current().canonicalPath() + PLUGINS_DIR_NAME;
    }
    qFatal("empty plugin path");
    return "";
}

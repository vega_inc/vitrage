#include "extronoptions.h"
#include "ui_controlleroptions.h"
#include <QSettings>

ExtronOptions::ExtronOptions(QWidget *parent) :
    QWidget(parent)
    //ui(new Ui::ControllerOptions)
{
    ui = new Ui::ControllerOptions();
    ui->setupUi(this);
//    ui->lEMessage->setEnabled(false);
    hideUnused(); // скрываем неиспользуемые виджеты на форме
    connect(ui->pushButtonConnect,SIGNAL(clicked(bool)),this,SIGNAL(switchOnStateChanged(bool)));
    connect(ui->lEHost,SIGNAL(textChanged(QString)),this,SLOT(someChanged(QString)));
    connect(ui->lEName,SIGNAL(textChanged(QString)),this,SLOT(someChanged(QString)));
    connect(ui->lEPort,SIGNAL(textChanged(QString)),this,SLOT(someChanged(QString)));
    connect(ui->lEPassword,SIGNAL(textChanged(QString)),this,SLOT(someChanged(QString)));
}

ExtronOptions::~ExtronOptions()
{
    delete ui;
}

///Getters&Setters
void    ExtronOptions::setHost(QString host)
{
    ui->lEHost->setText(host);
}
void    ExtronOptions::setPort(int port)
{
    ui->lEPort->setText(QString::number(port));
}
void    ExtronOptions::setLogin(QString name)
{
    ui->lEName->setText(name);
}
void    ExtronOptions::setPassword(QString pass)
{
    ui->lEPassword->setText(pass);
}
QString ExtronOptions::getHost()
{
    return ui->lEHost->text();
}
int     ExtronOptions::getPort()
{
    return ui->lEPort->text().toInt();
}
QString ExtronOptions::getLogin()
{
    return ui->lEName->text();
}
QString ExtronOptions::getPassword()
{
    return ui->lEPassword->text();
}

void ExtronOptions::loadSettings(QSettings *st)
{
    st->beginReadArray("ExtronOptions");
        ui->lEHost      ->setText(st->value("host","localhost"  ).toString());
        ui->lEPort      ->setText(st->value("port",""           ).toString());
        ui->lEName      ->setText(st->value("name","login"      ).toString());
        ui->lEPassword  ->setText(st->value("pass","pass"       ).toString());
        int conn = st->value("state","0"       ).toInt();
        if (conn == 1) m_connectFlag = true;
    st->endArray();
}

void ExtronOptions::saveSettings(QSettings *st)
{
    st->remove("ExtronOptions");
    st->beginGroup("ExtronOptions");
        st->setValue("host",QVariant::fromValue( ui->lEHost    ->text()));
        st->setValue("port",QVariant::fromValue( ui->lEPort    ->text()));
        st->setValue("name",QVariant::fromValue( ui->lEName    ->text()));
        st->setValue("pass",QVariant::fromValue( ui->lEPassword->text()));
        if (m_pController)
        st->setValue("state",QVariant::fromValue(static_cast<int>(m_pController->status())));
    st->endGroup();
}

void ExtronOptions::hideUnused()
{
    ui->tBClose->hide();
    ui->label->hide();
    ui->cBModel->hide();
}


void ExtronOptions::on_tBSave_clicked()
{
    m_pController->setProperty(CONTR_NAME,ui->lEName->text());
    m_pController->setProperty(CONTR_HOST,ui->lEHost->text());
    m_pController->setProperty(CONTR_PORT,ui->lEPort->text());
    m_pController->setProperty(CONTR_PASSWD,ui->lEPassword->text());
    ui->pushButtonConnect->setEnabled(true);
    ui->tBSave->setEnabled(false);
}
void ExtronOptions::setController(VController *ctr)
{
    m_pController = ctr;
    connect(m_pController,SIGNAL(statusChanged()),this,SLOT(newStatus()),Qt::QueuedConnection);
    m_pController->setProperty(CONTR_NAME,ui->lEName->text());
    m_pController->setProperty(CONTR_HOST,ui->lEHost->text());
    m_pController->setProperty(CONTR_PORT,ui->lEPort->text());
    m_pController->setProperty(CONTR_PASSWD,ui->lEPassword->text());
    if (m_connectFlag) m_pController->conn();
}

void ExtronOptions::someChanged(QString str)
{
    Q_UNUSED(str);
    QVariant name, host, port, passwd;

    VController *ctr = m_pController;
    if (!ctr) {
        ui->tBSave->setEnabled(false);
        return;
    }

    if (!(ctr->getProperty(CONTR_NAME,name))||
        !(ctr->getProperty(CONTR_HOST,host))||
        !(ctr->getProperty(CONTR_PASSWD,passwd))||
        !(ctr->getProperty(CONTR_PORT,port))) {
        return;
    }

    if ((ui->lEHost->text() != host.toString()) ||
       (ui->lEName->text() != name.toString()) ||
       (ui->lEPassword->text() != passwd.toString()) ||
       (ui->lEPort->text() != port.toString())) {
    ui->tBSave->setEnabled(true);
    ui->pushButtonConnect->setEnabled(false);
    } else {
    ui->tBSave->setEnabled(false);
    ui->pushButtonConnect->setEnabled(true);
    }
}

void ExtronOptions::on_toolButtonHelp_clicked()
{
    emit sendOpenHtml("index.html?210.htm");
}

void ExtronOptions::fillStatus()
{
    if (!m_pController) return;
    status_t st = m_pController->status();
    switch (st) {
    case status_t::start:
        break;
    case status_t::connected:
        ui->pushButtonConnect->setChecked(false);
        break;
    case status_t::disc:
        ui->pushButtonConnect->setChecked(false);
        setPropEnabled(true);
        break;
    case status_t::error:
        ui->pushButtonConnect->setChecked(false);
        setPropEnabled(true);
        break;
    default:
        ui->pushButtonConnect->setText("Неизвестное состояние");
    }
    ui->pushButtonConnect->setText(tr("On"));
}

void ExtronOptions::setPropEnabled(bool en)
{
    ui->lEHost->setEnabled(en);
    ui->lEPort->setEnabled(en);
    ui->lEName->setEnabled(en);
    ui->lEPassword->setEnabled(en);
    ui->cBModel->setEnabled(en);
}

void ExtronOptions::newStatus()
{
    fillStatus();
}

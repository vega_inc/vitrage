#include "vgraphicstext.h"

VGraphicsText::VGraphicsText(QGraphicsTextItem *parent) :
    QGraphicsTextItem(parent)
{
    setMode(VGraphicsWidget::EDITING);
}





void VGraphicsText::paintStatusForPicture(QPainter *painter)
{
    Q_UNUSED(painter);
    QBrush br1 = painter->brush();
    QBrush br = br1;
    switch (m_status ) {
    case (RUNTIME_ST) :
        //br.setColor();
        //painter->fillRect(QRect(0,0,this->boundingRect().width(),
//                                            this->boundingRect().height()),br1);
    break;
    case (SELECTED_ST) :
    //    br.setColor(SELECTED_COLOR);
          br.setColor(Qt::black);
//        painter->fillRect(QRect(0,0,this->boundingRect().width(),
//                                            this->boundingRect().height()),br);
                  painter->fillRect(QRect(0,0,100,100),br);
    break;
    case (ERROR_ST) :

    break;
    case (ERROR_SELECTED_ST) :
    break;
    case (OFF_ST) :
    break;
    }
}

void VGraphicsText::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                                                                      QWidget *widget)
{
    QGraphicsTextItem::paint(painter,option,widget);
    paintStatusForPicture(painter);
}

void VGraphicsText::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (m_mode == VGraphicsWidget::RUNTIME) {
    QDrag *drag = new QDrag(event->widget());
    QMimeData *mimeData = new QMimeData;
    mimeData->setText(this->toPlainText());
    drag->setMimeData(mimeData);
    drag->exec();
    }
    QGraphicsTextItem::mousePressEvent(event);
    //event->accept();
}

void VGraphicsText::setStatus(WGT_STATUS status)
{
    m_status = status;
}

/**
 * @brief VGraphicsText::setMode
 * @param mode
 */
void VGraphicsText::setMode(VGraphicsWidget::WGT_MODE mode)
{
    switch (mode) {
    case  VGraphicsWidget::EDITING  :
        //setMovable(true);
        setFlag(ItemIsSelectable);
        setFlag(ItemIsMovable,TRUE);
        setFlag(ItemIsFocusable);
       // setFlag(ItemIsPanel);
    break;
    case VGraphicsWidget::RUNTIME :
        //setMovable(false);
        setFlag(ItemIsMovable,false);
        setFlag(ItemIsSelectable,false);
    break;
    }
    m_mode = mode;
}

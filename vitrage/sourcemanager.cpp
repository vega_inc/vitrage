#include "sourcemanager.h"
#include "ui_sourcemanager.h"

#include "spdlog/spdlog.h"

const int ICON_COLUMN = 0;
const int NAME_COLUMN = 1;
const int DELETE_COLUMN = 2;

SourceManager::SourceManager(const QString &jsonPath, QWidget *parent)
    : QDialog(parent),
      pathToJsonFile(jsonPath),
      ui(new Ui::SourceManager)
{
    mode = IDLE;
    updating = true;
    ui->setupUi(this);

    ui->cbType->setProperty(PARAM_NAME,"type");
    ui->leName->setProperty(PARAM_NAME,"name");

    updateSourceTypeCB();
    ui->twSources->horizontalHeader()->setResizeMode(1,QHeaderView::Stretch);
    updating = false;
}

SourceManager::~SourceManager()
{
    delete ui;
}

QTableWidget *SourceManager::getTab()
{
    return ui->twSources;
}

void SourceManager::setMode(SourceManager::Mode _mode)
{

#ifdef SETMODE_DEBUG
    QString debugmes;
    if(mode == IDLE)                debugmes = "IDLE->";
    else if (mode == EDIT_EXISTED)  debugmes = "EDIT->";
    else if (mode == ADD_NEW)       debugmes = "ADD ->";
    else                            debugmes = "TYPE->";
#endif

    switch (_mode)
    {
    case IDLE:
        ui->twSources->setEnabled(true);
        ui->buttonBox->button(QDialogButtonBox::Apply)->setEnabled(false);
        ui->pbAdd->setEnabled(true);
#ifdef SETMODE_DEBUG
        debugmes.append("IDLE");
#endif
        break;
    case EDIT_EXISTED:
        ui->twSources->setEnabled(false);
        ui->pbAdd->setEnabled(false);
        ui->buttonBox->button(QDialogButtonBox::Apply)->setEnabled(isCorrect());
        //        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
#ifdef SETMODE_DEBUG
        debugmes.append("EDIT");
#endif
        break;
    case ADD_NEW:
        ui->twSources->setEnabled(false);
        ui->pbAdd->setEnabled(false);
        ui->buttonBox->button(QDialogButtonBox::Apply)->setEnabled(isCorrect());
        ui->cbType->setEnabled(true);

#ifdef SETMODE_DEBUG
        debugmes.append("ADD");
#endif
        break;
    default:
        QMessageBox::critical(this,"Error","Unknown mode","oh");
        break;
    }

    mode = _mode;
#ifdef SETMODE_DEBUG
    qDebug() << debugmes;
#endif
}

QVariantList SourceManager::getSourcesVariantListFromJson(QString jsonPath,bool & ok)
{
    QByteArray json;
    QFile file(jsonPath);
    ok=false;
    if(!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::critical(this,tr("Error"),tr("Can't open file"));
        return QVariantList();
    }
    json = file.readAll();
    file.close();
    QJson::Parser parser;
    bool m_ok;
    QVariantMap m_result;
    m_result = parser.parse (json, &m_ok).toMap();
    if (!m_ok)
    {
        qDebug("VAction::loadJson - Error while parsing Action");
        return QVariantList();
    }
    ok = true;
    return m_result.value(SRCMAP).toList();
}

void SourceManager::fillTable(const QVariantList &vl)
{
    ui->twSources->clear();
    ui->twSources->setRowCount(0);
    if (vl.isEmpty())
        return;
    ui->twSources->blockSignals(true);
    /// Заполняю
    foreach(QVariant var, vl) {
        QVariantMap map = var.toMap();
        if (map.isEmpty())
            break;

        QToolButton* tbTypeIcon = new QToolButton();
        setIcon(map.value("type","").toString(),tbTypeIcon);

        QToolButton* tbDeleteIcon = new QToolButton();
        setIcon("delete",tbDeleteIcon);

        connect(tbDeleteIcon,SIGNAL(clicked()),SLOT(deleteButtonClickedSlot()));
                tbDeleteIcon->setProperty("source name",QVariant::fromValue(map.value("name").toString()));

        int currentRow = ui->twSources->rowCount();
        ui->twSources->insertRow(currentRow);
        ui->twSources->setItem(currentRow,NAME_COLUMN,new QTableWidgetItem(map.value("name").toString()));
        ui->twSources->setCellWidget(currentRow,ICON_COLUMN,tbTypeIcon);
        ui->twSources->setCellWidget(currentRow,DELETE_COLUMN,tbDeleteIcon);
    }
    ui->twSources->resizeColumnsToContents();
    ui->twSources->blockSignals(false);
}

QVariantMap SourceManager::getSorceDataByName(const QString &name, const QVariantList &qvl, bool& ok)
{
    ok = false;
    foreach (QVariant var, qvl)
    {
        QVariantMap vm = var.toMap();
        if( vm.value("name").toString() == name)
        {
            ok = true;
            return vm;
        }
    }
    return QVariantMap();
}

QList<VSourceCreator::Param> SourceManager::getParamListBySourceType(QString st)
{
    auto map = VSrcManager::Instance().getSourceCreatorMap();
    if(!map.keys().contains(st)) {
        QMessageBox::critical(this,NULL,NULL,tr("error 1051"));
    }
    else {
        return map.value(st)->getParamsList();
    }
    return QList<VSourceCreator::Param>();
}

void SourceManager::updateSourceTypeCB()
{
    updating = true;
    ui->cbType->clear();
    ui->cbType->addItem("");
    ui->cbType->addItems(VSrcManager::Instance().getSourceCreatorMap().keys());

    QFormLayout *lay = dynamic_cast<QFormLayout*>(ui->groupBox_2->layout());
    if (lay) {
        QLabel    *la = new QLabel("label");
        QLineEdit *le = new QLineEdit();
        lay->addRow(la, le);
        wlist.push_back(la);
        wlist.push_back(le);
    }
    updating = false;
}

void SourceManager::addParam(VSourceCreator::Param param, QVariant value)
{
    QString paramName;
    if (param.mandatory)
        paramName = param.name + "*";
    else
        paramName = param.name;
    QLabel *Label = new QLabel(paramName);
    QLineEdit *paramPamPam = new QLineEdit(value.toString());
    connect(paramPamPam,SIGNAL(textChanged(const QString &)),SLOT(changedLineEdit(const QString &)));
    wlist.append(Label);
    paramPamPam->setProperty(PARAM_NAME,QVariant::fromValue(param.name));
    wlist.append(paramPamPam);
    ui->SpeceficOptionsLayout->addRow(Label,paramPamPam);
}

void SourceManager::open()
{
    setMode(IDLE);
    bool ok;
    sourcesVariantList = getSourcesVariantListFromJson(pathToJsonFile,ok);
    if (ok) {
        fillTable(sourcesVariantList);
    }
    else {
        qDebug()<< "* Error loadding Sources from Json file";
    }
    updateSourceTypeCB();
    if(ui->twSources->rowCount() > MINIMAL_ROW_COUNT) {
        ui->twSources->setCurrentCell(0,NAME_COLUMN);
        on_twSources_cellClicked(0);
    }
    QDialog::open();
}

void SourceManager::open(const QVariantList& list)
{
    spdlog::get("log")->info() << "Источников загруженно: " << list.size();
    sourcesVariantList = list;

    setMode(IDLE);
    fillTable(list);
    updateSourceTypeCB();
    if(ui->twSources->rowCount() > MINIMAL_ROW_COUNT) {
        ui->twSources->setCurrentCell(0,NAME_COLUMN);
        on_twSources_cellClicked(0);
    }
    QDialog::open();
}

void SourceManager::closeEvent(QCloseEvent *event)
{
    spdlog::get("log")->info() << "Закрытие окна Управление источниками";
    if (mode!=IDLE)
    {
        QMessageBox msgBox;
        QString str = tr("Do you realy want close source manager without saving?");
        msgBox.setText(str);
        QPushButton *yes = msgBox.addButton(tr("Yes"), QMessageBox::ActionRole);
        QPushButton *no = msgBox.addButton(tr("Save"),QMessageBox::ActionRole);
        QPushButton *cancel = msgBox.addButton(tr("Cancel"),QMessageBox::ActionRole);
        msgBox.exec();
        if (msgBox.clickedButton() == yes) {
            event->accept();
            return;
        } else if (msgBox.clickedButton() == no) {
            if(mode == ADD_NEW)
            {
                qDebug()<<"adding to list";
                addNewToSourcelist();
            }
            else
            {
                qDebug()<<"write to list";
                writeChangesToSourcelist();
            }

            writeSourceListToJson();

            event->accept();
            return;
        } else if (msgBox.clickedButton() == cancel) {
            event->ignore();
            return;
        }
    }
}

void SourceManager::on_twSources_cellClicked(int row)
{
    updating = true;
    clearParams();
    auto name = ui->twSources->item(row,NAME_COLUMN)->text();
    bool ok;
    QVariantMap map = getSorceDataByName(name, sourcesVariantList, ok);
    currentSourceName = map.value("name", "").toString();
    if(!ok || currentSourceName.isEmpty()) {
        QMessageBox::critical(this,"Error","* Error 1052","Oh");
    }
    else {
        ui->leName->setText(currentSourceName);
        auto type = map.value("type","").toString();
        ui->cbType->setCurrentIndex(ui->cbType->findText(type));

        QList<VSourceCreator::Param > plist = getParamListBySourceType(type);
        foreach (VSourceCreator::Param pam, plist)
        {
            if(pam.type != VSourceCreator::ICON)
                addParam(pam, map.value(pam.name));
        }
        auto text = new QLabel("* - обязательный параметр");
        wlist.append(text);
        ui->SpeceficOptionsLayout->addWidget(text);
    }
    updating = false;
}

void SourceManager::deleteButtonClickedSlot()
{
    auto sourceName = sender()->property("source name").toString();
    auto message = "Удалить источник информации: " + sourceName;
    QMessageBox msgBox;
    msgBox.setText(message);
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);
    int ret = msgBox.exec();
    switch (ret) {
    case QMessageBox::Ok:
        removeSource(sourceName);
        emit sourcesChanged();
        break;
    default:
        return;
        break;
    }
}

void SourceManager::on_pbAdd_clicked()
{
    setMode(ADD_NEW);
    updating = true;
    ui->leName->setText("Источник 1");
    ui->leName->selectAll();
    ui->leName->setFocus();
    ui->cbType->setCurrentIndex(0);
    clearParams();
    updating = false;
    ui->buttonBox->button(QDialogButtonBox::Apply)->setEnabled(isCorrect());
}

void SourceManager::specOpsChangedSlot()
{
    if(mode == IDLE )
    {
        setMode(EDIT_EXISTED);
    }
    ui->buttonBox->button(QDialogButtonBox::Apply)->setEnabled(isCorrect());
}

void SourceManager::clearParams()
{
    while (!wlist.isEmpty()) {
        delete wlist.at(0);
        wlist.pop_front();
    }
}

void SourceManager::setIcon(QString type, QToolButton *tb)
{
    QIcon icon;
    if (type == "delete") {
        icon.addFile(":/new_close.png", QSize(), QIcon::Normal, QIcon::Off);
        tb->setIcon(icon);
        return;
    }
    auto map = VSrcManager::Instance().getSourceCreatorMap();
    auto value = map.value(type, nullptr);
    if (!value)
        return;
    QList<VSourceCreator::Param> params = value->getParamsList();
    foreach (VSourceCreator::Param param, params) {
        QString str = param.desc;
        if (param.type == VSourceCreator::ICON) {
            icon.addFile(str, QSize(), QIcon::Normal, QIcon::Off);
        }
    }
    tb->setIcon(icon);
}

void SourceManager::removeSource(QString str)
{
    foreach (QVariant v, sourcesVariantList)
    {
        if (v.toMap().value("name")==str)
        {
            sourcesVariantList.removeOne(v);
            writeSourceListToJson();
            open();
            return;
        }
    }
}

void SourceManager::addNewToSourcelist()
{
    qDebug()<<"void SourceManager::addNewToSourcelist()";
    qDebug()<<"slCount"<<sourcesVariantList.count();
    sourcesVariantList.append(getDataFromForm());
    qDebug()<<"slCount"<<sourcesVariantList.count();
}

void SourceManager::writeChangesToSourcelist()
{
//    qDebug()<<"void SourceManager::writeChangesToSourcelist()";
    QVariant editedSource = getDataFromForm();
    for (int i = 0; i < sourcesVariantList.count(); ++i)
    {
        if(sourcesVariantList[i].toMap().value("name")==currentSourceName)
        {
            qDebug()<<"ELEMENT FINDED";
            sourcesVariantList[i] = editedSource;
            return;
        }
    }
}

void SourceManager::on_leName_textChanged ( const QString & text )
{
    Q_UNUSED(text);
    if(!updating)
    {
        if(mode == IDLE)
            setMode(EDIT_EXISTED);
        else
            isCorrect();
    }
}

void SourceManager::on_cbType_currentIndexChanged(const QString &text)
{
    ui->buttonBox->button(QDialogButtonBox::Apply)->setEnabled(false);
    if (!updating)
    {
        clearParams();
        if(!text.isEmpty()) {
            QList<VSourceCreator::Param> currentSourceTypeParamList = VSrcManager::Instance().getSourceCreatorMap().value(text)->getParamsList();
            foreach (VSourceCreator::Param param, currentSourceTypeParamList) {
                if (param.type != VSourceCreator::ICON)
                    addParam(param,QVariant());
            }
        }
    }
}

void SourceManager::on_buttonBox_clicked(QAbstractButton * button)
{
    if(mode != IDLE && isCorrect())
    {
        switch (ui->buttonBox->buttonRole(button)) {
        case QDialogButtonBox::AcceptRole:

            qDebug()<<"ACCEPT";
            if(mode==ADD_NEW)
            {
                qDebug()<<"adding to list";
                addNewToSourcelist();
            }
            else
            {
                qDebug()<<"write to list";
                writeChangesToSourcelist();
            }

            writeSourceListToJson();

            break;
        case QDialogButtonBox::ApplyRole:
            qDebug()<<"APPLY";

            if(mode==ADD_NEW)
                addNewToSourcelist();
            else
                writeChangesToSourcelist();

            writeSourceListToJson();
            open();

            break;
        case QDialogButtonBox::RejectRole:
            qDebug()<<"REJECT";

            break;

        default:
            break;

        }
        setMode(IDLE);
        emit sourcesChanged();
    }
}

void SourceManager::changedLineEdit(const QString &)
{
    specOpsChangedSlot();
}

void SourceManager::on_leName_editingFinished()
{
    for (int row = 0; row < ui->twSources->rowCount(); ++row)
    {
        if (ui->leName->text() == ui->twSources->item(row,TW_NAME_COLUMN)->text())
            if(ui->leName->text() != currentSourceName)
                QMessageBox::critical(NULL,NULL,tr("Source name cant be duplicated.") );
    }
}

bool SourceManager::isCorrect()
{
    for (int row = 0; row < ui->twSources->rowCount(); ++row)
    {
        if (ui->leName->text() == ui->twSources->item(row,TW_NAME_COLUMN)->text())
            if (ui->leName->text() != currentSourceName)
                return false;
    }



    if(ui->cbType->currentText().isEmpty()
      || ui->leName->text().isEmpty())
    {
        return false;
    }
    else
    {
        QString sourceName = ui->cbType->currentText();
        if(!sourceName.isEmpty())
        {
            //1. Получаем параметры (список) для данного ресурса
            QList<VSourceCreator::Param> params
                    = VSrcManager::Instance().getSourceCreatorMap().value(sourceName)->getParamsList();
            //1.1 Мне больше нравится работать с QMap и т.к. производительность сдесь не критична, преобразую лист в QMap
            QMap <QString,VSourceCreator::Param> paramMap;
            paramMap.clear();
            foreach (VSourceCreator::Param pam,params)
            {
                paramMap.insert(pam.name,pam);
            }
            //2. Перебираем виджеты, ищем виджеты параметров
            foreach(QWidget * w,wlist)
            {
                if(w->property(PARAM_NAME).isValid())
                {
                    VSourceCreator::Param currentParam = paramMap.value(w->property(PARAM_NAME).toString());
                    //3. проверяем есть ли обязательный параметр
                    if (currentParam.mandatory)
                    {
                        if(dynamic_cast<QLineEdit*>(w)->text().isEmpty()) return false;
                    }
                }
            }
        }
    }
    return true;
}

QVariant SourceManager::getDataFromForm()
{
    QVariantMap qvm;
    qvm.insert(ui->leName->property(PARAM_NAME).toString(),QVariant::fromValue(ui->leName->text()));
    qvm.insert(ui->cbType->property(PARAM_NAME).toString(),QVariant::fromValue(ui->cbType->currentText()));

    foreach (QWidget* w,wlist)
    {
        if(w->property(PARAM_NAME).isValid())
        {
            qvm.insert(w->property(PARAM_NAME).toString(),QVariant::fromValue(dynamic_cast<QLineEdit*>(w)->text()));

        }
    }
    return QVariant::fromValue(qvm);
}

void SourceManager::writeSourceListToJson()
{
    QVariantMap map2write;
    map2write.insert(SRCMAP,QVariant::fromValue(sourcesVariantList));
    QJson::Serializer serializer;
//    serializer.setIndentMode(QJson::IndentFull);
    bool ok = true;
    QByteArray json = serializer.serialize(map2write);
    if (ok)
    {
        QFile file(pathToJsonFile);
        if(!file.open(QIODevice::WriteOnly))
        {
            qDebug()<<"error "<<file.errorString();
            return;
        }

        file.write(json);
        file.close();
//        qDebug()<<"Write file ok";
    }
    else
    {
        QMessageBox::critical(NULL,NULL,tr("serialize json failed") );
    }
}

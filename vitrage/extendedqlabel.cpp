
#include "extendedqlabel.h"

ExtendedQLabel::ExtendedQLabel(const QString& text, QWidget * parent):
    QLabel(parent)

{
    this->setText(text);
//	this->setFrameShape(QFrame::Panel);
//	this->setFrameShadow(QFrame::Raised);
	setAutoFillBackground(true);
}

ExtendedQLabel::~ExtendedQLabel()
{

}

void ExtendedQLabel::mouseReleaseEvent(QMouseEvent *ev)
{
	Q_UNUSED(*ev);
    emit clicked();
}

void ExtendedQLabel::mouseMoveEvent(QMouseEvent *ev)
{
	Q_UNUSED(ev);
    emit hover();
}

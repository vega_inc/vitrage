#include "vaction.h"
#include "vsourcemanager.h"
#include "fillaction.h"

#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QTableWidget>
#include <QVariantList>
#include <QBitArray>
#include <QFile>
#include <QFileDialog>
#include <QMouseEvent>
#include <QMenu>
#include <QSpinBox>
#include <QBrush>
#include <QIcon>
#include <QHeaderView>
#include <QRadioButton>
#include <QPushButton>
#include <QVBoxLayout>
#include <QCheckBox>


#include <limits.h>
#include <qjson/parser.h>
#include <qjson/serializer.h>
#include "vcanva.h"
#include "croppingwidget.h"

#include "spdlog/spdlog.h"

#define TESTFILE "./testAction"

VAction::VAction(const QString& name, QTableWidget *t1, QTableWidget *t2, QTableWidget *t3)
    : m_running(false),
      m_pActiveScene(),
      m_name(name),
      m_pSourceTree(0),
      m_pSceneTree(0),
      m_pScheme(0),
      m_pSourceTable(t1),
      m_pSceneTable(t2),
      m_pSceneContentTable(t3)
{
    m_pSourceTable->setSortingEnabled(true);
    m_pSceneTable->setColumnCount(SCN_TABLE_COLUMNS);
    m_pSceneTable->horizontalHeader()->hide();
    connect(m_pSceneTable,SIGNAL(cellChanged(int,int)),this,SLOT(sceneCell(int,int)));
    m_pSceneContentTable->setColumnCount(4);
    m_pSceneContentTable->verticalHeader()->hide();
    m_pSceneContentTable->horizontalHeader()->hide();
    connect(m_pSceneContentTable,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(cloneDoubleClicked(int)));
    connect(m_pSceneContentTable,SIGNAL(cellClicked(int,int)),this,SLOT(cloneDoubleClicked(int)));

    m_pContextMenu = new QMenu(m_pSceneTree);
    addScene = new QAction(tr("Add Scene"),m_pContextMenu);
    QIcon addScene_i;
    addScene_i.addFile(QString::fromUtf8(":/goto-scene.png"), QSize(), QIcon::Normal, QIcon::Off);
    addScene->setIcon(addScene_i);

    QIcon removeScene_i;
    removeScene_i.addFile(QString::fromUtf8(":/delte.png"), QSize(), QIcon::Normal, QIcon::Off);
    removeScene = new QAction(tr("Remove Scene"),m_pContextMenu);
    removeScene->setIcon(removeScene_i);

    QIcon sceneUp_i;
    sceneUp_i.addFile(QString::fromUtf8(":/send_to_list_top.png"), QSize(), QIcon::Normal, QIcon::Off);
    sceneUp = new QAction(tr("Move Scene Up"),m_pContextMenu);
    sceneUp->setIcon(sceneUp_i);

    QIcon sceneDown_i;
    sceneDown_i.addFile(QString::fromUtf8(":/send_to_list_bottom.png"), QSize(), QIcon::Normal, QIcon::Off);
    sceneDown = new QAction(tr("Move Scene Down"),m_pContextMenu);
    sceneDown->setIcon(sceneDown_i);

    separ  = new QAction(m_pContextMenu);
    separ->setSeparator(true);
    QAction *separ1  = new QAction(m_pContextMenu);
    separ1->setSeparator(true);
    removeSrc = new QAction(tr("Remove Source"),m_pContextMenu);
    removeSrc->setIcon(removeScene_i);

    actionNameSort = new QAction("Сортировка по имени", m_pContextMenu);
    actionTypeSort = new QAction("Сортировка по типу", m_pContextMenu);
    actionStatusSort = new QAction("Сортировка по статусу источника", m_pContextMenu);

    QIcon srcBack_i;
    srcBack_i.addFile(QString::fromUtf8(":/send_to_back.png"), QSize(), QIcon::Normal, QIcon::Off);
    srcBack = new QAction(tr("Send to Back"),m_pContextMenu);
    srcBack->setIcon(srcBack_i);

    QIcon srcFront_i;
    srcFront_i.addFile(QString::fromUtf8(":/send_to_front.png"), QSize(), QIcon::Normal, QIcon::Off);
    srcFront = new QAction(tr("Bring to Front"),m_pContextMenu);
    srcFront->setIcon(srcFront_i);

    srcCrop = new QAction("Кадрировать", m_pContextMenu);
    srcCrop->setIcon(QIcon(":/crop.png"));

    srcCropDelete = new QAction("Показать весь контент", m_pContextMenu);
    srcCropDelete->setIcon(QIcon(":/crop_delete.png"));


    QIcon srcConnect_i;
    srcConnect_i.addFile(QString::fromUtf8(":/apply.png"), QSize(), QIcon::Normal, QIcon::Off);
    connectAct = new QAction(tr("Connection"),m_pContextMenu);
    connectAct->setCheckable(true);
    connectAct->setIcon(srcConnect_i);

    m_pContextMenu->addAction(addScene);
    m_pContextMenu->addAction(removeScene);
    m_pContextMenu->addAction(separ1);
    m_pContextMenu->addAction(sceneUp);
    m_pContextMenu->addAction(sceneDown);
    m_pContextMenu->addAction(separ);
    m_pContextMenu->addAction(srcBack);
    m_pContextMenu->addAction(srcFront);
    m_pContextMenu->addAction(srcCrop);
    m_pContextMenu->addAction(srcCropDelete);
    m_pContextMenu->addAction(separ);
    m_pContextMenu->addAction(removeSrc);

    connect(addScene,SIGNAL(triggered(bool)),this,SLOT(addScene_s()));
    connect(removeScene,SIGNAL(triggered(bool)),this,SLOT(removeScene_s()));
    connect(sceneUp,SIGNAL(triggered(bool)),this,SLOT(sceneUp_s()));
    connect(sceneDown,SIGNAL(triggered(bool)),this,SLOT(sceneDown_s()));
    connect(srcBack,SIGNAL(triggered(bool)),this,SLOT(srcBack_s()));
    connect(srcFront,SIGNAL(triggered(bool)),this,SLOT(srcFront_s()));
    connect(srcCrop,SIGNAL(triggered(bool)),this,SLOT(srcCrop_s()));
    connect(srcCropDelete,SIGNAL(triggered(bool)),this,SLOT(srcCropDelete_s()));
    connect(removeSrc,SIGNAL(triggered(bool)),this,SLOT(removeSrc_s()));

    connect(m_pContextMenu,SIGNAL(triggered(QAction*)),this,SLOT(actionTrigg()));

    setLoadPath("");

    menuSources = new QMenu(m_pSourceTable);
    removeSource = new QAction("Удалить", menuSources);
    menuSources->addAction(actionNameSort);
    menuSources->addAction(actionTypeSort);
    menuSources->addAction(actionStatusSort);
    menuSources->addSeparator();
    menuSources->addAction(removeSource);

    connect(actionNameSort, SIGNAL(triggered(bool)), this, SLOT(nameSort()));
    connect(actionTypeSort, SIGNAL(triggered(bool)), this, SLOT(typeSort()));
    connect(actionStatusSort, SIGNAL(triggered(bool)), this, SLOT(statusSort()));
    connect(removeSource, SIGNAL(triggered(bool)), this, SLOT(onRemoveSource()));
}

VAction::~VAction()
{
    for (auto s : m_scenes) {
        delete s;
    }
//    QList<VScene*>* tmp_list;
//    foreach(uint32_t key, m_sceneMap.keys()) {
//        tmp_list = &(m_sceneMap.find(key).value());
//        for (int i(0); i < tmp_list->count(); i++) {
//            delete tmp_list->at(i);
//        }
//    }
}

bool VAction::hasScene(const QString &name)
{
    for (auto s : m_scenes) {
        if (s->name() == name)
            return true;
    }
    return false;
    /*
    QList<VScene*> *tmp_list;
    foreach(uint32_t stage, m_sceneMap.keys())
    {
        tmp_list = &m_sceneMap.find(stage).value();
        foreach(VScene* scene, *tmp_list) {
            if (name ==scene->name()) return true;
        }
    }
    return false;*/
}

VScene *VAction::createScene(int count, int id, int tF, int tD, const QString &name, int stage)
{
    if (tF + tD > (int)m_duration)
        m_duration = tF + tD;

//    QList<VScene*> lst = m_scenes.toList();

    foreach(VScene *scn, m_scenes) {
        if (scn->id() == (uint32_t)id) {
            scn->setName(name);
            scn->setDuration(tD);
            emit somethingChanged();
            return scn;
        }
    }
    VScene* scene = new VScene(id, tF, tD, name);
//    QList<VScene*> *tmp_list;

    if (m_scenes.isEmpty())
        m_pActiveScene = scene;
    m_scenes << scene;

    /*
    if (m_sceneMap.contains(stage)) {
        tmp_list = &(m_sceneMap.find(stage).value());
    } else {
        QList<VScene*> list;
        m_sceneMap.insert(stage, list);
        tmp_list = &m_sceneMap.find(stage).value();
    }

    tmp_list->append(scene);
    if (count != -1)
        tmp_list->swap(count,tmp_list->count()-1);*/
    emit somethingChanged();
    return scene;
}

void VAction::renew()
{
    emit somethingChanged();
}

void VAction::addSource(const QString& name, VSource* source)
{
    if (m_sourceMap.contains(name))
        return;
    m_sourceMap.insert(name, source);
    connect(source, SIGNAL(statusForTable(QString)), this, SLOT(changeSrcStatus(QString)));
    emit somethingChanged();
}

void VAction::setSchemePath(const QString &path)
{
    m_SchemePath = path;
}

QString VAction::schemePath(void)
{
    return m_SchemePath;
}

void VAction::getSourceDetails()
{
    QString message;
    QList<QByteArray> names = sender()->dynamicPropertyNames();
    foreach(QByteArray arr, names) {
        if (QString(arr).contains("icon")) continue;
        if (QString(arr).contains("styleSheetWidgetF")) continue;
        QVariant var = dynamic_cast<QToolButton*>(sender())->property(QString(arr).toAscii().data());
        message += QString(arr) + QString(" :") + var.toString() +  QString("\r\n");
    }
    QMessageBox::information(m_pSourceTable, "Info", message);
}

bool VAction::isEmpty()
{
    return (m_sourceMap.isEmpty());
}

void VAction::start()
{
    m_running = true;
}

void VAction::stop()
{
    m_running = false;
}

void VAction::slotNewScene(uint32_t id, uint32_t stage)
{
//    QList<VScene*> *tmp_list;
//    if (m_sceneMap.contains(stage)) {
//        tmp_list = &m_sceneMap.find(stage).value();
//        m_pActiveScene = 0;
//        for (int i = 0; i < tmp_list->count(); i++) {
//            if (tmp_list->at(i)->id() == id) {
//                m_pActiveScene = tmp_list->at(i);
//            }
//        }
//    }
}

void VAction::copyScene()
{
    QString name = sender()->property("sceneName").toString();
    QString newname = name;

    while(hasScene(newname)) {
        newname += "_";
    }
    createScene(-1,getFreeSceneId(QList<uint32_t>()),0,getScene(name)->duration(),newname,0);

    for (int i = 0; i < getScene(name)->content.count();i++){
        frame fr = getScene(name)->content[i];
        getScene(newname)->content.push_back(fr);
    }
    fill(true);
    emit refillTimeline();
    emit somethingChanged();
}

QList<VScene*> VAction::scenes(uint32_t stage) const
{
    return m_scenes;
//    if (m_sceneMap.contains(stage))
//        return m_sceneMap.find(stage).value();
//    return {};
}

void VAction::fillSourcesTable()
{
    spdlog::get("log")->info() << "Добавление источников в список. Всего источников: " << m_sourceMap.size();
    m_pSourceTable->clear();
    m_pSourceTable->setRowCount(0);
    int typeRow = static_cast<int>(SRC_TABLE::COLUMN_COUNT);
    m_pSourceTable->setColumnCount(typeRow);
    m_pSourceTable->hideColumn(static_cast<int>(SRC_TABLE::TYPE));
    m_pSourceTable->hideColumn(static_cast<int>(SRC_TABLE::STATUS));
    auto setResizeMode = [](QTableWidget *table, const std::vector<uint> &columns){
        table->horizontalHeader()->hide();
        table->verticalHeader()->hide();
        table->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
        for (auto col : columns) {
            table->horizontalHeader()->setResizeMode(col, QHeaderView::ResizeToContents);
        }
    };
    setResizeMode(m_pSourceTable, {0,2}); // 0 - checkbox, 2 - info

    foreach(QString src_name, m_sourceMap.keys()) {
        VSource *src = m_sourceMap.value(src_name);
        fillItemsForSource(src);
        connect(src, SIGNAL(updateModelFromSource()), this, SLOT(updateSourceTable()));
    }
}

void VAction::fillScenesTable()
{
    m_pSceneTable->clear();
    m_pSceneTable->setRowCount(0);
    m_pSceneTable->verticalHeader()->hide();
    m_pSceneTable->horizontalHeader()->hide();

    for (auto s : m_scenes) {
        fillItemsForScene(s);
    }
    /*
    foreach(uint32_t stage, m_sceneMap.keys()) {
        auto tmp_list = m_sceneMap.find(stage).value();
        foreach(VScene* scene, tmp_list) {
            fillItemsForScene(scene);
        }
    }*/
}

void VAction::fillScenes()
{
    /*
//    QTreeWidgetItem *tmp_item = 0;
//    QList<QTreeWidgetItem *> items_list;

    if (m_pSceneTree) {
//        QList<QTreeWidgetItem*> items_list = m_pSceneTree->selectedItems();
        m_treeList = m_pSceneTree->findItems(tr("Scenes"), Qt::MatchFixedString);

        if (m_treeList.count() > 0) {
            auto tmp_item = m_treeList.at(0);
            tmp_item->takeChildren();
            QList<VScene*> *tmp_list = 0;
            foreach(uint32_t stage, m_sceneMap.keys()) {
                tmp_list = &m_sceneMap.find(stage).value();
                foreach(VScene* scene, *tmp_list) {
                    QTreeWidgetItem *tmp_src_item = new QTreeWidgetItem(QStringList(scene->name()));
                    tmp_item->addChild(tmp_src_item);
                    foreach(frame fr,scene->content) {
                        tmp_src_item->addChild(new QTreeWidgetItem(QStringList(fr.clone_name)));
                    }
                }
            }
        }
    }
    */
}

void VAction::fill(bool s)
{
    if (!s)
        fillSourcesTable();
    fillScenesTable();
    fillContentForScene(m_pActiveScene);

    emit somethingChanged();
}

void VAction::fillTable(QTableWidget *table, CREATOR create, FillAction *act)
{
    /*
    uint32_t jobs_num  = 0;
    QList<VScene*> *tmp_list = 0;

    foreach(uint32_t key, m_sceneMap.keys())    {
        jobs_num += m_sceneMap.find(key).value().size();
    }*/

    table->clearContents();
    table->setRowCount(m_scenes.size());

    int row = 0;
    for (auto scene : m_scenes) {
        QTableWidgetItem *name = new QTableWidgetItem(scene->name(),1000);
        table->setItem(row, 0, name);
        QTimeEdit *edit;
        QFrame *fr = create(table, scene->duration(), &edit);
        fr->setProperty("sceneId", scene->id());
        connect(edit, SIGNAL(dateTimeChanged(QDateTime)), act, SLOT(somethingChanged()));
        table->setCellWidget(row++, 1, fr);
    }
    /*
    foreach(uint32_t stage, m_sceneMap.keys())    {
        int32_t row = 0;
        tmp_list = &m_sceneMap.find(stage).value();
        foreach(VScene* scene, *tmp_list)        {
            QTableWidgetItem *name = new QTableWidgetItem(scene->name(),1000);
            table->setItem(row,0,name);
            QTimeEdit *edit;
            QFrame *fr = create(table, scene->duration(), &edit);
            fr->setProperty("sceneId", scene->id());
            connect(edit, SIGNAL(dateTimeChanged(QDateTime)), act, SLOT(somethingChanged()));
            table->setCellWidget(row++,1,fr);
        }
    }*/
}

VScene *VAction::getScene(const QString &name) const
{
    for (auto s : m_scenes) {
        if (name == s->name()) {
            return s;
        }
    }
    /*
    foreach(uint32_t stage, m_sceneMap.keys())    {
        auto tmp_list = &m_sceneMap.find(stage).value();
        foreach(VScene* scene, *tmp_list)        {
            if (name == scene->name()) {
                return scene;
            }
        }
    }*/
    return nullptr;
}

VSource *VAction::getSource(const QString &name) const
{
    return m_sourceMap.value(name, nullptr);
}

/**
* @brief save Action to file
*
* @return false - successful 
*/
bool VAction::saveJson(QString fileName)
{
    if (fileName == "") {
        if (m_loadPath != "") {
            fileName = m_loadPath;
        }
        else {
            fileName = QFileDialog::getSaveFileName(nullptr, "Сохранить мероприятие",
                                                    "",
                                                    "Мероприятия (*.act);;Все файлы (*.*)");
            if (fileName.isNull()) {
                return true;
            }
            if (!fileName.endsWith(ACTION_FILE)) {
                fileName.append(ACTION_FILE);
            }
        }
    }

    QVariantList devices = getSources();
    QVariantList scenes = getScenes();

    QVariantList name = {m_name};

    QVariantMap config;
    config.insert(SRCMAP,devices);
    config.insert(SCNMAP,scenes);
    config.insert(ACTNAME,name);
    config.insert(ICONPATH,iconPath());

    QVariant _sPath = "";
    config.insert(SCHEMEPATH,_sPath);
    config.insert(ICONPATH,iconPath());

    QJson::Serializer serializer;
//    serializer.setIndentMode(QJson::IndentFull);
    QByteArray json = serializer.serialize(config);
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
        return true;
    int res = file.write(json);
    file.close();
    if (res <= 0) {
        QMessageBox::critical(nullptr, "Витраж", "Невозможно сохранить в файл");
        return true;
    }
    setIconPath("");
    setLoadPath(fileName);
    return false;
}
/**
* @brief fill QVarList with all scenes in action
*
* @return QVariantList
*/
QVariantList VAction::getScenes()
{
    QVariantList list;
    QVariantMap scenes;

    for (auto s : m_scenes) {
        //scenes.insert("stage",stage);
        scenes.insert("name", s->name());
        scenes.insert("duration", s->duration());

        QVector<frame> frames = s->getContent();
        QVariantList content;
        for (int i = 0; i < frames.count();i++) {
            QVariantMap frame;
            frame.insert("source_name", frames[i].source_name);
            frame.insert("clone_name", frames[i].clone_name);
            frame.insert("position lt_x",frames[i].pos.lt.x());
            frame.insert("position lt_y",frames[i].pos.lt.y());
            frame.insert("position rb_x",frames[i].pos.rb.x());
            frame.insert("position rb_y",frames[i].pos.rb.y());
            frame.insert("position cropLxW",frames[i].pos.getCropLxW());
            frame.insert("position cropTyH",frames[i].pos.getCropTyH());
            frame.insert("position cropSowOnWiw",frames[i].pos.getCropSowOnWiw());
            frame.insert("position cropSohOnWih",frames[i].pos.getCropSohOnWih());
            content.append(frame);
        }
        scenes.insert("content",content);
        list.append(scenes);
    }

    /*
    foreach (uint32_t stage, m_sceneMap.keys()) {
        QList<VScene *> s_list = m_sceneMap.value(stage);

        foreach (VScene *sc, s_list) {
            scenes.insert("stage",stage);
            scenes.insert("name",sc->name());
            scenes.insert("duration", sc->duration());

            QVector<frame> frames = sc->getContent();
            QVariantList content;
            for (int i = 0; i < frames.count();i++) {
                QVariantMap frame;
                frame.insert("source_name", frames[i].source_name);
                frame.insert("clone_name", frames[i].clone_name);
                frame.insert("position lt_x",frames[i].pos.lt.x());
                frame.insert("position lt_y",frames[i].pos.lt.y());
                frame.insert("position rb_x",frames[i].pos.rb.x());
                frame.insert("position rb_y",frames[i].pos.rb.y());
                frame.insert("position cropLxW",frames[i].pos.getCropLxW());
                frame.insert("position cropTyH",frames[i].pos.getCropTyH());
                frame.insert("position cropSowOnWiw",frames[i].pos.getCropSowOnWiw());
                frame.insert("position cropSohOnWih",frames[i].pos.getCropSohOnWih());
                content.append(frame);
            }
            scenes.insert("content",content);
            list.append( scenes);
        }
    }*/
    return list;
}

/**
* @brief fill QVarList with all sources in action
*
* @return QVariantList
*/
QVariantList VAction::getSources()
{
    QVariantList devices;

    foreach (QString name, m_sourceMap.keys()) {
        QVariantMap source;
        VSource *src = m_sourceMap.value(name);
        source.insert("logicName", name);
        source.insert("physName", src->getPhysName());
        source.insert("type",QVariant(src->gettype()));
        QMap<QString,QVariant> map = src->getMapProperty();
        foreach(QString proper,map.keys()) {
            if (proper == QString("icon")) {
                QByteArray ar = map.value("icon").toByteArray();
                for  (int i = 0; i < ar.count();i++)    {
                    source.insert(QString("iconSet") + QString::number(i),QVariant(QChar(ar.at(i))));
                }
                continue;
            }
            source.insert(proper, map.value(proper));
        }
        devices.append(source);
    }
    return devices;
}

/**
 * @brief VAction::loadJson
 * load action from selected file
 * @param file - QString selected path
 * @return false - successfil
 */
bool VAction::loadJson(QString fileName)
{
    if (fileName == "") {
        if (loadPath() != "") {
            fileName = loadPath();
        } else return true;
    }
    QByteArray json;
    bool ok = true;
    emit clearCanvas();
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly)) return true;
    json = file.readAll();
    file.close();

    QJson::Parser parser;
    QVariantMap result = parser.parse (json, &ok).toMap();
    if (!ok) {
        QString str = tr("Can't load action");
        QMessageBox::critical(NULL,QString("FC1000"),str);
        qDebug("VAction::loadJson - Error while parsing Action");
        return true;
    }
    QVariantList srcs_list = result.value(SRCMAP).toList();
    QVariantList scns_list = result.value(SCNMAP).toList();
    QVariantList name_list = result.value(ACTNAME).toList();
    QVariant scheme_Path = result.value(SCHEMEPATH).toString();
    setIconPath(result.value(ICONPATH).toString());
    if (!((srcs_list.count()) ||  (scns_list.count())
          || (name_list.count()))) {
        QString str = tr("Can't load action");
        QMessageBox::critical(NULL,QString("FC1000"),str);
        return true;
    }
    this->setName(name_list.at(0).toString());
    emit loadStatus(0,srcs_list.count());
    clearScenes();
    clearSources();
    setSources(srcs_list);
    setScenes(scns_list);
    fill();
    setLoadPath(fileName);
    setSchemePath(scheme_Path.toString());
    return false;
}

/**
* @brief fill sources container from list
*
* @param lst - [in] QVariantList list 
*/
void VAction::setSources(QVariantList &lsts)
{
    int i = 1;
    foreach(QVariant source,lsts) {
        QVariantMap src_map  = source.toMap();
        QString str = src_map.value("type").toString();
        if (str.isEmpty()){
            qDebug("VAction::setSources - Empty string in plugin");
            return ;
        }

        VSource* src = VSrcManager::Instance().createVSource(str);
        if (!src)  {
            qDebug("VAction::setSources - Error Can't find source in sourcemanager");
            return ;
        }
        src->setname(src_map.value("logicName").toString());
        src->setPhysName(src_map.value("physName").toString());
        foreach(QString prop, src_map.keys()) {
            src->setProperty(prop, src_map.value(prop));
        }
        addSource(src->getname(),src);
        //addSource(src->getPhysName(),src);
        Q_ASSERT(src);
        QByteArray icon;

        QString tmp;

        QMap<int,QChar> map_;
        foreach(QString key,src_map.keys()) {
            if (key.startsWith(QString("iconSet"))) {
                tmp = key;
                int pos = key.remove(QString("iconSet")).toInt();
                QChar ch = src_map.value(tmp).toChar();
                map_.insert(pos,ch);
            }
        }
        QList<int> lst = map_.keys();
        qSort(lst);
        foreach(int key,map_.keys()) {
            icon.append(map_.value(key));
        }
        src->setProperty(QString("icon"),icon);
        src->start();
        emit loadStatus(i++,(int)lsts.count());
        QCoreApplication::processEvents();
    }
}
/**
* @brief fill scenes container from list
*
* @param lst - [in] QVariantList - list of scenes 
*/
void VAction::setScenes(QVariantList &lst)
{
    foreach(QVariant scene,lst) {
        QVariantMap scn_map = scene.toMap();
        VScene *scn = createScene(-1,getFreeSceneId(),0,scn_map.value("duration").toInt(),
                                  scn_map.value("name").toString(),
                                  scn_map.value("stage").toInt());

        QVariantList cont_lst = scn_map.value("content").toList();
        foreach(QVariant frame_v,cont_lst) {
            QVariantMap frame_map = frame_v.toMap();

            frame fr;
            fr.source_name =  frame_map.value("source_name").toString();
            fr.clone_name =  frame_map.value("clone_name").toString();
            fr.pos.lt.setX(frame_map.value("position lt_x").toInt());
            fr.pos.lt.setY(frame_map.value("position lt_y").toInt());
            fr.pos.rb.setX(frame_map.value("position rb_x").toInt());
            fr.pos.rb.setY(frame_map.value("position rb_y").toInt());
            fr.pos.setCropLxW(frame_map.value("position cropLxW").toDouble());
            fr.pos.setCropTyH(frame_map.value("position cropTyH").toDouble());
            fr.pos.setCropSowOnWiw(frame_map.value("position cropSowOnWiw").toDouble());
            fr.pos.setCropSohOnWih(frame_map.value("position cropSohOnWih").toDouble());
            scn->content.append(fr);
        }
    }
}

/**
* @brief clear sources container
*/
void VAction::clearSources()
{
    emit clearCanvas();
    clearAll(m_sourceMap.values());
    m_sourceMap.clear();
    emit somethingChanged();
}

void VAction::clearAll(QList<VSource *> lst)
{
    foreach(VSource *src,lst) {
        src->stop();
        src->finalize();
        disconnect(this);
        disconnect(src);
    }
}

/**
* @brief clear scenes container
*/
void VAction::clearScenes()
{
    for (auto s : m_scenes)
        delete s;
    /*
    foreach(uint32_t stage, m_sceneMap.keys()) {
        foreach(VScene * scene, m_sceneMap.value(stage)) {
            delete scene;
        }
    } */
    m_pActiveScene = nullptr;
    m_sourceMap.clear();
    m_scenes.clear();
    emit somethingChanged();
}

void VAction::contextMenu(QEvent *event, TreeType treetype)
{
    if (treetype == TreeType::SourceWidget) {
        menuSources->exec(QCursor::pos());
        return;
    }
    if (treetype == TreeType::SceneWidget) {
        QList<QTableWidgetItem*> items = m_pSceneTable->selectedItems();
        if(!items.count())
            return;
        addScene->setVisible(true);
        removeScene->setVisible(true);
        sceneUp->setVisible(true);
        sceneDown->setVisible(true);
        removeSrc->setVisible(false);
        srcBack->setVisible(false);
        srcFront->setVisible(false);
        srcCrop->setVisible(false);
        srcCropDelete->setVisible(false);
        connectAct->setVisible(false);
    }
    if (treetype == TreeType::SceneContentWidget) {
        addScene->setVisible(false);
        removeScene->setVisible(false);
        sceneUp->setVisible(false);
        sceneDown->setVisible(false);
        removeSrc->setVisible(true);
        srcBack->setVisible(true);
        srcFront->setVisible(true);
        srcCrop->setVisible(true);
        srcCropDelete->setVisible(true);
        connectAct->setVisible(true);
        bool connSh = false;
        bool connSrc = false;
        checkForActionConnect(connSh,connSrc);
        connectAct->setVisible(connSh);
        connectAct->setChecked(connSrc);
    }
    m_pContextMenu->show();
    QMouseEvent *e = static_cast<QMouseEvent*>(event);
    m_pContextMenu->move(e->globalPos());
}

void VAction::addScene_s()
{
    FillAction action(this);
    action.exec();
    emit refillTimeline();
    fill(true);
    m_pContextMenu->hide();
}

void VAction::removeScene_s()
{
    int currentRow = m_pSceneTable->currentRow();

    QTableWidgetItem *currentItem = m_pSceneTable->item(currentRow, SCN_NAME);
    if (!currentItem)
        return;

    auto currentSceneName = currentItem->text();
    deleteScene(currentSceneName);

    QTableWidgetItem *nextItem = m_pSceneTable->item(currentRow+1, SCN_NAME);
    if (nextItem) {
        m_pActiveScene = getScene(nextItem->text());
    }
    else {
        m_pActiveScene = nullptr;
    }
    emit clearCanvas();
    emit refillTimeline();
    fill();
}

void VAction::sourceRighrClick(QPoint p)
{
    QMouseEvent ev(QEvent::None,p,p,Qt::RightButton,Qt::RightButton,Qt::NoModifier);
    contextMenu(&ev, TreeType::SceneContentWidget);
}

void VAction::sceneUp_s()
{
    /*
    QVariant var = sender()->property("row");
    if (var.isValid()) {
        m_pSceneTable->selectRow(var.toInt());
    }
    int row = m_pSceneTable->currentRow();
    QTableWidgetItem *item = m_pSceneTable->item(row,SCN_NAME);
    if (!item) {
        return;
    }
    QString sceneName = item->text();

    foreach(uint32_t stage, m_sceneMap.keys()) {
        QList<VScene *>  list = m_sceneMap.value(stage);
        QString name = sceneName;
        foreach(VScene *scn, list) {
            if (scn->name() == name) {
                int ind = list.indexOf(scn);
                if (!ind)
                    return;
                m_sceneMap.remove(stage);
                list.swap(ind, ind - 1);
                m_sceneMap.insert(stage,list);
                this->fill(true);
            }
        }
    }
    m_pSceneTable->selectRow(row-1);
    emit setScenesItem(row-1,0);
    emit somethingChanged();
    emit refillTimeline();
    m_pContextMenu->hide();*/
}

void VAction::sceneDown_s()
{
    /*
    QString sceneName;
    QVariant  var = sender()->property("row");
    if (var.isValid()) {
        m_pSceneTable->selectRow(var.toInt());
    }
    int row = m_pSceneTable->currentRow();
    QTableWidgetItem *item = m_pSceneTable->item(row,SCN_NAME);
    if (!item ) {
        return;
    }
    sceneName = item->text();

    foreach(uint32_t stage, m_sceneMap.keys()) {
        QList<VScene *>  list = m_sceneMap.value(stage);
        foreach(VScene *scn, list) {
            if (scn->name() == sceneName){
                int ind = list.indexOf(scn);
                if (ind == list.count()-1) return ;
                m_sceneMap.remove(stage);
                list.swap(ind,ind + 1);
                m_sceneMap.insert(stage,list);
            }
        }
    }
    this->fill(true);
    m_pSceneTable->selectRow(row+1);
    emit setScenesItem(row+1,0);
    emit somethingChanged();
    emit refillTimeline();*/
}

void VAction::removeSrc_s()
{
    int row = m_pSceneContentTable->currentRow();
    QTableWidgetItem *item = m_pSceneContentTable->item(row,CNT_NAME);


    int row1 = m_pSceneTable->currentRow();
    QTableWidgetItem *item1 = m_pSceneTable->item(row1,SCN_NAME);
    if (!item) return;
    if (!item1) return;

    QString scene = item1->text();
    QString source = item->text();

    foreach(VScene *scn, m_scenes) {
        if (scn->name() == scene) {
            for(int i = 0; i < scn->content.count(); i++) {
                if (scn->content[i].clone_name == source) {
                    scn->content.remove(i);
                    scn->removeSourceFromConts(source);
                    emit somethingChanged();
                    break;
                }
            }
        }
    }

    /*
    foreach(uint32_t stage, m_sceneMap.keys()) {
        QList<VScene *>  list = m_sceneMap.value(stage);
        foreach(VScene *scn, list) {
            if (scn->name() == scene) {
                for(int i = 0; i < scn->content.count();i++)  {
                    if (scn->content[i].clone_name == source) {
                        // LOG(INFO) << L"Удаление ИИ " << scn->content[i].clone_name << L" со сцены.";
                        scn->content.remove(i);
                        m_sceneMap.remove(stage);
                        scn->removeSourceFromConts(source);
                        m_sceneMap.insert(stage,list);
                        emit somethingChanged();
                    }
                }
            }
        }
    }*/
    fill(true);
}

void VAction::srcFront_s()
{
    QVariant  var = sender()->property("row");
    if (var.isValid()) {
        m_pSceneContentTable->selectRow(var.toInt());
    }
    int row = m_pSceneContentTable->currentRow();
    QTableWidgetItem *item = m_pSceneContentTable->item(row,CNT_NAME);
    if (!item ) {
        return;
    }

    int row1 = m_pSceneTable->currentRow();
    QTableWidgetItem *item1 = m_pSceneTable->item(row1,SCN_NAME);
    if (!item) return;

    QString scene = item1->text();
    QString source = item->text();

    /*
    foreach(uint32_t stage, m_sceneMap.keys()) {
        QList<VScene *> list = m_sceneMap.value(stage);
        foreach(VScene *scn, list) {
            if (scn->name() == scene) {
                for(int i = 1; i < scn->content.count();i++)  {
                    if (scn->content[i].clone_name == source) {
                        frame fr = scn->content[i];
                        scn->content[i] = scn->content[i-1];
                        scn->content[i-1] = fr;
                        m_sceneMap.remove(stage);
                        m_sceneMap.insert(stage,list);
                        fill(true);
                        m_pSceneContentTable->selectRow(row-1);
                        cloneDoubleClicked(row-1);
                        return;
                    }
                }
            }
        }
    }*/
}

void VAction::srcCrop_s()
{
    QVariant var = sender()->property("row");
    if (var.isValid()) {
        m_pSceneContentTable->selectRow(var.toInt());
    }
    int row = m_pSceneContentTable->currentRow();
    QTableWidgetItem *item = m_pSceneContentTable->item(row,CNT_NAME);
    if (!item ) {
        return;
    }

    int row1 = m_pSceneTable->currentRow();
    QTableWidgetItem *item1 = m_pSceneTable->item(row1,SCN_NAME);
    if (!item1) return;

    QString scene = item1->text();
    QString source = item->text();

    foreach(VScene *scn, m_scenes) {
        if (scn->name() == scene) {
            for (int i = 0; i < scn->content.count(); i++) {
                if (scn->content[i].clone_name == source) {
                    QSharedPointer<VSource> src = mp_ModelCanva->getCloneByName(source);
                    if (!src)
                        return;
                    QSharedPointer<VSource> clone = src->clone();
                    clone->setSelected(false);
                    CroppingWidget* srcCropWidget = new CroppingWidget(src->parentWidget(),
                                                                       clone,
                                                                       src->size(),
                                                                       scn,
                                                                       src);
                    srcCropWidget->show();
                    return ;
                }
            }
        }
    }
}

void VAction::checkForActionConnect(bool &show, bool &connected)
{
    show = false;
    connected = false;
    if(!sender())
        return;
    QVariant var = sender()->property("row");
    if (var.isValid()) {
        m_pSceneContentTable->selectRow(var.toInt());
    }
    int row = m_pSceneContentTable->currentRow();
    QTableWidgetItem *item = m_pSceneContentTable->item(row,CNT_NAME);
    if (!item ) {
        return;
    }

    int row1 = m_pSceneTable->currentRow();
    QTableWidgetItem *item1 = m_pSceneTable->item(row1,SCN_NAME);
    if (!item) return;
    if (!item1) return;

    QString scene = item1->text();
    QString source = item->text();

    foreach(VScene *scn, m_scenes) {
        if (scn->name() == scene) {
            for(int i = 0; i < scn->content.count();i++) {
                if (scn->content[i].clone_name == source) {
                    QSharedPointer<VSource> src = mp_ModelCanva->getCloneByName(source);
                    if (src->gettype().contains("VNC")) {
                        show = true;
                        connected = src->srcConnected();
                    }
                    return ;
                }
            }
        }
    }
}
/// set scene from scene name
void VAction::setScene(const QString &name)
{
    auto nrow = m_pSceneTable->rowCount();
    for (int row = 0; row < nrow; ++row) {
        auto item = m_pSceneTable->item(row, 1);
        if (name == item->text()) {
            auto widget = qobject_cast<QRadioButton*>(m_pSceneTable->cellWidget(row, 0));
            widget->setChecked(true);
            return;
        }
    }
}

void VAction::srcConnect_s(bool b)
{
    QVariant  var = sender()->property("row");
    if (var.isValid()) {
        m_pSceneContentTable->selectRow(var.toInt());
    }
    int row = m_pSceneContentTable->currentRow();
    QTableWidgetItem *item = m_pSceneContentTable->item(row,CNT_NAME);
    if (!item ) {
        return;
    }

    int row1 = m_pSceneTable->currentRow();
    QTableWidgetItem *item1 = m_pSceneTable->item(row1,SCN_NAME);
    if (!item) return;
    if (!item) return;

    QString scene = item1->text();
    QString source = item->text();

    foreach(VScene *scn, m_scenes) {
        if (scn->name() == scene) {
            for(int i = 0; i < scn->content.count();i++) {
                if (scn->content[i].clone_name == source) {

                    QSharedPointer<VSource> src = mp_ModelCanva->getCloneByName(source);
                    if (src->gettype().contains("VNC")) {
                        src->connectSrc(b);
                    }
                    return ;
                }
            }
        }
    }
    /*
    foreach(uint32_t stage, m_sceneMap.keys()) {
        QList<VScene *>  list = m_sceneMap.value(stage);
        foreach(VScene *scn, list) {
            if (scn->name() == scene) {
                for(int i = 0; i < scn->content.count();i++) {
                    if (scn->content[i].clone_name == source) {

                        QSharedPointer<VSource> src = mp_ModelCanva->getCloneByName(source);
                        if (src->gettype().contains("VNC")) {
                            src->connectSrc(b);
                        }
                        return ;
                    }
                }
            }
        }
    }*/
}

void VAction::onRemoveSource()
{
    auto row = m_pSourceTable->currentRow();
    auto col = static_cast<int>(SRC_TABLE::NAME);
    QString name = m_pSourceTable->item(row, col)->text();
    removeSourseComple(name);
    m_pSourceTable->removeRow(row);
}

void VAction::onRemoveScene()
{
    removeScene_s();
}

void VAction::sceneTimeChanged(const QTime &time)
{
    QString name = sender()->property("sceneName").toString();
    auto duration = std::abs(time.secsTo(QTime()));

    auto scene = getScene(name);
    if (scene) {
        scene->setDuration(duration);
        fill(true);
    }
}

void VAction::nameSort()
{
    m_pSourceTable->sortItems(static_cast<int>(SRC_TABLE::NAME));
}

void VAction::typeSort()
{
    m_pSourceTable->sortItems(static_cast<int>(SRC_TABLE::TYPE));
}

void VAction::statusSort()
{
    m_pSourceTable->sortItems(static_cast<int>(SRC_TABLE::STATUS));
}

void VAction::srcCropDelete_s()
{
    QVariant  var = sender()->property("row");
    if (var.isValid()) {
        m_pSceneContentTable->selectRow(var.toInt());
    }
    int row = m_pSceneContentTable->currentRow();
    QTableWidgetItem *item = m_pSceneContentTable->item(row,CNT_NAME);
    if (!item ) {
        return;
    }

    int row1 = m_pSceneTable->currentRow();
    QTableWidgetItem *item1 = m_pSceneTable->item(row1,SCN_NAME);
    if (!item) return;
    if (!item) return;

    QString scene = item1->text();
    QString source = item->text();

    foreach(VScene *scn, m_scenes) {
        if (scn->name() == scene) {
            for(int i = 0; i < scn->content.count();i++)  {
                if (scn->content[i].clone_name == source) {
                    QSharedPointer<VSource> src = mp_ModelCanva->getCloneByName(source);
                    if (!src)
                        return;
                    VPosition tmpPosition = src->getPosition();

                    tmpPosition.setCropLxW(0.0);
                    tmpPosition.setCropTyH(0.0);
                    tmpPosition.setCropSowOnWiw(0.0);
                    tmpPosition.setCropSohOnWih(0.0);
                    src->setPosition(tmpPosition);
                    src->setCropped(false);
                    src->m_cropMode = true;
                    scn->setPosition(*src);

                    emit somethingChanged();
                    fill(true);

                    return;
                }
            }
        }
    }
}

void VAction::srcBack_s()
{
    QVariant var = sender()->property("row");
    if (var.isValid()) {
        m_pSceneContentTable->selectRow(var.toInt());
    }
    int row = m_pSceneContentTable->currentRow();
    QTableWidgetItem *item = m_pSceneContentTable->item(row,CNT_NAME);
    if (!item ) {
        return;
    }

    int row1 = m_pSceneTable->currentRow();
    QTableWidgetItem *item1 = m_pSceneTable->item(row1,SCN_NAME);
    if (!item1) return;
    QString scene = item1->text();
    QString source = item->text();

    /*
    foreach(uint32_t stage, m_sceneMap.keys()) {
        QList<VScene *> list = m_sceneMap.value(stage);
        foreach(VScene *scn, list) {
            if (scn->name() == scene) {
                for(int i = 0; i < scn->content.count()-1; i++) {
                    if (scn->content.at(i).clone_name == source) {
                        frame fr = scn->content[i+1];
                        scn->content[i+1] = scn->content[i];
                        scn->content[i] = fr;
                        m_sceneMap.remove(stage);
                        m_sceneMap.insert(stage,list);
                        fill(true);
                        m_pSceneContentTable->selectRow(row+1);
                        cloneDoubleClicked(row+1);
                        return;
                    }
                }
            }
        }
    }*/
}

void VAction::deleteScene(const QString &name)
{
    for (auto s : m_scenes) {
        if (s->name() == name) {
            m_scenes.removeOne(s);
            delete s;
            emit somethingChanged();
            break;
        }
    }
/*
    foreach(uint32_t stage, m_sceneMap.keys()) {
        QList<VScene*> list = m_sceneMap.value(stage);
        foreach(VScene *scn, list) {
            if (scn->name() == name){
                m_sceneMap.remove(stage);
                list.removeOne(scn);
                m_sceneMap.insert(stage,list);
                emit somethingChanged();
            }
        }
    }
    */
}

void VAction::setScheme(VScheme *scheme)
{
    m_pScheme = scheme;
    connect(this,SIGNAL(sourceStatusChanged(QString)),m_pScheme,
            SLOT(sourceStatusChanged(QString)));
    connect(scheme,SIGNAL(setActiveWidget(QString)),this,SLOT(setActiveSource(QString)));
}

/**
 * @brief VAction::removeSourseComple
 * completely remove source from action
 * @param name - QString name
 */
void VAction::removeSourseComple(const QString &name)
{
    foreach (VScene* scene, m_scenes) {
        scene->removeSource(name);
    }
    emit sourceRemoved(name);
    delete m_sourceMap.value(name);
    m_sourceMap.remove(name);
    emit somethingChanged();
}

void VAction::renameSource(QString name, VSource *source)
{
    foreach(VSource *src, m_sourceMap) {
        if (src == source) {
            src->setname(name);
            QVariant lname;
            src->getProperty("logicName",lname);
            m_sourceMap.remove(lname.toString());
            m_sourceMap.insert(name,src);
            foreach(VScene* scn, m_scenes) {
                renameSourceScene(scn,lname.toString(),name);
            }
        }
    }
}

void VAction::renameSourceScene(VScene *scn,QString old, QString name)
{
    for (int i = 0 ; i < scn->content.count(); i++) {
        frame fr = scn->content[i];
        if (fr.source_name == old) {
            scn->content.remove(i);
            fr.source_name = name;
            fr.clone_name = fr.clone_name.replace(old,name);
            scn->content.append(fr);
        }
    }
}

uint32_t VAction::getFreeSceneId(QList<uint32_t> lst )
{
    uint32_t id = 1;
    bool f = false;
    for (uint32_t i = 1 ; i < UINT_MAX; i++) {
        f = false;
        foreach(VScene *scn, m_scenes) {
            if (scn->id() == i) {
                f = true;
                break;
            }
        }
        foreach(uint32_t id_,lst) {
            if  (i == id_ ) {
                f = true;
                break;
            }
        }
        if (!f) {
            id = i;
            return id;
        }
    }
    return id;
}

VScheme* VAction::scheme(void)
{
    return m_pScheme;
}

void VAction::setLoadPath(QString path)
{
    m_loadPath = path;
}

QString VAction::loadPath()
{
    return m_loadPath;
}

/**
* @brief change status in table for single source
*
* @param src_name - [in] source name
*/
void VAction::changeSrcStatus(const QString &src_name)
{
    auto vsources = sources();
    for (int i = 0; i < m_pSourceTable->rowCount(); i++) {
        QTableWidgetItem *item = m_pSourceTable->item(i, static_cast<int>(SRC_TABLE::NAME));
        QTableWidgetItem *itemStatus = m_pSourceTable->item(i, static_cast<int>(SRC_TABLE::STATUS));
        if (item->text() == src_name) {
            foreach(VSource *src, vsources) {
                if (src->getname() == src_name && src->gettype() == "VNCClient") {
                    if (!item)
                        break;
                    auto status = src->getStatus();
                    itemStatus->setText(src->statusText());
                    switch(status) {
                    case VSource::ExecStatus::None:
                    case VSource::ExecStatus::ErrorStatus:
                        item->setIcon(QIcon("://delte.png"));
                        break;
                    case VSource::ExecStatus::RunStatus:
                        item->setIcon(QIcon("://vnc-server.png"));
                        break;
                    default:
                        item->setIcon(QIcon("://delte.png"));
                    }
                }
            }
        }
    }
    emit sourceStatusChanged(src_name);
}

QMap <QString, VSource*> &VAction::returnSources(void)
{
    return m_sourceMap;
}

void VAction::sendClearCanvas()
{
    emit clearCanvas();
}

void VAction::sendResetTimeline(void)
{
    emit resetTimeline();
}

/**
* @brief fill Items to fill collpsible frame
*
* @param table - [in] VSource *   

* @return bool 
*/
bool VAction::fillItemsForSource(VSource *src)
{
    QVariant var;
    if (!src->getProperty("logicName", var))
        return true;
    auto nameSource = var.toString();
    spdlog::get("log")->info() << "Добавление ИИ в таблицу ИИ " << nameSource.toStdString();
    QTableWidgetItem *logicName = new QTableWidgetItem(nameSource);
    logicName->setFlags(logicName->flags() ^Qt::ItemIsEditable);


    QCheckBox *box = new QCheckBox;
    box->setCheckState(Qt::Checked);
    if (!src->getEnabledFlag()) box->setCheckState(Qt::Unchecked);
    box->setProperty("sourceName",var.toString());
    box->setStyleSheet("QCheckBox::indicator {width : 15px;height : 15px;}"
                       "QCheckBox { spacing: 5px; }");
    connect(box,SIGNAL(toggled(bool)),this,SLOT(checkToggledSources(bool)));

    QIcon icon("://delte.png");
    if (src->gettype() == "VNCClient") {
        switch(src->getStatus()) {
        case VSource::ExecStatus::None:
        case VSource::ExecStatus::StopStatus:
        case VSource::ExecStatus::ErrorStatus:
            break;
        default:
            icon = QIcon("://vnc-server.png");
        }
    }
    else if (src->gettype() == "NamedSource") {
        icon = QIcon("://dvi_port.png");
    }
    else if (src->gettype() == "APPSource") {
        icon = QIcon("://vega_logo.png");
    }
    else if (src->gettype() == "FileSource") {
        icon = QIcon("://ava5.png");
    }
    logicName->setIcon(icon);

    QToolButton* tbtnIcon = new QToolButton();
    tbtnIcon->setStyleSheet("*{background-color: rgba(0,125,0,0) }");

    QMap<QString, QVariant> mapp = src->getMapProperty();
    foreach(QString arr, mapp.keys()) {
        QVariant var = mapp.value(arr);
        tbtnIcon->setProperty(arr.toAscii().data(),var);
    }
    tbtnIcon->setIcon(QIcon(":/info.png"));
    connect(tbtnIcon, SIGNAL(clicked()), this, SLOT(getSourceDetails()));

    m_pSourceTable->setRowCount(m_pSourceTable->rowCount()+1);
    // set cells
    auto r = m_pSourceTable->rowCount()-1;
    auto c = 0;
    m_pSourceTable->setCellWidget(r, c++, box);
    m_pSourceTable->setItem(r, c++, logicName);
    m_pSourceTable->setCellWidget(r, c++, tbtnIcon);
    m_pSourceTable->setItem(r, c++, new QTableWidgetItem(src->gettype()));
    auto status = new QTableWidgetItem;
    auto text = src->statusText();
    status->setData(Qt::DisplayRole, text);
    m_pSourceTable->setItem(r, c++, status);
    return false;
}

void VAction::sceneCell(int r, int c)
{
    if (c == SCN_RADIO) {
        qDebug() << m_pSceneTable->rowCount() << m_pSceneTable->colorCount();
        QRadioButton *button = dynamic_cast<QRadioButton*>(m_pSceneTable->cellWidget(r,SCN_RADIO));
        button->setChecked(true);
    }
    else if (c == SCN_NAME) {
        QRadioButton *butt = dynamic_cast<QRadioButton*>(m_pSceneTable->cellWidget(r,SCN_RADIO));
        if (!butt) return;
        QVariant name_var = butt->property("sceneName");
        QTableWidgetItem *item = m_pSceneTable->item(r,c);
        if ((!item) || (name_var.toString() == "")) return ;
        if (!renameScene(name_var.toString(),item->text())) {
            butt->setProperty("sceneName",item->text());
        } else {
            item->setText(name_var.toString());
        }
        emit refillTimeline();
    }
}

void VAction::fillItemsForScene(VScene *scn)
{
    if (!scn)
        return;

    auto rowCount = m_pSceneTable->rowCount();
    m_pSceneTable->setRowCount(rowCount + 1);

    // radio
    QRadioButton *radioButton = new QRadioButton;
    connect(radioButton, SIGNAL(toggled(bool)), this, SLOT(radioToggled(bool)));
    radioButton->setProperty("sceneName", scn->name());
    radioButton->setProperty("row",rowCount);
    QVariant sel = scn->property("selected");
    if (sel.toString() == "true") {
        radioButton->setChecked(true);
    }
    // time
    QTime time = QTime().addSecs(scn->duration());
    QTimeEdit *timeEdit = new QTimeEdit(time);
    timeEdit->setMinimumTime(QTime(0,0,10));
    timeEdit->setDisplayFormat("hh:mm:ss");
    timeEdit->setStyleSheet("QTimeEdit { background-color: rgba(0,125,0,0); }"
                            "QTimeEdit:hover { color: white }");
    timeEdit->setProperty("sceneName", scn->name());
    connect(timeEdit, SIGNAL(timeChanged(QTime)), this, SLOT(sceneTimeChanged(QTime)));
    // copy
    QPushButton *copy = new QPushButton(QIcon(":/duplicate.png"), "");
    copy->setFlat(true);
    copy->setProperty("sceneName", scn->name());
    connect(copy, SIGNAL(clicked()), this, SLOT(copyScene()));
    // up button
    QPushButton *up = new QPushButton(QIcon(":/send_to_list_top.png"), "");
    up->setFlat(true);
    up->setProperty("row",rowCount);
    connect(up, SIGNAL(clicked(bool)), this, SLOT(sceneUp_s()));
    // down button
    QPushButton *down = new QPushButton(QIcon(":/send_to_list_bottom.png"), "");
    down->setFlat(true);
    down->setProperty("row", rowCount);
    connect(down, SIGNAL(clicked(bool)), this, SLOT(sceneDown_s()));

    m_pSceneTable->setCellWidget(rowCount, SCN_RADIO, radioButton);
    m_pSceneTable->setItem(rowCount,       SCN_NAME, new QTableWidgetItem(scn->name()));
    m_pSceneTable->setCellWidget(rowCount, SCN_DURATION, timeEdit);
    m_pSceneTable->setCellWidget(rowCount, SCN_COPY, copy);
    m_pSceneTable->setCellWidget(rowCount, SCN_UP, up);
    m_pSceneTable->setCellWidget(rowCount, SCN_DOWN, down);
}

bool VAction::renameScene(QString old, QString newname)
{
    foreach(VScene* scene, m_scenes) {
        if (newname == scene->name())
            return true;
    }
    foreach(VScene* scene, m_scenes) {
        if (old == scene->name()) {
            scene->setName(newname);
            emit somethingChanged();
        }
    }
    return false;
}

void VAction::setSchemeScene(VScene *scheme)
{
    QList<QString >lst;
    foreach(frame fr,scheme->content) {
        lst.append(fr.source_name);
    }
    if (m_pScheme) {
        m_pScheme->setLightedWidgets(lst);
    }
}

void VAction::fillContentForScene(const QString &str)
{
    foreach(VScene* scene, m_scenes) {
        if (str == scene->name()) {
            if (!scene)
                return;
            m_pActiveScene = scene;
            fillContentForScene(scene);
            break;
        }
    }
}

void VAction::fillContentForScene(VScene *scene)
{
    m_pSceneContentTable->clear();
    m_pSceneContentTable->setRowCount(0);
    if (!scene)
        return;
    m_pSceneContentTable->setRowCount(scene->content.size());
    int row = 0;
    foreach(frame fr, scene->content) {
        QTableWidgetItem *itemName = new QTableWidgetItem(fr.clone_name);
        itemName->setFlags(itemName->flags() ^Qt::ItemIsEditable);
        if(VSource *src = m_sourceMap.find(fr.source_name).value()) {
            QIcon icon("://delte.png");
            if (src->gettype() == "VNCClient") {
                switch(src->getStatus()) {
                case VSource::ExecStatus::None:
                case VSource::ExecStatus::StopStatus:
                case VSource::ExecStatus::ErrorStatus:
                    break;
                default:
                    icon = QIcon("://vnc-server.png");
                }
            }
            else if (src->gettype() == "NamedSource") {
                icon = QIcon("://dvi_port.png");
            }
            else if (src->gettype() == "APPSource") {
                icon = QIcon("://vega_logo.png");
            }
            else if (src->gettype() == "FileSource") {
                icon = QIcon("://ava5.png");
            }
            itemName->setIcon(icon);
        }

        QPushButton *down = new QPushButton(QIcon(":/send_to_back.png"), "");
        down->setFlat(true);
        down->setProperty("row", row);

        QPushButton *up = new QPushButton(QIcon(":/send_to_front.png"), "");
        up->setFlat(true);
        up->setProperty("row", row);

        QCheckBox *box = new QCheckBox;
        if(VSource *src = m_sourceMap.find(fr.source_name).value()) {
            auto flag = src->getEnabledFlag();
            box->setCheckState(flag?Qt::Checked:Qt::Unchecked);
            box->setText(fr.clone_name);
            connect(src, SIGNAL(updateModelFromSource()), this, SLOT(updateContentTable()));
        }

        connect(box, SIGNAL(toggled(bool)), this, SLOT(checkToggledContent(bool)));
        connect(down, SIGNAL(clicked(bool)), this, SLOT(srcBack_s()));
        connect(up, SIGNAL(clicked(bool)), this, SLOT(srcFront_s()));

        m_pSceneContentTable->setCellWidget(row, CNT_CHECK, box);
        m_pSceneContentTable->setItem(row, CNT_NAME, itemName);
        m_pSceneContentTable->setCellWidget(row, CNT_DOWN, down);
        m_pSceneContentTable->setCellWidget(row, CNT_UP, up);

        row++;
    }
}

void VAction::radioToggled(bool b)
{
    if (!b)
        return;
    QVariant var = sender()->property("sceneName");
    QString nam = var.toString();
    VScene *scn = getScene(nam);
    if (!scn)
        return;

    foreach(VScene* scene, m_scenes) {
        scene->setProperty("selected", "false");
    }
    QVariant var1 = sender()->property("row");
    m_pSceneTable->selectRow(var1.toInt());
    scn->setProperty("selected", "true");
    emit setPresetScene(var.toString());
    emit setScenesItem(var1.toInt(),1);
    setSchemeScene(scn);
}

void VAction::checkToggledSources(bool flag)
{
    QVariant var = sender()->property("sourceName");
    if(VSource *src = m_sourceMap.find(var.toString()).value()) {
        src->setVisualise(flag);
        src->setEnabledFlag(flag);
    }
}

void VAction::checkToggledContent(bool flag)
{
    auto clone_name = qobject_cast<QCheckBox*>(sender())->text();
    for (auto scene : m_scenes) {
        for (auto& c : scene->content) {
            if (c.clone_name == clone_name) {
                c.visible = flag;
                return;
            }
        }
    }
    /*
    VSource *src = m_sourceMap.find(source_name).value();
    if (src) {
        src->setVisualise(flag);
        src->setEnabledFlag(flag);
    }*/
}

void VAction::cloneDoubleClicked(int row)
{
    QString clone_name = m_pSceneContentTable->item(row,CNT_NAME)->text();
    mp_ModelCanva->setSelectedSource(clone_name);
}
/**
 * @brief VAction::updateSourceTable
 * renew view for source model in sources table
 */
void VAction::updateSourceTable()
{
    VSource *src = dynamic_cast<VSource *>(sender());
    QString name = src->getname();
    auto col = static_cast<int>(SRC_TABLE::CHECK);
    for (int r = 0; r < m_pSourceTable->rowCount(); r++) {
        if (m_pSourceTable->item(r, static_cast<int>(SRC_TABLE::NAME))->text() == name) {
            QCheckBox *chk = static_cast<QCheckBox*>(m_pSourceTable->cellWidget(r, col));
            if (!chk)
                abort();
            chk->setChecked(src->getEnabledFlag());
        }
    }
}

void VAction::actionTrigg()
{
    QMenu *srn = dynamic_cast<QMenu*>(sender());
    if (srn) {
        srn->hide();
    }
}

void VAction::updateContentTable()
{
    auto src = qobject_cast<VSource *>(sender());
    if (!src)
        return;
    QString name = src->getname();
    for (int i = 0; i < m_pSceneContentTable->rowCount();i++) {
        QCheckBox *chk = static_cast<QCheckBox*>(m_pSceneContentTable->cellWidget(i,CNT_CHECK));
        auto srcName = chk->property("sourceName");
        if (srcName.toString() == name) {
            chk->setChecked(src->getEnabledFlag());
        }
        if (src->getCloneName() == (m_pSceneContentTable->item(i,CNT_NAME)->text())) {
            if (src->getSelected()||src->activated())  {
                m_pSceneContentTable->selectRow(i);
                if (m_pScheme)
                    m_pScheme->setSelectedSource(src->getname());
            }
        }
    }
}

void VAction::setActiveSource(QString str)
{
    emit activateSource(str);
}

void VAction::setCanva(CanvaType type, VCanva *canva)
{
    switch(type) {
    case CanvaType::Model :
        mp_ModelCanva = canva;
        break;
    case CanvaType::Runtime :
        mp_RunTimeCanva = canva;
        break;
    default :
        break;
    }
}

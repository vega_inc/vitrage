#ifndef MIXEROPTIONS_H
#define MIXEROPTIONS_H

#include <QWidget>
#include "ui_controlleroptions.h"
#include "vcontroller/vcontroller.h"
#include <QSettings>


class MixerOptions : public QWidget
{
    Q_OBJECT
    
public:
    explicit MixerOptions(QWidget *parent = 0);
    ~MixerOptions();
    void    setHost     (QString);
    void    setPort     (int    );
    void    setLogin    (QString);
    void    setPassword (QString);
    QString getHost     ();
    int     getPort     ();
    QString getLogin    ();
    QString getPassword ();
    /**
     * @brief setSettings Вызвав этот метод можно заполнить поля из ини файла.
     * @param st
     */
    void loadSettings(QSettings *st);//Установить настройки в форме
    void saveSettings(QSettings *st);//Считать настройки с формы
    void setController(VController *);
    void setPropEnabled(bool en);
    void fillStatus();

signals:
    void switchOnStateChanged(bool);
    void saveClickedSignal();
    void sendOpenHtml(const QString &page);

private slots:
    void on_tBSave_clicked();
    void newStatus();
    void someChanged(const QString &str);
    void on_toolButtonHelp_clicked();

private:
    Ui::ControllerOptions * ui;
    void hideUnused();
    VController *m_pController;
    bool m_connectFlag;
};

#endif // MIXEROPTIONS_H

#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QStackedLayout>
#include <QMainWindow>
#include <QMap>
#include <QTreeWidgetItem>
#include <QMenu>

#include <QMessageBox>
#include <QTabWidget>
#include <QDialog>
#include <QtWebKit>

#include "../vsource/vsource.h"
#include "newsource.h"
#include "canvaoptions.h"
#include "controlleroptions.h"
#include "../vcontroller/vcontroller.h"
#include "vgraphicseditor.h"
#include "collapsibleframe.h"
#include "logicsourcemanager.h"
#include "vgraphicsviewer.h"
#include "timelinewidget.h"
#include "vstatusbar.h"
#include "configpath.h"
#include "aboutprogram.h"
#include "sourcetablewidget.h"

#include "extronoptions.h" // виджет настройки экстрона
#include "mixeroptions.h"  // виджет настройки михера

class VAction;
class VCanva;

const int SOURCE_NUM = 0;  /* number in splitter */
const int SCENES_NUM = 1;  /* number in splitter */
const int BARE_NUM   = 2;

namespace Ui
{
	class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

    static QString getTranslatePath();

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void addLayoutToCanva(VCanva *canva, QLayout *lay,int row, int column,
                          int rowspan,int colspan);
    void setDefaultSettings(QSettings &settings);
    void setWindowTitle();
    void setLoadPath(const QString &path);
    QString loadPath() const;

public slots:
    void onOffExtron(bool flag);
    void onOffMixer(bool flag);
    void clearCanvas();
	void clearCanvas(QString);
    void errorFromController(QString);
    void messFromController(QString);
    void fillTimeline(void);
	void sceneRadioSet(QString str);
    void openGEditor();
    void openGEditorForAction(VAction *act);
    void setConfigs(int,int,int,int,int,int);
    void setConfigs2(int,int,int,int,int,int);
    void actionModefied();
    void schemeLoadedInEditor();
    void enableButtons(bool previous, bool next);
    void enableToRun(bool b);
    void hideGraphicsEditor(void);
    void sceneCellClicked(int row);
    void controllerStatusChanged();
    void sceneFinishedCond(bool ok);
    void getLoadStatus(int,int);

    void timelineChanged(QTimeLine::State state);
    void openHtml(const QString &page);
    void fucker();
private:
    QTranslator tor;
    QMap<QString,VSourceCreator*> src_creators;
    QList<QString> m_lastLoaded;
    QString m_loadPath; /* loading path */
    Ui::MainWindow *ui;
    CanvaOptions *m_canvaOptions;
    QTabWidget * m_pOptionsTW;          // Tab Widget настроек оборудования (контроллера, экстрона, миксера)
    ControllerOptions *m_contrOptions;  // Настролйки контроллера
    ExtronOptions * m_pExtronOptions;   // Настролйки Экстрона
    MixerOptions * m_pMixerOptions;     // Настролйки Миксера
    QDialog * m_pOptionsDialog;

    VCanva *m_modelCanva;
    VCanva *m_runtimeCanva;
    VController *m_pController = nullptr;
    VAction *m_pActiveAction;
    LogicSourceManager* m_lsmgr;
    QScrollArea *m_scrollWgtM;
    QScrollArea *m_scrollWgtRT;
    TimelineWidget *m_timeline = nullptr;
    VScene * m_bareScene;
    QDockWidget *dockSources;
    QDockWidget *dockScenes;
    QDockWidget *dockContents;
    QMainWindow *m_innerMW;
    SourceTableWidget *m_tableSources;
    QTableWidget *m_tableScenes;       /* scenes for action */
	QTableWidget *m_tableSceneContent; /* scene's content */

	CollapsibleFrame *m_SourceManager; /* sources table */
	CollapsibleFrame *m_ScenesManager; /* scenes table */
	CollapsibleFrame *m_SceneContentManager; /* content for scene table */
    VStatusBar *m_pStatusBar;

    VGraphicsEditor *m_pGraphicsEditor; /* editor object */

    AboutProgram *aboutWind;
    QWidget *helpWidget;
    QWebView helpWebView;

	void setOptionsForTable(QTableWidget *);
    void createCollapsibles();
    void initStartingMenu();
    void initAllDockWidgets(QSettings &);
    void saveSettings(void);
    void openResources(void);
    void initPlugins();
    void initServer();
    void createDefaultScene();
    void setResizeModeTables();
    bool saveActionMessageBox();
    void optionsWidgetsSetup();
    void setFirstScene();
    void restoreAfterCrash();
    void saveAction(const QString &fileName);
    void loadAction(const QString &fileName);
    QTimer timer;
    QTimer timerCheckSourceItems;
    bool control = false;
	int m_sourceOldSize;
	int m_scenesOldSize;
	int m_contentOldSize;
    /* configs */
    int m_wallW;
    int m_wallH;
    int m_cellW;
    int m_cellH;
    int m_resolvH;
    int m_resolvW;
    QList<int> m_splitterSizes;
    QList<int> m_InnerSplitterSizes;

signals:
    void global_mouse_move(QPoint);
    void setMinCanvaHeight(int);
    void setModelScene(VScene *);

protected:
    bool eventFilter(QObject *obj, QEvent *ev);
    void mouseMoveEvent(QMouseEvent *);
    void closeEvent(QCloseEvent *);

private slots:
    void onSourceMoved();
    void setRuntimeSceneFromTimeLine(QString);
    void openCanvaOptions();
    void slotCreateAction();
    void slotCreateScenary();
    void renameSource(QString name, QString oldName);
    void checkSourceItems();

	void setControllerParam(int,int,int,int,int,int);
    void on_save_action_triggered();
    void on_load_action_triggered();
    void on_actionGrid_toggled(bool arg1);
    void on_actionFit_triggered();
    void onTimeLineStatus(bool);
    void on_actionToRun_triggered();
    void timelineFinished();
    void on_actionTimePlay_triggered();
    void on_actionTimeStop_triggered();
    void on_actionControllerParams_triggered();
	void showContent(void);
	void smallSplitterMoved(int,int);
    void sourceClickedOnCanva();
    void setControllerConnects(VController *controller);
	void setCanvasForAction(void);
    void on_actionOpenScheme_triggered();
    void actionUpdatedInManager();
    void on_actionClose_triggered();
    void on_actionLoad_Action_triggered();
    void on_actionSave_As_triggered();
    void on_actionSave_Action_triggered();
    void on_actionOpen_Graphics_triggered();
    void actionTimeChanged(void);
    void on_actionStop_triggered();
    void on_actionStart_triggered();
    void on_actionRunTime_Wall_triggered(bool checked);
    void on_actionGrahics_Editor_triggered(bool checked);
    void on_actionTimeline_Widget_triggered(bool checked);
    void on_actionViewSources_triggered(bool checked);
    void on_actionViewScenes_triggered(bool checked);
    void on_actionViewContents_triggered(bool checked);

    void on_actionModelWall_triggered(bool checked);
    void on_actionZoomIn_triggered();
    void on_actionZoomOut_triggered();
    void on_actionNext_triggered();
    void on_actionPrev_triggered();

    void on_actionSources_triggered();
    void on_actionEditSource_triggered();

    void on_actionPlay_active_scene_triggered();
    void on_actionWall_options_triggered();
    void on_actionController_options_triggered();

    void on_actionStatusBar_triggered(bool checked);

    void on_actionStandart_triggered();
    void on_actionPlaying_triggered();
    void on_actionConfigurations_triggered();
    void on_actionScale_triggered();
    void setController(VController *str);

    void on_actionCascade_triggered();
    void on_actionActionTile_triggered();
    void on_actionActionClear_triggered();
    void on_actionAboutVitrage_triggered();
    void on_actionHelp_triggered();
    void on_actionHelpMainWindow_triggered();
    void on_actionCloseAction_triggered();

    void on_actionChangeUiMode_triggered(bool flag);
    void on_actionResetUi_triggered();
};

#endif // MAINWINDOW_H

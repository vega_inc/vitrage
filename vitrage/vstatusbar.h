#ifndef VSTATUSBAR_H
#define VSTATUSBAR_H

#include <QObject>
#include <QStatusBar>
#include <QDateTimeEdit>
#include "QSpacerItem"
#include "QHBoxLayout"
#include "QLabel"
#include "QTimer"

class VStatusBar : public QObject
{
    Q_OBJECT
public:
    explicit VStatusBar(QObject *parent = 0);
    explicit VStatusBar(QStatusBar * bar);
    ~VStatusBar();

    void setConnectStatus(const QString &str);
    void setMessage(const QString &mess);
    void setStatusBar(QStatusBar * bar);
    
private:
    QStatusBar * m_pStatusBar;
    QWidget *m_pMainWgt;
    QHBoxLayout *m_pLay;
    QLabel *m_pMessage;
    QLabel *m_pConnect;
    QLabel *m_pDateTime;
    QTimer *m_pTimer;

private slots:
    void renewTime(void);
};

#endif // VSTATUSBAR_H

#ifndef ARROW_H
#define ARROW_H

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsLineItem>
#include <QMouseEvent>

#include "diagramitem.h"
#include "../vsource/vsource.h"

QT_BEGIN_NAMESPACE
class QGraphicsPolygonItem;
class QGraphicsLineItem;
class QGraphicsScene;
class QRectF;
class QGraphicsSceneMouseEvent;
class QPainterPath;
QT_END_NAMESPACE

class Arrow : public QGraphicsLineItem
{
public:
    enum { Type = UserType + 4 };

    Arrow(DiagramItem *startItem, DiagramItem *endItem, QGraphicsItem *parent = 0, QGraphicsScene *scene = 0);
    Arrow(const QLineF &line, QGraphicsItem *parent = 0, QGraphicsScene *scene = 0);

    int type() const
    { return Type; }
    QRectF boundingRect() const;
    QPainterPath shape() const;
    void setColor(const QColor &color)
    { myColor = color; }
    DiagramItem *startItem() const
    { return myStartItem; }
    DiagramItem *endItem() const
    { return myEndItem; }

    void updatePosition();
    void setSource(VSource* s) { source = s; }
    void createEntity();

protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget = 0);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private:
    VSource *source = nullptr;
    QLineF m_line;
    DiagramItem *myStartItem;
    DiagramItem *myEndItem;
    QColor myColor;
    QPolygonF arrowHead;

    bool m_moving;

    void changeLine(const QLineF &line);
};

#endif

#ifndef MARKERVIEW_H
#define MARKERVIEW_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QEvent>

class MarkerView : public QGraphicsView
{
    Q_OBJECT

public:
    MarkerView(QGraphicsScene *scene) :
        QGraphicsView(scene)
    {
    }
protected:
    void mousePressEvent(QMouseEvent *event) {
        QGraphicsView::mousePressEvent(event);
    }
};

#endif // MARKERVIEW_H

#include <QMenu>
#include <QPainter>
#include <QGraphicsSceneContextMenuEvent>
#include <QDebug>

#include "diagramitem.h"
#include "arrow.h"
#include "vega/proto.h"

#include <qjson/qjson_export.h>
#include <qjson/serializer.h>
#include <qjson/parser.h>

DiagramItem::DiagramItem(DiagramType diagramType, QMenu *contextMenu,
                         QGraphicsItem *parent, QGraphicsScene *scene)
    : QGraphicsPolygonItem(parent, scene)
{
    myDiagramType = diagramType;
    myContextMenu = contextMenu;

    QPainterPath path;
    switch (myDiagramType) {
    case StartEnd:
        path.moveTo(200, 50);
        path.arcTo(150, 0, 50, 50, 0, 90);
        path.arcTo(50, 0, 50, 50, 90, 90);
        path.arcTo(50, 50, 50, 50, 180, 90);
        path.arcTo(150, 50, 50, 50, 270, 90);
        path.lineTo(200, 25);
        myPolygon = path.toFillPolygon();
        break;
    case Conditional:
        myPolygon << QPointF(-100, 0) << QPointF(0, 100)
                  << QPointF(100, 0) << QPointF(0, -100)
                  << QPointF(-100, 0);
        break;
    case Rectangle:
        myPolygon << QPointF(-100, -100) << QPointF(100, -100)
                  << QPointF(100, 100) << QPointF(-100, 100)
                  << QPointF(-100, -100);
        break;
    default:
        myPolygon << QPointF(-120, -80) << QPointF(-70, 80)
                  << QPointF(120, 80) << QPointF(70, -80)
                  << QPointF(-120, -80);
        break;
    }
    setPolygon(myPolygon);
    setFlag(QGraphicsItem::ItemIsMovable, true);
    setFlag(QGraphicsItem::ItemIsSelectable, true);
    setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
}

void DiagramItem::removeArrow(Arrow *arrow)
{
    int index = arrows.indexOf(arrow);

    if (index != -1)
        arrows.removeAt(index);
}

void DiagramItem::removeArrows()
{
    foreach (Arrow *arrow, arrows) {
        arrow->startItem()->removeArrow(arrow);
        arrow->endItem()->removeArrow(arrow);
        scene()->removeItem(arrow);
        delete arrow;
    }
}

void DiagramItem::addArrow(Arrow *arrow)
{
    arrows.append(arrow);
}

QPixmap DiagramItem::image() const
{
    QPixmap pixmap(250, 250);
    pixmap.fill(Qt::transparent);
    QPainter painter(&pixmap);
    painter.setPen(QPen(Qt::black, 8));
    painter.translate(125, 125);
    painter.drawPolyline(myPolygon);

    return pixmap;
}

void DiagramItem::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    scene()->clearSelection();
    setSelected(true);
    myContextMenu->exec(event->screenPos());
}

QVariant DiagramItem::itemChange(GraphicsItemChange change,
                                 const QVariant &value)
{
    if (change == QGraphicsItem::ItemPositionChange) {
        foreach (Arrow *arrow, arrows) {
            arrow->updatePosition();
        }
    }

    return value;
}

void DiagramItem::createEntity()
{
    if (!source)
        return;

    using namespace proto;

    QByteArray ba;
    for (int i = 0; i < myPolygon.size()-1; i++) {
        QVariantMap json;
        json.insert(field::type, type::command);
        json.insert(field::command, commands::send_source_cmd);
        QVariantMap args;
        args.insert(args::source_name, drawing::source_name);
        args.insert(field::command, commands::create_entity);
        {
            QVariantMap args2;
            args2.insert(args::type, "arrow");
            QVariantList points;
            auto point1 = myPolygon.at(i);
            auto point2 = myPolygon.at(i+1);
            QVariantList p1 = { point1.x(), point1.y() };
            QVariantList p2 = { point2.x(), point2.y() };
            points << QVariant::fromValue(p1) << QVariant::fromValue(p2);
            args2.insert(entities::points, points);
            args2.insert(entities::line_width, 10);
            QVariantList color;
            color << myColor.redF() << myColor.greenF() << myColor.blueF() << myColor.alphaF();
            args2.insert(entities::line_color, color);
            args2.insert(entities::start, "-1");
            args2.insert(entities::end, "-1");
            args.insert(field::args, args2);
        }
        json.insert(field::args, args);
        QJson::Serializer serializer;
        bool ok;
        auto ba_ = serializer.serialize(json, &ok);
        if (!ok) {
            qCritical() << "[Arrow] Json serialize error:" << serializer.errorMessage();
            return;
        }
        else {
            ba.append(ba_);
            ba.append('\n');
        }
    }
    source->setShapeCmd(ba);
}

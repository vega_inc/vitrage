#include <QtGui>

#include "diagramtextitem.h"
#include "diagramscene.h"

#include "vega/proto.h"
#include <qjson/qjson_export.h>
#include <qjson/serializer.h>
#include <qjson/parser.h>


DiagramTextItem::DiagramTextItem(QGraphicsItem *parent, QGraphicsScene *scene)
    : QGraphicsTextItem(parent, scene)
{
    setFlag(QGraphicsItem::ItemIsMovable);
    setFlag(QGraphicsItem::ItemIsSelectable);
}

void DiagramTextItem::createEntity()
{
    if (!source)
        return;

    using namespace proto;

    QVariantMap json;
    json.insert(field::type,    type::command);
    json.insert(field::command, commands::send_source_cmd);
    QVariantMap arg1;
    arg1.insert(args::source_name,  drawing::source_name);
    arg1.insert(field::command,     commands::create_entity);
    {
        QVariantMap args2;
        args2.insert(args::type, "caption");
        args2.insert(args::text, "Qt5");
        args2.insert(args::font_family, "/usr/share/fonts/ttf/liberation/LiberationMono-Regular.ttf");
        args2.insert(args::size, 24);
        QVariantList color;
        color << myColor.redF() << myColor.greenF() << myColor.blueF() << myColor.alphaF();
        args2.insert(args::color, color);
        args2.insert(args::z, 50);
        args2.insert("align", 2);
        args2.insert("valign", 1);
        arg1.insert(field::args, args2);
    }
    json.insert(field::args, arg1);
    QJson::Serializer serializer;
    bool ok;
    auto ba = serializer.serialize(json, &ok);
    if (!ok) {
        qCritical() << "[Arrow] Json serialize error:" << serializer.errorMessage();
        return;
    }
    //else qDebug() << ba;
    source->setShapeCmd(ba);
}

QVariant DiagramTextItem::itemChange(GraphicsItemChange change,
                     const QVariant &value)
{
    if (change == QGraphicsItem::ItemSelectedHasChanged)
        emit selectedChange(this);
    return value;
}

void DiagramTextItem::focusOutEvent(QFocusEvent *event)
{
    setTextInteractionFlags(Qt::NoTextInteraction);
    emit lostFocus(this);
    QGraphicsTextItem::focusOutEvent(event);
}

void DiagramTextItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    if (textInteractionFlags() == Qt::NoTextInteraction)
        setTextInteractionFlags(Qt::TextEditorInteraction);
    QGraphicsTextItem::mouseDoubleClickEvent(event);
}


#include <QPen>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QDebug>

#include "arrow.h"
#include "vega/proto.h"
#include <qjson/qjson_export.h>
#include <qjson/serializer.h>
#include <qjson/parser.h>

const qreal Pi = 3.14;

//! [0]
Arrow::Arrow(DiagramItem *startItem, DiagramItem *endItem,
             QGraphicsItem *parent, QGraphicsScene *scene)
    : QGraphicsLineItem(parent, scene)
{
    myStartItem = startItem;
    myEndItem = endItem;
    setFlag(QGraphicsItem::ItemIsSelectable, true);
    myColor = Qt::black;
    setPen(QPen(myColor, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
}

Arrow::Arrow(const QLineF &line, QGraphicsItem *parent, QGraphicsScene *scene)
    : QGraphicsLineItem(parent, scene), m_line(line)
{
    setFlag(QGraphicsItem::ItemIsMovable);
    setFlag(QGraphicsItem::ItemIsSelectable);
    myColor = Qt::black;
    setPen(QPen(myColor, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    changeLine(line);
}

QRectF Arrow::boundingRect() const
{
    qreal extra = (pen().width() + 20)/* / 2.0*/;

    return QRectF(line().p1(), QSizeF(line().p2().x() - line().p1().x(),
                                      line().p2().y() - line().p1().y()))
            .normalized()
            .adjusted(-extra, -extra, extra, extra);
}

QPainterPath Arrow::shape() const
{
    QPainterPath path = QGraphicsLineItem::shape();
    path.addPolygon(arrowHead);
    return path;
}

void Arrow::updatePosition()
{
    QLineF line(mapFromItem(myStartItem, 0, 0), mapFromItem(myEndItem, 0, 0));
    changeLine(line);
}

void Arrow::createEntity()
{
    if (!source)
        return;

    using namespace proto;

    QVariantMap json;
    json.insert(field::type, type::command);
    json.insert(field::command, commands::send_source_cmd);
    QVariantMap args;
    args.insert(args::source_name, drawing::source_name);
    args.insert(field::command, commands::create_entity);
    {
        QVariantMap args2;
        args2.insert(args::type, "arrow");
        QVariantList points;
        QVariantList p1 = { m_line.p1().x(), m_line.p1().y() };
        QVariantList p2 = { m_line.p2().x(), m_line.p2().y() };
        points << QVariant::fromValue(p1) << QVariant::fromValue(p2);

        args2.insert(entities::points, points);
        args2.insert(entities::line_width, 10);
        QVariantList color;
        color << myColor.redF() << myColor.greenF() << myColor.blueF() << myColor.alphaF();
        args2.insert(entities::line_color, color);
        args2.insert(entities::start, "-1");
        QVariantMap end;
        end.insert(entities::color, color);
        args2.insert(entities::end, end);
        args.insert(field::args, args2);
    }
    json.insert(field::args, args);
    QJson::Serializer serializer;
    bool ok;
    auto ba = serializer.serialize(json, &ok);
    if (!ok) {
        qCritical() << "[Arrow] Json serialize error:" << serializer.errorMessage();
        return;
    }
    source->setShapeCmd(ba);
    /*
    QVariantMap arrow;
    arrow.insert(entities::type, "arrow");
    QVariantList points;
    QVariantList p1 = { m_line.p1().x(), m_line.p1().y() };
    QVariantList p2 = { m_line.p2().x(), m_line.p2().y() };
    points << QVariant::fromValue(p1) << QVariant::fromValue(p2);
    arrow.insert(entities::points, points);
    arrow.insert(entities::line_width, 10);
    arrow.insert(entities::line_color, QVariantList() << 0.9 << 0 << 0 << 0.5);
    arrow.insert(entities::start, "-1");

    //QVariantMap end;
    //end.insert(entities::color, "square");//QVariantList() << 0.9 << 0 << 0 << 0.5
    //arrow.insert(entities::end, end);

    QVariantList arrEntities;
    arrEntities << arrow;

    args.insert(field::entities, arrEntities);
    json.insert(field::args, args);

    QJson::Serializer serializer;
    QByteArray ba = serializer.serialize(json);
    if (source)
        source->setShapeCmd(ba);
    /*
    QString json;
    std::string cmd = R"(
{
    "type": "command",
    "command": "create_source",
    "args": {
        "source_name": "drawing test",
        "src_type": "drawing",
        "width": 1000,
        "height": 500,
        "caption_defaults": {
            "font_family": "/usr/share/fonts/ttf/liberation/LiberationMono-Regular.ttf"
        },
            "entities": [
                {
                    "type": "arrow",
                    "points": [ [ 0, 250 ], [ 600, 250 ] ],
                    "line_width": 10,
                    "line_color": [ 0.9, 0, 0, 0.5 ],
                    "start": -1,
                    "end": {
                        "color": [ 0.9, 0, 0, 0.5 ]
                    }
                },
                {
                    "type": "caption",
                           "text": "xxx",
                           "size": 96,
                           "color": [ 0, 0.5, 0, 1 ],
                           "align": 3,
                           "valign": 2,
                           "z": -10
                       }
                   ]
    }
}
{
    "type": "command",
    "command": "create_window",
    "args": {
        "source_name": "drawing test",
        "top": 0,
        "left": 0,
        "width": 1000,
        "height": 500
    }
}
)";*/
    /*{
    "type": "command",
    "command": "create_source",
    "args": {
        "source_name": "drawing test",
        "src_type": "drawing",
        "width": 640,
        "height": 480,
        "caption_defaults": {
            "font_family": "/usr/share/fonts/ttf/liberation/LiberationSerif-Italic.ttf"
        },
        "entities": [
            {
                "type": "arrow",
                "points": [ [ 10, 10 ], [ 200, 200 ], [ 400, -100 ] ],
                "line_width": 20,
                "line_color": [ 0.5, 0.5, 0.8, 0.6 ]
            },
            {
                "type": "caption",
                "text": "test test test",
                "size": 96,
                "color": [ 0, 0.4, 0, 1 ],
                "align": 3,
                "valign": 2,
                "z": -10
            },
            {
                "type": "arrow",
                "points": [ [ 10, 10 ], [ 200, 200 ], [ 400, -100 ] ],
                "line_width": 20,
                "line_color": [ 0.5, 0.5, 0.8, 0.6 ]
            }
        ]
    }
}*/
}

void Arrow::changeLine(const QLineF &line)
{
    createEntity();
    setLine(line);
}

void Arrow::paint(QPainter *painter, const QStyleOptionGraphicsItem *,
                  QWidget *)
{
    if (!m_line.isNull()) {
        painter->setPen(QPen(myColor));
        painter->setBrush(myColor);
    }
    else if (!myStartItem && !myEndItem) {
        if (myStartItem->collidesWithItem(myEndItem))
            return;

        QPen myPen = pen();
        myPen.setColor(myColor);

        painter->setPen(myPen);
        painter->setBrush(myColor);

        QLineF centerLine(myStartItem->pos(), myEndItem->pos());
        QPolygonF endPolygon = myEndItem->polygon();
        QPointF p1 = endPolygon.first() + myEndItem->pos();
        QPointF p2;
        QPointF intersectPoint;
        QLineF polyLine;
        for (int i = 1; i < endPolygon.count(); ++i) {
            p2 = endPolygon.at(i) + myEndItem->pos();
            polyLine = QLineF(p1, p2);
            QLineF::IntersectType intersectType =
                    polyLine.intersect(centerLine, &intersectPoint);
            if (intersectType == QLineF::BoundedIntersection)
                break;
            p1 = p2;
        }

        changeLine(QLineF(intersectPoint, myStartItem->pos()));
    }
    double angle = ::acos(line().dx() / line().length());
    if (line().dy() >= 0)
        angle = (Pi * 2) - angle;

    qreal arrowSize = 20;
    QPointF arrowP1 = line().p1() + QPointF(sin(angle + Pi / 3) * arrowSize,
                                            cos(angle + Pi / 3) * arrowSize);
    QPointF arrowP2 = line().p1() + QPointF(sin(angle + Pi - Pi / 3) * arrowSize,
                                            cos(angle + Pi - Pi / 3) * arrowSize);

    arrowHead.clear();
    arrowHead << line().p1() << arrowP1 << arrowP2;
    painter->drawLine(line());
    painter->drawPolygon(arrowHead);
    if (isSelected()) {
        painter->setPen(QPen(myColor, 1, Qt::DashLine));
        QLineF myLine = line();
        myLine.translate(0, 4.0);
        painter->drawLine(myLine);
        myLine.translate(0,-8.0);
        painter->drawLine(myLine);
    }
}

void Arrow::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    auto m_pos = event->pos();
    if (arrowHead.containsPoint(m_pos, Qt::OddEvenFill))
    {
        m_moving = true;
        myColor = Qt::red;
        setFlag(QGraphicsItem::ItemIsMovable, false);
        setFlag(QGraphicsItem::ItemIsSelectable, false);
        update();
        return;
    }
    QGraphicsLineItem::mousePressEvent(event);
}

void Arrow::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (m_moving) {
        m_line = QLineF(event->pos(), m_line.p2());
        changeLine(m_line);
        return;
    }
    QGraphicsLineItem::mouseMoveEvent(event);
}

void Arrow::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (m_moving) {
        m_line = QLineF(event->pos(), m_line.p2());
        changeLine(m_line);
    }
    m_moving = false;
    myColor = Qt::black;
    setFlag(QGraphicsItem::ItemIsMovable);
    setFlag(QGraphicsItem::ItemIsSelectable);
    update();
    QGraphicsLineItem::mouseReleaseEvent(event);
}

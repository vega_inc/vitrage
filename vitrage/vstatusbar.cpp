#include "vstatusbar.h"
#include <QDateTime>
VStatusBar::VStatusBar(QObject *parent) :
    QObject(parent),m_pStatusBar(NULL)
{

}

VStatusBar::VStatusBar(QStatusBar *bar)
{
    m_pStatusBar = bar;
    m_pMainWgt = new QWidget;
    m_pLay = new QHBoxLayout;
    m_pLay->setContentsMargins(0,0,0,0);
    m_pMainWgt->setLayout(m_pLay);
    m_pMessage  = new QLabel;
    m_pConnect  = new QLabel;

    m_pConnect->setFrameShape(QFrame::Box );
    m_pDateTime = new QLabel;

   // m_pDateTime->setAlignment(Qt::AlignRight);

    m_pLay->addWidget(m_pMessage);
    m_pLay->setStretch(3,123);
    QSpacerItem *item = new QSpacerItem(9999,22,QSizePolicy::Expanding,
                                                        QSizePolicy::Expanding);
    m_pLay->addSpacerItem(item);
    m_pLay->addWidget(m_pConnect);
    m_pLay->addWidget(m_pDateTime);

    m_pMessage->setSizePolicy(QSizePolicy::MinimumExpanding,
                                                 QSizePolicy::MinimumExpanding);
    m_pDateTime->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
    m_pConnect->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
    m_pMessage->setMinimumSize(m_pMessage->sizeHint());

    m_pTimer = new QTimer;
    m_pTimer->setSingleShot(true);
    m_pTimer->setInterval(60000);
    renewTime();
    connect(m_pTimer,SIGNAL(timeout()),this,SLOT(renewTime()));
    m_pTimer->start();
    m_pStatusBar->addWidget(m_pMainWgt);
}

void VStatusBar::setConnectStatus(const QString& str)
{
    m_pConnect->setText(tr("Controller status: ") + str);
}

void VStatusBar::setMessage(const QString &mess)
{
    m_pMessage->setText(mess);
    m_pMessage->setMinimumSize(m_pMessage->sizeHint());
}

VStatusBar::~VStatusBar()
{
    delete m_pTimer;
}

void VStatusBar::setStatusBar(QStatusBar *bar)
{
    m_pStatusBar = bar;
}

void VStatusBar::renewTime()
{
    m_pDateTime->setText(QDateTime::currentDateTime().toString("hh:mm  dd.MM.yyyy "));
    m_pTimer->start();
}

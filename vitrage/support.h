#ifndef SUPPORT_H
#define SUPPORT_H

#include <QString>
#include <QDir>
#include <QCoreApplication>

namespace support {
static const QString getSettingsPath()
{
    QString file_path = QDir::home().canonicalPath();

    if (!file_path.endsWith('/'))
        file_path += '/';

    auto appName = QCoreApplication::arguments().at(0);
    file_path += ("." + QFileInfo(appName).fileName());

    if (!file_path.endsWith('/'))
        file_path += '/';

    file_path += QFileInfo(appName).fileName() + ".ini";

    return file_path;
}
} // end of namespace

#endif // SUPPORT_H

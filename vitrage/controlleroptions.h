#ifndef CONTROLLEROPTIONS_H
#define CONTROLLEROPTIONS_H


#include "vcontroller/vcontroller.h"

#include <QDialog>
#include <QString>
#include <QList>
#include <QSettings>

#include "configpath.h"

namespace Ui {
class ControllerOptions;
}

class ControllerOptions : public QWidget
{
    Q_OBJECT
public:
    explicit ControllerOptions(QWidget *parent = 0);
    ~ControllerOptions();
    void setControllerProperty(const QString &fname);
    void stateChanged(bool isConnected);

private:
    Ui::ControllerOptions *ui;
    QString settingsName;
    VController *m_pController;
    QMap<QString,VController *> m_Controllers;
    static QString getControllersPath();
    void fillParams(VController *controller);
    void setController(VController *);
    void setController(const QString &name);
    bool loadControllers();
    VController *controller() { return m_pController; }

protected:
    void showEvent(QShowEvent *);

public slots:
    void onOffClicked(bool);
    void setPropEnabled(bool en);
    void someChanged();
    void on_tBSave_clicked();
    void on_toolButtonHelp_clicked();
    void modelIndexChanged(const QString &str);

signals:
    void conn(VController *ctr);
    void sendOpenHtml(const QString &page);
};

#endif // CONTROLLEROPTIONS_H

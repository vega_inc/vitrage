#ifndef VGRAPHICSVIEWER_H
#define VGRAPHICSVIEWER_H

#include <QMainWindow>
#include "vscheme.h"

namespace Ui {
class VGraphicsViewer;
}

class VGraphicsViewer : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit VGraphicsViewer(QWidget *parent = 0);
    void setScheme(VScheme *scheme);
    ~VGraphicsViewer();
    
private:
    Ui::VGraphicsViewer *ui;
    QHBoxLayout * m_layout;
    VScheme * m_scheme;
private slots:
    void editorClose(void);
    void on_actionEditor_triggered();
    void on_actionClose_triggered();

signals:
    void openEditor(void);
};

#endif // VGRAPHICSVIEWER_H

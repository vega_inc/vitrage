#ifndef VGRAPHICSWIDGET_H
#define VGRAPHICSWIDGET_H

#include <QtGui>
#include "../vsource/vsource.h"
#include <QFileDialog>
#include <QtSvg/QGraphicsSvgItem>
#include <QtSvg/QSvgRenderer>

#include "widgetskins.h"

#include "configpath.h"


class VGraphicsText;

static const QString RUNTIME_ID = "RunTime";
static const QString SELECTED_ID = "Selected";
static const QString ERROR_ID = "Error";
static const QString OFF_ID = "Off";

static const QString no_signal = "_no_signal";
static const QString off = "_off";
static const QString off_nosignal = "_off_nosignal";
static const QString off_selected = "_off_selected";
static const QString on = "_on";
static const QString on_selected = "_on_selected";
static const QString selected_off_nosignal = "_selected_off_nosignal";
static const QString selected_on_nosignal = "_selected_on_nosignal";
static const QString bare = "";

/* defines for exporting QMap */
static const QString ELEM_PHIS_NAME = "phisiscalName";
static const QString ELEM_LOGIC_NAME = "logicName";
static const QString ELEM_PICTURE = "elementPicture";
static const QString ELEM_POS_X = "elementPosition_X";
static const QString ELEM_POS_Y = "elementPosition_Y";
static const QString ELEM_POS_W = "elementPosition_W";
static const QString ELEM_POS_H = "elementPosition_H";
static const QString ELEM_LABEL_X = "elementLabelPosition_X";
static const QString ELEM_LABEL_Y = "elementLabelPosition_Y";


typedef enum {RUNTIME_ST,
              SELECTED_ST,
              ERROR_ST,
              ERROR_SELECTED_ST,
              OFF_ST} WGT_STATUS; /* status for widget */

class VGraphicsWidget :public QGraphicsSvgItem
{
    Q_OBJECT

    static QString getImagesPath();

public:
   typedef enum resize{
        LEFT_H,
        LEFT_L,
        RIGHT_H,
        RIGHT_L,
        LEFT,
        RIGHT,
        HIGH,
        LOW,
        NONE
    } resize_t;



   typedef enum WGT_TYPE{PICTURE,
                 ICON,
                 WGT } WGT_TYPE; /* picture or set of picture from us */

   static const int MARG     = 8; /*margines for resize cursor in pixels*/
   static const int BIG_MARG  = 25;
   static const int BOUND_W = 200;
   static const int BOUND_H = 200;
   static const int DELTA_PROC =10 ; /* % of widget size for status draw */
   static const int SMALL_DELTA = 10 ; /* % of widget size for status draw */
   static const int RESIZE = 5; /* resize cube */


   typedef enum {EDITING,RUNTIME} WGT_MODE;
   explicit VGraphicsWidget(VSource *);
   //explicit VGraphicsWidget(const VGraphicsWidget&);
    VGraphicsWidget(QIcon,QString lgName,QString phName);
    VGraphicsWidget();

    VGraphicsWidget::WGT_MODE mode(void);
    void setMode(VGraphicsWidget::WGT_MODE);
    void setStatus(WGT_STATUS status);

    WGT_STATUS status(void);
    void setType(VGraphicsWidget::WGT_TYPE type);
    VGraphicsWidget::WGT_TYPE getType(void);
    QImage getOriginImage(void);
    void paintStatusForPicture(QPainter * painter);
    void paintErrorForPicture(QPainter * painter, bool selected = false);
    //VGraphicsWidget
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                                                             QWidget *widget);

    void contextMenuEvent(QContextMenuEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void hoverMoveEvent(QGraphicsSceneHoverEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

    void setIcon(QIcon icon);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void setText(QString);
    void setMovable(bool);
    void setLight(bool);
    void setLogicName(QString str);
    void setPicture(QString picture);
    void createSvgRender(QString str);
    void  setResizeType(resize_t);

    QString logicName(void);
    QString phisName(void);
    void getImage(void);
    void changeCursor(QPoint p);
    bool getLight();
    int getXDelta(void);
    int getYDelta(void);
    int getSmallDelta(void);
    void setPictureSize(int,int);
    VSource *getSource(void);
    void setSource(VSource *src);
    VGraphicsText *textItem(void);
    void paintSelected(QPainter *painter);
    void getMap(QMap<QString, QVariant> &);
    QImage getIconPicture(void);
    QString defPicturePath(void);
    void setDefPicPath(QString path);
private:
    int m_pictureWidth;
    int m_pictureHeight;
    WGT_MODE m_mode;
    QIcon m_icon;
    QString m_PhisName;
    QString m_LogicName;
    QString m_defPicturePath; /* default pucture size */
    virtual QRectF boundingRect() const;
    void paintSelectedRect(QPainter *painter);
    VSource * m_pSource;
    QImage m_Image;
    QMap<QString,QImage> m_Images; /* images for different statuses */
    QImage m_ImageOrig;
    WGT_STATUS m_status;
    WGT_TYPE m_type;
    bool m_resizeFlag;
    bool m_pressedFlag;
    bool m_lightOn;
    /* Actions */
    QAction *m_pActionSetPicture;
    QAction *m_pActionSetSize;
    QAction *m_pActionSetSkin;
    QAction *m_pActionBare;
    QAction *m_pActionRotate;
    resize_t m_resizeType;

    /* attrs for svg */
    QSvgRenderer *m_svgRenderer;
    //QGraphicsSvgItem *m_svnItem; /* simple svg */
    VGraphicsText *m_textItem; /* label for source name */

    QString       m_selectedWidtetFilePath; /* path to svg with selected path */
    //QAction *
private slots:
    void setWgtPicture(void); /* slot for context menu to call set picture window */
    void setWgtSize(void); /* slot for context menu to call set picture window */
    void setSkin(void); /* open skins window for selection */
    void rotate90(void);
    void selectedWgt(QString);
    //void updateView(void);

public slots:
    void mouseModeSlot(QPoint pos);
    void updateView(void);
    void setSize(int,int);
    void setSizeEmit(int,int);
    void widgetActivate(void);
signals:
    void modefied(void);
    void updateParent(void);
    void setResizeFlag(bool flag);
    void setSizeSignal(int w,int h);
    void sigDefPicturePath(QString);
    void widgetPressed(QString);
    void setActivate(void);

};

#endif // VGRAPHICSWIDGET_H

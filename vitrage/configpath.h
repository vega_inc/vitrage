#ifndef CONFIGPATH_H
#define CONFIGPATH_H

#define FIRST_SHARE_PATH "/usr/local/share/"
#define FIRST_LIB_PATH "/usr/lib64/"
#define TRANSLATE_DIR_NAME "/translate"
#define IMAGES_DIR_NAME "/images"
#define PLUGINS_DIR_NAME "/plugins"
#define CONTROLLERS_DIR_NAME "/controllers"
#define CONFIG_DIR_NAME "/config"
#define RES_PATH "/usr/local/share/vitrage/resource/vitrage.rcc"
#define RES_PATH_DEF "./resource/vitrage.rcc"

#endif // CONFIGPATH_H

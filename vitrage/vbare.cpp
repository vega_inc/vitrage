#include <QGridLayout>
#include "vbare.h"
#include <QResizeEvent>

/* FIXME :: remove */
VBare::VBare(QWidget * parent, uint32_t width, uint32_t height)
    :VSource(parent)
{
	Q_UNUSED(width);
	Q_UNUSED(height);

   /* QPalette p(palette());
    p.setColor(QPalette::Background, Qt::white);
    this->setPalette(p);
    this->setMinimumSize(width,height);
    layout = new QVBoxLayout();
    imagelabel = new QLabel(this);
    imagelabel->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    //QImage dummy(width,height,QImage::Format_RGB32);
    //QImage image = dummy;

    layout->addWidget(imagelabel);
    layout->setContentsMargins(0,0,0,0);
    imagelabel->setContentsMargins(0,0,0,0);*/
    /*  for (uint32_t x = 0; x < width; x ++)
        for (uint32_t y =0; y < height; y++)
        image.setPixel(x,y,qRgb(x, y,255 * x/width));
        pixmap_image =  QPixmap::fromImage(image);
        imagelabel->setPixmap(pixmap_image);*/ // fill widget (for debugging)
   /* imagelabel->show();
    imagelabel->setAutoFillBackground(true);

    this->setLayout(layout);
    layout->setContentsMargins(0,0,0,0);
    this->show();
    //this->setBackgroundRole(QPalette::NoRole );

    this->setAutoFillBackground(true);
    this->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
    this->setMouseTracking(true);
    setAttribute(Qt::WA_NoSystemBackground);

    this->setAcceptDrops(true);*/
//    this->imagelabel->setAcceptDrops(true);
//	this->pixmap_image.setAcceptDrops(true);
    this->setMouseTracking(true);

}

/**
* @brief dest
*/
VBare::~VBare(void)
{
    //delete layout;
    //delete imagelabel;
}

/**
* @brief
*
* @param event
*/
void   VBare::mouseMoveEvent(QMouseEvent *event)
{
Q_UNUSED(event);
}


/**
* @brief
*
* @param q
*/
void VBare::paintEvent(QPaintEvent *q)
{
	Q_UNUSED(q)
    //  qDebug("VBare Paint Event");
}


/**
* @brief enable drag
*
* @param event - [in] - QDragEnterEvent *
*/
void VBare::dragEnterEvent(QDragEnterEvent *event)
{
    event->acceptProposedAction();
}

/**
* @brief  dropEvent
*
* @param event - [in] QDropEvent *
*/
void  VBare::dropEvent(QDropEvent *event)
{
    if (event->mimeData()->hasText()) {
        emit drop_signal(event->mimeData()->text(), getPosition().lt.y(), getPosition().lt.x());
        event->acceptProposedAction();
    }
}

void VBare::mousePressEvent (QMouseEvent * event )
{
Q_UNUSED(event);
}
/**
* @brief
*
* @param event
*/
void VBare::resizeEvent(QResizeEvent *event) {
    Q_UNUSED(event);
    setAcceptDrops(true);
}

#include <timelinewidget.h>
#include <QPainter>
#include <QLabel>
#include <QMessageBox>
#include <QPainter>
#include <QMouseEvent>
#include <QVBoxLayout>

#include <cassert>

#include "vaction.h"

constexpr int step_timeout = 30; // ms

QString extractHours(long long ms)
{
    auto h = (ms/(60*60*1000));
    return QString("%1").arg(h, 2, 10, QLatin1Char('0'));
}
QString extractMinutes(long long ms)
{
    auto m = (ms/(60*1000))%60;
    return QString("%1").arg(m, 2, 10, QLatin1Char('0'));
}
QString extractSeconds(long long ms)
{
    auto s = (ms/1000)%60;
    return QString("%1").arg(s, 2, 10, QLatin1Char('0'));
}
QString extractMilliSeconds(long long ms)
{
    ms = ms%1000;
    return QString("%1").arg(ms);
}

using namespace std::chrono;

QString timeToString(milliseconds seconds)
{
    auto t = seconds.count();
    auto h = extractHours(t);
    t %= 3600000;
    auto m = extractMinutes(t);
    t %= 60000;
    auto s = extractSeconds(t);
    auto ms = extractMilliSeconds(t);
    return h + ":" + m + ":" + s + "," + ms;
}

TimelineWidget::TimelineWidget(QWidget* parent) :
    QWidget(parent)
{
    setMinimumHeight(10);

    QWidget *wgt = new QWidget;
    this->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
    QVBoxLayout *qvb = new QVBoxLayout;
    m_timeLabel = new QLabel("00:00:00");
    m_timeLabel->setAlignment(Qt::AlignCenter);
    m_timeLabel->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
    wgt->setLayout(qvb);

    qvb->addWidget(m_timeLabel);
    qvb->addWidget(this);

    m_dock = new QDockWidget;
    m_dock->setWidget(wgt);
    m_dock->setAllowedAreas(Qt::RightDockWidgetArea);
    m_dock->setObjectName("TimeLine");
    m_dock->setWindowTitle(tr("Timeline Widget"));

    setMouseTracking(true);
}

QDockWidget *TimelineWidget::dock()
{
    return m_dock;
}

QSize TimelineWidget::sizeHint() const
{
    return QSize(1000, 20);
}

void TimelineWidget::play()
{
    m_pause = false;
    current_time = std::chrono::milliseconds{0};

    if (m_intervals.isEmpty()) {
        QMessageBox::critical(0, tr("Warning!"), tr("No intervals on timeline!"));
    }
    if (!m_timeline.isActive()) {
        m_timeline.start(step_timeout, this);
    }

    QString name = m_intervals.at(m_currentIndex).name;
    emit setRuntimeScene(name);
    slotTimeout();
}

void TimelineWidget::stop()
{
    m_timeline.stop();
    start_time = std::chrono::milliseconds(0);
    m_currentIndex = 0;
    tpoint = {};
    for (TimelineItem &item : m_intervals) {
        item.t = std::chrono::milliseconds{0};
        item.pause_time = std::chrono::milliseconds{0};
    }
    m_timeLabel->setText("00:00:00");
    emit finished();
}

void TimelineWidget::addInterval(uint32_t number, uint32_t duration, const QString& name)
{
    assert(duration <= 60*60*24);
    auto nameItem = (name.isEmpty())?tr("item"):name;
    TimelineItem item(number, std::chrono::seconds(duration), nameItem);
    m_intervals.append(item);
    m_currentIndex = 0;
    m_duration += milliseconds(duration*1000);
    update();
}

void TimelineWidget::reset()
{
    m_duration = seconds(0);
    m_currentIndex = 0;
    m_intervals.clear();
    m_timeLabel->setText("00:00:00");
    update();
}

bool TimelineWidget::isRunning() const
{
    return m_timeline.isActive();
}

bool TimelineWidget::isPaused() const
{
    return m_pause;
}
/// curent time in ms
std::chrono::milliseconds TimelineWidget::currentTime() const
{
    if (m_pause)
        return current_time;

    std::chrono::milliseconds time{0};
    for (const TimelineItem &item : m_intervals) {
        long int t = std::min(item.t.count(), item.duration.count());
        time += std::chrono::milliseconds(t);
    }
    return time;
}

std::chrono::milliseconds TimelineWidget::sumDuration(bool full) const
{
    if (full) {
        return m_duration;
    }
    milliseconds duration(0);
    for (const TimelineItem &item : m_intervals) {
//        milliseconds d = item.duration - item.pause_time;
        duration += (item.duration - item.pause_time);
    }
    return duration;
}
/// check for next scene
void TimelineWidget::slotTimeout()
{
    if (m_currentIndex < 0 && m_currentIndex >= m_intervals.size())
        return;
    {
        current_time = currentTime();
        milliseconds sum = sumDuration(false);
        assert(sum.count() <= 60*60*24*1000*7);
        QString current_time_string = timeToString(current_time);
        QString full_time_string = timeToString(sum);
        auto text = current_time_string + " - " + full_time_string;
        m_timeLabel->setText(text);
    }
    auto item = m_intervals.at(m_currentIndex);
    auto ms = item.pause_time + item.t;
    if (ms >= item.duration) {
        item.t = item.duration;
        ++m_currentIndex;
        bool ok = validItem();
        if (ok) {
            auto name = m_intervals.at(m_currentIndex).name;
            emit setRuntimeScene(name);
        }
        else {

            stop();
            //reset();
            play();
        }
    }
}

void TimelineWidget::mousePressEvent(QMouseEvent *event)
{
    if (event->button() != Qt::LeftButton)
        return;
    if (!m_intervals.isEmpty() && !isRunning()) {
        tpoint = timeFromPoint(event->pos());
        m_currentIndex = std::get<1>(tpoint);
        start_time = std::chrono::milliseconds(std::get<3>(tpoint));
        std::chrono::milliseconds duration;
        for (int i = 0; i < m_intervals.size(); i++) {
            if (i < m_currentIndex) {
                auto d = m_intervals.at(i).duration;
                duration += d;
            }
            else if (i == m_currentIndex) {
                auto t = start_time - duration;
                m_intervals[i].t = t;
            }
            else {
                m_intervals[i].t = std::chrono::milliseconds(0);
            }
        }
        auto text = timeToString(start_time);
        m_timeLabel->setText(text);
        auto name = m_intervals.at(m_currentIndex).name;
        emit setScene(name);
        update();
    }
}

void TimelineWidget::mouseMoveEvent(QMouseEvent */*event*/)
{
    if (m_intervals.isEmpty())
        return;
//    auto point = event->pos();
//    auto t = timeFromPoint(point);
//    auto message = std::get<0>(t);
//    QToolTip::showText(mapToGlobal(point), message, this);
}

void TimelineWidget::paintEvent(QPaintEvent* event)
{
    if (m_intervals.isEmpty()) {
        QWidget::paintEvent(event);
        return;
    }
    auto progress = getProgress();
    auto x0 = progress.first;
    auto x = progress.second;
    if (x < x0) {
        qDebug() << "#########" << Q_FUNC_INFO << x0 << x << "w=" << width();
        QWidget::paintEvent(event);
        return;
    }

    QPainter painter(this);
    painter.setRenderHint(QPainter::HighQualityAntialiasing);
    paintProgress(&painter, x0, x);
    paintScenes(&painter);
    QWidget::paintEvent(event);
}

void TimelineWidget::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == m_timeline.timerId()) {
        m_intervals[m_currentIndex].t += std::chrono::milliseconds(step_timeout);
        slotTimeout();
        update();
    }
    else {
        QWidget::timerEvent(event);
    }
}

tuplePoint TimelineWidget::timeFromPoint(const QPoint &point)
{
    if (m_intervals.isEmpty())
        return {};
    int w = width()-1;
    int index = 0;
    int x = point.x();
    int x_start = 0;
    long long duration_all = m_duration.count();

    std::chrono::milliseconds t(0);
    std::chrono::milliseconds t_all(0);
    std::chrono::milliseconds current(0);

    for (int i = 0 ; i < m_intervals.count(); i++) {
        auto w_scene = m_intervals.at(i).duration.count()*w/duration_all;
        auto x_end = x_start + w_scene;
        if (x > x_start && x < x_end) {
            index = i;
            float px = (float)m_intervals.at(i).duration.count()/w_scene; // пикселей в одном таймпоинте
            std::chrono::milliseconds t_0(0);
            for (int i = 0 ; i < index; i++) {
                t_0 += m_intervals.at(i).duration;
            }
            int a = x*px;
            t_all = std::chrono::milliseconds(a);
            t = t_all - t_0;
            current += t;
            break;
        }
        x_start = x_end;
    }
    if (index >= m_intervals.count() || index < 0) {
        return {};
    }
    QString name = m_intervals.at(index).name;
    QString t1 = timeToString(current);
    QString t2 = timeToString(m_intervals.at(index).duration);
    QString label = name + "\n[" + t1 + " - " + t2 + "]";

    return std::make_tuple(label, index, x, t, t_all);
}

void TimelineWidget::setButtStatus()
{
    if (m_intervals.isEmpty())
        return;
    if (m_currentIndex <= 0) {
        emit enableButtons(false, true);
        return;
    }
    else if (m_currentIndex >= m_intervals.count()-1) {
        emit enableButtons(true, false);
        return;
    }
    emit enableButtons(true, true);
}

bool TimelineWidget::validItem() const
{
    return (m_currentIndex < 0 || m_currentIndex >= m_intervals.size())?false:true;
}
/// calc coordinates for progressbar
QPair<long long, long long> TimelineWidget::getProgress() const
{
    auto sum_duration = m_duration.count();
    if (sum_duration < 1)
        return {0, 0};
    auto w = width() - 1;
//    auto x0 = w*start_time.count()/sum_duration;
    auto x0 = std::get<2>(tpoint);
    if (!isRunning())
        return {x0, x0};
    float t = float(current_time.count())/sum_duration;
    long long x = w*t;
    return {x0, x};
}

void TimelineWidget::paintProgress(QPainter *painter, int x0, int x)
{
    const int h = height() - 1;
    if (x > x0) {
        QRect rect(x0, 0, x-x0, h);
        painter->fillRect(rect, QBrush(QColor(65,100,154)));
    }
    QPen pen(Qt::red);
    pen.setWidth(3);
    painter->setPen(pen);
    painter->drawLine(x, 0, x, h);
}

void TimelineWidget::paintScenes(QPainter *painter)
{
    const int w = width() - 1;
    const int h = height() - 1;
    int x_start = 0;
    long long duration_all = m_duration.count();
    for (int i = 0 ; i < m_intervals.count(); i++) {
        painter->setBrush(Qt::NoBrush);
        painter->setPen(QPen(Qt::white));
        auto w_scene = m_intervals.at(i).duration.count()*w/duration_all;
        QRect rect(x_start, 0, w_scene, h);
        x_start += w_scene;
        painter->drawRect(rect);
        QString sceneName = m_intervals.at(i).name;
        auto brect = painter->boundingRect(rect, Qt::AlignCenter, sceneName);
        if (brect.width() < rect.width() && brect.height() < rect.height()) {
            if (i == m_currentIndex)
                painter->setPen(QPen(QColor(Qt::black)));
            painter->drawText(rect, Qt::AlignCenter, sceneName);
        }
    }
}

void TimelineWidget::pause()
{
    m_timeline.stop();
    if (m_currentIndex < 0 && m_currentIndex >= m_intervals.size())
        return;
    m_pause = true;
    m_intervals[m_currentIndex].pause_time = m_intervals.at(m_currentIndex).t;
//    current_time = currentTime();
}

void TimelineWidget::setNext()
{
    if (m_intervals.isEmpty() && !isRunning())
        return;
//    auto duration = m_intervals.at(m_currentIndex).duration;
//    m_intervals[m_currentIndex].timeline->setCurrentTime(duration.count());
    slotTimeout();
}

void TimelineWidget::setPrev()
{
    /*
    if (m_intervals.isEmpty() && !isRunning())
        return;
    m_intervals[m_currentIndex].timeline->stop();
    m_intervals[m_currentIndex].timeline->setCurrentTime(0);
    --m_currentIndex;
    if (!validItem()) {
        m_currentIndex = 0;
    }
    m_intervals[m_currentIndex].t0 = std::chrono::milliseconds(0);
    m_intervals[m_currentIndex].timeline->stop();
    m_intervals[m_currentIndex].timeline->setCurrentTime(0);
    m_intervals[m_currentIndex].timeline->start();
    auto name = m_intervals.at(m_currentIndex).name;
    emit setRuntimeScene(name);
    slotTimeout();*/
}

#include "pluginsmanager.h"
#include "configpath.h"
#include <qdir.h>
#include <QMessageBox>
#include <QPluginLoader>
#include <QDialog>
#include <QVariantMap>
#include <QTableWidgetItem>
#include <QCoreApplication>

QString PluginsManager::getPluginsPath()
{
    auto args = QCoreApplication::arguments();
    auto fname = QFileInfo(args.at(0)).fileName();
    QString pluginsFilePath = FIRST_LIB_PATH + fname+ CONTROLLERS_DIR_NAME;
    QDir dir(pluginsFilePath);
    if(dir.exists() && dir.cd(pluginsFilePath))
        return pluginsFilePath;
    else
        return pluginsFilePath = QDir::current().canonicalPath() + CONTROLLERS_DIR_NAME;
}

bool PluginsManager::loadPlugins()
{
    QDir dir(getPluginsPath());
    if (!dir.exists()) {
        QMessageBox::critical(0, "", "Plugins directory does not exist");
        return false;
    }

    foreach (QString fileName, dir.entryList(QDir::Files)) {
        QPluginLoader loader(dir.absoluteFilePath(fileName));
        if (fileName.contains("extron")) {
            Extron *ctr = static_cast<Extron*>(loader.instance());
            if (!ctr) {
                qDebug() << loader.errorString();
                continue;
            }
            qDebug("Set extron Instance");
            m_extron = ctr;
            m_extronFlag = true;
        }
        if (fileName.contains("vega")) {
            Vega *ctr = static_cast<Vega*>(loader.instance());
            ctr = static_cast<Vega*>(ctr->clone());
            if (!ctr)  {
                qDebug() << loader.errorString();
                continue;
            }
            qDebug("Set vega Instance");
            m_vega = ctr;
            m_mixerFlag = true;
        }
    }
    return true;
}

PluginsManager &PluginsManager::Instance()
{
    static PluginsManager mng;
    return mng;
}

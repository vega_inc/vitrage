#include "collapsibleframe.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSplitter>

CollapsibleFrame::CollapsibleFrame(QObject *parent, QString headerText)
{
    m_headerText = headerText;
    m_parent = parent;
    m_frameState = CLOSED;
	m_pSplitter = NULL;

    m_headerArea = new QLabel;
    m_widgetArea = new QWidget;
	m_frame      = new QFrame;
	m_frame->setMinimumHeight(0);
	m_frame->setFrameShape(QFrame::NoFrame);
    m_frame->setFrameShadow(QFrame::Raised);
	m_frame->setContentsMargins(0,0,0,0);
	QVBoxLayout* frameLayout = new QVBoxLayout;
	frameLayout->setContentsMargins(0,0,0,0);
	m_frame->setLayout(frameLayout);	
	m_frame->setMinimumHeight(0);

    m_headerArea->setMaximumHeight(25);
	m_headerArea->setMinimumHeight(25);
	m_headerArea->setContentsMargins(0,0,0,0);
	m_widgetArea->setContentsMargins(0,0,0,0);
    //m_widgetArea->setSpacing(0);

    //m_widgetArea->setMaximumHeight(1000);
    m_headerArea->setFrameShape(QFrame::NoFrame);
	m_headerArea->setFrameShadow(QFrame::Raised);

//    setStyleSheet("QWidget { border: 1px solid #000000; }");

    icon = new ExtendedQLabel;
    icon->setMaximumSize(15, 15);
    icon->setPixmap(m_collapseIcon);
    connect(icon, SIGNAL(clicked()), this, SLOT(changeState()));

    m_down = new ExtendedQLabel;
    m_down->setStyleSheet("QLabel { image: url(:/move-down.png) no-repeat; } ");
    m_down->setMaximumSize(15, 15);
    connect(m_down, SIGNAL(clicked()), this, SIGNAL(moveDown()));

    m_up = new ExtendedQLabel;
    m_up->setStyleSheet("QLabel { image: url(:/move-up.png) no-repeat; } ");
    m_up->setMaximumSize(15, 15);
    connect(m_up, SIGNAL(clicked()), this, SIGNAL(moveUp()));

    headerTextLabel = new QLabel;
    headerTextLabel->setText(m_headerText);

    widgetLayout = new QVBoxLayout;
	widgetLayout->setContentsMargins(4,0,0,0);
	widgetLayout->setSpacing(0);
	
//  widgetLayout->setMargin(0);
//  widgetLayout->setSpacing(0);
    m_widgetArea->setLayout(widgetLayout);
    m_widgetArea->setMinimumHeight(0);

    headerLayout = new QHBoxLayout;
    headerLayout->setMargin(0);
    headerLayout->addWidget(headerTextLabel); /*, 0, Qt::AlignTop | Qt:: AlignLeft);*/
	headerLayout->addWidget(icon); /*, 0, Qt::AlignTop | Qt:: AlignLeft);*/

    m_headerArea->setLayout(headerLayout);

    QVBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->setMargin(0);
    mainLayout->setSpacing(0);
	frameLayout->addWidget(m_headerArea);
	frameLayout->addWidget(m_widgetArea);
	mainLayout->addWidget(m_frame);

    setLayout(mainLayout);
    determineIcon();

	setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Minimum);	

	m_widgetArea->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Minimum);
	frameLayout->setSpacing(0);
}

void CollapsibleFrame::determineIcon()
{
    if(m_frameState == OPEN) {
        icon->setStyleSheet("QLabel { image: url(:/expand.png) no-repeat; } ");
        m_widgetArea->show();
		emit showMe();
    }
    else {
        icon->setStyleSheet("QLabel { image: url(:/collapse.png) no-repeat; }");
        m_widgetArea->hide();
//		m_frame->resize(QSize(22,22));
		emit hideMe();
    }
}

void CollapsibleFrame::changeState()
{
    if(m_frameState == OPEN) {
        m_frameState = CLOSED;
/* 		m_pCloseAnimation->setStartValue(QRect(0, 0, 200, 200));
		m_pCloseAnimation->setEndValue(QRect(22, 22, 200, 10));
	    m_pCloseAnimation->start();*/
	    m_widgetArea->hide();
    }
    else {
        m_frameState = OPEN;
        m_widgetArea->show();
    }
    determineIcon();
}

void CollapsibleFrame::addWidget(QWidget * widget)
{
    widgetLayout->addWidget(widget);
//	widgetLayout->setAlignment(widget,Qt::AlignTop);
}

void CollapsibleFrame::addLayout(QLayout *layout)
{
    widgetLayout->addLayout(layout);
}

void CollapsibleFrame::setTopBackground(QColor color)
{
//	m_headerArea->setPalette(QPalette(color));
	QPalette p;
	m_headerArea->setAutoFillBackground(true);
	p.setColor(headerTextLabel->backgroundRole(),color);
	p.setColor(headerTextLabel->foregroundRole(),QColor(255,255,255));

	headerTextLabel->setPalette(p);
	m_headerArea->setPalette(p);

}
/**
* @brief add new inner element and create splitter
*
* @param wgti -[in] QWidget *wgt
*/
void CollapsibleFrame::addInnerSplitted(QWidget *wgt)
{
    if (!m_pSplitter) {
        m_pSplitter = new QSplitter(m_widgetArea);

        widgetLayout->addWidget(m_pSplitter);
        m_pSplitter->setOrientation(Qt::Vertical);
    }
    m_pSplitter->addWidget(wgt);
}

void CollapsibleFrame::hideWgt()
{
	changeState();
}

//void CollapsibleFrame::showWgt()
//{
//    changeState();
//}

void CollapsibleFrame::setInnerSplitter(int sz)
{
	QList<int> sz_list = m_pSplitter->sizes();
	sz_list[1] = sz;
	m_pSplitter->setSizes(sz_list);
}

bool CollapsibleFrame::isCollapsed(void)
{
	return (m_frameState == CLOSED) ;
}

void CollapsibleFrame::setSplitterSizes(QList<int> list)
{
	if (m_pSplitter) {
	m_pSplitter->setSizes(list);
	}
}

QList<int> CollapsibleFrame::getSplitterSize(void)
{	
	QList<int> tmp = m_pSplitter->sizes();
	return tmp;
}

QSplitter *CollapsibleFrame::splitter(void) 
{
	return m_pSplitter;
}	

void CollapsibleFrame::setHeaderHeight(int h)
{
	m_headerArea->setMaximumHeight(h);
    m_headerArea->setMinimumHeight(h);
}

void CollapsibleFrame::setMoveUpEnable(bool flag)
{
    m_up->setEnabled(flag);
}

void CollapsibleFrame::setMoveDownEnable(bool flag)
{
    m_up->setEnabled(flag);
}

void CollapsibleFrame::addUpDown()
{
    headerLayout->removeWidget(icon);
    headerLayout->addWidget(m_up); /*, 0, Qt::AlignTop | Qt:: AlignLeft);*/
    headerLayout->addWidget(m_down); /*, 0, Qt::AlignTop | Qt:: AlignLeft);*/
    headerLayout->addWidget(icon);
}

void CollapsibleFrame::hideHeader()
{
    m_headerArea->hide();
}

void CollapsibleFrame::hideButton()
{
    icon->hide();
}

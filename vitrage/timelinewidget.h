#ifndef TIMELINEWIDGET_H
#define TIMELINEWIDGET_H

#include <QWidget>
#include <QBasicTimer>
#include <QDockWidget>
#include <QToolTip>
#include <stdint.h>
#include <chrono>
#include <tuple>

#define START_INTERVAL 1

class QLabel;
class VAction;
class VScene;

class TimelineItem {
public:
    TimelineItem(int number_, std::chrono::milliseconds d, const QString &name_)
        : number(number_), duration(d), name(name_) {}
    TimelineItem(){}
    int number;
    std::chrono::milliseconds t{0};
    std::chrono::milliseconds duration{0};
    std::chrono::milliseconds pause_time{0};
    QString name;
};

using tuplePoint = std::tuple<QString, int, int, std::chrono::milliseconds, std::chrono::milliseconds>;

class TimelineWidget : public QWidget
{
    Q_OBJECT
public:
    explicit TimelineWidget(QWidget* parent = 0);
    QDockWidget *dock();
    void stop();
    void pause();
    void setNext(void);
    void setPrev(void);
    void play();
    void addInterval(uint32_t number, uint32_t duration, const QString& name = "");
    bool isRunning() const;
    bool isPaused() const;

private:
    QBasicTimer m_timeline;
    bool m_pause = false;

    QVector<TimelineItem> m_intervals;
    int m_currentIndex = 0;

    std::chrono::milliseconds m_duration{0}; // продолжительность сценария(всех сцен)
    std::chrono::milliseconds start_time{0}; // время начала проигрывания
    std::chrono::milliseconds current_time{0};

    std::chrono::milliseconds currentTime() const;
    std::chrono::milliseconds sumDuration(bool full) const;

    QLabel *m_timeLabel;
    QDockWidget *m_dock;

    tuplePoint tpoint;
    tuplePoint timeFromPoint(const QPoint &point);
    int intervalBySecond(int);
    void setButtStatus();
    bool validItem() const;
    QPair<long long, long long> getProgress() const;
    void paintProgress(QPainter *painter, int x0, int x);
    void paintScenes(QPainter *painter);

private slots:
    void slotTimeout();

public slots:
    void reset();

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
    void timerEvent(QTimerEvent *event);
    QSize sizeHint() const;

signals:
    void finished();
    void setScene(const QString&);
    void setRuntimeScene(QString);
    void itemActivated(int32_t id, uint32_t stage);
    void statusStarted(bool status);
    void timeElapsed();
    void enableButtons(bool previous, bool next);
    void enabToPlay(bool b);
};

#endif // TIMELINE_H

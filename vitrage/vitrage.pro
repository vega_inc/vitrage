include(../common.pri)

CONFIG += rtti
CONFIG += c++11

QT += network svg webkit
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app

TARGET = $${PROJECT_NAME}

HEADERS += canvaoptions.h \
    mainwindow.h \
    vcanva.h \
    vgrid.h \
    vsourcemanager.h \
    logicsourcemanager.h \
    vscene.h \
    vaction.h \
    fillaction.h \
    timelinewidget.h \
    extendedqlabel.h \
    collapsibleframe.h \
    controlleroptions.h \
    vgraphicseditor.h \
    vscheme.h \
    vgraphicswidget.h \
    vgraphicsviewer.h \
    widgetskins.h \
    setsize.h \
    vgraphicstext.h \
    vstatusbar.h \
    croppingwidget.h \
    configpath.h \
    aboutprogram.h \
    extronoptions.h \
    mixeroptions.h \
    sourcemanager.h \
    pluginsmanager.h \
    spdlog/details/async_log_helper.h \
    spdlog/details/async_logger_impl.h \
    spdlog/details/file_helper.h \
    spdlog/details/format.h \
    spdlog/details/line_logger.h \
    spdlog/details/log_msg.h \
    spdlog/details/logger_impl.h \
    spdlog/details/mpmc_bounded_q.h \
    spdlog/details/null_mutex.h \
    spdlog/details/os.h \
    spdlog/details/pattern_formatter_impl.h \
    spdlog/details/registry.h \
    spdlog/details/spdlog_impl.h \
    spdlog/sinks/android_sink.h \
    spdlog/sinks/base_sink.h \
    spdlog/sinks/dist_sink.h \
    spdlog/sinks/file_sinks.h \
    spdlog/sinks/null_sink.h \
    spdlog/sinks/ostream_sink.h \
    spdlog/sinks/sink.h \
    spdlog/sinks/stdout_sinks.h \
    spdlog/sinks/syslog_sink.h \
    spdlog/async_logger.h \
    spdlog/common.h \
    spdlog/formatter.h \
    spdlog/logger.h \
    spdlog/spdlog.h \
    spdlog/tweakme.h \
    sourcetablewidget.h \
    support.h \
    models/modellogicsources.h

FORMS += canvaoptions.ui mainwindow.ui newsource.ui fillaction.ui \
    controlleroptions.ui \
    vgraphicseditor.ui \
    vgraphicsviewer.ui \
    widgetskins.ui \
    setsize.ui \
    croppingwidget.ui \
    aboutprogram.ui \
    sourcemanager.ui \
    logicsourcemanager.ui

SOURCES += canvaoptions.cpp \
    main.cpp \
    mainwindow.cpp \
    vcanva.cpp \
    vgrid.cpp \
    vsourcemanager.cpp \
    logicsourcemanager.cpp \
    vscene.cpp \
    vaction.cpp \
    fillaction.cpp \
    timelinewidget.cpp \
    extendedqlabel.cpp \
    collapsibleframe.cpp \
    controlleroptions.cpp \
    vgraphicseditor.cpp \
    vscheme.cpp \
    vgraphicswidget.cpp \
    vgraphicsviewer.cpp \
    widgetskins.cpp \
    setsize.cpp \
    vgraphicstext.cpp \
    vstatusbar.cpp \
    croppingwidget.cpp \
    aboutprogram.cpp \
    extronoptions.cpp \
    mixeroptions.cpp \
    sourcemanager.cpp \
    pluginsmanager.cpp \
    spdlog/details/format.cc \
    sourcetablewidget.cpp \
    models/modellogicsources.cpp
            
INCLUDEPATH += $$INSTALL_PATH_INCLUDE

LIBS += -L$${INSTALL_PATH_LIB} \
        -lvsource \
        -lvcontroller \
        -lqjson

TRANSLATIONS += vitrage.ts

RESOURCES += \
    resources.qrc

DESTDIR = $$INSTALL_PATH_BIN
unix {
     target.path = /$(DESTDIR)
     INSTALLS += target
}

#ifndef NEWSOURCE_H
#define NEWSOURCE_H

#include <QDialog>
#include <vsource/vsource.h>

namespace Ui {
class NewSource;
}

class NewSource : public QDialog {
    Q_OBJECT

public:
    explicit NewSource(QWidget *parent = 0);
    ~NewSource();
    void fillSrcsTypes(void);
    void closeEvent(QCloseEvent *event);
    void setCurrentSrc(QString str);
    void delCurrentSrc(void);
	void  vovatest(void);

public slots:
    void select_clicked(void);
    void current_type_changed(QString str);
    void saveCurrentSrc(void);
private:
    VSource *current_src;
    Ui::NewSource *ui;
signals:
    void close_sig(void);
};

#endif // NEWSOURCE_H

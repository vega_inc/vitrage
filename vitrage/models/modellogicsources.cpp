#include <QIcon>
#include "modellogicsources.h"

ModelLogicSources::ModelLogicSources(QObject *parent)
    : QAbstractTableModel(parent) {

}

void ModelLogicSources::setSources(const QVector<VSource *> &s)
{
    m_sources = s;
    reset();
}

void ModelLogicSources::removeSource(int i)
{
    beginRemoveRows(QModelIndex(), i, i);
    m_sources.remove(i);
    endRemoveRows();
}

Qt::ItemFlags ModelLogicSources::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;
    return Qt::ItemIsSelectable |/*  Qt::ItemIsEditable | */Qt::ItemIsEnabled;
}

int ModelLogicSources::rowCount(const QModelIndex &) const
{
    return m_sources.size();
}

int ModelLogicSources::columnCount(const QModelIndex &) const
{
    return 3;
}

QVariant ModelLogicSources::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    auto row = index.row();
    auto column = index.column();
    if (row >= m_sources.size())
        return {};
    VSource *source = m_sources.at(row);
    if (!source)
        return {};

    if (row >= m_sources.size())
        return QVariant();

    if (role == Qt::DisplayRole) {
        if (column == 0) {
            return source->getname();
        }
        else if (column == 1) {
            return source->getPhysName();
        }
        return QVariant();
    }
    else if (role == Qt::DecorationRole)
    {
        if (column == 0) {
            if (source->gettype() == "VNCClient") {
                return QIcon("://vnc-server.png");
            }
            else if (source->gettype() == "NamedSource") {
                return  QIcon("://dvi_port.png");
            }
            else if (source->gettype() == "APPSource") {
                return  QIcon("://vega_logo.png");
            }
            else if (source->gettype() == "FileSource") {
                return  QIcon("://ava5.png");
            }
            return QIcon("://delte.png");;
        }
        return {};
    }
    return QVariant();
}

bool ModelLogicSources::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role == Qt::EditRole)
    {
        int row = index.row();
        switch (index.column()) {
        case 0:
            m_sources[row]->setname(value.toString());
            break;
        case 1:
            m_sources[row]->setPhysName(value.toString());
            break;
        default:
            break;
        }
    }
    return true;
}

QVariant ModelLogicSources::headerData(int, Qt::Orientation, int) const
{
    return QVariant();
}

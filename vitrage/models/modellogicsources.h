#ifndef MODELLOGICSOURCES_H
#define MODELLOGICSOURCES_H
#include <QAbstractTableModel>
#include <QVector>
#include <QObject>
#include "../vsource/vsource.h"

class ModelLogicSources : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit ModelLogicSources(QObject *parent = 0);
    void setSources(const QVector<VSource*> &s);
    QVector<VSource*> sources() const { return m_sources; }
    void removeSource(int i);

    Qt::ItemFlags flags(const QModelIndex &index) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);
    QVariant headerData(int, Qt::Orientation, int) const;

private:
    QVector<VSource*> m_sources;
};

#endif // MODELLOGICSOURCES_H

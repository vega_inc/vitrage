#include "croppingwidget.h"
#include "ui_croppingwidget.h"
#include <QResizeEvent>
#include <QPainter>
#include <QTimer>

#include <QDebug>

CroppingWidget::CroppingWidget(QWidget *parent, QSharedPointer<VSource> src, QSize srcSize, VScene *pScene, QSharedPointer<VSource> pSrc) :
    QDialog(parent,Qt::Window|Qt::WindowTitleHint),
    ui(new Ui::CroppingWidget),
    m_vsrc(src),
    parentScene(pScene),
    parentSource(pSrc),
    m_hFW( qAbs(( (double)srcSize.height() ) / ( (double) srcSize.width())) ),
    firstSrcSize(srcSize)
{
    ui->setupUi(this);

    ui->buttonBox->button(QDialogButtonBox::Ok)->setText("Ок");
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText("Отмена");
    ui->buttonBox->button(QDialogButtonBox::Apply)->setText("Применить");
    setWindowTitle("Кадрирование");
    if(firstSrcSize.width() >= firstSrcSize.height()) {
        ui->horizontalSpacer->changeSize(0,0,QSizePolicy::Fixed,QSizePolicy::Fixed);
    }
    m_vsrc->resize(firstSrcSize);

    if(parentSource->getPosition().getCropSowOnWiw()>0 && parentSource->getPosition().getCropSohOnWih()>0)
    {
        VPosition tmpPosition = parentSource->getPosition();

        QPoint tmpRubberPointTL( qRound(firstSrcSize.width() * tmpPosition.getCropLxW()),
                                 qRound(firstSrcSize.height() * tmpPosition.getCropTyH() ));
        int rubberWidth = qRound(firstSrcSize.width() * tmpPosition.getCropSowOnWiw());
        int rubberHeight = qRound(firstSrcSize.height() * tmpPosition.getCropSohOnWih());

        firstRubberRect.setTopLeft(tmpRubberPointTL);
        firstRubberRect.setWidth(rubberWidth);
        firstRubberRect.setHeight(rubberHeight);

        m_vsrc->setRubberCropRect(firstRubberRect);
        m_vsrc->setVisibleCropRubberRect(true);
    }

    m_vsrc->setParent(ui->frame);
    m_vsrc->start();

    if(m_vsrc->width() >= m_vsrc->height())
    {
        m_vsrc->setMinimumSize(firstSrcSize.width(),qRound(firstSrcSize.width()*m_hFW));
    }
    else {
        m_vsrc->setMinimumSize(qRound( ((double)firstSrcSize.height())/m_hFW ),firstSrcSize.height());
        magicNumberHeight = firstSrcSize.height() * 2;
    }
    ui->frame->layout()->addWidget(m_vsrc.data());

    m_vsrc->m_isCroppedClone = true;

    setAttribute(Qt::WA_TransparentForMouseEvents,true);
    ui->frame->setAttribute(Qt::WA_TransparentForMouseEvents,true);
    m_vsrc->setAttribute(Qt::WA_TransparentForMouseEvents,false);

    connect(ui->buttonBox,SIGNAL(clicked(QAbstractButton*)),this,SLOT(buttonsClicked(QAbstractButton*)));
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateCropShapeTimer()));
    timer->start(200);

    show();
    adjustSize();

    if( ui->frame->height() < (m_vsrc->height()+3*VALUE_MARGIN_LAYER) )
        ui->frame->setMinimumHeight( m_vsrc->height()+3*VALUE_MARGIN_LAYER );
    if(m_vsrc->width() < m_vsrc->height())
    {
        ui->frame->setMinimumSize(m_vsrc->width(), m_vsrc->height());
        m_vsrc->update();
    }
    resize(size()+QSize(1,1));
    resize(size()-QSize(1,1));
  }

CroppingWidget::~CroppingWidget()
{
    removeEventFilter(this);
    if(QApplication::overrideCursor())
        QApplication::restoreOverrideCursor();
    delete timer;
    m_vsrc->finalize();
    delete ui;
}

void CroppingWidget::mouseMoveEvent(QMouseEvent *event)
{
    m_vsrc->mouseMoveEvent(event);
    QDialog::mouseMoveEvent(event);
}

void CroppingWidget::mousePressEvent(QMouseEvent *event)
{
    m_vsrc->mousePressEvent(event);
    QDialog::mousePressEvent(event);
}

void CroppingWidget::mouseReleaseEvent(QMouseEvent *event)
{
    m_vsrc->mouseReleaseEvent(event);
    if(m_vsrc->isCropRubberRectShown())
    {
        QRect tmpRect = m_vsrc->getRubberCropRect().normalized();

        QPoint tmpRubberPointTL = tmpRect.topLeft();
        int rubberWidth = tmpRect.width();
        int rubberHeight = tmpRect.height();

        tmpRubberPointTL.setX( qRound( ( (double) tmpRubberPointTL.x() ) / stepXfromFirstSz ) );
        tmpRubberPointTL.setY( qRound( ( (double) tmpRubberPointTL.y() )/ stepYfromFirstSz ) );

        rubberWidth = qRound( ( (double) rubberWidth )  /  stepXfromFirstSz  );
        rubberHeight = qRound( ( (double) rubberHeight ) /  stepYfromFirstSz  );

        firstRubberRect.setTopLeft(tmpRubberPointTL);
        firstRubberRect.setWidth(rubberWidth);
        firstRubberRect.setHeight(rubberHeight);
    }
    QDialog::mouseReleaseEvent(event);
}

void CroppingWidget::resizeEvent(QResizeEvent *)
{
    if(m_vsrc->width() >= m_vsrc->height())
    {
        m_vsrc->resize(m_vsrc->width(),qRound(m_hFW * m_vsrc->width()));
    }
    else
    {
        int tmpW = qRound( ((double)m_vsrc->height())/m_hFW );
        int tmpH = m_vsrc->height();

        ui->frame->setMinimumSize(tmpW, tmpH);

        m_vsrc->resize(tmpW,tmpH);

        ui->frame->adjustSize();
        if( tmpH < magicNumberHeight)
            adjustSize();
        update();
    }


    stepXfromFirstSz = qAbs(( (double) m_vsrc->width() ) / ( (double) firstSrcSize.width() ));
    stepYfromFirstSz = qAbs(( (double) m_vsrc->height() ) / ( (double) firstSrcSize.height() ));

    if( m_vsrc->isCropRubberRectShown())
    {
        QRect tmpRubberRect = m_vsrc->getRubberCropRect();

        QPoint tmpRubberPointTL = firstRubberRect.topLeft();

        tmpRubberPointTL.setX( qRound(  tmpRubberPointTL.x()  * stepXfromFirstSz ) );
        tmpRubberPointTL.setY( qRound(  tmpRubberPointTL.y()  * stepYfromFirstSz ) );

        int rubberWidth = firstRubberRect.width();
        int rubberHeight = firstRubberRect.height();

        rubberWidth = qRound( rubberWidth * stepXfromFirstSz );
        rubberHeight = qRound( rubberHeight * stepYfromFirstSz );

        tmpRubberRect.setTopLeft(tmpRubberPointTL);
        tmpRubberRect.setWidth(rubberWidth);
        tmpRubberRect.setHeight(rubberHeight);

        m_vsrc->setRubberCropRect(tmpRubberRect);
    }
}

void CroppingWidget::buttonsClicked(QAbstractButton* button)
{
    switch(ui->buttonBox->standardButton(button))
    {
    case QDialogButtonBox::Ok:
        onOkClicked();
        break;
    case QDialogButtonBox::Apply:
        onButtonEnableCropClicked();
        break;
    case QDialogButtonBox::Cancel:
        onButtonCancelCropClicked();
        break;
    default: break;
    }
}

void CroppingWidget::onOkClicked()
{
    onButtonEnableCropClicked();
    close();
}

void CroppingWidget::onButtonCancelCropClicked()
{
    if (m_vsrc) {
        m_vsrc->setCropped(false);
    }
    close();
}

void CroppingWidget::onButtonEnableCropClicked()
{
    if( firstRubberRect.width()>2 && firstRubberRect.height()>2 )
    {
        m_vsrc->setCropped(true);

        VPosition tmpPosition = parentSource->getPosition();
        double tmpDiffx = (double)( (tmpPosition.rb.x() - tmpPosition.lt.x())*tmpPosition.getWidth() )/((double)firstSrcSize.width());
        double tmpDiffy = (double)( (tmpPosition.rb.y() - tmpPosition.lt.y())*tmpPosition.getHeight())/((double)firstSrcSize.height());

        QPoint tmpRubberPointTL = firstRubberRect.topLeft();
        tmpRubberPointTL.setX( qRound(  tmpRubberPointTL.x()  * tmpDiffx ) );
        tmpRubberPointTL.setY( qRound(  tmpRubberPointTL.y()  * tmpDiffy ) );

        int rubberWidth = firstRubberRect.width();
        int rubberHeight = firstRubberRect.height();

        rubberWidth = qRound( rubberWidth * tmpDiffx );
        rubberHeight = qRound( rubberHeight * tmpDiffy );

        tmpPosition.setCropLxW(((double)firstRubberRect.topLeft().x())/((double)firstSrcSize.width()) );
        tmpPosition.setCropTyH(((double)firstRubberRect.topLeft().y())/((double)firstSrcSize.height()) );
        tmpPosition.setCropSowOnWiw( ((double)firstRubberRect.width())/((double)firstSrcSize.width()) );
        tmpPosition.setCropSohOnWih( ((double)firstRubberRect.height())/((double)firstSrcSize.height()) );

        parentSource->setPosition(tmpPosition);
        parentSource->setCropped(true);
        parentSource->setSelected(true);
        parentSource->m_cropMode = m_vsrc->m_cropMode;
        parentScene->setPosition(*parentSource);
    }
}

void CroppingWidget::updateCropShapeTimer()
{
    m_vsrc->blinkCrop = !m_vsrc->blinkCrop;
}

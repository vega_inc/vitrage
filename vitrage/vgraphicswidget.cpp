#include "vgraphicswidget.h"
#include "vgraphicstext.h"
#include "setsize.h"

#include <QPolygon>
#define DEFAULT_WIDTH  32
#define DEFAULT_HEIGHT 32


#define PICS_DEFAULT_WIDTH  100
#define PICS_DEFAULT_HEIGHT 100
#define COS45 0.7071

#define ERROR_COLOR QColor(255,0,0)

#define SMALL_D 2/3  /* scaler for small circle delta */
#define SELECT_COLOR QColor(10,59,100)


VGraphicsWidget::VGraphicsWidget(VSource *src)
{
    m_pSource = src;
}

VGraphicsWidget::VGraphicsWidget(QIcon icon, QString lgName, QString phName)
{
    m_icon = icon;
    m_pSource = NULL;
    m_PhisName = phName;
    m_LogicName = lgName;
    setType(VGraphicsWidget::ICON);
    setFlag(ItemIsMovable,true);
    m_pActionSetPicture = new QAction(tr("Set Picture"),this);
    connect(m_pActionSetPicture,SIGNAL(triggered()),this,SLOT(setWgtPicture()));
    m_pActionSetSize = new QAction(tr("Set Size"),this);
    connect(m_pActionSetSize,SIGNAL(triggered()),this,SLOT(setWgtSize()));
    m_pActionSetSkin = new QAction(tr("Set Skin"),this);
    connect(m_pActionSetSkin,SIGNAL(triggered()),this,SLOT(setSkin()));
    m_pActionBare = new QAction(tr("Bare Action"),this);
    //connect(m_pActionBare,SIGNAL(triggered()),this,SLOT(bare()));
    m_pActionRotate = new QAction(tr("Rotate 90"),this);
    connect(m_pActionRotate,SIGNAL(triggered()),this,SLOT(rotate90()));
    m_pictureHeight = DEFAULT_WIDTH;
    m_pictureWidth  =  DEFAULT_HEIGHT;
    setMovable(true);
    setFlag(ItemIsSelectable,true);
    //setAcceptsHoverEvents(true);
    setAcceptHoverEvents(true);
    setLight(false);
    m_resizeFlag = false;
    m_pressedFlag = false;
  //  createSvgRender("/home/vbelianin/test3.svg");
    m_textItem = new VGraphicsText;
    //m_textItem->setFlag(ItemIsMovable,true);
    //m_textItem->setFlag(ItemIsSelectable,true);
    setStatus(RUNTIME_ST);
    setMode(VGraphicsWidget::EDITING);
    this->setText(lgName);
    QString tmpStr = getImagesPath();
    setDefPicPath(tmpStr);
    //setAcceptTouchEvents(true);
    setActive(false);
}

VGraphicsWidget::VGraphicsWidget()
{
}

VGraphicsWidget::WGT_MODE VGraphicsWidget::mode()
{
    return m_mode;
}


void  VGraphicsWidget::setMode(VGraphicsWidget::WGT_MODE mode)
{
    switch (mode) {
    case  VGraphicsWidget::EDITING  :
        setMovable(true);
        setFlag(QGraphicsItem::ItemIsSelectable);
    break;
    case VGraphicsWidget::RUNTIME :
        setMovable(false);
     //   m_textItem->setFlag(ItemIsMovable,false);
     //   m_textItem->setFlag(ItemIsSelectable,false);
    break;
    }
    textItem()->setMode(mode);
    m_mode = mode;
    this->update();
}


/**
 * @brief VGraphicsWidget::setStatus status for painting mode
 * @param status - VGraphicsWidget::WGT_STATUS
 */
void VGraphicsWidget::setStatus(WGT_STATUS status)
{
    m_status = status;
    switch (m_status) {
    case (RUNTIME_ST) :
        this->setElementId(RUNTIME_ID);
    break;
    case (SELECTED_ST) :
        this->setElementId(SELECTED_ID);
    break;
    case (ERROR_ST) :
        this->setElementId(ERROR_ID);
    break;
    case (OFF_ST) :
        this->setElementId(OFF_ID);
    case (ERROR_SELECTED_ST) :
        this->setElementId(OFF_ID);
    break;
    }
    textItem()->setStatus(status);
    update();
    emit updateParent();
}


/**
 * @brief VGraphicsWidget::status return m_status
 * @return  VGraphicsWidget::WGT_STATUS
 */
WGT_STATUS VGraphicsWidget::status()
{
    return m_status;
}

/**
 * @brief VGraphicsWidget::setType set new type for widget
 * @param type - VGraphicsWidget::WGT_TYPE
 */
void VGraphicsWidget::setType(VGraphicsWidget::WGT_TYPE type)
{
    switch (type) {
    case  (VGraphicsWidget::ICON)  :
        //setMovable(true);
    break;
    case (VGraphicsWidget::PICTURE) :
        //setMovable(false);
        //setFlag(ItemIsPanel);
        //setFlag(ItemIsSelectable,false);
        //setAcceptDrops(true);
        //setFlag(QGraphicsItem::ItemIsSelectable);
    break;
    case (VGraphicsWidget::WGT) :
    break;
    }
    m_type = type;
}

VGraphicsWidget::WGT_TYPE VGraphicsWidget::getType()
{
    return m_type;
}

QImage VGraphicsWidget::getOriginImage()
{
    return m_ImageOrig;
}

void VGraphicsWidget::paintStatusForPicture(QPainter *painter)
{
    VSource *src = getSource();
    if (!src)  {
		qDebug() << "No Source for VGraphicsWidget";				
        return;
      //  abort();
    }
    VSource::ExecStatus status = src->getStatus();
    switch(status){
    case (VSource::ExecStatus::RunStatus) :
			qDebug() << "Case RunStatus";
        if (src->getEnabledFlag()) {

            if (getSource()) {
            getSource()->paintStatusOn(painter,getXDelta(),getXDelta(),
                                               m_pictureWidth,m_pictureHeight);
            getSource()->paintStatusIcon(painter,VSource::StatusIcon::EnabledIcon,
                                               m_pictureWidth,m_pictureHeight);
            }
        } else {
            if (getSource()) {
                getSource()->paintStatusOff(painter,getXDelta(),getXDelta(),
                                              m_pictureWidth,m_pictureHeight);

                getSource()->paintStatusIcon(painter,VSource::StatusIcon::DisabledIcon,
                                               m_pictureWidth,m_pictureHeight);
            }
        }
    break;
    case (VSource::ExecStatus::StopStatus) :
		qDebug() << "Case StopStatus";
    break;
    case (VSource::ExecStatus::ErrorStatus) :
	qDebug() << "Case Error status";
            if (src->getEnabledFlag()) {
            if (getSource()) {
                getSource()->paintStatusError(painter,getXDelta(),getXDelta(),
                                              m_pictureWidth,m_pictureHeight);
                getSource()->paintStatusIcon(painter,VSource::StatusIcon::ErrorIcon,
                                                m_pictureWidth,m_pictureHeight);
            }
            } else {
            if (getSource()) {
                getSource()->paintStatusErrorOff(painter,getXDelta(),getXDelta(),
                                                 m_pictureWidth,m_pictureHeight);
                getSource()->paintStatusIcon(painter,VSource::StatusIcon::DisabledErrorIcon,
                                                m_pictureWidth,m_pictureHeight);
            }
            }
    break;
    }
    if (src->getSelected()) {
				paintSelected(painter);
	}
}

void VGraphicsWidget::paintSelected(QPainter *painter)
{
    QBrush br = painter->brush();
	QPen pen = painter->pen();
	QPen pen1 = pen;
    QBrush br1;

    if (mode() == VGraphicsWidget::RUNTIME) {
    pen1.setWidth(getXDelta());
    pen1.setColor(SELECT_COLOR);
	painter->setPen(pen1);
    br1 = br;
    br1.setColor(SELECT_COLOR);
	painter->setBrush(br1);
    painter->drawRect(QRect(getXDelta()/2,getXDelta()/2,m_pictureWidth-getXDelta()
                                                 ,m_pictureHeight-getXDelta()));
    painter->setBrush(br);
    }

}

void VGraphicsWidget::getMap(QMap<QString, QVariant> &map)
{
    map.clear();
    map.insert(ELEM_LOGIC_NAME,logicName());
    map.insert(ELEM_PHIS_NAME,phisName());
    map.insert(ELEM_POS_X,pos().x());
    map.insert(ELEM_POS_Y,pos().y());
    map.insert(ELEM_POS_W,m_pictureWidth);
    map.insert(ELEM_POS_H,m_pictureHeight);
    map.insert(ELEM_LABEL_X,this->textItem()->pos().x());
    map.insert(ELEM_LABEL_Y,this->textItem()->pos().y());
}

QImage VGraphicsWidget::getIconPicture()
{
    return m_icon.pixmap(QSize(m_pictureWidth,m_pictureHeight)).toImage();
}

QString VGraphicsWidget::defPicturePath()
{
    return m_defPicturePath;
}

void VGraphicsWidget::setDefPicPath(QString path)
{
    m_defPicturePath = path;
}


void VGraphicsWidget::paintErrorForPicture(QPainter *painter,bool selected)
{
    QBrush br = painter->brush();
    QPen pen = painter->pen();
    QBrush br1 = painter->pen().brush();
    br1.setColor(ERROR_COLOR);
    int radius;
    radius = ((m_pictureWidth>m_pictureHeight)?m_pictureHeight:m_pictureWidth)/6;
    pen.setBrush(br1);
    painter->setOpacity(VSource::OPAC);
    painter->fillRect(QRect((!selected)?getXDelta():0,(!selected)?getXDelta():0,
    m_pictureWidth - ((!selected)?2*getXDelta():0), m_pictureHeight - ((!selected)?2*getXDelta():0)),br1);
    pen.setColor(Qt::white);
    pen.setWidth(2);
    painter->setPen(pen);
    painter->setOpacity(1);
    painter->drawEllipse(QPoint(m_pictureWidth/2,m_pictureHeight*2/5),radius,radius);
    painter->setBrush(br);

    int delta = radius * COS45;
    QPoint leftTop = QPoint(m_pictureWidth/2 - delta,m_pictureHeight*2/5 - delta);
    QPoint rightButtom = QPoint(m_pictureWidth/2 + delta,m_pictureHeight*2/5 + delta);
    painter->drawLine(leftTop,rightButtom);
    QFont ft = painter->font();
    ft.setPixelSize(((m_pictureWidth>m_pictureHeight)?m_pictureHeight:m_pictureWidth)/8);
    painter->setFont(ft);
    painter->drawText(QRect(m_pictureWidth/4,m_pictureHeight*3/5,m_pictureWidth*2/3,
                                                  m_pictureHeight*3/5),tr("No signal!"));

}

void VGraphicsWidget::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                                                                      QWidget *widget)
{
    switch (m_type) {
    case (VGraphicsWidget::PICTURE) :
        painter->drawImage(QRect(getXDelta(),getXDelta(),
            m_pictureWidth-2*getXDelta(),m_pictureHeight-2*getXDelta()),m_Image);
        paintStatusForPicture(painter);
    break;
    case (VGraphicsWidget::ICON) :
        m_icon.paint(painter,QRect(getXDelta(),getXDelta(),
              m_pictureWidth-2*getXDelta(),m_pictureHeight-2*getXDelta()));
        paintStatusForPicture(painter);
    break;
    case (VGraphicsWidget::WGT) :
    break;
    }
    paintSelectedRect(painter);
    QGraphicsSvgItem::paint(painter,option,widget);
    //QGraphicsItem::paint(painter,option,widget);
}

void VGraphicsWidget::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu menu;
    //QAction act1;1
    switch (m_mode) {
    case (EDITING) :
        menu.addAction(m_pActionSetPicture);
        //menu.addAction(m_pActionSetSkin);
        menu.addAction(m_pActionSetSize);
        menu.addAction(m_pActionRotate);
    break;
    case (RUNTIME) :
        menu.addAction(m_pActionBare);
    break;
    }
    menu.exec(event->globalPos());
    //   connect(tmp,SIGNAL(triggered()),this,SLOT(setWgtPicture()));
}

void VGraphicsWidget::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
   if (m_resizeFlag && m_pressedFlag) {
   QPoint p = this->mapToItem(this,event->pos().toPoint()).toPoint();
   m_pictureWidth = p.x();
   m_pictureHeight = p.y();
   if (m_type == VGraphicsWidget::PICTURE) {
       m_Image = m_ImageOrig.scaled(QSize(m_pictureWidth,m_pictureHeight),
                              Qt::IgnoreAspectRatio,Qt::SmoothTransformation);
   }
   if (m_type == VGraphicsWidget::WGT) {
       QRectF rect = renderer()->boundsOnElement(RUNTIME_ID);
       qreal ww = rect.width();
       qreal hh = rect.height();
       float x_m = p.x()/ww;
       float y_m = p.y()/hh;
       this->scale(x_m,y_m);
       emit modefied();
   }
   this->update();
   event->accept();
   } else {
        QGraphicsItem::mouseMoveEvent(event);
        event->accept();
        this->update();
   }
  emit  updateParent();
}

void VGraphicsWidget::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
  //  qDebug() << "hoverMoveEvent";
   // Q_UNUSED(event);
    QCursor cursor = this->cursor();
    if (m_resizeFlag && m_pressedFlag) {
    QPoint p = event->pos().toPoint();
    qDebug() << p.x()  << p.y();
    }
    if (m_type != VGraphicsWidget::ICON)
    changeCursor(event->pos().toPoint());
    //event->ignore();
    event->accept();
}

void VGraphicsWidget::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    //qDebug() << "Mouse Release event";
    m_resizeFlag = false;
    m_pressedFlag = false;
    setMovable(true);
  //  event->accept();
    QGraphicsItem::mouseReleaseEvent(event);
    event->accept();
    emit setResizeFlag(false);
}

void VGraphicsWidget::changeCursor(QPoint p)
{
  //  if (this->resize_f) return;
  //  QPoint pt = mapQWidget::mapFromGlobal(p);
    QPoint pt = p;
    int x = pt.x();
    int y = pt.y();
    if (x > this->m_pictureWidth - MARG) {
      //  this->setCursor(Qt::SizeHorCursor);
     //   this->setResizeType(RIGHT);
//        if ((y >= 0) && (y <= BIG_MARG)) {
//            setCursor(Qt::SizeBDiagCursor);
//            setResizeType(RIGHT_H);
//            return;
//        }
        if (y >= (this->m_pictureHeight - 2*BIG_MARG))   {
            setCursor(Qt::SizeFDiagCursor);
            setResizeType(RIGHT_L);
            emit setResizeFlag(true);
            return;
        }
        return;
    }


//    if (x <= MARG) {
//        setResizeType(LEFT);
//        setCursor(Qt::SizeHorCursor);
//        if ((y >= 0) && (y <= BIG_MARG)) {
//            setResizeType(LEFT_H);
//            setCursor(Qt::SizeFDiagCursor);
//            return;
//        }
//        if (y >= (this->m_pictureHeight - BIG_MARG))   {
//            setResizeType(LEFT_L);
//            setCursor(Qt::SizeBDiagCursor);
//            return;
//        }
//        return;QGraphicsWidget:
//    }

    if (y > this->m_pictureHeight - MARG) {
    //    this->setResizeType(LOW);

    //    this->setCursor(Qt::SizeVerCursor);
       // if ((x >= 0) && (x <= BIG_MARG)) {
        //    setResizeType(RIGHT_L);
        //    setCursor(Qt::SizeBDiagCursor);
            return;
     //   }
        if (x >= (this->m_pictureWidth - 2*BIG_MARG))   {
            setResizeType(RIGHT_L);
            setCursor(Qt::SizeFDiagCursor);
             emit setResizeFlag(true);
            return;
        }
        return;
    }


//    if (y <= MARG) {
//        setResizeType(HIGH);
//        setCursor(Qt::SizeVerCursor);
//        if ((x >= 0) && (x <= BIG_MARG)) {
//            setCursor(Qt::SizeFDiagCursor);
//            setResizeType(LEFT_H);
//            return;
//        }
//        if (x >= (this->m_pictureWidth - BIG_MARG))   {
//            setResizeType(RIGHT_H);
//            setCursor(Qt::SizeBDiagCursor);
//            return;
//        }
//        return;
//    }

    setCursor(Qt::ArrowCursor);
    emit setResizeFlag(false);
    setResizeType(NONE);
}

bool VGraphicsWidget::getLight()
{
    return m_lightOn;
}

int VGraphicsWidget::getXDelta()
{
    return (DELTA_PROC * m_pictureWidth/100);
}


int VGraphicsWidget::getYDelta()
{
    return (DELTA_PROC * m_pictureHeight/100);
}

int VGraphicsWidget::getSmallDelta()
{
    return (SMALL_DELTA * m_pictureHeight/100);
}

void VGraphicsWidget::setPictureSize(int w , int h)
{
    m_pictureHeight = h;
    m_pictureWidth = w;
}

VSource *VGraphicsWidget::getSource()
{
    return m_pSource;
}

void VGraphicsWidget::setSource(VSource *src)
{
    m_pSource = src;
    connect(m_pSource,SIGNAL(updateModelFromSource()),this,SLOT(updateView()));
}



VGraphicsText *VGraphicsWidget::textItem()
{
    return m_textItem;
}



void VGraphicsWidget::setIcon(QIcon icon)
{
    m_icon = icon;
    emit updateView();
}

void VGraphicsWidget::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    m_pressedFlag = true;
    QCursor c =  this->cursor();
    Qt::CursorShape shape =  c.shape();
    if (m_mode != RUNTIME) {
        if (shape != Qt::ArrowCursor) {
            m_resizeFlag = true;
            setMovable(false);
            //event->ignore();
            event->accept();
            return;
        }
    }
    if (m_mode == RUNTIME) {
        if (getSource()) {
            //getSource()->setActivated(true);
        }
        //emit setActivate();
        //emit widgetPressed(this->logicName());
        QDrag *drag = new QDrag(event->widget());
        QMimeData *mimeData = new QMimeData;
        mimeData->setText(m_LogicName);
        drag->setMimeData(mimeData);
        drag->exec();
    }
}

void VGraphicsWidget::setText(QString str)
{
    m_textItem->setPlainText(str);
}

void VGraphicsWidget::setMovable(bool flag)
{
    setFlag(ItemIsMovable,flag);
}

void VGraphicsWidget::setLight(bool light)
{
    m_lightOn = light;
}

void VGraphicsWidget::setLogicName(QString str)
{
    m_LogicName = str;
}


/**
 * @brief VGraphicsWidget::setPicture
 * set picture for widget
 * @param picture - [in] QString path to picture file */
void VGraphicsWidget::setPicture(QString picture)
{
    if (!m_Image.load(picture,"png")) {
            //abort();
            return;
    }
    setType(VGraphicsWidget::PICTURE);
    m_ImageOrig = m_Image;
    emit updateView();
}


/**
 * @brief VGraphicsWidget::createSvgRender set path to svg and create new
 * QSvgRender
 * @param str - QString path to svg file
 */
void VGraphicsWidget::createSvgRender(QString str)
{
    m_svgRenderer = new QSvgRenderer(str);
    this->setSharedRenderer(m_svgRenderer);
    //this->setBoundingRegionGranularity();
    QRectF rect = m_svgRenderer->boundsOnElement(RUNTIME_ID);
    m_pictureWidth = rect.width();
    m_pictureHeight = rect.height();
    //m_svgRenderer->viewBox().
    this->setElementId(RUNTIME_ID);
}

void VGraphicsWidget::setResizeType(resize_t t)
{
    m_resizeType = t;
}

void VGraphicsWidget::setSize(int w, int h)
{
    m_pictureWidth = w;
    m_pictureHeight = h;
}

void VGraphicsWidget::setSizeEmit(int w, int h)
{
    emit setSizeSignal(w,h);
    setSize(w,h);
}

void VGraphicsWidget::widgetActivate()
{
}


QString VGraphicsWidget::logicName()
{
    return m_LogicName;
}

QString VGraphicsWidget::phisName()
{
    return m_PhisName;
}
/**
 * @brief VGraphicsWidget::getImage set m_Image for VSource status
 * @return
 */
void VGraphicsWidget::getImage()
{
    VSource *src = getSource();
    if (!src)  {
        if (!m_Images.contains(bare)) {
            abort();
        }
        m_Image = m_Images.value(bare);
          m_ImageOrig = m_Image;
        return;
      //  abort();
    }
    VSource::ExecStatus status = src->getStatus();
    switch(status){
    case (VSource::ExecStatus::RunStatus) :
    if (src->getSelected()) {
        if (src->getEnabledFlag()) {
            m_Image = m_Images.value(on_selected);
        } else {
            m_Image = m_Images.value(off_selected);
        }
    } else {
        if (src->getEnabledFlag()) {
            m_Image = m_Images.value(on);
        } else {
            m_Image = m_Images.value(off);
        }
    }
    break;
    case (VSource::ExecStatus::StopStatus) :
    break;
    case (VSource::ExecStatus::ErrorStatus) :
        if (src->getSelected()) {
            if (src->getEnabledFlag()) {
                m_Image = m_Images.value(selected_on_nosignal);
            } else {
                m_Image = m_Images.value(selected_off_nosignal);
            }
        } else {
            if (src->getEnabledFlag()) {
                m_Image = m_Images.value(no_signal);
            } else {
                m_Image = m_Images.value(off_nosignal);
            }
        }
    break;
    }
    m_ImageOrig = m_Image;
}


QRectF VGraphicsWidget::boundingRect() const
{
    return QRect(0,0,m_pictureWidth,m_pictureHeight);
}

void VGraphicsWidget::paintSelectedRect(QPainter *painter)
{
    VGraphicsWidget::WGT_MODE mode_ = mode();
    if (isSelected()) {
    if (mode_ == VGraphicsWidget::EDITING) {
    QPen pen = painter->pen();
    QPen pen_tmp = pen;
        pen_tmp.setWidth(1);
        pen_tmp.setColor(Qt::black);
        painter->setPen(pen_tmp);
        painter->drawRect(0,0,m_pictureWidth,m_pictureHeight);
        QBrush br =pen.brush();
        br.setColor(Qt::black);
        painter->fillRect(m_pictureWidth-RESIZE,m_pictureHeight-RESIZE,
                                                             RESIZE,RESIZE,br);
        painter->setPen(pen);
    }
    }
    if (mode_ == VGraphicsWidget::RUNTIME) {
        if (getSource()) {
            if (getSource()->activated())
            getSource()->paintSelected(painter,m_pictureWidth,m_pictureHeight,0,0);
        }
    }
}



/**
 * @brief VGraphicsWidget::setWgtPicture set pictures for all statuses
 */
void VGraphicsWidget::setWgtPicture()
{
    this->setElementId("test3");
    QString fileName = QFileDialog::getOpenFileName(NULL,tr("set image"),
                                                    defPicturePath(),
                                                    "*");
    if (!m_Image.load(fileName)) {
        QString str = tr("Can't set picture");
        QMessageBox::critical(NULL,QString("FC1000"),str);
        return;
    }
    setType(VGraphicsWidget::PICTURE);
    m_ImageOrig = m_Image;
    m_pictureWidth = PICS_DEFAULT_WIDTH;
    m_pictureHeight = PICS_DEFAULT_HEIGHT;
    setDefPicPath(fileName);
    emit sigDefPicturePath(fileName);
    emit updateView();
}

void VGraphicsWidget::setWgtSize()
{
    SetSize sz(NULL,m_pictureWidth,m_pictureHeight);
    connect(&sz,SIGNAL(sizes(int,int)),this,SLOT(setSizeEmit(int,int)));
    sz.exec();
}

void VGraphicsWidget::setSkin()
{
    WidgetSkins *window  = new  WidgetSkins("./skins");
    connect(window,SIGNAL(selectedWidget(QString)),this,SLOT(selectedWgt(QString)));
    if (window->exec()) {
        createSvgRender(m_selectedWidtetFilePath);
        setType(VGraphicsWidget::WGT);
    }
    delete window;
}

//this->setCursor(Qt::SizeHorCursor);


void VGraphicsWidget::rotate90()
{
    QTransform trans;
    trans.rotate(90);
//    getImage();
    m_ImageOrig = m_ImageOrig.transformed(trans);
    m_Image = m_ImageOrig.scaled(QSize(m_pictureWidth,m_pictureHeight),
                           Qt::IgnoreAspectRatio,Qt::SmoothTransformation);
    //rotate(90);
}

void VGraphicsWidget::selectedWgt(QString str)
{
    m_selectedWidtetFilePath = str;
}

void VGraphicsWidget::mouseModeSlot(QPoint pos)
{
    Q_UNUSED(pos);
    //changeCursor(pos);
}

void VGraphicsWidget::updateView()
{
 //   getImage();
    update();
    this->updateParent();
}

QString VGraphicsWidget::getImagesPath()
{
    QString imageFilesPath = FIRST_SHARE_PATH
                             + QFileInfo(QString(qApp->argv()[0])).fileName()
                             + IMAGES_DIR_NAME;
    QDir dir(imageFilesPath);
    if(dir.exists() && dir.cd(imageFilesPath))
        return imageFilesPath;
    else
    {
        imageFilesPath = QDir::current().canonicalPath() + IMAGES_DIR_NAME;
        QDir dir(imageFilesPath);
        if(dir.exists() && dir.cd(imageFilesPath))
            return imageFilesPath;
        else
            return "";
    }

}









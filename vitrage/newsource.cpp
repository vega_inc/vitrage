#include "newsource.h"
#include "ui_newsource.h"
#include "vsourcemanager.h"


#include <QDebug>


NewSource::NewSource(QWidget *parent) :
        QDialog(parent),
        ui(new Ui::NewSource) {
    ui->setupUi(this);
    connect(this->ui->select_pB,SIGNAL(clicked()),this,SLOT(select_clicked()));
    connect(this->ui->srcs_types_cB,SIGNAL(currentIndexChanged(QString)),
            this,SLOT(current_type_changed(QString)));
    this->current_src = NULL;
}




NewSource::~NewSource() {
    delete ui;
}

void NewSource::fillSrcsTypes(void) {
    //qDebug() << QString("NewSource::fillSrcsTypes");
    this->ui->srcs_types_cB->clear();
    this->ui->srcs_types_cB->addItem(QString("..."));
    foreach(QString str,VSrcManager::Instance().src_creators.keys()) {
        this->ui->srcs_types_cB->addItem(VSrcManager::Instance()
                                         .src_creators.value(str)->getSourceType());
    }

    this->ui->cancel_pB_2->setEnabled(false);
    this->ui->select_pB->setEnabled(false);

}






void NewSource::select_clicked(void)
{
    QString current_type = this->ui->srcs_types_cB->itemText(this->ui->srcs_types_cB
                           ->currentIndex());
    VSourceCreator *VSrc_crt =  VSrcManager::Instance().
                                src_creators.value(current_type);
    VSource *new_source = VSrc_crt->createVSource();

    QWidget * wgt = new_source->getOptionsWindow();
    this->ui->horizontalLayout->addWidget(wgt);

}



void NewSource::current_type_changed(QString str) {
    delCurrentSrc();
    if (str == QString("...") || str == QString("")) {
        return ;
    }

    this->setCurrentSrc(str);
}



void NewSource::closeEvent(QCloseEvent *event) {
	Q_UNUSED(event);
    delCurrentSrc();
    emit close_sig();
}

void NewSource::saveCurrentSrc(void)
{
//    qDebug("NewSource::saveCurrentSrc");
//    VSrcManager::Instance().addSource(this->current_src);
//    disconnect((this->current_src),SIGNAL(save_clicked()),
//               this,SLOT(saveCurrentSrc()));
//    (current_src->getOptionsWindow())->setParent(current_src);
//    this->current_src = NULL;
//    this->ui->srcs_types_cB->setCurrentIndex(0);
}


void NewSource::setCurrentSrc(QString str) {
    VSourceCreator *VSrc_crt =  VSrcManager::Instance().
                                src_creators.value(str);
    VSource *src= VSrc_crt->createVSource();
    this->current_src = src;
    this->ui->horizontalLayout->addWidget(current_src->getOptionsWindow());
    connect((this->current_src),SIGNAL(save_clicked()),
            this,SLOT(saveCurrentSrc()));
}


void NewSource::delCurrentSrc(void) {
    if ((this->current_src)) {
        disconnect((this->current_src),SIGNAL(save_clicked()),
                   this,SLOT(saveCurrentSrc()));
        (current_src->getOptionsWindow())->setParent(current_src);
        delete this->current_src;
        this->current_src = NULL;
    }
}





#ifndef LOGICSOURCEMANAGER_H
#define LOGICSOURCEMANAGER_H

#include <QDialog>
#include <QVariantMap>
#include <QTableWidgetItem>
#include <QFileSystemModel>
#include <QToolButton>
#include <QFutureWatcher>
#include "sourcemanager.h"
#include "models/modellogicsources.h"

class VAction;
class VSource;

namespace Ui
{
	class setDev;
}

class LogicSourceManager : public QDialog
{
    Q_OBJECT
    enum {LGICON, LGNAME, LGPHNAME,  LGDEL} LOGIC_TABLE;
    enum class PHIS_TABLE { TYPEICON, NAME, DETAIL };

    QString getConfigFileNameWithPath();

public:
    explicit LogicSourceManager(VAction* action, QWidget *parent = 0);
    void fillProperty(VSource *src, QString physName);
    void removeSources(void);
    QString getTypeForName(QString name);
    QTableWidget *getTableWidget();
    void addDeviceForRow(int row);
    void on_btNewAction_clicked();
    void showEditSource();
    void loadSettings(const QSettings &settings);
    void setActionName(const QString &name);

    QTableWidget *getLogicTableWidget();
    QMap <QString, VSource*> &returnCurrentActionSources();

signals:
    void setRuntimeScene(const QString &str);
    void openGEditorS();
    void setDeviceToAction(bool);
    void renameSourceSig(QString name, QString oldName);
    void sendOpenHtml(const QString &page);

private:
    Ui::setDev *ui;
    QTableWidgetItem* m_pCurrentItem;
    VAction* m_pCurrentAction;
    QTimer *dummy;
    QVariantMap m_result;
    QList <VSource*> m_sourceList;
    bool m_update = false;
    SourceManager *m_pSourceManager;
    QCompleter *completerPhS = nullptr;
    QCompleter *completerLogS = nullptr;
    QFileSystemModel *modelMediaData;
    ModelLogicSources *modelLogicSource;
    QFutureWatcher<void> watcher;

    void fillSourceProperty(const QString &name, VSource* source);
    void setIcon(const QString &type,QToolButton * tb);
    void initProgressBar(int size, const QString &str);
    void setValueProgressBar(int value);
    void loadPhysicalDevices();
    void on_btUseGraphics_clicked();
    void addSource(const QString &filename);
    void fillLogicView(const QList<QTableWidgetItem *> &items);
    void setupMVC();

protected:
    void closeEvent(QCloseEvent *);
    void showEvent(QShowEvent *);

private slots:
    //void actionUpdated();
    void incrProgressBar();
    void textCh();
    void slotSetDeviceToAction();
    void slotTreeButtonClicked();
    void slotSetLogicIcon();
    void slotLogicDel();
    void getDetails();
    void on_pushButton_AddAll_clicked();
    void on_pushButton_Add_clicked();
    void on_btRem_clicked();
    void on_btSourcesManager_clicked();
    void on_toolButtonClose_clicked() { close(); }
    void sourcesChangedSlot();
    void enableCopySources();
    void showPhSource(const QString& text);
    void showSource(const QString& text);
    void resizeBack();
    void clickedHelp();
    void addMedia();
    void addMediaToAction();
    void addAllMediaToAction();
    void copyFinished();
    void removeOneSource();
    void selectionChangedSlot(const QItemSelection &, const QItemSelection &);

public slots:
    void on_btRemAll_clicked();
};

#endif // LOGICSOURCEMANAGER_H

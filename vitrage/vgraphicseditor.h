#ifndef VGRAPHICSEDITOR_H
#define VGRAPHICSEDITOR_H

#include <QMainWindow>
#include <QtGui>
#include "vaction.h"
#include "vscheme.h"
#include "vgraphicswidget.h"
#include "collapsibleframe.h"
#include "vsourcemanager.h"
#include "logicsourcemanager.h"
#include "widgetskins.h"
namespace Ui {
class VGraphicsEditor;
}


const int SOURCES_NUM = 0;       /* number in splitter */
const int SCHEMESOURCES_NUM = 1; /* number in splitter */
const int BARES_NUM = 2;         /* nubmer in splitter */


class VGraphicsEditor : public QMainWindow
{
    Q_OBJECT
    
public:
    enum {PH_ICON, PH_NAME, PH_INFO} PH_TABLE;
    enum {LG_ICON, LG_NAME,LG_PHNAME, LG_DEL} LG_TABLE;
    typedef enum {G_EDITING,G_RUNTIME}  G_MODE;


    explicit VGraphicsEditor(QWidget *parent = 0);
    ~VGraphicsEditor();
    void openForAction(VAction *);
    void setPropMap(QVariantMap *);
    void setAction(VAction *act);
    void setSourceManager(LogicSourceManager *src);
    void setSaved(bool  saved);
    void setMode(G_MODE mode);
    void setAction();
    bool isSaved();
    void setSavePath(QString path);
    void clearScheme();
    bool loadFromSaved();
    void setSourceTable(QTableWidget *table);
    void renameSource(QString name,QString oldName);
    QDockWidget *dock();
    VScheme *scheme();
    bool startAllWidgets();
    QString savePath();
    void setLogicTable(QTableWidget *table);
signals:
    void closeSig();
    void sourcesUpdated();
    void schemeLoaded(QString str);
    void schemeMod();
    void hideMe();
    void sendOpenHtml(const QString &page);

private:
    G_MODE m_mode;
    QString m_savePath;
    bool m_saved; /* flag for mode controll */
    void    changeCursor(QPoint);
    QList<VScheme*> m_schemes; /* list of modefied sources */
    VScheme *m_scheme = nullptr;         /* current active scheme */
    Ui::VGraphicsEditor *ui;
    VAction* m_pAction = nullptr;                  /* current Action */
    CollapsibleFrame *m_pSourcesM;       /* sources table */
    CollapsibleFrame *m_pSchemeSourcesM; /* sources in Sceme table */
    LogicSourceManager *    m_pLogicSourceManager; /* source manager */
    QDockWidget * m_dock;
    QDockWidget * m_innerDoc;
    //QTableWidget *//m_pSources;       /* table widget with source */
    QTableWidget *m_pSchemeSources; /* table widget with sources for table */
    QTableWidget *m_pSources;       /*    */
    QTableWidget *m_pLogicSources;
    QVariantMap *m_result;          /* pointer to map with properties */

    int m_sourceOldSize;         /* old size for collapsible */
    int m_schemeSourcesOldSize;  /* old size for collapsible */
    QList<int> m_splitterSizes;  /* sizes for sorces / content splitter */

    void addSourceToContentTable(QString name, QString logicName,QIcon* icon_p ,bool red = false);
    void createCollapsibles(void);
    void fillSourceProperty(uint index, VSource* source);
    void setOptionsForTable(QTableWidget *table);
    bool eventFilter(QObject *obj, QEvent *ev);
    void closeEvent(QCloseEvent *event);
    void showEvent(QShowEvent *);
    void setSizeForContent(void);
    bool removeSourcesFromAction(void);
    void on_actionCreate_triggered();

private slots:
    void closeWind(QCloseEvent *event);
    void schemeSourcesCellClicked(int row, int column);
    void sourceDropedToScheme(QString, QPoint );
    void slotSetLogicIcon();
    void resizeContentTable(QEvent *ev);
    void nameChanged(int,int);
    void slotLogicDel(void);
    void on_actionLoad_triggered();
    void sourceRemovedFromAction(QString name);
    void on_actionClose_triggered();
    void on_actionSave_2_triggered();
    void on_actionSave_As_triggered();
    void on_actionEditMode_triggered();
    void on_actionRuntimeMode_triggered();
    void on_actionExit_triggered();
    void on_actionCreate_Scheme_triggered();
    void on_actionHelp_triggered();

public slots:
    void schemeModefied(void);
    void on_actionSave_triggered();
};

#endif // VGRAPHICSEDITOR_H

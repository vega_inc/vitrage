#include "controlleroptions.h"
#include "ui_controlleroptions.h"
#include <QDir>
#include <QMessageBox>
#include <QPluginLoader>
#include <QHostAddress>
#include <QDebug>

ControllerOptions::ControllerOptions(QWidget *parent) :
    QWidget(parent)
  ,ui(new Ui::ControllerOptions)
  ,m_pController(NULL)
{
    ui->setupUi(this);

    ui->tBClose->hide();
    ui->tBSave->setEnabled(false);
    loadControllers();

    connect(ui->pushButtonConnect, SIGNAL(clicked(bool)), this, SLOT(onOffClicked(bool)));
    connect(ui->lEHost, SIGNAL(textChanged(QString)), this, SLOT(someChanged()));
    connect(ui->lEName, SIGNAL(textChanged(QString)), this, SLOT(someChanged()));
    connect(ui->lEPort, SIGNAL(textChanged(QString)), this, SLOT(someChanged()));
    connect(ui->lEPassword, SIGNAL(textChanged(QString)), this, SLOT(someChanged()));
    connect(ui->cBModel, SIGNAL(currentIndexChanged(QString)), this, SLOT(modelIndexChanged(QString)));
}

ControllerOptions::~ControllerOptions()
{
    delete ui;
}

bool ControllerOptions::loadControllers()
{
    QDir dir(getControllersPath());                                             // Директория с объектными файлами контроллеров
    if (!dir.exists()) {                                                        // если нет
        QMessageBox::critical(0, "", "Controllers directory does not exist");   // сообщение об ошибке и выход
        return false ;
    }
    ui->cBModel->clear();
    ui->cBModel->addItem(tr("..."));                                            // Зачем это переводить
    foreach (QString fileName, dir.entryList(QDir::Files)) {                    // создание объектов из плагинов
        QPluginLoader loader(dir.absoluteFilePath(fileName));
        VController *ctr = qobject_cast<VController*>(loader.instance());
        if (!ctr)  {
            qDebug() << loader.errorString();
            return true;
        }
        ctr->setProperty("fileName",fileName);                                  // контроллеру присваивается свойство с названием файла
        m_Controllers.insert(fileName,ctr);                                     // и запихивается в мап
        ui->cBModel->addItem(fileName);                                         // в КБ доб итем с соотв. именем
    }
    return false;
}
/// set controller in combo box
void ControllerOptions::setController(const QString &name)
{
    ui->cBModel->setCurrentIndex(ui->cBModel->findText(name));
}
/// load previus dsettings
void ControllerOptions::setControllerProperty(const QString &fname)
{
    settingsName = fname;
    QSettings settings(fname, QSettings::IniFormat);
    QString controllerFileName = settings.value("ControllerFileName").toString();

    m_pController = m_Controllers.value(controllerFileName);
    if (!m_pController) {
        return;
    }
    QString controllerName = m_pController->getName();
    if (!controllerName.isEmpty()) {
        settings.beginGroup(controllerName);
        auto address = settings.value("address").toString();
        auto port = settings.value("port").toString().toUInt();
        settings.endGroup();
        m_pController->setAddress(QHostAddress(address));
        m_pController->setPort(port);
    }
    m_pController->setProperty("host",     settings.value("ControllerHost").toString());
    m_pController->setProperty("port",     settings.value("ControllerPort").toString());
    m_pController->setProperty("username", settings.value("ControllerUserName").toString());
    m_pController->setProperty("passwd",   settings.value("ControllerPasswd").toString());
    m_pController->setProperty("timeout",  settings.value("ControllerTimeout").toString());
    m_pController->setProperty("fileName", settings.value("ControllerFileName").toString());

    setController(controllerFileName);
}

void ControllerOptions::stateChanged(bool isConnected)
{
    ui->pushButtonConnect->setChecked(isConnected);
    ui->pushButtonConnect->setText(isConnected?"Отключить":"Подключить");
}

void ControllerOptions::showEvent(QShowEvent *)
{
    if (!m_pController) {
        ui->cBModel->setCurrentIndex(0);
        ui->lEHost->setEnabled(false);
        ui->lEName->setEnabled(false);
        ui->lEPassword->setEnabled(false);
        ui->lEPort->setEnabled(false);
    } else {
        for (int i = 0; i < ui->cBModel->count(); i++) {
            QString str = ui->cBModel->itemText(i);
            VController *ctr = m_Controllers.value(str);
            if (ctr == m_pController) {
                ui->cBModel->setCurrentIndex(i);
                return;
            }
        }
    }
}

void ControllerOptions::setController(VController *ctr)
{
    if (!m_pController)
        return;
    m_pController = ctr;
    connect(m_pController,SIGNAL(statusChanged()),this,SLOT(newStatus()),Qt::QueuedConnection);
}

void ControllerOptions::fillParams(VController *controller)
{
    if (!controller) {
        ui->lEHost->clear();
        ui->lEPort->clear();
        ui->lEName->clear();
        ui->lEPassword->clear();
        return;
    }
    if (!controller->getName().contains("datapath"))
        ui->lEPort->setEnabled(true);
    auto address = controller->address();
    auto port = controller->port();
    if (!address.isNull()) {
        ui->lEHost->setText(address);
    }
    if (port > 100) {
        ui->lEPort->setText(QString::number(controller->port()));
    }
}

void ControllerOptions::onOffClicked(bool flag)
{
    if (!m_pController)
        return;
    m_pController->setAddress(QHostAddress(ui->lEHost->text()));
    m_pController->setPort(ui->lEPort->text().toUInt());
    if (flag) {
        ui->pushButtonConnect->setChecked(true);
        ui->pushButtonConnect->setText("Идет подключение...");
        ui->cBModel->setEnabled(false);
        emit conn(m_pController);
    }
    else {
        m_pController->disconn();
        ui->pushButtonConnect->setChecked(false);
        ui->pushButtonConnect->setText("Идет отключение...");
        ui->cBModel->setEnabled(true);
    }
}

void ControllerOptions::setPropEnabled(bool en)
{
    ui->lEHost->setEnabled(en);
    ui->lEPort->setEnabled(en);
    ui->lEName->setEnabled(en);
    ui->lEPassword->setEnabled(en);
    ui->cBModel->setEnabled(en);
}

void ControllerOptions::someChanged()
{
    auto name = ui->cBModel->currentText();
    VController *ctr = m_Controllers.value(name);
    if (!ctr) {
        ui->tBSave->setEnabled(false);
        return;
    }
    if (name.contains("datapath")) {
        ui->lEPort->setEnabled(false);
    }
    else {
        ui->lEPort->setEnabled(true);
    }
    QHostAddress host(ui->lEHost->text());
    bool ok = !host.isNull();
    ui->tBSave->setEnabled(ok);
    ui->pushButtonConnect->setEnabled(ok);
}

void ControllerOptions::on_tBSave_clicked()
{
    QString name = ui->cBModel->currentText();
    VController *controller = m_Controllers.value(name);
    if (!controller)
        return;
    ui->tBSave->setEnabled(false);
    QSettings settings(settingsName, QSettings::IniFormat);
    settings.beginGroup(controller->getName());
    settings.setValue("address", ui->lEHost->text());
    if (name.contains("datapath"))
        settings.setValue("port", "20200");
    else
        settings.setValue("port", ui->lEPort->text());
    settings.endGroup();
    settings.setValue("ControllerFileName", name);
}

void ControllerOptions::on_toolButtonHelp_clicked()
{
    emit sendOpenHtml("index.html?210.htm");
}

void ControllerOptions::modelIndexChanged(const QString &str)
{
    VController *controller = m_Controllers.value(str);
    if (!controller)
        return;
    fillParams(controller);
    QVariant var;
    if (m_pController) {
        m_pController->getProperty("fileName",var);
    }
    if (var.toString() != str) {
        ui->tBSave->setEnabled(true);
        ui->pushButtonConnect->setEnabled(false);
        setController(controller);
    } else {
        ui->tBSave->setEnabled(false);
        ui->pushButtonConnect->setEnabled(true);
    }
}

QString ControllerOptions::getControllersPath()
{
    auto args = QCoreApplication::arguments();
    QString controllersFilePath = FIRST_LIB_PATH
            + QFileInfo(args.at(0)).fileName()
            + CONTROLLERS_DIR_NAME;
    QDir dir(controllersFilePath);
    if(dir.exists() && dir.cd(controllersFilePath)) {
        return controllersFilePath;
    }
    return controllersFilePath = QDir::current().canonicalPath() + CONTROLLERS_DIR_NAME;
}

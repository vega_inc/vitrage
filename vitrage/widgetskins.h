#ifndef WIDGETSKINS_H
#define WIDGETSKINS_H

#include <QDialog>
#include <QSvgRenderer>
#include <QtSvg/QGraphicsSvgItem>
#include "vgraphicswidget.h"
#include <QGraphicsItem>

namespace Ui {
class WidgetSkins;
}

class WidgetSkins : public QDialog
{
    Q_OBJECT
    
public:
    explicit WidgetSkins(QWidget *parent = 0);
    explicit WidgetSkins(QString path);
    ~WidgetSkins();
    QList<QSvgRenderer*> m_renders;
    QList<QGraphicsSvgItem* > m_items;
    
private:
    Ui::WidgetSkins *ui;
    QString m_path;
    QGraphicsScene * m_scene;
private slots:
    void selectionCh(void);
signals:
    void selectedWidget(QString);
};

#endif // WIDGETSKINS_H

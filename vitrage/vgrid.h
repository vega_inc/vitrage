#ifndef VGRID_H
#define VGRID_H

#include <QPaintEvent>
#include <QWidget>

class VCanva;

class VGrid : public QWidget {
    Q_OBJECT
public:
    explicit VGrid(VCanva *c);

    void setS_Grids(int w, int h);
    void setB_Grids(int w, int h);
	void enableSmall(bool e);
	void enableBig(bool e);
    VCanva *canva();

    void paintEvent(QPaintEvent *event);

private:
	int s_grid_w;
	int s_grid_h;
	int b_grid_w;
	int b_grid_h;
    VCanva * m_pCanva;
    bool small = true;
    bool big = true;

signals:
    void updateParent();
};


#endif /* VGRID */

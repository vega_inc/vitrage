#include "canvaoptions.h"
#include "ui_canvaoptions.h"
#include <QMessageBox>

CanvaOptions::CanvaOptions(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CanvaOptions)
{
    ui->setupUi(this);
    ui->Save->setEnabled(false);
    connect(ui->HorSize, SIGNAL(valueChanged(QString)), this, SLOT(somethingChanged()));
    connect(ui->VerSize, SIGNAL(valueChanged(QString)), this, SLOT(somethingChanged()));
    connect(ui->HorStep, SIGNAL(valueChanged(QString)), this, SLOT(somethingChanged()));
    connect(ui->VerStep, SIGNAL(valueChanged(QString)), this, SLOT(somethingChanged()));
    connect(ui->HorResol, SIGNAL(currentIndexChanged(QString)), this, SLOT(somethingChanged()));
    connect(ui->VerResol, SIGNAL(currentIndexChanged(QString)), this, SLOT(somethingChanged()));
    connect(ui->HorResol, SIGNAL(editTextChanged(QString)), this, SLOT(somethingChanged()));
    connect(ui->VerResol, SIGNAL(editTextChanged(QString)), this, SLOT(somethingChanged()));
    connect(ui->Save, SIGNAL(clicked()), this, SLOT(save()));
}

CanvaOptions::~CanvaOptions()
{
    delete ui;
}

void CanvaOptions::setCubsW(int cubs)
{
    cubs_w = cubs;
}

void CanvaOptions::setCubsH(int h)
{
    cubs_h = h;
}

void CanvaOptions::setResolvW(int w)
{
    resolv_w = w;
}

void CanvaOptions::setResolvH(int h)
{
    resolv_h = h;
}

void CanvaOptions::setStepW(int w)
{
    step_w = w;
}

void CanvaOptions::setStepH(int h)
{
    step_h = h;
}

int CanvaOptions::cubsW() const
{
    return cubs_w ;
}

int CanvaOptions::cubsH() const
{
    return cubs_h;
}


int CanvaOptions::resolvW() const
{
    return resolv_w;
}

int CanvaOptions::resolvH() const
{
    return resolv_h;
}

int CanvaOptions::stepW() const
{
    return step_w;
}

int CanvaOptions::stepH() const
{
    return step_h;
}

void CanvaOptions::setOptions(int cubs_w, int cubs_h, int resol_w, int resol_h, int step_w, int step_h)
{
    setCubsW(cubs_w);
    ui->HorSize->setValue(cubs_w);
    setCubsH(cubs_h);
    ui->VerSize->setValue(cubs_h);

    setResolvW(resol_w);
    int res = ui->VerResol->findText(QString::number(resol_h));
    if (res == -1) {
        ui->VerResol->addItem(QString::number(resol_h));
        ui->VerResol->setCurrentIndex(ui->VerResol->count()-1);
    } else
        ui->VerResol->setCurrentIndex(res);

    setResolvH(resol_h);
    res = ui->HorResol->findText(QString::number(resol_w));
    if (res == -1) {
        ui->HorResol->addItem(QString::number(resol_w));
        ui->HorResol->setCurrentIndex(ui->HorResol->count()-1);
    }
    else {
        ui->HorResol->setCurrentIndex(res);
    }

    setStepW(step_w);
    ui->HorStep->setValue(step_w);

    setStepH(step_h);
    ui->VerStep->setValue(step_h);

    ui->Save->setEnabled(false);
}

void CanvaOptions::somethingChanged()
{
    ui->Save->setEnabled(true);
}

void CanvaOptions::save()
{
    int cubsW_ = ui->HorSize->value();
    int cubsH_ = ui->VerSize->value();
    int resolvH = ui->VerResol->currentText().toInt();
    int resolvW = ui->HorResol->currentText().toInt();
    int stepW_ = ui->HorStep->value();
    int stepH_= ui->VerStep->value();

    if ((!cubsW_) || (!cubsH_) || (!resolvW) || (!resolvH) || (!stepW_) || (!stepH_)) {
        QMessageBox::critical(this, "Ошибка настройки видеостены", tr("Parametrs can't be NULL") );
        ui->HorSize->setValue(cubsW());
        ui->VerSize->setValue(cubsH());
        ui->HorStep->setValue(stepH());
        ui->HorStep->setValue(stepW());
        return ;
    }
    setCubsW(cubsW_);
    setCubsH(cubsH_);
    setResolvH(resolvH);
    setResolvW(resolvW);
    setStepW(stepW_);
    setStepH(stepH_);
    emit setOptionsCanva(cubs_w, cubs_h, resolv_w, resolv_h, step_w, step_h);
    ui->Save->setEnabled(false);
}

void CanvaOptions::on_close_clicked()
{
    close();
}

void CanvaOptions::on_toolButtonHelp_clicked()
{
    emit sendOpenHtml("index.html?211.htm");
}

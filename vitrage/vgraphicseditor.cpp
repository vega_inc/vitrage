#include "vgraphicseditor.h"
#include "ui_vgraphicseditor.h"

#include "vgraphicstext.h"

VGraphicsEditor::VGraphicsEditor(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::VGraphicsEditor)
{
    ui->setupUi(this);
    createCollapsibles();
    QVBoxLayout * layout = new QVBoxLayout;
    layout->setContentsMargins(1,1,1,1);
    ui->workingSpace->setLayout(layout);

    VScheme * news = new VScheme;
    connect(news,SIGNAL(sourceDroped(QString,QPoint)),this,
                                    SLOT(sourceDropedToScheme(QString,QPoint)));
    //connect(this,SIGNAL(),this,SLOT(closeWind()));
    news->setMode(VScheme::EDITING);
    m_scheme = news;
    m_pAction = NULL;
    connect(m_scheme,SIGNAL(modefied()),this,SLOT(schemeModefied()));
    m_schemes.append(news);
    news->setAction(m_pAction);
    //ui->mdiArea->addSubWindow(news);
    ui->workingSpace->layout()->addWidget(news);
    news->showFullScreen();

    setSaved(true);
    ui->actionSave->setEnabled(false);
    m_dock = new QDockWidget;
    m_dock->setWidget(this);
    m_dock->setAllowedAreas(Qt::RightDockWidgetArea);
    m_dock->setObjectName("GraphEditor");
    m_dock->setWindowTitle(tr("Graphics scheme editor"));
    m_dock->activateWindow();
    m_dock->setWindowFlags(Qt::Window);

    m_innerDoc = new QDockWidget;
    m_innerDoc->setWidget(m_pSchemeSources);
    m_innerDoc->setAllowedAreas(Qt::AllDockWidgetAreas);
    m_innerDoc->setObjectName("GraphEditorSources");
    m_innerDoc->setWindowTitle(tr("Sources on scheme"));
    m_innerDoc->activateWindow();
    m_innerDoc->setWindowFlags(Qt::Window);

    addDockWidget(Qt::LeftDockWidgetArea,m_innerDoc);
    setObjectName("GraphEditorMainW");
    setMode(G_EDITING);
}

VGraphicsEditor::~VGraphicsEditor()
{
    delete ui;
    //delete m_dock;
}

/**
 * @brief VGraphicsEditor::openForAction
 * fill tables for action
 * @param act - [in] VAction*
 */
void VGraphicsEditor::openForAction(VAction *act)
{
    m_pAction = act;
   // m_scheme->removeItems();
    connect(m_pAction,SIGNAL(sourceRemoved(QString)),this,SLOT(sourceRemovedFromAction(QString)));
    QList<VSource*> list= act->sources();
    foreach (VSource *src,list) {
        QVariant var;
        src->getProperty("icon",var);
        QPixmap  px;
        px.loadFromData(var.toByteArray(),"PNG");
        QIcon icon(px);
        if (!m_scheme->getWidget(src->getname())) {
            addSourceToContentTable(src->getPhysName(),src->logicName(),&icon,true);
        } else
            addSourceToContentTable(src->getPhysName(),src->logicName(),&icon);
    }
    this->show();
}

void VGraphicsEditor::setPropMap(QVariantMap *map)
{
    m_result = map;
}

void VGraphicsEditor::setAction(VAction *act)
{
   m_pAction = act;
}

void VGraphicsEditor::createCollapsibles()
{
    m_pSources = new QTableWidget;
    m_pSources->hide();
    m_pSources->installEventFilter(this);
    m_pSources->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    m_pSources->setColumnCount(3);
    m_pSchemeSources = new QTableWidget;
    m_pSchemeSources->setColumnCount(4);
    m_pSchemeSources->installEventFilter(this);
    connect(m_pSchemeSources,SIGNAL(cellPressed(int,int)),this,
                                             SLOT(schemeSourcesCellClicked(int,int)));
    setOptionsForTable(m_pSchemeSources);
    QVBoxLayout * slayout = new QVBoxLayout();
    slayout->setContentsMargins(1,1,1,1);
    QVBoxLayout * sclayout = new QVBoxLayout();
    sclayout->setContentsMargins(1,1,1,1);

    setSizeForContent();
    setSavePath("");
}

void VGraphicsEditor::setOptionsForTable(QTableWidget *table)
{
    table->setSelectionMode(QAbstractItemView::SingleSelection);
    table->setSelectionBehavior(QAbstractItemView::SelectRows);
    table->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    table->setShowGrid(false);
    table->setDragEnabled(true);
    table->setMinimumHeight(30);
    table->setStyleSheet("QTableWidget {	background:rgb(225,225,225);}");
    table->horizontalHeader()->setVisible(false);
    table->verticalHeader()->setVisible(false);
}



void VGraphicsEditor::setSourceManager(LogicSourceManager *src)
{
    m_pLogicSourceManager = src;
}

void VGraphicsEditor::setSaved(bool saved)
{
    if (saved) {
        ui->actionSave->setEnabled(false);
        ui->actionSave_2->setEnabled(false);
    } else {
        ui->actionSave->setEnabled(true);
        ui->actionSave_2->setEnabled(true);
    }
    m_saved = saved;
}

void VGraphicsEditor::setMode(G_MODE mode)
{
    switch(mode) {
    case G_EDITING:
        m_innerDoc->setMaximumWidth(SHRT_MAX);
        m_innerDoc->show();
        ui->actionEditMode->setVisible(false);
        ui->actionRuntimeMode->setVisible(true);
        break;
    case G_RUNTIME:
        m_innerDoc->hide();
        m_innerDoc->setMaximumWidth(1);
        ui->actionEditMode->setVisible(true);
        ui->actionRuntimeMode->setVisible(false);
        ui->actionCreate_Scheme->setVisible(false);
        break;
    }
    m_mode = mode;
}

void VGraphicsEditor::setAction()
{
    if (!m_pAction) return;
    if (m_pAction->scheme()) {
    m_scheme  = m_pAction->scheme();
    this->ui->workingSpace->layout()->addWidget(m_scheme);
    switch(m_mode) {
        case G_EDITING:
        m_scheme->setMode(VScheme::EDITING);
        break;
        case G_RUNTIME:
        m_scheme->setMode(VScheme::RUNTIME);
        break;
    }
    }
}

bool VGraphicsEditor::isSaved()
{
    return m_saved;
}

void VGraphicsEditor::setSavePath(QString path)
{
    m_savePath = path;
    if (m_scheme)
        m_scheme->setSavedFile(path);
}

void VGraphicsEditor::clearScheme()
{
    //disconnect()]
    m_pSchemeSources->setRowCount(0);
    m_scheme->removeItems();
}

bool VGraphicsEditor::loadFromSaved()
{
    clearScheme();
    setMode(G_EDITING);
    setSaved(false);
    //m_scheme->cle
    //m_scheme->removeItems();
    m_scheme->setSavedFile(m_savePath);
    return m_scheme->loadJson(true);
}

void VGraphicsEditor::setSourceTable(QTableWidget *table)
{
    m_pSources = table;
}
void VGraphicsEditor::setLogicTable(QTableWidget *table)
{
    m_pLogicSources = table;
}

void VGraphicsEditor::renameSource(QString name, QString oldName)
{
    for(int i = 0;  i < m_pSchemeSources->rowCount();i++) {
        if (m_pSchemeSources->item(i,LG_NAME)->text() == oldName) {
            m_pSchemeSources->item(i,LG_NAME)->setText(name);
            m_scheme->renameItem(m_pSchemeSources->item(i,LG_PHNAME)->text(),name);
        }
    }
}

QDockWidget *VGraphicsEditor::dock()
{
    return m_dock;
}

VScheme *VGraphicsEditor::scheme()
{
    return m_scheme;
}

QString VGraphicsEditor::savePath()
{
    return m_savePath;
}

void VGraphicsEditor::changeCursor(QPoint point)
{
    Q_UNUSED(point);
     setCursor(Qt::OpenHandCursor);
}





void VGraphicsEditor::resizeContentTable(QEvent *ev)
{
    QResizeEvent *res = static_cast<QResizeEvent*>(ev);

    int result = m_pSchemeSources->columnWidth(0) +
    m_pSchemeSources->columnWidth(2) + m_pSchemeSources->columnWidth(3) + 4;

    if (m_pSchemeSources->verticalScrollBar()->isVisible())  {
        result += m_pSchemeSources->verticalScrollBar()->width();
    }
//	qDebug() << result;
    m_pSchemeSources->setColumnWidth(1,res->size().width()- result);
}

void VGraphicsEditor::nameChanged(int r, int c)
{
    QString newname = m_pSchemeSources->item(r,c)->text();
    QTableWidgetItem * item = m_pSchemeSources->item(r,LG_PHNAME);

    for (int i = 0 ; i < m_pSchemeSources->rowCount(); i++) {
        if (i == r)continue;
        QTableWidgetItem * itm = m_pSchemeSources->item(i,LG_NAME);
        QString nm = itm->text();
        if (nm == newname) {
            m_pSchemeSources->item(r,LG_NAME)->setText(item->text());
            return;
        }
    }

    if (!item) return;
    QString phisName = item->text();
    if (phisName != newname)
        m_scheme->renameItem(phisName,newname);
}

void VGraphicsEditor::slotLogicDel()
{
    for (int i = 0; i < m_pSchemeSources->rowCount(); i++)
        if(m_pSchemeSources->cellWidget(i, LG_DEL) == sender()) {
            QString logicName = m_pSchemeSources->item(i,LG_NAME)->text();
          //  m_pSchemeSources->removeRow(i);
            m_pSchemeSources->item(i,LG_NAME)->setBackground(Qt::red);
           m_pSchemeSources->cellWidget(i, LG_DEL)->setEnabled(false);
            m_scheme->removeItem(logicName);
            ui->actionSave->setEnabled(true);
            break;
    }
}


bool VGraphicsEditor::eventFilter(QObject *obj, QEvent *ev)
{
    if (obj == m_pSchemeSources) {
        if (ev->type()  == QEvent::Resize){
        resizeContentTable(ev);
        }
    }

    return false;
}

void VGraphicsEditor::closeEvent(QCloseEvent *event)
{
    event->ignore();
    //closeWind(event);
    emit hideMe();
}

void VGraphicsEditor::showEvent(QShowEvent *)
{
    ui->actionSave->setEnabled(false);
    setSaved(true);
    setAction();
}



void VGraphicsEditor::on_actionCreate_triggered()
{
    if (!m_scheme->isEmpty()) {
        QMessageBox msgBox;
        msgBox.setText(tr("Do you realy want to delete scheme"));
        QPushButton *yes = msgBox.addButton(tr("Yes"), QMessageBox::ActionRole);
        QPushButton *no = msgBox.addButton(tr("No"),QMessageBox::ActionRole);
        msgBox.exec();
        if (msgBox.clickedButton() == yes) {
            delete (m_scheme);
            VScheme * news = new VScheme;
            connect(news,SIGNAL(sourceDroped(QString,QPoint)),this,
                                    SLOT(sourceDropedToScheme(QString,QPoint)));
            news->setMode(VScheme::EDITING);
            m_scheme = news;
            m_schemes.append(news);
            news->setAction(m_pAction);
            ui->workingSpace->layout()->addWidget(news);
            news->showFullScreen();
            m_pSchemeSources->setRowCount(0);
            m_scheme->removeItems();
            openForAction(m_pAction);
        } else if (msgBox.clickedButton() == no) {
        }
    }
}



void VGraphicsEditor::schemeSourcesCellClicked(int row, int column)
{
    Q_UNUSED(column);
    QTableWidgetItem *item = m_pSchemeSources->item(row,LG_NAME);
    QDrag *drag = new QDrag(this);
    QMimeData *mimeData = new QMimeData;
    QString temp = item->text();
    mimeData->setText(temp);
    drag->setMimeData(mimeData);
    Qt::DropAction dropAction;
    dropAction = drag->exec();
    Q_UNUSED(dropAction);
}

//void VGraphicsEditor::scroll()
//{
//    Q_UNUSED(colun);
//    QTableWidgetItem *item = m_pSources->item(row,1);
//    QDrag *drag = new QDrag(this);
//    QMimeData *mimeData = new QMimeData;
//    QString temp = item->text();
//    mimeData->setText(temp);
//    drag->setMimeData(mimeData);
//    Qt::DropAction dropAction;
//    dropAction = drag->exec();
//}






void VGraphicsEditor::sourceDropedToScheme(QString src_name,QPoint point)
{
    int row = -1;
    if (src_name.isEmpty()) return;

    VSource *src =  m_pAction->getSource(src_name);
    if (!src) return;
    QString physName = src->getPhysName();


    for (int j = 0; j < m_pSchemeSources->rowCount();j++) {
        QString name_tmp2 = m_pSchemeSources->item(j,LG_NAME)->text();
        if (name_tmp2 == src_name)  {
            if ((m_pSchemeSources->item(j,LG_NAME)->background().color()) !=
                    Qt::red ) {
                qDebug("Not red");
                QString text  = tr("Source are in scene");
                QMessageBox::critical(NULL,NULL,text);
                return;
            }
        }
    }

    if (physName.isEmpty()) {
        physName = src_name;
    }
    for (int i = 0 ; i < m_pSources->rowCount() ; i++) {
        QString str =m_pSources->item(i,LG_NAME)->text();
       if (str == physName) {
            row = i;
       }
    }

    QIcon icon;
    QToolButton* tb ;
    bool find = false;
    for (int i  = 0; i < m_pSchemeSources->rowCount();i++) {
        if (src_name == m_pSchemeSources->item(i,LG_NAME)->text()) {
                find = true;
            tb = dynamic_cast<QToolButton*>(m_pSchemeSources->cellWidget(i,LG_ICON));
        }
    }

    if (!find)
    tb = dynamic_cast<QToolButton*>(m_pSources->cellWidget(row,PH_ICON));
    if (!tb) return;
    addSourceToContentTable(physName,src_name,NULL);

    icon = tb->icon();
    //icon.addFile(QString::fromUtf8(":/settings.png"), QSize(), QIcon::Normal, QIcon::Off);
    VGraphicsWidget *wgt;
    if (physName.isEmpty()) {wgt = new VGraphicsWidget(icon,physName,src_name);} else {
        wgt = new VGraphicsWidget(icon,src_name,physName);
    }
    if (!wgt) abort();
    wgt->setPos(point);
    QPoint p(point.x()+30,point.y());
    wgt->textItem()->setPos(p);
    m_scheme->addSourceWidget(wgt);
    setSizeForContent();
    setSaved(false);
}



/**
 * @brief VGraphicsEditor::addSourceToContentTable
 * insert field in content table
 * @param name - [in] QString  source name
 * @param logicName - [in] QString
 * @param row - [in] int number of row in source table
 */
void VGraphicsEditor::addSourceToContentTable(QString name,QString logicName,QIcon *icon_p ,bool red  )
{
    QString phName;
  //  int row = m_pSources->currentRow();
    for (int j = 0; j < m_pSources->rowCount();j++) {
        QString  name_tmp = m_pSources->item(j,PH_NAME)->text();
        if (name_tmp == name) {
     //   row = j;
        }
    }
    /* check for source in table*/
    for (int j = 0;j < m_pSchemeSources->rowCount();j++) {
        if (m_pSchemeSources->item(j,LG_NAME)->text() == logicName) {
            if (m_pSchemeSources->item(j,LG_NAME)->background().color() == Qt::red) {
                if (!red) {
                m_pSchemeSources->item(j,LG_NAME)->setBackground(
                       m_pSchemeSources->item(j,LG_PHNAME)->background());
                m_pSchemeSources->cellWidget(j,LG_DEL)->setEnabled(true);
                }
            }
           QToolButton* tb = static_cast<QToolButton *>(m_pSchemeSources->cellWidget(j, LG_ICON));
           if (tb  && icon_p)   tb->setIcon(*icon_p);
           return;
        }
    }

    QTableWidget* pT = m_pSchemeSources;
    pT->setRowCount(pT->rowCount() + 1);
    QIcon icon;

    if (icon_p) {
        icon = *icon_p;
    } else  {
    for (int i = 0; i < m_pSources->rowCount();i++) {
        QString name_p  = m_pSources->item(i,PH_NAME)->text();
        if (name == name_p) {
             QToolButton* tb = dynamic_cast<QToolButton*>(m_pSources->cellWidget(i,PH_ICON));
            icon = tb->icon();
        }
    }
    }

    for (int i = 0; i < m_pSources->rowCount();i++) {
        QString name_lg  = m_pSources->item(i,LG_NAME)->text();
        if (name == name_lg) {
            phName = m_pSources->item(i,PH_NAME)->text();
        }
    }
    QToolButton* tbtnIcon = new QToolButton();
    tbtnIcon->setIcon(icon);
    tbtnIcon->setProperty("logicName",logicName);
    pT->setCellWidget(pT->rowCount() - 1, LG_ICON, tbtnIcon);

    QTableWidgetItem *nameItem = new QTableWidgetItem(logicName);
    if (red) {
        nameItem->setBackground(Qt::red);
    }
    nameItem->setFlags(nameItem->flags() ^ Qt::ItemIsEditable);
    pT->setItem(pT->rowCount() - 1, LG_NAME, nameItem);

    QTableWidgetItem *it = new QTableWidgetItem(phName);
    it->setFlags(it->flags() ^Qt::ItemIsEditable);
    it->setIcon(icon);
    pT->setItem(pT->rowCount() - 1, LG_PHNAME,it);
    pT->resizeColumnsToContents();

    /* delete button */
    QToolButton* tbtnDel = new QToolButton();
    QIcon icon2;
    icon2.addFile(QString::fromUtf8(":/delte.png"), QSize(), QIcon::Normal, QIcon::Off);
    tbtnDel->setIcon(icon2);
    connect(tbtnDel, SIGNAL(clicked()), this, SLOT(slotLogicDel()), Qt::QueuedConnection);
    tbtnDel->setText("qwerqwer");
    pT->setCellWidget(pT->rowCount() - 1, LG_DEL, tbtnDel);
    if (red) {
        pT->cellWidget(pT->rowCount() - 1,LG_DEL)->setEnabled(false);
    }
    setSaved(false);
}

void VGraphicsEditor::on_actionSave_triggered()
{
    QString fileName;
    if (m_scheme->savedFile() == ("") ) {
        fileName = QFileDialog::getSaveFileName();
        if (fileName.isEmpty()) return;
        m_scheme->setSavedFile(fileName);
    } else fileName = m_scheme->savedFile();
    if (fileName.isNull()) {
        return ;
    }
    if (startAllWidgets()) return;
    m_pAction->setScheme(m_scheme);
    m_scheme->saveJson(fileName);
    setSaved(true);
}

void VGraphicsEditor::fillSourceProperty(uint index, VSource* source)
{
    source->setProperty("logicName", m_pSchemeSources->item(index, LG_NAME)->text());
    source->setname(m_pSchemeSources->item(index, LG_PHNAME)->text());
    QString physName = m_pSchemeSources->item(index, LG_PHNAME)->text();
    source->setProperty("physName",physName);

    m_pLogicSourceManager->fillProperty(source,physName);

    QToolButton* tb = dynamic_cast<QToolButton*>(m_pSchemeSources->cellWidget(index, LG_ICON));
    if(tb) {
    QByteArray arr;
    QBuffer buf(&arr);
    buf.open(QIODevice::ReadWrite);
    tb->icon().pixmap(32).save(&buf, "PNG");
    QVariant fdf(arr);
    source->setProperty("icon",fdf);
        //source->setProperty("icon",tb->icon());
    }
}


void VGraphicsEditor::slotSetLogicIcon()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("set icon"), "", "*.png"  );
    if (fileName == "") return;
    QIcon icon(fileName);
    QString name = sender()->property("logicName").toString();
    QToolButton * bt = dynamic_cast<QToolButton*>(sender());
    if (!bt) return;
    bt->setIcon(icon);
    //m_pSetDev->btSaveAction->setEnabled(true);
    //QString name = bt->property("logicName").toString();
    VGraphicsWidget *wgt;
    wgt = m_scheme->getWidget(name);
    if (wgt)  {
        wgt->setIcon(icon);
    }
    ui->actionSave->setEnabled(true);
    //wgt->s
}



void VGraphicsEditor::setSizeForContent()
{
    int result = m_pSchemeSources->columnWidth(0) +
    m_pSchemeSources->columnWidth(2) + m_pSchemeSources->columnWidth(3) + 4;
    if (m_pSchemeSources->verticalScrollBar()->isVisible())  {
        result += m_pSchemeSources->verticalScrollBar()->width();
    }
    //m_pSchemeSources->setColumnWidth(1,m_pSchemeSourcesM->width()- result);

}
/**
 * @brief VGraphicsEditor::removeSourcesFromAction
 *
 * @return false - sources removed
 */
bool VGraphicsEditor::removeSourcesFromAction()
{
    /* delete sources from action */
    QList<QString> lst;
    QMap <QString, VSource*> map;
    if (!m_pAction) return false;
    map = m_pAction->returnSources();
    foreach(QString name,map.keys()) {
        bool flag = false;
        for (int i = 0; i < m_pSchemeSources->rowCount(); i++) {
            QString tmp_name =m_pSchemeSources->item(i, LG_PHNAME)->text();
            if (name == tmp_name) flag = true;
        }
        if (!flag) lst.push_back(name);
   }

    if (lst.empty()) return false;
    QMessageBox msgBox;
    QString names;
    for(int j = 0;j < lst.count();j++)   {
        names += lst.at(j) + " ";
    }

    QString str = tr("Do you realy want to delete sources: ") + names +
            tr("sources will be deleted from all scenes");
    msgBox.setText(str);
    QPushButton *yes = msgBox.addButton(tr("Yes"), QMessageBox::ActionRole);
    QPushButton *no = msgBox.addButton(tr("No"),QMessageBox::ActionRole);
    msgBox.exec();
    if (msgBox.clickedButton() == yes) {
        for(int j = 0;j < lst.count();j++)   {
           m_pAction->removeSourseComple(lst.at(j));
        }
        return false;
    } else if (msgBox.clickedButton() == no) {
        return true;
    }
    return false;
}

/**
 * @brief VGraphicsEditor::startAllWidgets
 * set all widgets to src objects
 */
bool VGraphicsEditor::startAllWidgets()
{
    //bool ret = false;
   // if (removeSourcesFromAction()) {
  //      return !ret;
  //  }
    for (int i = 0; i < m_pSchemeSources->rowCount(); i++) {
        QString type;
        bool camera;
        QMap <QString, VSource*> map;
        //QString tmp_name =m_pSchemeSources->item(i, LG_PHNAME)->text();
        QString tmp_name =m_pSchemeSources->item(i, LG_NAME)->text();
        type = m_pSchemeSources->item(i,LG_PHNAME)->text();
        map = m_pAction->returnSources();
        bool flag = false;
        foreach(QString name,map.keys()) {
            if (name == tmp_name) flag = true;
        }
        if (flag) {
           VGraphicsWidget *wgt = m_scheme->getWidget(tmp_name);
           VSource *src = map.value(tmp_name);
           fillSourceProperty(i, src);
           if (wgt) {
              wgt->setSource(src);
              src->setStatus(VSource::ExecStatus::RunStatus);
              continue;
           }
        }
        if (type == QString("WebCamera")) {
            if (camera) continue;
           camera = true;
        }
//        type = m_pLogicSourceManager->getTypeForName(tmp_name);
//        VSource* source = VSrcManager::Instance().createVSource(type);

//         if(source) {
//                fillSourceProperty(i, source);
//                source->start();
//                source->setStatus(VSource::RunStatus);
//                m_pAction->addSource(m_pSchemeSources->item(i,LG_NAME)->text(),source);
//                VGraphicsWidget *wgt = m_scheme->getWidget(source->getname());
//                if  (wgt) wgt->setSource(source);
//                //wgt->updateParent();
//        }
    }
    emit sourcesUpdated();
    m_scheme->updatePicture();
    return false;
}



/**
 * @brief VGraphicsEditor::closeWind
 * slot for close signal
 */
void VGraphicsEditor::closeWind(QCloseEvent *event)
{
    // m_pAction->setScheme(m_scheme);
   //  m_scheme->setMode(VScheme::RUNTIME);
    if (!isSaved()) {
        QMessageBox msgBox;
        msgBox.setText(tr("Do you realy want to exit without saving  scheme"));
        QPushButton *yes = msgBox.addButton(tr("Yes "), QMessageBox::ActionRole);
        QPushButton *save = msgBox.addButton(tr("Save (Save and exit)"),QMessageBox::ActionRole);
        QPushButton *no = msgBox.addButton(tr("Cencel"),QMessageBox::ActionRole);
        msgBox.exec();
        if (msgBox.clickedButton() == yes) {
            /*  load last saved scheme and make no changes in action */
            if (!m_scheme->loadJson(true)) {
            on_actionSave_triggered();
            //startAllWidgets();
            setSaved(false);
            }
            event->accept();
        } else if (msgBox.clickedButton() == no) {
            event->ignore();
            /* return to editor */
        } else if (msgBox.clickedButton() ==  save) {
            on_actionSave_triggered();
            event->accept();
        }

    }
}




void VGraphicsEditor::on_actionLoad_triggered()
{
    if (!m_scheme->loadJson()) {
        setSaved(false);
        emit schemeLoaded(savePath());
        if (m_mode == G_RUNTIME) {
            setMode(G_RUNTIME);
        }

    }
}

void VGraphicsEditor::sourceRemovedFromAction(QString name)
{
    m_scheme->removeItem(name);
}

void VGraphicsEditor::schemeModefied()
{
    setSaved(false);
    emit schemeMod();
}


void VGraphicsEditor::on_actionClose_triggered()
{
    close();
}

void VGraphicsEditor::on_actionSave_2_triggered()
{
    on_actionSave_triggered();
}


void VGraphicsEditor::on_actionSave_As_triggered()
{
    QString  fileName = QFileDialog::getSaveFileName();
    if (fileName.isEmpty()) return;
        m_scheme->setSavedFile(fileName);
    if (startAllWidgets()) return;
    m_pAction->setScheme(m_scheme);
    m_scheme->saveJson(fileName);
    setSaved(true);
}

void VGraphicsEditor::on_actionEditMode_triggered()
{
    if (!m_pAction) return;
    setMode(G_EDITING);
    m_pAction->setScheme(m_scheme);
    setAction();
}

void VGraphicsEditor::on_actionRuntimeMode_triggered()
{
    setMode(G_RUNTIME);
    if (!m_pAction) return;
    m_pAction->setScheme(m_scheme);
    if (startAllWidgets()) return;
    setAction();
}

void VGraphicsEditor::on_actionExit_triggered()
{
 emit hideMe();
}



void VGraphicsEditor::on_actionCreate_Scheme_triggered()
{
    on_actionCreate_triggered();
}

void VGraphicsEditor::on_actionHelp_triggered()
{
    emit sendOpenHtml("index.html?214.htm");
}

#ifndef VGRAPHICSTEXT_H
#define VGRAPHICSTEXT_H

#include <QGraphicsTextItem>
#include "vgraphicswidget.h"
class VGraphicsWidget;
class VGraphicsText : public QGraphicsTextItem
{
    Q_OBJECT
public:
    explicit VGraphicsText(QGraphicsTextItem *parent = 0);
    void paintStatusForPicture(QPainter * painter);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                                                             QWidget *widget);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void setStatus(WGT_STATUS status);


     VGraphicsWidget::WGT_MODE mode(void);
     void setMode(VGraphicsWidget::WGT_MODE mode);


signals:

private:
    VGraphicsWidget::WGT_MODE m_mode;
    WGT_STATUS m_status;
    
public slots:
    
};

#endif // VGRAPHICSTEXT_H

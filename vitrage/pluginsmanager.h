#ifndef PLUGINSMANAGES_H
#define PLUGINSMANAGES_H

#include <QObject>
#include "vega/vega.h"
#include "extron/extron.h"

class PluginsManager
{
public:
    static PluginsManager &Instance();
    QString getPluginsPath();
    bool loadPlugins();
    bool extronFlag() const { return m_extronFlag; }
    bool mixerFlag() const { return m_mixerFlag; }
    Extron &extron() { return *m_extron; }
    Vega &mixer()    { return *m_vega; }

private:
    bool m_extronFlag = false;
    bool m_mixerFlag = false;

    Extron *m_extron = nullptr;
    Vega   *m_vega = nullptr;

    PluginsManager(){}
};

#endif // PLUGINSMANAGES_H

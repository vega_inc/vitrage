#include "vgrid.h"
#include "vcanva.h"
#include <QPaintEvent>
#include <QPainter>
//#include <QDebug>

VGrid::VGrid(VCanva *c)
    : m_pCanva(c)
{
    setAcceptDrops(true);
}

VCanva* VGrid::canva()
{
    return m_pCanva;
}

void VGrid::paintEvent(QPaintEvent *event)
{
    static bool ere = FALSE;
    QPainter p(this);
    int w = width();
    int h = height();
    int w_step = (w*1000)/(s_grid_w);
    int h_step = (h*1000)/(s_grid_h);
    int w_stepB = (w*1000)/(b_grid_w);
    int h_stepB = (h*1000)/(b_grid_h);

    QPen pen(Qt::darkGray,1);
    p.setPen(pen);
    if (small) {
        for (int i = 0 ; i < s_grid_w; i++) {
            auto x = i*w_step/1000;
            p.drawLine(x, 0, x, h);
        }
        for (int i = 0 ; i < s_grid_h; i++) {
            auto y = i*h_step/1000;
            p.drawLine(0, y, w, y);
        }
    }

    QPen pen1(Qt::darkGray,3);
    p.setPen(pen1);
    if (big) {
        for (int i = 0 ; i <= b_grid_w; i++) {
            p.drawLine(i*w_stepB/1000,0,i*w_stepB/1000,h);
            if ( i == b_grid_w)	p.drawLine(i*w_stepB/1000-2,0,i*w_stepB/1000-2,h);
            if ( !i )	p.drawLine(i*w_stepB/1000+1,0,i*w_stepB/1000+1,h);
        }
        for (int i = 0 ; i <= s_grid_h; i++) {
            p.drawLine(0,i*h_stepB/1000,w,i*h_stepB/1000);
            if ( i == b_grid_h)	p.drawLine(0,i*h_stepB/1000-2,w,i*h_stepB/1000-2);
            if (!i)	p.drawLine(0,i*h_stepB/1000+1,w,i*h_stepB/1000+1);
        }
        canva()->paintAllSources(&p);
    }
    QWidget::paintEvent(event);
    p.end();
    if (ere) {
        emit updateParent();
    }
    ere = !ere;
}

void VGrid::setS_Grids(int w, int h)
{
    this->s_grid_w = w;
    this->s_grid_h = h;
}

void VGrid::setB_Grids(int w, int h)
{
    this->b_grid_w = w;
    this->b_grid_h = h;
}

void VGrid::enableSmall(bool e)
{
    small = e;
}

void VGrid::enableBig(bool e)
{
    big = e;
}

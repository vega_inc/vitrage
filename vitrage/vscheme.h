#ifndef VSCHEME_H
#define VSCHEME_H

#include <QtGui>
#include <QWidget>
//#include "vaction.h"
#include "vgraphicswidget.h"

#include "configpath.h"

class VAction;

static const QString SCM_NAME = "SchemeName";
static const QString SCM_UNDER = "SchemeUnderlayer";
static const QString SCM_UNDER_TYPE = "SchemeUnderlayerType";
static const QString SCM_OBJECTS = "ScenemeObjects";
static const QString SCM_BGRND_SIZE = "SchemeBackgroundSize";

#define SCHEME_FILE ".sch"

class VScheme : public QGraphicsView
{
    Q_OBJECT

    static QString getImagesPath();

public:
    static constexpr qreal MIN_SCALE = 0.2;
    static constexpr qreal MAX_SCALE = 4.;
    typedef enum {EDITING,RUNTIME} SCHEME_MODE;
    VScheme();
    ~VScheme();
    void setBackgroundImage(QString path);
    void setBackgroundSvg(QString path);
    void setAction(VAction *);
    void addSourceWidget(VGraphicsWidget *editor);
    VGraphicsWidget *getWidget(QString name);
    virtual void contextMenuEvent(QContextMenuEvent *event);
    virtual void wheelEvent(QWheelEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
    void setMode(SCHEME_MODE);
    void setLightedWidgets(QList<QString> str);
    void renameItem(QString ,QString);
    void removeItem(QString str);
    bool isEmpty(void);
    SCHEME_MODE mode(void);
    void setSavedFile(QString file);
    QString savedFile(void);
    QString picturePath(void);
    void setPicturePath(QString path);
    void setSelectedWidget(QString str);
    QString selectedWidget(void);
    void setSelectedSource(QString source);
    bool saveJson(QString fileName);
    bool savePictures(const QString &dir);
    bool loadJson(bool load = false);
    QVariantList getElements();
    void removeItems();

    QSize getBackgroundSize();
public slots:
    void sourceStatusChanged(QString src_name);
    void widgetClicked(QString str);
private:

    typedef  enum {
        UNDER_NONE,
        UNDER_SVG,
        UNDER_PNG,
        UNDER_JPG
    } UNDERLAYER_TYPE;
    QString m_selectedWidget;
    QString m_picturePath;
    QString m_saveFile;
    UNDERLAYER_TYPE m_underType;
    QString m_underFile;
    SCHEME_MODE m_mode;
    void changeCursor(QPoint point);
    void dropEvent(QDropEvent *event);
    bool eventFilter(QObject *, QEvent *);
    void setAllPictures(QString path);
    void replaceAllWidgets(QVariantList &list);
    void paintSelectedWidget(QPainter *painter);
    QPixmap *m_image;
    QGraphicsScene *m_pScene;    /* scene */
    QGraphicsPixmapItem *m_back; /* background item */
    QGraphicsSvgItem *m_backSvg; /* background item */

    QGraphicsItem *m_backGrd; /* background widget */

    QSvgRenderer *m_backRenderer;/* background svg renderer */
    VAction *m_pAction;
    bool m_wgtResized;

    int m_backWidth;
    int m_backHeight;
    bool m_empty; /* empty scheme flag */

    /* Actions for context menu */
    QAction *m_pActionSetBack;
    QAction *m_pActionSetSvg;
    QAction *m_pActionSetSize;
    QMap<QString,VGraphicsWidget *> m_Items; /* scheme content */
signals:
    void sourceDroped(QString name,QPoint);
    void mouse_move_sig(QPoint);
    void modefied(void);
    void setActiveWidget(QString);
private slots:
    void setBackSlot(void);
    void setSvgSlot(void);
    void setSizeSlot(void);
    void setBackgroundSize(int w, int h);
    void widgetActivated(void);
public slots:
    void setWidgetsSizes(int w,int h);
    void resizeWgtEnabled(bool enabled);
    void updatePicture(void);
    void setWgtPicturePath(QString path);
};



#endif // VSCHEME_H

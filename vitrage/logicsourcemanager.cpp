#include "unistd.h"
#include "logicsourcemanager.h"
#include "ui_logicsourcemanager.h"
#include "support.h"
#include <QTimer>
#include <QBuffer>
#include <QDataStream>
#include <qtconcurrentrun.h>
#include <qjson/qjson_export.h>
#include <qjson/serializer.h>
#include <qjson/parser.h>

extern bool camera;

#include "vsourcemanager.h"
#include "vaction.h"

const QStringList filters = { "*.png", "*.jpg", "*.bmp", "*.avi", "*.mp4" };
const QStringList formats = { ".png", ".jpg", ".bmp", ".avi", ".mp4" };

LogicSourceManager::LogicSourceManager(VAction *action, QWidget *parent) :
    QDialog(parent, Qt::Window),
    ui(new Ui::setDev),
    m_pCurrentAction(action),
    modelMediaData(new QFileSystemModel)
{
    ui->setupUi(this);

    ui->progressBar->hide();
    ui->tblLogicSource->setSelectionMode(QAbstractItemView::ExtendedSelection);
    ui->tblPhisSource->setSelectionMode(QAbstractItemView::ExtendedSelection);
    loadPhysicalDevices();
    connect(ui->btCreateScenary, SIGNAL(clicked()), parent, SLOT(slotCreateScenary()));
    connect(ui->leActionName, SIGNAL(textEdited(QString)), this, SLOT(textCh()));
    connect(ui->tblPhisSource, SIGNAL(itemClicked(QTableWidgetItem*)), this,SLOT(slotTreeButtonClicked()));

    ui->btCreateScenary->setEnabled(false);
    ui->leActionName->setText(action->name());
    ui->btSaveAction->setEnabled(false);
    ui->btRemAll->setEnabled(false);
    ui->btRem->setEnabled(false);
    ui->pushButton_Add->setEnabled(false);

    connect(ui->tblPhisSource, SIGNAL(itemSelectionChanged()),this,SLOT(enableCopySources()));
    dummy = new QTimer(this);
    connect(dummy, SIGNAL(timeout()), this, SLOT(resizeBack()));
    auto path = getConfigFileNameWithPath();
    m_pSourceManager = new SourceManager(path, this);
    connect(m_pSourceManager,SIGNAL(sourcesChanged()),SLOT(sourcesChangedSlot()));
    connect(ui->toolButtonHelp, SIGNAL(clicked(bool)), this, SLOT(clickedHelp()));
    auto setResizeMode = [](QTableView *table, const std::vector<uint> &columns){
        table->horizontalHeader()->hide();
        table->verticalHeader()->hide();
        table->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
        for (auto col : columns) {
            table->horizontalHeader()->setResizeMode(col, QHeaderView::ResizeToContents);
        }
    };
    setResizeMode(ui->tblPhisSource, {0,2});
    setResizeMode(ui->tblLogicSource, {0,2});

    auto config_path = support::getSettingsPath();
    QSettings settings(config_path, QSettings::IniFormat);
    loadSettings(settings);
    setupMVC();

    connect(ui->pushButtonAddMedia, SIGNAL(clicked(bool)), this, SLOT(addMedia()));
    connect(ui->pushButtonAddMediaToAction, SIGNAL(clicked(bool)), this, SLOT(addMediaToAction()));
    connect(ui->pushButtonAddAllMediaToAction, SIGNAL(clicked(bool)), this, SLOT(addAllMediaToAction()));
}

void LogicSourceManager::setupMVC()
{
    modelMediaData->setNameFilters(filters);
    modelMediaData->setNameFilterDisables(false);
    modelMediaData->setRootPath(ui->label_pathMedia->text());

    ui->treeView->setModel(modelMediaData);
    ui->treeView->setRootIndex(modelMediaData->index(ui->label_pathMedia->text()));
    ui->treeView->setSelectionMode(QAbstractItemView::MultiSelection);
    ui->treeView->update();

    modelLogicSource = new ModelLogicSources(this);
    ui->tblLogicSource->setModel(modelLogicSource);
    ui->tblLogicSource->setSelectionMode(QAbstractItemView::MultiSelection);
    auto header = ui->tblLogicSource->horizontalHeader();
    header->setResizeMode(2, QHeaderView::Fixed);
    header->setDefaultSectionSize(24);
    ui->tblLogicSource->setSelectionBehavior(QAbstractItemView::SelectRows);
    //selection changes shall trigger a slot
    QItemSelectionModel *selectionModel = ui->tblLogicSource->selectionModel();
    connect(selectionModel, SIGNAL(selectionChanged (const QItemSelection &, const QItemSelection &)),
                 this, SLOT(selectionChangedSlot(const QItemSelection &, const QItemSelection &)));
}

void LogicSourceManager::loadPhysicalDevices()
{
    auto path = getConfigFileNameWithPath();
    if (path.isEmpty()) {
        QMessageBox::critical(this, tr("Error"), tr("Config file path is empty"));
        return;
    }
    QFile file(path);
    if(!file.open(QIODevice::ReadOnly)) {
        QMessageBox::critical(this,tr("Error"),tr("Can't open file"));
        return;
    }
    auto json = file.readAll();
    file.close();

    QJson::Parser parser;
    bool ok;
    m_result = parser.parse (json, &ok).toMap();
    if (!ok) {
        qDebug("VAction::loadJson - Error while parsing Action");
        return ;
    }
    QVariantList srcs_list = m_result.value(SRCMAP).toList();
    QSet<QString> names;

    ui->tblPhisSource->blockSignals(true);
    ui->tblPhisSource->clear();
    ui->tblPhisSource->setRowCount(0);
    int counter = 0;
    QString duplicateName;
    for (int i = 0; i < srcs_list.count(); i++ ) {
        QVariantMap src = srcs_list.at(i).toMap();
        auto name = src.value("name").toString();
        if (names.contains(name)) {
            counter++;
            duplicateName = name;
        }
        else {
            names << name;
            QToolButton* tbtnIcon1 = new QToolButton();
            QString type_icon = src.value("type").toString();
            setIcon(type_icon, tbtnIcon1);
            ui->tblPhisSource->setRowCount(ui->tblPhisSource->rowCount()+1);
            QTableWidgetItem *item = new QTableWidgetItem(name);
            item->setFlags(item->flags() ^Qt::ItemIsEditable);
            auto row = ui->tblPhisSource->rowCount()-1;
            ui->tblPhisSource->setItem(row,1,item);
            QToolButton* tbtnIcon = new QToolButton();
            tbtnIcon->setIcon(QIcon(":/detailed-info.png"));
            tbtnIcon->setProperty("row", row);
            tbtnIcon->setProperty("type", src.value("type").toString());
            foreach(QString name ,src.keys()) {
                tbtnIcon->setProperty(name.toAscii().data(),src.value(name));
            }

            connect(tbtnIcon, SIGNAL(clicked()), this, SLOT(getDetails()));
            ui->tblPhisSource->setCellWidget(row, static_cast<int>(PHIS_TABLE::DETAIL), tbtnIcon);
            ui->tblPhisSource->setCellWidget(row, static_cast<int>(PHIS_TABLE::TYPEICON), tbtnIcon1);
        }
    }
    delete completerPhS;
    completerPhS = new QCompleter(names.toList(), this);
    completerPhS->setCaseSensitivity(Qt::CaseInsensitive);
    ui->lineEditComplPhS->setCompleter(completerPhS);
    connect(completerPhS, SIGNAL(activated(const QString&)), this, SLOT(showPhSource(const QString&)));

    if (counter) {
        QMessageBox msgBox;
        msgBox.setText(QString("%1 одинаковых имени ИИ. Последнее имя: %2").arg(counter).arg(duplicateName));
        msgBox.exec();
    }
    ui->tblPhisSource->blockSignals(false);
    ui->btSaveAction->setEnabled(false);
    ui->pushButton_AddAll->setFocus();
}
/**
 * @brief LogicSourceManager::actionUpdated
 * slot to renew tables
 */
/*
void LogicSourceManager::actionUpdated()
{
    fillDevices(m_pCurrentAction);
}
*/
void LogicSourceManager::fillLogicView(const QList<QTableWidgetItem*> &items)
{
    QVector<VSource*> sources;
    for (auto item : items) {
        QString type;
        QVariantList srcs_list = m_result.value(SRCMAP).toList();
        for (auto srcs : srcs_list) {
            QVariantMap src = srcs.toMap();
            if (src.value("name") == item->text()) {
                type = src.value("type").toString();
                break;
            }
        }
        if (type == "WebCamera") {
            if (camera)
                continue;
            camera = true;
        }
        auto source = VSrcManager::Instance().createVSource(type);
        if(source) {
            fillSourceProperty(item->text(), source);
            sources << source;
        }
    }
    if (sources.isEmpty())
        return;

    modelLogicSource->setSources(sources);
    int r = 0;
    for (auto s : sources) {
        auto deleteButton = new QPushButton(QIcon("://delte.png"), "");
        deleteButton->setFlat(true);
        deleteButton->setObjectName(s->getname());
        ui->tblLogicSource->setIndexWidget(modelLogicSource->index(r++,2), deleteButton);
        connect(deleteButton, SIGNAL(clicked(bool)), this, SLOT(removeOneSource()));
    }
    ui->btSaveAction->setEnabled(true);
    ui->btRemAll->setEnabled(true);
}

void LogicSourceManager::slotTreeButtonClicked()
{
    ui->pushButton_Add->setEnabled(true);
}

void LogicSourceManager::slotSetLogicIcon()
{
    QString fileName;
    if (m_pCurrentAction->iconPath() == "" ) {
        fileName = QFileDialog::getOpenFileName(this, tr("set icon"), "","*.png");
        if (fileName.isEmpty()) return;
        m_pCurrentAction->setIconPath(fileName);
    } else  {
        fileName = QFileDialog::getOpenFileName(this, tr("set icon"),
                                                m_pCurrentAction->iconPath(),"*.png");
        if (fileName.isEmpty()) return;
        m_pCurrentAction->setIconPath(fileName);
    }
    QIcon icon(fileName);
    if(dynamic_cast<QToolButton*>(sender()))((QToolButton*)(sender()))->setIcon(icon);
    ui->btSaveAction->setEnabled(true);
}

void LogicSourceManager::slotLogicDel()
{
    // TODO
    /*
    for (int i = 0; i < ui->tblLogicSource->rowCount(); i++) {
        if(ui->tblLogicSource->cellWidget(i, LGDEL) == sender()) {
            ui->tblLogicSource->removeRow(i);
            ui->btSaveAction->setEnabled(true);
            break;
        }
    }
    resize(QSize(width(),height()+1));
    dummy->setSingleShot(true);
    dummy->start(1);*/
}

void LogicSourceManager::slotSetDeviceToAction()
{
    if(ui->leActionName->text().isEmpty()) {
        QMessageBox::information(this, tr("information"), tr("set name of the action!"));
        return;
    }
    removeSources();
    ui->btSaveAction->setEnabled(false);
    QString name = ui->leActionName->text();
    if (name.size() > 50) name.resize(50);
    m_pCurrentAction->setName(name);
    auto sources = modelLogicSource->sources();
    for (auto s : sources)
        m_pCurrentAction->addSource(s->getname(), s);
    ui->btCreateScenary->setEnabled(true);
    emit setDeviceToAction(true);
}

void LogicSourceManager::fillSourceProperty(const QString &name, VSource* source)
{
    if (!source)
        return;

    source->setProperty("logicName", name);
    source->setProperty("physName", name);
    source->setname(name);
    source->setPhysName(name);
    fillProperty(source, name);
}

void LogicSourceManager::on_btNewAction_clicked()
{
    if (!m_pCurrentAction) return;
    m_pCurrentAction->sendClearCanvas();
    m_pCurrentAction->clearScenes();
    m_pCurrentAction->clearSources();
    m_pCurrentAction->setLoadPath("");
    camera = false;

    QMap <QString, VSource*> map;
    map = m_pCurrentAction->returnSources();

    ui->leActionName->setText(tr("New Action"));
    m_pCurrentAction->setName(tr("New Action"));
    // TODO
    //    ui->tblLogicSource->setRowCount(0);
}

void LogicSourceManager::showEditSource()
{
    m_pSourceManager->show();
}

void LogicSourceManager::loadSettings(const QSettings &settings)
{
    QString mediaPath = settings.value("mediaPath").toString();
    ui->label_pathMedia->setText(mediaPath);
}

void LogicSourceManager::setActionName(const QString &name) { ui->leActionName->setText(name); }

void LogicSourceManager::textCh()
{
    ui->btSaveAction->setEnabled(true);
}

void LogicSourceManager::getDetails()
{
    QString message;
    int row = dynamic_cast<QToolButton*>(sender())->property("row").toInt();
    if (row == -1) return;
    QTableWidgetItem *item = ui->tblPhisSource->item(row, static_cast<int>(PHIS_TABLE::NAME));
    if (!item) return;
    QVariantList srcs_list = m_result.value(SRCMAP).toList();

    for (int i = 0;i < srcs_list.count();i++)
    {
        if (srcs_list.at(i).toMap().value("name") == item->text())
        {
            foreach (QString name,srcs_list.at(i).toMap().keys())
            {
                message +=name + QString(" :") +
                        srcs_list.at(i).toMap().value(name).toString()
                        + QString("\r\n");
            }
            QMessageBox::critical(this,NULL,message);
        }
    }
}

void LogicSourceManager::on_pushButton_AddAll_clicked()
{
    QVariantList srcs_list = m_result.value(SRCMAP).toList();
    QSet<QString> names;
    for (int i = 0; i < ui->tblPhisSource->rowCount(); i++ ) {
        QVariantMap src = srcs_list.at(i).toMap();
        auto name = src.value("name").toString();
        names << name;
    }
    delete completerLogS;
    completerLogS = new QCompleter(names.toList(), this);
    completerLogS->setCaseSensitivity(Qt::CaseInsensitive);
    ui->lineEditComplS->setCompleter(completerLogS);
    connect(completerLogS, SIGNAL(activated(const QString&)), this, SLOT(showSource(const QString&)));

    int rowCount = ui->tblPhisSource->rowCount();
    QList<QTableWidgetItem*> items;
    for (int r = 0; r < rowCount; r++) {
        items << ui->tblPhisSource->item(r,1);
    }
    fillLogicView(items);
}

void LogicSourceManager::on_pushButton_Add_clicked()
{
    QList<QTableWidgetItem*> items = ui->tblPhisSource->selectedItems();
    QList<QString> names;
    for (auto item : items) {
        names << item->text();
    }
    delete completerLogS;
    completerLogS = new QCompleter(names, this);
    completerLogS->setCaseSensitivity(Qt::CaseInsensitive);
    ui->lineEditComplS->setCompleter(completerLogS);
    connect(completerLogS, SIGNAL(activated(const QString&)), this, SLOT(showSource(const QString&)));

    fillLogicView(items);
    ui->progressBar->hide();
}

void LogicSourceManager::setIcon(const QString &type, QToolButton * tb)
{
    QIcon icon;

    if(VSrcManager::Instance().getSourceCreatorMap().contains(type))
    {
        QList<VSourceCreator::Param> params = VSrcManager::Instance().getSourceCreatorMap().value(type)->getParamsList();
        foreach (VSourceCreator::Param param, params)
        {
            QString str = param.desc;
            if (param.type == VSourceCreator::TYPE::ICON) {
                icon = QIcon(str);
                break;
            }
        }
        tb->setIcon(icon);
    }
}
/// set value of progress bar sources
void LogicSourceManager::setValueProgressBar(int value)
{
    ui->progressBar->setValue(value);
    if (ui->progressBar->value() >= ui->progressBar->maximum()) {
        ui->progressBar->hide();
    }
}
/// increment value of progress bar sources
void LogicSourceManager::incrProgressBar()
{
    ui->progressBar->setValue(ui->progressBar->value()+1);
    if (ui->progressBar->value() == ui->progressBar->maximum()) {
        ui->progressBar->hide();
    }
}
/// init of progress bar (adding/connecting sources)
void LogicSourceManager::initProgressBar(int size, const QString& str)
{
    if (!size) return;
    ui->progressBar->setRange(0, size);
    ui->progressBar->setValue(0);
    ui->progressBar->setFormat(str + ", %v из " +
                               QString::number(size) + "...");
    ui->progressBar->show();
}

void LogicSourceManager::sourcesChangedSlot()
{
    loadPhysicalDevices();
    // TODO
    /*
    for (int row = 0; row < ui->tblLogicSource->rowCount(); ++row) {
        bool result = false;
        for (int phisRow = 0; phisRow < ui->tblPhisSource->rowCount(); ++phisRow) {
            if (ui->tblPhisSource->item(phisRow,PDNAME)->text() == ui->tblLogicSource->item(row,LGPHNAME)->text()) {
                result = true;
                break;
            }
        }
        if(!result) {
            ui->tblLogicSource->removeRow(row);
            ui->btSaveAction->setEnabled(true);
        }
    }
    */
}

void LogicSourceManager::enableCopySources()
{
    QList<QTableWidgetItem*> lst = ui->tblPhisSource->selectedItems();
    ui->pushButton_Add->setEnabled(!lst.isEmpty());
}
/// highlight row in table physical sources
void LogicSourceManager::showPhSource(const QString &text)
{
    QTableWidgetItem *item;
    for (int r = 0; r < ui->tblPhisSource->rowCount(); ++r) {
        if (ui->tblPhisSource->item(r, static_cast<int>(PHIS_TABLE::NAME))->text() == text) {
            item = ui->tblPhisSource->item(r, static_cast<int>(PHIS_TABLE::NAME));
            break;
        }
    }
    if (item)
        ui->tblPhisSource->setCurrentItem(item);
}

void LogicSourceManager::showSource(const QString &)
{
    // TODO
//    QTableWidgetItem *item;
//    for (int r = 0; r < ui->tblLogicSource->rowCount(); ++r) {
//        if (ui->tblLogicSource->item(r, LGPHNAME)->text() == text) {
//            item = ui->tblLogicSource->item(r, LGPHNAME);
//            break;
//        }
//    }
//    if (item)
//        ui->tblLogicSource->setCurrentItem(item);
}

void LogicSourceManager::resizeBack()
{
    QWidget *par = ui->progressBar->parentWidget();
    if (par) {
        QSize sz = par->size();
        QSize szn(width(),sz.height()-1);
        par->resize(szn);
    }
}

void LogicSourceManager::clickedHelp()
{
    emit sendOpenHtml("index.html?331.htm");
}
/// copy files from choosen dir to the mediapath
void LogicSourceManager::addMedia()
{
    auto fileNames = QFileDialog::getOpenFileNames(this,
                                                   "Выберите файл или папку для добавления на медиасервер",
                                                   ui->label_pathMedia->text(),
                                                   tr("Все (*.*);;Изображения (*.png *.jpg *.bmp);;Видео файлы (*.avi)"));
    connect(&watcher, SIGNAL(finished()), this, SLOT(copyFinished()));

    ui->stackedWidgetTreeView->setCurrentIndex(1);

    struct Copied
    {
        Copied(QString path)
            : m_path(path) { }

        typedef bool result_type;

        bool operator()(const QFileInfo &finfo)
        {
            auto fname = finfo.absoluteFilePath();
            auto newName = m_path + "/" + finfo.fileName();
            bool ok = QFile::copy(fname, newName);
            return ok;
        }

        QString m_path;
    };
    QFileInfoList list;
    for (auto f : fileNames)
        list << QFileInfo(f);
    auto path = ui->label_pathMedia->text();
    watcher.setFuture(QtConcurrent::mapped(list, Copied(path)));
}
/// create sources from mediapath
void LogicSourceManager::addMediaToAction()
{
    QModelIndexList indexes = ui->treeView->selectionModel()->selectedRows();
    QVector<VSource*> sources = modelLogicSource->sources();
    for (const QModelIndex &index : indexes) {
        /*int row = 0;
        if (modelMediaData->isDir(index)) {
            auto child = index.child(row, 0);
            while (child.isValid())
                if (child.isValid()) {
                    auto fname = modelMediaData->fileName(child);
                    if (fname.contains(format)) {
                        //auto fpath = modelMediaData->filePath(child);
                        auto source = VSrcManager::Instance().createVSource("FileSource");
                        if(source) {
                            fillSourceProperty(fname, source);
                            sources << source;
                        }
                    }
                    child = index.child(++row, 0);
                }
        }
        else */
        {
            auto fname = modelMediaData->fileName(index);
            auto fpath = modelMediaData->filePath(index);
            for (auto format :formats) {
                if (fname.contains(format)) {
                    auto source = VSrcManager::Instance().createVSource("FileSource");
                    if (source) {
                        source->setProperty("muraHost", fpath);
                        source->setProperty("linuxhost", fpath);
                        fillSourceProperty(fname, source);
                        sources << source;
                    }
                    break;
                }
            }
        }
    }
    modelLogicSource->setSources(sources);
    int r = 0;
    for (auto s : sources) {
        auto deleteButton = new QPushButton(QIcon("://delte.png"), "");
        deleteButton->setFlat(true);
        deleteButton->setObjectName(s->getname());
        ui->tblLogicSource->setIndexWidget(modelLogicSource->index(r++,2), deleteButton);
        connect(deleteButton, SIGNAL(clicked(bool)), this, SLOT(removeOneSource()));
    }
    ui->btSaveAction->setEnabled(true);
    ui->btRemAll->setEnabled(true);
}

void LogicSourceManager::addAllMediaToAction()
{
    QDir directory(modelMediaData->rootPath());
    directory.setFilter(QDir::Files);
    directory.setNameFilters(filters);
    if (!directory.exists()) {
        QMessageBox::information(this, "Ошибка медиасервера",
                                 QString("Директория медиасервера не существует."
                                         "Проверьте  директорию: %1").arg(directory.path()));
        return;
    }
    QVector<VSource*> sources;
    QDirIterator it(directory);
    while (it.hasNext()) {
        it.next();
        auto fname = it.fileName();
        for (auto format :formats) {
            if (fname.contains(format)) {
                auto source = VSrcManager::Instance().createVSource("FileSource");
                if (source) {
                    fillSourceProperty(fname, source);
                    sources << source;
                    break;
                }
            }
        }
    }

    modelLogicSource->setSources(sources);
    int r = 0;
    for (auto s : sources) {
        auto deleteButton = new QPushButton(QIcon("://delte.png"), "");
        deleteButton->setFlat(true);
        deleteButton->setObjectName(s->getname());
        ui->tblLogicSource->setIndexWidget(modelLogicSource->index(r++,2), deleteButton);
        connect(deleteButton, SIGNAL(clicked(bool)), this, SLOT(removeOneSource()));
    }
    ui->btSaveAction->setEnabled(true);
    ui->btRemAll->setEnabled(true);
}

void LogicSourceManager::copyFinished()
{
    ui->stackedWidgetTreeView->setCurrentIndex(0);
}

void LogicSourceManager::removeOneSource()
{
    QString name = sender()->objectName();
    auto sources = modelLogicSource->sources();
    for (int i = 0; i < sources.size(); ++i) {
        if (sources.at(i)->getname() == name) {
            sources.remove(i);
            modelLogicSource->removeSource(i);
            return;
        }
    }
}

void LogicSourceManager::selectionChangedSlot(const QItemSelection &, const QItemSelection &)
{
    QItemSelectionModel *selectionModel = ui->tblLogicSource->selectionModel();
    auto indexes = selectionModel->selectedIndexes();
    ui->btRem->setEnabled(!indexes.isEmpty());
}
/*
void LogicSourceManager::fillDevices(VAction *action)
{
    // TODO
    m_update  = true;
    auto map = action->returnSources();
    ui->tblLogicSource->setRowCount(0);
    foreach(QString name, map.keys()) {
        VSource * src = map.value(name);
        ui->tblLogicSource->setRowCount(ui->tblLogicSource->rowCount() + 1);

        QToolButton* tbtnIcon = new QToolButton();
        QVariant var;
        src->getProperty("icon",var);
        QPixmap  px;
        px.loadFromData(var.toByteArray(),"PNG");
        QIcon icon(px);
        tbtnIcon->setIcon(icon);
        connect(tbtnIcon, SIGNAL(clicked()), this, SLOT(slotSetLogicIcon()));
        ui->tblLogicSource->setCellWidget(ui->tblLogicSource->rowCount() - 1, LGICON, tbtnIcon);
        src->getProperty("logicName",var);
        ui->tblLogicSource->setItem(ui->tblLogicSource->rowCount() - 1, LGNAME,
                                    new QTableWidgetItem(var.toString()));
        src->getProperty("physName",var);
        QTableWidgetItem *it = new QTableWidgetItem(var.toString());
        it->setFlags(it->flags() ^Qt::ItemIsEditable);
        QToolButton tmp_tb;
        setIcon(src->gettype(),&tmp_tb);
        QIcon ico = tmp_tb.icon();
        it->setIcon(ico);
        ui->tblLogicSource->setItem(ui->tblLogicSource->rowCount() - 1, LGPHNAME, it);

        QToolButton* tbtnDel = new QToolButton();
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/delte.png"), QSize(), QIcon::Normal, QIcon::Off);
        tbtnDel->setIcon(icon2);
        connect(tbtnDel, SIGNAL(clicked()), this, SLOT(slotLogicDel()), Qt::QueuedConnection);
        ui->tblLogicSource->setCellWidget(ui->tblLogicSource->rowCount() - 1, LGDEL, tbtnDel);
    }
    ui->tblLogicSource->resizeColumnsToContents();
    m_update  = false;
}*/

void LogicSourceManager::on_btUseGraphics_clicked()
{
    close();
    emit openGEditorS();
}
/*
void LogicSourceManager::addSource(const QString &filename)
{
    // TODO
   for (int j = 0; j < ui->tblLogicSource->rowCount();j++) {
        QString fname = ui->tblLogicSource->item(j,LGPHNAME)->text();
        if (fname == filename) {
            QMessageBox box;
            box.setText(QString("Одинаковые имена источников, имя: %1, строка: %2. Исправте.").arg(fname).arg(QString::number(j)));
            return;
        }
    }
    QFileInfo file(filename);
    ui->btSaveAction->setEnabled(true);
    ui->tblLogicSource->setRowCount(ui->tblLogicSource->rowCount() + 1);
    auto row = ui->tblLogicSource->rowCount() - 1;

    QToolButton *tbtnIcon = new QToolButton();
    tbtnIcon->setIcon(QIcon("://vega_logo.png"));
    connect(tbtnIcon, SIGNAL(clicked()), this, SLOT(slotSetLogicIcon()));

    QToolButton* tbtnDel = new QToolButton();
    tbtnDel->setIcon(QIcon("://delte.png"));
    connect(tbtnDel, SIGNAL(clicked()), this, SLOT(slotLogicDel()), Qt::QueuedConnection);

    QTableWidgetItem *it = new QTableWidgetItem(file.fileName());
    it->setIcon(QIcon("://vega_logo.png"));

    ui->tblLogicSource->setCellWidget(row, LGICON, tbtnIcon);
    ui->tblLogicSource->setCellWidget(row, LGDEL, tbtnDel);
    ui->tblLogicSource->setItem(row, LGNAME, new QTableWidgetItem(file.fileName()));
    ui->tblLogicSource->setItem(row, LGPHNAME, it);

    ui->btRemAll->setEnabled(true);

    auto value = ui->tblLogicSource->rowCount();
    setValueProgressBar(value);
}
*/

void LogicSourceManager::fillProperty(VSource *source, QString physName)
{
    QVariantList srcs_list = m_result.value(SRCMAP).toList();
    for (int i = 0; i < srcs_list.count(); i++ ) {
        QVariantMap src = srcs_list.at(i).toMap();
        if (src.value("name") == physName) {
            foreach(QString name,src.keys()) {
                source->setProperty(name, src.value(name));
            }
        }
    }
}

void LogicSourceManager::removeSources()
{
    QList<VSource *> srcs = m_pCurrentAction->sources();
    QList<VSource *> srcs_new = srcs;
    // TODO
    /*
    for(int j = 0;j < srcs.count();j++)
    {

        VSource *src = srcs.at(j);
        for (int i = 0; i < ui->tblLogicSource->rowCount(); i++) //перебор всех логических источников
        {
            QVariant var;
            src->getProperty("physName",var);
            if (var.toString() == ui->tblLogicSource->item(i,LGPHNAME)->text())
            {
                srcs_new.removeOne(src);
                src->getProperty("logicName",var);
                if (var.toString() != ui->tblLogicSource->item(i,LGNAME)->text())
                {
                    m_pCurrentAction->renameSource(ui->tblLogicSource->item(i,LGNAME)->text(),src);
                    QVariant lname;
                    src->getProperty("logicName",lname);
                    emit renameSourceSig(ui->tblLogicSource->
                                         item(i,LGNAME)->text(),lname.toString());

                }
            }
        }
    }*/
    if (srcs_new.isEmpty())
        return;
    QMessageBox msgBox;
    QString names;
    for(int j = 0;j < srcs_new.count();j++)   {
        names += srcs_new.at(j)->getname() + " ";
    }

    QString str = tr("Do you realy want to delete sources: ") + names +
            tr("sources will be deleted from all scenes");
    msgBox.setText(str);
    QPushButton *yes = msgBox.addButton(tr("Yes"), QMessageBox::ActionRole);
    QPushButton *no = msgBox.addButton(tr("No"),QMessageBox::ActionRole);
    msgBox.exec();
    if (msgBox.clickedButton() == yes) {
        for(int j = 0;j < srcs_new.count();j++)   {
            m_pCurrentAction->removeSourseComple(srcs_new.at(j)->getname());
        }
    } else if (msgBox.clickedButton() == no) {
    }

}

QString LogicSourceManager::getTypeForName(QString name)
{
    QString type;
    QVariantList srcs_list = m_result.value(SRCMAP).toList();
    for (int j = 0; j < srcs_list.count(); j++ ) {
        QVariantMap src = srcs_list.at(j).toMap();
        if (src.value("name") == name) {
            type = src.value("type").toString();
        }
    }
    return type;
}

QTableWidget *LogicSourceManager::getTableWidget()
{
    return ui->tblPhisSource;
}

QTableWidget *LogicSourceManager::getLogicTableWidget()
{
    // TODO
    return nullptr;//ui->tblLogicSource;
}

QMap<QString, VSource *> &LogicSourceManager::returnCurrentActionSources()
{
    return m_pCurrentAction->returnSources();
}
    /*
void LogicSourceManager::addDeviceForRow(int row)
{
    // TODO

    QString name_tmp = ui->tblPhisSource->item(row,PDNAME)->text();

    for (int j = 0; j < ui->tblLogicSource->rowCount();j++) {
        QString name_tmp2 = ui->tblLogicSource->item(j,LGPHNAME)->text();
        if (name_tmp2 == name_tmp) {
            return;
        }
    }
    ui->btSaveAction->setEnabled(true);
    ui->tblLogicSource->setRowCount(ui->tblLogicSource->rowCount() + 1);

    QToolButton *tb = dynamic_cast<QToolButton*>(ui->tblPhisSource->cellWidget(row,TYPEICON));
    QIcon icon = tb->icon();
    if (icon.isNull()) {
        icon.addFile(QString::fromUtf8(":/settings.png"), QSize(), QIcon::Normal, QIcon::Off);
    }
    QToolButton *tbtnIcon = new QToolButton();
    tbtnIcon->setIcon(icon);
    connect(tbtnIcon, SIGNAL(clicked()), this, SLOT(slotSetLogicIcon()));
    ui->tblLogicSource->setCellWidget(ui->tblLogicSource->rowCount() - 1, LGICON, tbtnIcon);

    QToolButton* tbtnDel = new QToolButton();
    QIcon icon2;
    icon2.addFile(QString::fromUtf8(":/delte.png"), QSize(), QIcon::Normal, QIcon::Off);
    tbtnDel->setIcon(icon2);
    connect(tbtnDel, SIGNAL(clicked()), this, SLOT(slotLogicDel()), Qt::QueuedConnection);
    ui->tblLogicSource->setCellWidget(ui->tblLogicSource->rowCount() - 1, LGDEL, tbtnDel);

    ui->tblLogicSource->setItem(ui->tblLogicSource->rowCount() - 1, LGNAME,
                                new QTableWidgetItem(ui->tblPhisSource->item(row,PDNAME)->text()));
    QTableWidgetItem *it = new QTableWidgetItem(ui->tblPhisSource->item(row,PDNAME)->text());
    it->setFlags(it->flags() ^Qt::ItemIsEditable);
    QIcon ico = tb->icon();
    it->setIcon(ico);
    ui->tblLogicSource->setItem(ui->tblLogicSource->rowCount() - 1, LGPHNAME,it);
    ui->btRemAll->setEnabled(true);
    auto value = ui->tblLogicSource->rowCount();
    setValueProgressBar(value);
}
*/
void LogicSourceManager::showEvent(QShowEvent *)
{
    ui->pushButton_AddAll->clearFocus();
    //fillDevices(m_pCurrentAction);
    ui->btSaveAction->setEnabled(false);
}

void LogicSourceManager::closeEvent(QCloseEvent *event)
{
    if (ui->btSaveAction->isEnabled()) {
        /*
        QMessageBox msgBox;
        QString str = tr("Do you realy want close manager without saving ");
        msgBox.setText(str);
        QPushButton *yes = msgBox.addButton(tr("Yes"), QMessageBox::ActionRole);
        QPushButton *no = msgBox.addButton(tr("Save"),QMessageBox::ActionRole);
        QPushButton *cancel = msgBox.addButton(tr("Cancel"),QMessageBox::ActionRole);
        msgBox.exec();
        if (msgBox.clickedButton() == yes) {
            event->accept();
            return;
        } else if (msgBox.clickedButton() == no) {
            slotSetDeviceToAction();
            event->accept();
            return;
        } else if (msgBox.clickedButton() == cancel) {
            event->ignore();
            return;
        }
        */
        QMessageBox box;
        box.setText("Создание мероприятия.");
        box.setInformativeText("Вы хотите применить изменения?");
        box.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
        box.setDefaultButton(QMessageBox::No);
        int ret = box.exec();
        switch (ret) {
        case QMessageBox::Yes:
            slotSetDeviceToAction();
            event->accept();
            break;
        case QMessageBox::No:
            event->accept();
            break;
        case QMessageBox::Cancel:
            event->ignore();
            break;
        default:
            // should never be reached
            break;
        }
    }
}

void LogicSourceManager::on_btRem_clicked()
{
    /*
    QList<QTableWidgetItem*> list = ui->tblLogicSource->selectedItems();
    QMap<int,QTableWidgetItem*> forrem;
    for (QTableWidgetItem* item : list) {
        forrem.insert(item->row(),item);
    }
    for (auto r : forrem.values()) {
        ui->tblLogicSource->removeRow(r->row());
    }
    ui->btRem->setEnabled(false);
    ui->btSaveAction->setEnabled(true);
    resize(QSize(width(),height()+1));
    dummy->setSingleShot(true);
    dummy->start(1);*/

    auto selected = ui->tblLogicSource->selectionModel()->selectedIndexes();
    QSet<int> rows;
    for (auto s : selected) {
        rows << s.row();
    }
    QList<int> list = rows.toList();
    qSort(list);
    for (int i = list.size()-1; i>=0; i--) {
        modelLogicSource->removeSource(list.at(i));
    }
    ui->btRem->setEnabled(false);
    ui->btSaveAction->setEnabled(true);
}

void LogicSourceManager::on_btRemAll_clicked()
{
    modelLogicSource->setSources({});
    ui->tblLogicSource->update();
    ui->btRemAll->setEnabled(false);
}

void LogicSourceManager::on_btSourcesManager_clicked()
{
    auto result = m_result.value(SRCMAP, QVariant());
    auto list = result.toList();
    m_pSourceManager->open(list);
}

QString LogicSourceManager::getConfigFileNameWithPath()
{
    const QString appName = QFileInfo(QCoreApplication::arguments().at(0)).fileName();
    const QString configFilePath = QDir::home().canonicalPath() + "/." + appName;
    const QString config = configFilePath + "/sources";
    const QString configDefault = FIRST_SHARE_PATH + appName + CONFIG_DIR_NAME + "/sources";

    if (!QDir(configFilePath).exists())
        if (QDir().mkpath(configFilePath))
            return "";
    if (QDir(configFilePath).exists()) {
        if(QFile(config).exists())
            return config;
        else if (!QFile::copy(configDefault, config)) {
            /*
            QString message = tr("Can't copy config file in user home directory") + ", " + configDefault;
            QMessageBox::critical(this, tr("Error"), message);
            */
            QFile file(config);
            if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
                return "";
            file.close();
            return config;
        }
    }
    else {
        QString path = QDir::home().canonicalPath() + "." + appName;
        if (!QDir().mkpath(path)) {
            QString message = tr("Can't make folder for config file in user home directory") + ", path = " + path;
            QMessageBox::critical(this,tr("Error"), message);
            return "";
        }
        else {
            if (!QFile::copy(configDefault, config)) {
                QFile file(config);
                if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
                    return "";
                file.close();
                return config;
            }
            return config;
        }
        return "";
    }
    return "";
}

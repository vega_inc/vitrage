#include "fillaction.h"
#include "ui_fillaction.h"
#include <QMessageBox>
#include <QPalette>
#include <QFrame>

FillAction::FillAction(VAction *action, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FillAction)
{
    ui->setupUi(this);
    m_pActiveAction = action;
    auto setResizeMode = [](QTableWidget *table, const std::vector<uint> &columns){
        table->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
        for (auto col : columns) {
            table->horizontalHeader()->setResizeMode(col, QHeaderView::ResizeToContents);
        }
    };
    setResizeMode(ui->actions, {0});

    connect(ui->actions,SIGNAL(cellClicked(int,int)),this,SLOT(cellClicked(int)));
    connect(ui->close, SIGNAL(clicked(bool)), SLOT(close()));
    connect(ui->NewScene,SIGNAL(clicked()),this,SLOT(addNewScene()));
}

FillAction::~FillAction()
{
    delete ui;
}

void FillAction::addNewScene()
{
    m_change = true;
    ui->actions->setRowCount(ui->actions->rowCount()+1);

    int def_duration;
    int rowNumber = ui->actions->rowCount();
    if (rowNumber-1 == 0) {
        def_duration = DEFAULT_DURATION;
    } else {
        QTimeEdit *te = returnTime(ui->actions->rowCount()-2);
        QTime time = te->time();
        time = time.addSecs(te->time().second());
        time = time.addSecs(te->time().minute()*60);
        time = time.addSecs(te->time().hour()*60*60);
        def_duration = te->time().secsTo(time);
    }
    QTimeEdit *edit;
    QFrame *frame = createTime(ui->actions, def_duration, &edit);
    frame->setProperty("sceneId",getFreeId());
    if (!edit)
        return;
    ui->actions->setCellWidget(ui->actions->rowCount()-1,1,frame);
    auto sceneName = getDefaulSceneName();
    QTableWidgetItem *name_new  = new QTableWidgetItem(sceneName);
    ui->actions->setItem(ui->actions->rowCount()-1,0,name_new);
}

void FillAction::saveScenes()
{
    for (auto scene : deletedScenes)
        m_pActiveAction->deleteScene(scene);
    deletedScenes.clear();

    uint32_t next_begin = 0;
    auto rowCount = ui->actions->rowCount();
    for (int i = 0; i < rowCount; i++) {
        QString name = ui->actions->item(i,0)->text();
        if (name.isEmpty()) {
            QMessageBox::critical(this, "Предупреждение", "Пустое имя сцены");
            continue;
        }
        QTimeEdit *sb = returnTime(i);
        auto duration = std::abs(sb->time().secsTo(QTime()));
        if (!duration) {
            QMessageBox::critical(this, "Предупреждение", "Пустая продолжительность сцены");
            continue;
        }
        QFrame *fr = dynamic_cast<QFrame *>(ui->actions->cellWidget(i,1));
        if (!fr) return;
        uint32_t id = fr->property("sceneId").toUInt();
        /* check for equal names */
        for (int j = 0; j < ui->actions->rowCount(); j++ ) {
            if (i == j) continue;
            if (ui->actions->item(j,0)->text() == name) {
                QString message = QString("Сцена %1 уже существует").arg(name);
                QMessageBox::critical(this, "Предупреждение", message);
                return;
            }
        }
        m_pActiveAction->createScene(i, id, next_begin, duration, name, 0);
        next_begin += duration;
    }
}

/**
* @brief starting initialization
*
* @return false - successed
*/
void FillAction::exec()
{
    m_pActiveAction->fillTable(ui->actions,(createTime),this);
    m_count_scenes = ui->actions->rowCount();
    ui->CopyScene->setEnabled(false);
    ui->RemoveScene->setEnabled(false);
    ui->up->setEnabled(false);
    ui->down->setEnabled(false);
    QDialog::exec();
}

void FillAction::on_RemoveScene_clicked()
{
    int d = ui->actions->currentRow();
    if (d == -1)
        return;
    QString str = "Вы действительно хотите удалить сцену " + ui->actions->item(d,0)->text();
    QMessageBox msgBox;
    msgBox.setText(str);
    QPushButton *yes = msgBox.addButton(tr("Yes "), QMessageBox::ActionRole);
    QPushButton *no = msgBox.addButton(tr("No"),QMessageBox::ActionRole);
    msgBox.exec();
    if (msgBox.clickedButton() == yes) {
        auto row = ui->actions->currentRow();
        deletedScenes << ui->actions->item(row,0)->text();
        ui->actions->removeRow(row);
    } else if (msgBox.clickedButton() == no) {
        return;
    }
}

/**
* @brief slot for on cell clicked signal
*
* @param r - [in] int row
* @param c - [in] int column count
*/
void FillAction::cellClicked(int r)
{
    m_change = true;
    bool up = false;
    bool down = false;
    ui->CopyScene->setEnabled(true);
    ui->RemoveScene->setEnabled(true);

    if (r) up = true;
    if (r != (ui->actions->rowCount()-1)) down = true;

    ui->up->setEnabled(up);
    ui->down->setEnabled(down);
}

void FillAction::somethingChanged()
{
    m_change = true;
}

/**
* @brief copy selected scene
*/
void FillAction::on_CopyScene_clicked()
{
    QTableWidgetItem *name  = ui->actions->item(ui->actions->currentRow(),0);
    if (!name ) return;
    ui->actions->setRowCount(ui->actions->rowCount()+1);
    QTableWidgetItem *name_new  = new QTableWidgetItem(*name);
    name_new->setText(name->text()+ QString("_"));
    ui->actions->setItem(ui->actions->rowCount()-1,0,name_new);

    QTimeEdit *te = returnTime(ui->actions->currentRow());
    QTime time = te->time();
    time = time.addSecs(te->time().second());
    time = time.addSecs(te->time().minute()*60);
    time = time.addSecs(te->time().hour()*60*60);
    int duration = te->time().secsTo(time);
    QFrame *te1 = createTime(ui->actions,duration,&te);
    ui->actions->setCurrentCell(ui->actions->currentRow()+1,
                                ui->actions->currentColumn());
    te1->setProperty("sceneId",getFreeId());
    ui->actions->setCellWidget(ui->actions->rowCount()-1,1,te1);
}

/**
* @brief swap with (under)overlaying scene
*
* @param down - [in] bool down flag (if true swap with underlaying)
*/
void FillAction::swap(bool down)
{
    int i = 1;
    if (!down) i = -1;
    QTableWidgetItem *name = ui->actions->item(ui->actions->currentRow(),0);
    QTableWidgetItem *name1 = ui->actions->item(ui->actions->currentRow()+i,0);
    if (!(name && name1) ) return;
    QString name_str = name->text();
    QString nameq_str = name1->text();

    name1->setText(name_str);
    name->setText(nameq_str);

    QTimeEdit *te = returnTime(ui->actions->currentRow());
    QTime time = te->time();
    QTimeEdit *te1 = returnTime(ui->actions->currentRow()+i);
    QTime time1 = te1->time();
    te1->setTime(time);
    te->setTime(time1);

    QVector<frame> content;
    if(m_pActiveAction->getScene(name_str) &&m_pActiveAction->getScene(nameq_str))  {
        for (int i = 0; i < m_pActiveAction->getScene(name_str)->content.count();i++) {
            frame fr = m_pActiveAction->getScene(name_str)->content[i];
            content.push_back(fr);
        }
        m_pActiveAction->getScene(name_str)->content.clear();

        for (int i = 0; i < m_pActiveAction->getScene(nameq_str)->content.count();i++) {
            frame fr = m_pActiveAction->getScene(nameq_str)->content[i];
            m_pActiveAction->getScene(name_str)->content.push_back(fr);
        }

        m_pActiveAction->getScene(nameq_str)->content.clear();

        for (int i = 0; i < content.count();i++){
            frame fr = content[i];
            m_pActiveAction->getScene(nameq_str)->content.push_back(fr);
        }

        VScene *sc1 = m_pActiveAction->getScene(nameq_str);
        VScene *sc2 = m_pActiveAction->getScene(name_str);
        sc1->setName(name_str);
        sc2->setName(nameq_str);
    }

    ui->actions->setCurrentCell(ui->actions->currentRow()+i, ui->actions->currentColumn());
    cellClicked(ui->actions->currentRow());
}

uint32_t FillAction::getFreeId()
{
    QList<uint32_t> lst;
    for (int i  = 0 ; i < ui->actions->rowCount(); i++ ) {
        QFrame *fr = dynamic_cast<QFrame *>(ui->actions->cellWidget(i,1));
        if (!fr) break;
        uint32_t id = fr->property("sceneId").toUInt();
        lst.push_back(id);
    }
    return m_pActiveAction->getFreeSceneId(lst);
}

QString FillAction::getDefaulSceneName() const
{
    int rowCount = ui->actions->rowCount();
    if (rowCount == 1)
        return "Сцена 1";
    int max = 0;
    for (int r = 0; r < rowCount; ++r) {
        if (ui->actions->item(r,0)) {
            auto name = ui->actions->item(r,0)->text().split(" ");
            if (!name.isEmpty()) {
                bool ok;
                auto i = name.last().toInt(&ok);
                if (ok) {
                    max = std::max(max, i);
                }
            }
        }
    }
    if (max > 0)
        return "Сцена " + QString::number(max+1);
    return "Неизвестная сцена";
}

void FillAction::on_up_clicked()
{
    swap(false);
}

void FillAction::on_down_clicked()
{
    swap(true);
}

void FillAction::on_toolButtonHelp_clicked()
{
    emit sendOpenHtml("index.html?213.htm");
}

void FillAction::closeEvent(QCloseEvent *event)
{
    if (m_count_scenes == ui->actions->rowCount() && !m_change) {
        m_change = false;
        event->accept();
        return;
    }
    QMessageBox box;
    box.setText("Заполнить мероприятие.");
    box.setInformativeText("Изменились сцены. Вы хотите применить изменения?");
    box.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
    box.setDefaultButton(QMessageBox::No);
    int ret = box.exec();
    switch (ret) {
    case QMessageBox::Yes:
        saveScenes();
        event->accept();
        break;
    case QMessageBox::No:
        event->accept();
        break;
    case QMessageBox::Cancel:
        event->ignore();
        break;
    default:
        // should never be reached
        break;
     }
}


/**
* @brief create QFrame with onnir QTimeEdit
*
* @param r - [in] QWidget * parent widget
* @param duration - [in] uint32_t duration id seconds
*
* @return QFrame *
*/
QFrame* FillAction::createTime(QWidget * r,uint32_t duration,QTimeEdit **edit)
{
    QFrame *frame = new QFrame(r);
    frame->setContentsMargins(6,3,6,3);
    QHBoxLayout *layout = new QHBoxLayout;
    layout->setContentsMargins(1,1,1,1);
    QTimeEdit *tmp = new QTimeEdit();

    tmp->setTimeRange(QTime(0,0,10), QTime(24,0,0));
    tmp->setDisplayFormat("hh:mm:ss");
    tmp->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Minimum);
    tmp->setStyleSheet("QTimeEdit {"
                       "background-color: qlineargradient(x1: 0,y1: 0,x2: 0,y2: 1,stop: 0 #bcbcbc,stop: 1 #979797);"
                       "border-style: outset;"
                       "border-radius: 4px;"
                       "border-width: 1px;"
                       "border-color: #fafafa;"
                       "}"
                       "QTimeEdit::down-button {"
                       "width: 20 px;"
                       "}"
                       "QTimeEdit::up-button {"
                       "width: 20 px;"
                       "}");
    QTime time;
    int hour = duration/(60*60);
    int minute = (duration - hour*60*60)/60;
    int seconds = (duration - hour*60*60 - minute*60);
    time.setHMS(hour,minute,seconds);
    tmp->setTime(time);
    layout->addWidget(tmp);
    *edit = tmp;
    frame->setLayout(layout);
    return frame;
}

/**
* @brief return QTimeEdit from position in QTableWidget
*
* @param position - [in] - int row in table
*
* @return QTimeEdit * 
*/
QTimeEdit *FillAction::returnTime(uint32_t position)
{
    QTimeEdit * sb;
    QWidget *wgt_tmp,*wgt_tmp1;
    QFrame * frame;
    QHBoxLayout *lay;
    if ((!(wgt_tmp = ui->actions->cellWidget(position,1)))  ||
            (!(frame = static_cast<QFrame *>(wgt_tmp)))            ||
            (!(lay = static_cast<QHBoxLayout *>(frame->layout()))) ||
            (!(wgt_tmp1 = lay->itemAt(0)->widget()))               ||
            (!(sb = static_cast<QTimeEdit *>(wgt_tmp1)))) return NULL;
    return sb;
}

void FillAction::on_SetTime_clicked()
{
    QTableWidgetItem *name  = ui->actions->item(ui->actions->currentRow(),0);
    if (!name ) return;
    ui->actions->setCurrentCell(ui->actions->currentRow(),1);
}

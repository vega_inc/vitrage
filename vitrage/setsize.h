#ifndef SETSIZE_H
#define SETSIZE_H

#include <QDialog>

namespace Ui {
class SetSize;
}

class SetSize : public QDialog
{
    Q_OBJECT
    
public:
    explicit SetSize(QWidget *parent = 0);
    explicit SetSize(QWidget *parent, int w = 0, int h = 0);
    ~SetSize();
    
private:
    Ui::SetSize *ui;
    int m_oldw;
    int m_oldh;
signals:
    void sizes(int w,int h);

private slots:
    void on_buttonBox_accepted();
};

#endif // SETSIZE_H

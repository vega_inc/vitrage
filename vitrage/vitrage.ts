<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>AboutProgram</name>
    <message utf8="true">
        <source>Программный Комплекс Управления
Отображением Информации «Витраж» 1.0</source>
        <translation>Программный Комплекс Управления
Отображением Информации «Витраж» 1.0</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Закрыть</translation>
    </message>
    <message>
        <source>About program</source>
        <translation>О программе</translation>
    </message>
    <message>
        <source>Dialog</source>
        <translation>О программе</translation>
    </message>
    <message>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message utf8="true">
        <source>
Филиал открытого акционерного общества «Концерн радиостроения «Вега»
 в г. Санкт-Петербурге


e-mail: mail@spb.vega.su

</source>
        <translation>
Филиал открытого акционерного общества «Концерн радиостроения «Вега»
 в г. Санкт-Петербурге


e-mail: mail@spb.vega.su
</translation>
    </message>
</context>
<context>
    <name>CanvaOptions</name>
    <message>
        <source>Parametrs can&apos;t be NULL</source>
        <translation>Параметр не может быть пустым</translation>
    </message>
    <message utf8="true">
        <source>Настройки модели видеостены</source>
        <translation>Настройки модели видеостены</translation>
    </message>
    <message utf8="true">
        <source>         Размеры видеостены (в &quot;кубах&quot;)</source>
        <translation>Размеры видеостены (в &quot;кубах&quot;)</translation>
    </message>
    <message utf8="true">
        <source>По горизонтали</source>
        <translation>По горизонтали</translation>
    </message>
    <message utf8="true">
        <source>По вертикали</source>
        <translation>По вертикали</translation>
    </message>
    <message utf8="true">
        <source>          Размер &quot;куба&quot; (в пикселях)</source>
        <translation>Размер &quot;куба&quot; (в пикселях)</translation>
    </message>
    <message>
        <source>600</source>
        <translation>600</translation>
    </message>
    <message>
        <source>800</source>
        <translation>800</translation>
    </message>
    <message>
        <source>1024</source>
        <translation>1024</translation>
    </message>
    <message>
        <source>1080</source>
        <translation>1080</translation>
    </message>
    <message>
        <source>1280</source>
        <translation>1280</translation>
    </message>
    <message>
        <source>1920</source>
        <translation>1920</translation>
    </message>
    <message utf8="true">
        <source>        Шаг сетки &quot;куба&quot; (в долях)</source>
        <translation>Шаг сетки &quot;куба&quot; (в долях)</translation>
    </message>
    <message utf8="true">
        <source>       Шаг сетки &quot;куба&quot; (в долях)</source>
        <translation type="obsolete">Шаг сетки &quot;куба&quot; (в долях)</translation>
    </message>
</context>
<context>
    <name>ControllerOptions</name>
    <message>
        <source>Trying to connect</source>
        <translation>Попытка подключения</translation>
    </message>
    <message>
        <source>Connected</source>
        <translation>Подключен</translation>
    </message>
    <message>
        <source>Stopped</source>
        <translation>Остановлено</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation>Отключен</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <source>On</source>
        <translation>Включить</translation>
    </message>
    <message>
        <source>Controller Options</source>
        <translation>Настройки контроллера</translation>
    </message>
    <message>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message utf8="true">
        <source>Модель контроллера</source>
        <translation>Модель контроллера</translation>
    </message>
    <message>
        <source>                 Connection parameters</source>
        <translation>                 Параметры подключения</translation>
    </message>
    <message utf8="true">
        <source>Адрес</source>
        <translation>Адрес</translation>
    </message>
    <message utf8="true">
        <source>Порт</source>
        <translation>Порт</translation>
    </message>
    <message utf8="true">
        <source>Логин</source>
        <translation>Логин</translation>
    </message>
    <message utf8="true">
        <source>Пароль</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <source>            Connection status</source>
        <translation>            Статус подключения</translation>
    </message>
    <message utf8="true">
        <source>Не подключен</source>
        <translation>Не подключен</translation>
    </message>
    <message utf8="true">
        <source>Сообщение(отладка)</source>
        <translation>Сообщение(отладка)</translation>
    </message>
    <message utf8="true">
        <source>Включить</source>
        <translation>Включить</translation>
    </message>
</context>
<context>
    <name>CroppingWidget</name>
    <message>
        <source>Dialog</source>
        <translation>Копирование виджета</translation>
    </message>
    <message>
        <source>Other view</source>
        <translation>Другой вид</translation>
    </message>
    <message>
        <source>Cropping</source>
        <translation>Кадрирование</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Применить</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>FillAction</name>
    <message>
        <source>Action&apos;s content creation</source>
        <translation>Заполнить мероприятие</translation>
    </message>
    <message>
        <source>Copy</source>
        <extracomment>copy</extracomment>
        <translation>Копировать</translation>
    </message>
    <message>
        <source>Scene Name</source>
        <translation>Название сцены</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>Продолжительность</translation>
    </message>
    <message>
        <source>Empty scene&apos;s name</source>
        <translation>Пустое имя сцены</translation>
    </message>
    <message>
        <source>Empty scene&apos;s duration</source>
        <translation>Пустая продолжительность сцены</translation>
    </message>
    <message>
        <source>Do you realy want to remove scene </source>
        <translation>Вы действительно хотите удалить сцены</translation>
    </message>
    <message>
        <source>Yes </source>
        <translation>Да</translation>
    </message>
    <message>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <source>Scene</source>
        <translation>Сцена</translation>
    </message>
    <message>
        <source>Scene </source>
        <translation>Сцена</translation>
    </message>
    <message>
        <source>exist</source>
        <translation>существует</translation>
    </message>
</context>
<context>
    <name>LogicSourceManager</name>
    <message>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <source>set icon</source>
        <translation>Установить иконку</translation>
    </message>
    <message>
        <source>information</source>
        <translation>информация</translation>
    </message>
    <message>
        <source>set name of the action!</source>
        <translation>Установите название мероприятия</translation>
    </message>
    <message>
        <source>New Action</source>
        <translation>Новое мероприятие</translation>
    </message>
    <message>
        <source>Sources can&apos;t have equals names</source>
        <translation>Источники не могут иметь одинаковые имена</translation>
    </message>
    <message>
        <source>Do you realy want to delete sources: </source>
        <translation>Вы действительно хотите удалить источники:</translation>
    </message>
    <message>
        <source>sources will be deleted from all scenes</source>
        <translation>Источники будут удалены из всех сцен</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <source>Do you realy want close manager without saving </source>
        <translation>Вы действительно хотите закрыть менеджер без сохранения</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <source>Can&apos;t open file</source>
        <translation>Невозможно открыть файл</translation>
    </message>
    <message>
        <source>Can&apos;t copy config file in user home directory</source>
        <translation>Невозможно скопировать конфигурационный файл в домашнюю директорию пользователя</translation>
    </message>
    <message>
        <source>Can&apos;t make folder for config file in user home directory</source>
        <translation>Невозможно создать папку для конфигурационного файла в домашней директории пользователя</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Action media sources</source>
        <translation type="obsolete">Источники информации </translation>
    </message>
    <message>
        <source>Sources</source>
        <translation>Источники</translation>
    </message>
    <message>
        <source>Action scenes</source>
        <translation type="obsolete">Сцены мероприятия</translation>
    </message>
    <message>
        <source>Scenes</source>
        <translation type="obsolete">Сцены</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>Пауза</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Старт</translation>
    </message>
    <message>
        <source>   Vitrage</source>
        <translation>Витраж</translation>
    </message>
    <message>
        <source>Save Action Before close</source>
        <translation>Сохранить мероприятие перед закрытием</translation>
    </message>
    <message>
        <source>Yes (Save) </source>
        <translation>Да (Сохранить)</translation>
    </message>
    <message>
        <source>No (Delete changes)</source>
        <translation>Нет (Удалить изменения)</translation>
    </message>
    <message>
        <source>Cencel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <source>Waiting</source>
        <translation>Ожидайте</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation>Нет подключения</translation>
    </message>
    <message>
        <source>Empty Scene</source>
        <translation>Пустая сцена</translation>
    </message>
    <message>
        <source>Save action </source>
        <translation>Сохранить мероприятие </translation>
    </message>
    <message>
        <source> before close</source>
        <translation> до закрытия</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <source>No binded scheme</source>
        <translation>Мероприятие не имеет графической схемы</translation>
    </message>
    <message>
        <source>Sources Manager</source>
        <translation>Источники информации</translation>
    </message>
    <message>
        <source>Scenes Manager</source>
        <translation>Сцены</translation>
    </message>
    <message>
        <source>Scene&apos;s content</source>
        <translation>Контент сцены</translation>
    </message>
    <message>
        <source>Connection started</source>
        <translation>Подключение инициализировано</translation>
    </message>
    <message>
        <source>Connected OK</source>
        <translation>Подключение установлено</translation>
    </message>
    <message>
        <source>Controller connected</source>
        <translation>Контроллер подключен</translation>
    </message>
    <message>
        <source>Not Connected</source>
        <translation>Контроллер НЕ подключен</translation>
    </message>
    <message>
        <source>Controller Stopped</source>
        <translation>Контроллер остановлен</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation>Отключен</translation>
    </message>
    <message>
        <source>Controller disconnected</source>
        <translation>Контроллер отключен</translation>
    </message>
    <message>
        <source>Connection Error</source>
        <translation>Ошибка подключения</translation>
    </message>
    <message>
        <source>Action: </source>
        <translation>Мероприятие</translation>
    </message>
    <message>
        <source>Action</source>
        <translation>Мероприятие</translation>
    </message>
    <message>
        <source>Service</source>
        <translation type="obsolete">Сервис</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <source>toolBar</source>
        <translation type="obsolete">Панель инструментов</translation>
    </message>
    <message>
        <source>Create</source>
        <translation>Создать</translation>
    </message>
    <message>
        <source>Add Source</source>
        <translation>Добавить источник</translation>
    </message>
    <message>
        <source>Model video wall</source>
        <translation type="obsolete">Модель видеостены</translation>
    </message>
    <message utf8="true">
        <source>Витраж</source>
        <translation>Витраж</translation>
    </message>
    <message>
        <source>ToolBar</source>
        <translation>Панель инструментов</translation>
    </message>
    <message>
        <source>toolBar Main</source>
        <translation>Основная панель</translation>
    </message>
    <message>
        <source>toolBar CloseHelp</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <source>toolBar Playing</source>
        <translation>Управление воспроизведением</translation>
    </message>
    <message>
        <source>toolBar Configuration</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>Scenes manager</source>
        <translation>Сцены</translation>
    </message>
    <message>
        <source>Tile</source>
        <translation>Плитка</translation>
    </message>
    <message>
        <source>Sources manager</source>
        <translation>Источники информации</translation>
    </message>
    <message>
        <source>Timeline Widget</source>
        <translation>Временная диаграмма мероприятия</translation>
    </message>
    <message>
        <source>Graphics Editor</source>
        <translation>Редактор схем</translation>
    </message>
    <message>
        <source>ModelWall</source>
        <translation>Модель стены</translation>
    </message>
    <message>
        <source>Play active scene</source>
        <translation>Воспроизвести выбранную сцену</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation>Вырезать</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation>Переимевать</translation>
    </message>
    <message>
        <source>Standart</source>
        <translation>Стандартная</translation>
    </message>
    <message>
        <source>Playing</source>
        <translation>Воспроизведение</translation>
    </message>
    <message>
        <source>View</source>
        <translation>Вид</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>toolBar_2</source>
        <translation type="obsolete">Панель инструментов</translation>
    </message>
    <message>
        <source>Model Wall</source>
        <translation>Модель видеостены</translation>
    </message>
    <message>
        <source> Create Action</source>
        <translation>Создать мероприятие</translation>
    </message>
    <message>
        <source>Create Scenary</source>
        <translation>Создать сценарий</translation>
    </message>
    <message>
        <source>Save Action</source>
        <translation>Сохранить мероприятие</translation>
    </message>
    <message>
        <source>Save Actions</source>
        <translation>Сохранить сцены</translation>
    </message>
    <message>
        <source>Load Action</source>
        <translation>Загрузить мероприятие</translation>
    </message>
    <message>
        <source>Load Actions</source>
        <translation>Загрузить мероприятие</translation>
    </message>
    <message>
        <source>Delete Source</source>
        <translation>Удалить источник</translation>
    </message>
    <message>
        <source>fit</source>
        <translation>Форма</translation>
    </message>
    <message>
        <source>grid</source>
        <translation>Сетка</translation>
    </message>
    <message>
        <source>Send To Run1</source>
        <translation>Запустить</translation>
    </message>
    <message>
        <source>Send to Run1</source>
        <translation>Запустить</translation>
    </message>
    <message>
        <source>timePlay</source>
        <translation>Пуск</translation>
    </message>
    <message>
        <source>timeStop</source>
        <translation>Стоп</translation>
    </message>
    <message>
        <source>controllerParams</source>
        <translation>Параметры контроллера</translation>
    </message>
    <message>
        <source>openScheme</source>
        <translation>Открыть схему</translation>
    </message>
    <message>
        <source>help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <source>close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <source>Open Graphics</source>
        <translation>Открыть графический редактор</translation>
    </message>
    <message>
        <source>Last Loaded</source>
        <translation>Предыдущие документы</translation>
    </message>
    <message>
        <source>Save As ...</source>
        <translation>Сохранить как</translation>
    </message>
    <message>
        <source>Play/Pause</source>
        <translation>Старт/Пауза</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>Стоп</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <source>Wall options</source>
        <translation>Параметры видеостены</translation>
    </message>
    <message>
        <source>Controller options</source>
        <translation>Параметры контроллера</translation>
    </message>
    <message>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <source>RunTime Wall</source>
        <translation>Действующая стена</translation>
    </message>
    <message>
        <source>Configurations</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>StatusBar</source>
        <translation>Строка состояния</translation>
    </message>
    <message>
        <source>Scale</source>
        <translation>Масштабировать</translation>
    </message>
    <message>
        <source>Vitrage manual</source>
        <translation>Описание</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation>Контакты</translation>
    </message>
    <message>
        <source>ZoomIn</source>
        <translation>Увеличить</translation>
    </message>
    <message>
        <source>ZoomOut</source>
        <translation>Уменьшить</translation>
    </message>
    <message>
        <source>next</source>
        <translation>Следующий</translation>
    </message>
    <message>
        <source>Prev</source>
        <translation>Предыдущий</translation>
    </message>
    <message>
        <source>editSource</source>
        <translation>Редактировать источники</translation>
    </message>
    <message>
        <source>cascade</source>
        <translation>Выстроить каскадом</translation>
    </message>
    <message>
        <source>actionTile</source>
        <translation>Выстроить плиткой</translation>
    </message>
    <message>
        <source>actionClear</source>
        <translation>Очистить стену</translation>
    </message>
    <message>
        <source>Action content</source>
        <translation type="obsolete">Сценарий</translation>
    </message>
    <message>
        <source>new action</source>
        <translation>Новое мероприятие</translation>
    </message>
    <message>
        <source>Resources</source>
        <translation>Источники</translation>
    </message>
    <message>
        <source>Can,t load resources</source>
        <translation>Невозможно загрузить источники</translation>
    </message>
    <message>
        <source>Hardware options</source>
        <translation>Настройка оборудования</translation>
    </message>
    <message>
        <source>Controller</source>
        <translation>Контроллер</translation>
    </message>
    <message>
        <source>Extron</source>
        <translation>Экстрон</translation>
    </message>
    <message>
        <source>Mixer</source>
        <translation>Микшер</translation>
    </message>
</context>
<context>
    <name>NewSource</name>
    <message>
        <source>Add new source</source>
        <translation>Добавить источник</translation>
    </message>
    <message>
        <source>Select the type of source</source>
        <translation>Выбрать тип источника</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>Выбрать</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Scene</source>
        <translation>Сцена</translation>
    </message>
    <message utf8="true">
        <source>Витраж</source>
        <translation>Витраж</translation>
    </message>
    <message utf8="true">
        <source>Невозможно сохранить в файл</source>
        <translation>Невозможно сохранить в файл</translation>
    </message>
</context>
<context>
    <name>SetSize</name>
    <message>
        <source>Dialog</source>
        <translation>Установить размер</translation>
    </message>
    <message>
        <source>Width</source>
        <translation>Ширина</translation>
    </message>
    <message>
        <source>Height</source>
        <translation>Высота</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Применить</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>SourceManager</name>
    <message>
        <source>pic</source>
        <translation type="obsolete">Картинка</translation>
    </message>
    <message>
        <source>name</source>
        <translation type="obsolete">имя</translation>
    </message>
    <message>
        <source>del</source>
        <translation type="obsolete">Удал.</translation>
    </message>
    <message>
        <source>Specific Options</source>
        <translation>Специфичные свойства</translation>
    </message>
    <message>
        <source>General Options</source>
        <translation>Обшие свойства</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <source>Add new source</source>
        <translation>Добавить источник</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <source>Can&apos;t open file</source>
        <translation>Невозможно открыть файл</translation>
    </message>
    <message>
        <source>error 1051</source>
        <translation>Ошибка 1051</translation>
    </message>
    <message>
        <source>Do you realy want close source manager without saving?</source>
        <translation>Вы уверены что хотите закончить управление источниками не сохранив изменений?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <source>serialize json failed</source>
        <translation>Создание json файла не удалось</translation>
    </message>
    <message>
        <source>Source management</source>
        <translation>Управление источниками</translation>
    </message>
    <message>
        <source>Source name cant be duplicated.</source>
        <translation>Имя ресурса должно быть уникальным</translation>
    </message>
</context>
<context>
    <name>TimelineWidget</name>
    <message>
        <source>Timeline Widget</source>
        <translation>Временная диаграмма мероприятия</translation>
    </message>
    <message>
        <source>TimeLine Widget</source>
        <translation>Временная диаграмма мероприятия</translation>
    </message>
    <message>
        <source>Warning!</source>
        <translation>Таймлайн</translation>
    </message>
    <message>
        <source>No intervals on timeline!</source>
        <translation>Нет интервалов</translation>
    </message>
    <message>
        <source>item</source>
        <translation>строка</translation>
    </message>
</context>
<context>
    <name>VAction</name>
    <message>
        <source>Add Scene</source>
        <translation>Добавить сцену</translation>
    </message>
    <message>
        <source>Remove Scene</source>
        <translation>Удалить сцену</translation>
    </message>
    <message>
        <source>Move Scene Up</source>
        <translation>Подвинуть вверх</translation>
    </message>
    <message>
        <source>Move Scene Down</source>
        <translation>Подвинуть вниз</translation>
    </message>
    <message>
        <source>Remove Source</source>
        <translation>Удалить источник</translation>
    </message>
    <message>
        <source>Send to Back</source>
        <translation>Опустить на слой</translation>
    </message>
    <message>
        <source>Bring to Front</source>
        <translation>Поднять на слой</translation>
    </message>
    <message>
        <source>Crop Source</source>
        <translation>Кадрировать</translation>
    </message>
    <message>
        <source>Delete Crop from Source</source>
        <translation>Показывать весь контент</translation>
    </message>
    <message>
        <source>Sources</source>
        <translation>Источники</translation>
    </message>
    <message>
        <source>Scenes</source>
        <translation>Сцены</translation>
    </message>
    <message>
        <source>Can&apos;t load action</source>
        <translation>Невозможно загрузить мероприятие</translation>
    </message>
    <message>
        <source>That type of source is not supporting by this controller</source>
        <translation type="obsolete">Данный тип источника не поддерживает операцию кадрирования</translation>
    </message>
</context>
<context>
    <name>VCanva</name>
    <message>
        <source>No selected scene</source>
        <translation>Сцена не выбрана!</translation>
    </message>
    <message>
        <source>Preset Wall</source>
        <translation>Редактируемая сцена</translation>
    </message>
    <message>
        <source>Runtime Wall</source>
        <translation>Отображаемая сцена</translation>
    </message>
    <message>
        <source>Source of this type cannot be cloned</source>
        <translation type="obsolete">Источник данного типа уже присутствует в сцене</translation>
    </message>
    <message>
        <source>Scene not selected</source>
        <translation>Сцена не выбрана!</translation>
    </message>
    <message>
        <source>Controller NOT connected</source>
        <translation>Контроллер НЕ подключен</translation>
    </message>
</context>
<context>
    <name>VGraphicsEditor</name>
    <message>
        <source>Sources Manager</source>
        <translation type="obsolete">Источники информации</translation>
    </message>
    <message>
        <source>Scheme Content</source>
        <translation type="obsolete">Содержимое схемы</translation>
    </message>
    <message>
        <source>Graphics scheme editor</source>
        <translation>Редактор графической схемы мероприятия</translation>
    </message>
    <message>
        <source>Sources on scheme</source>
        <translation>Источники информации</translation>
    </message>
    <message>
        <source>Do you realy want to delete scheme</source>
        <translation>Вы действительно хотите удалить схему</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <source>Source are in scene</source>
        <translation>Источник в сцене</translation>
    </message>
    <message>
        <source>set icon</source>
        <translation>Установить иконку</translation>
    </message>
    <message>
        <source>Do you realy want to delete sources: </source>
        <translation>Вы действительно хотите удалить источники</translation>
    </message>
    <message>
        <source>sources will be deleted from all scenes</source>
        <translation>источники будут удалены из всех сцен</translation>
    </message>
    <message>
        <source>Do you realy want to exit without saving  scheme</source>
        <translation>Вы действительно хотите выйти не сохранить схему</translation>
    </message>
    <message>
        <source>Yes </source>
        <translation>Да</translation>
    </message>
    <message>
        <source>Save (Save and exit)</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <source>Cencel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <source>GraphicsEditor</source>
        <translation>Редактор графических схем</translation>
    </message>
    <message>
        <source>Scheme</source>
        <translation>Схема</translation>
    </message>
    <message>
        <source>Actions</source>
        <translation type="obsolete">Мероприятия</translation>
    </message>
    <message>
        <source>toolBar</source>
        <translation>Панель инструментов</translation>
    </message>
    <message>
        <source>toolBar_2</source>
        <translation>Панель инструментов</translation>
    </message>
    <message>
        <source>Create</source>
        <translation type="obsolete">Создать</translation>
    </message>
    <message>
        <source>Create Scheme</source>
        <translation>Создать схему</translation>
    </message>
    <message>
        <source>save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <source>load</source>
        <translation>Загрузить</translation>
    </message>
    <message>
        <source>close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <source>help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <source>Open Scheme</source>
        <translation>Открыть схему</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation>Сохранить как</translation>
    </message>
    <message>
        <source>Close Scheme</source>
        <translation>Закрыть схему</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <source>editMode</source>
        <translation>Режим редактирования</translation>
    </message>
    <message>
        <source>runtimeMode</source>
        <translation>Режим работы</translation>
    </message>
</context>
<context>
    <name>VGraphicsViewer</name>
    <message>
        <source>Scheme Viewer</source>
        <translation>Графическая схема</translation>
    </message>
    <message>
        <source>toolBar</source>
        <translation>Панель инструментов</translation>
    </message>
    <message>
        <source>toolBar_2</source>
        <translation>Панель инструментов</translation>
    </message>
    <message>
        <source>editor</source>
        <translation>Редеактор</translation>
    </message>
    <message>
        <source>help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <source>close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>VGraphicsWidget</name>
    <message>
        <source>Set Picture</source>
        <translation>Установить изображение</translation>
    </message>
    <message>
        <source>Set Size</source>
        <translation>Установить размер</translation>
    </message>
    <message>
        <source>Set Skin</source>
        <translation>Установить SVG</translation>
    </message>
    <message>
        <source>Bare Action</source>
        <translation>Нет действия</translation>
    </message>
    <message>
        <source>Rotate 90</source>
        <translation>Поворот 90</translation>
    </message>
    <message>
        <source>set image</source>
        <translation>Установить изображение</translation>
    </message>
    <message>
        <source>Can&apos;t set picture</source>
        <translation>Невозможно установить изображение</translation>
    </message>
    <message>
        <source>set icon</source>
        <translation type="obsolete">Установить иконку</translation>
    </message>
</context>
<context>
    <name>VScheme</name>
    <message>
        <source>Set Background</source>
        <translation>Установить фон</translation>
    </message>
    <message>
        <source>Set SVG file for background</source>
        <translation>Устновить SVG файл на фон</translation>
    </message>
    <message>
        <source>Set underlayer size</source>
        <translation>Установить размер фона</translation>
    </message>
    <message>
        <source>Can&apos;t set image</source>
        <translation>Невозможно устновить изображение</translation>
    </message>
    <message>
        <source>set background image</source>
        <translation>Устновить изображение</translation>
    </message>
    <message>
        <source>set svg file</source>
        <translation>утсновить SVG файл</translation>
    </message>
    <message>
        <source>set icon</source>
        <translation type="obsolete">Установить иконку</translation>
    </message>
</context>
<context>
    <name>VSource</name>
    <message>
        <source>Enabled</source>
        <translation type="obsolete">Включен</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="obsolete">Выключен</translation>
    </message>
</context>
<context>
    <name>VStatusBar</name>
    <message>
        <source>Controller status: </source>
        <translation>Статус контроллера  </translation>
    </message>
</context>
<context>
    <name>WidgetSkins</name>
    <message>
        <source>Dialog</source>
        <translation>Установить виджет</translation>
    </message>
</context>
<context>
    <name>setDev</name>
    <message>
        <source>Create Action</source>
        <translation>Создать мероприятие</translation>
    </message>
    <message>
        <source>action name</source>
        <translation type="obsolete">Название мероприятия</translation>
    </message>
    <message>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <source>        action name</source>
        <translation>Название мероприятия</translation>
    </message>
    <message>
        <source>forming a sources of information</source>
        <translation>Создать список источник информации</translation>
    </message>
    <message>
        <source>Source of information</source>
        <translation>Источник информации</translation>
    </message>
    <message>
        <source>name</source>
        <translation>имя</translation>
    </message>
    <message>
        <source>source of information</source>
        <translation>Источник информации</translation>
    </message>
    <message>
        <source>location</source>
        <translation type="obsolete">Расположение</translation>
    </message>
    <message>
        <source>Open sources Manager</source>
        <translation>Открыть окно управления источниками</translation>
    </message>
    <message>
        <source>pic</source>
        <translation>pic</translation>
    </message>
</context>
</TS>

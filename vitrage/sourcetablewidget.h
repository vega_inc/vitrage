#ifndef SOURCETABLEWIDGET_H
#define SOURCETABLEWIDGET_H
#include <QTableWidget>

class SourceTableWidget : public QTableWidget
{
public:
    SourceTableWidget();
protected:
    void mousePressEvent(QMouseEvent *event);
};

#endif // SOURCETABLEWIDGET_H

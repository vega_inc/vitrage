#ifndef VSOURCEMANAGER_H
#define VSOURCEMANAGER_H

#include <QObject>

#include "../vsource/vsource.h"
#include "configpath.h"


class VSrcManager : public QObject
{
    Q_OBJECT
    static QString getPluginsPath();

public:
    static VSrcManager &Instance();
    bool loadPlugins(void);
    VSource* createVSource(const QString& type);
    QMap<QString,VSourceCreator*> getSourceCreatorMap();

private:
    QMap<QString,VSourceCreator*> src_creators;
    VSrcManager(){}
};

#endif /* VSOURCEMANAGER */

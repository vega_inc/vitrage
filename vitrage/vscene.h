#ifndef VSCENE_H
#define VSCENE_H

#include <QList>
#include <QString>

#include <stdint.h>

#include "../vsource/vsource.h"

#define MIXERVNC    "MixerVNCClient"
#define EXTRON      "ExtronInput"
#define EXTRONMIXER "MixerExtronVNCClient"

class VSource;

struct frame {
    QString source_name;
    QString clone_name;
    VPosition pos;
    bool visible = true;
};

class VScene : public QObject
{
    Q_OBJECT
    uint32_t m_id;
    uint32_t m_tFrom;
    uint32_t m_tDuration;
    //uint32_t m_number;
    QString m_name;

    bool m_active;

    int getPosMargin(frame fr);
    QMap<QString,QSharedPointer<VSource>> m_mixerVNC;
    QMap<QString,QSharedPointer<VSource>> m_extronInput;
    QMap<QString,QSharedPointer<VSource>> m_extronMixer;

    int m_DVIInputs = 0;
    int m_maxDVIInputs = 8;
    int m_maxExtronMixer = 8;
    int m_maxMixer = 4;

public:
    VScene(uint32_t id, const uint32_t& tF, const uint32_t& tD, const QString& name = "");

    QList<QSharedPointer<VSource>> getMixerVNCList();
    QList<QSharedPointer<VSource>> getExtronInput();
    QList<QSharedPointer<VSource>> getExtronMixer();

    friend class VCanva;
    QVector<frame> content;
    void removeSourceFromConts(const QString & name);
    void removeSource(QString name);
    bool contentContains(QString str);
    bool validateSource(QSharedPointer<VSource> src, QString &message);
    uint32_t id() const;
    void setVisible(const QString &cloneName, bool);
    bool getVisible(QString clone);
    void removeClone(QString clone_name);
    QVector<frame> & getContent(void);
    void setDuration(uint32_t dura);
    void setName(const QString& n);
    QString name() const;
    uint32_t timeFrom() const;
    uint32_t duration() const;
    void setActive(bool active);
    bool isActive() const;
    uint32_t number() const;
    bool haveInput() const;
    QList<QSharedPointer<VSource> > getExtronAll();
    QList<QSharedPointer<VSource> > getMixerAll();
    QList<QSharedPointer<VSource> > getMixerAll(QList<QSharedPointer<VSource> > &ll);
    QList<QSharedPointer<VSource> > getExtronAll(QList<QSharedPointer<VSource> > &ll);
    bool validateSource2(QSharedPointer<VSource> src);

public slots:
    void addSource(VSource & );
    void setPosition(VSource &);
};

#endif // VSCENE_H

#ifndef FILLACTION_H
#define FILLACTION_H

#include <QDialog>
#include <QTimeEdit>
#include "vaction.h"
#include "vcontroller/vcontroller.h"
#include <QTimeEdit>
#include <QFrame>


namespace Ui {
	class FillAction;
}

class FillAction : public QDialog
{
    Q_OBJECT
public:
    static const int DEFAULT_DURATION = 5;
    explicit FillAction(VAction *action, QWidget *parent = 0);
    ~FillAction();
    void open();
    void exec();
    static QFrame *createTime(QWidget *,uint32_t duration = 0,QTimeEdit **e = NULL);
    QTimeEdit *returnTime(uint32_t positin);

public slots:
    void addNewScene();
    void saveScenes();
    void cellClicked(int);
    void somethingChanged();
    
private slots:
    void on_RemoveScene_clicked();
    void on_CopyScene_clicked();
    void on_up_clicked();
    void on_down_clicked();
    void on_toolButtonHelp_clicked();

protected:
    void closeEvent(QCloseEvent *event);

signals:
    void sendOpenHtml(const QString &page);

private:
    void swap(bool down); /* down = true swap with underlaying scene */
    uint32_t getFreeId();
    QString getDefaulSceneName() const;
    void on_SetTime_clicked();

    Ui::FillAction *ui;
    VAction *m_pActiveAction;
    int m_count_scenes = 0;
    bool m_change = false;
    QStringList deletedScenes;
};

#endif // FILLACTION_H

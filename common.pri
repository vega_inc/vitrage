#defines
PROJECT_NAME           = vitrage
ROOT_DIR               = ../
IO_PLUGINS_PATH        = plugins/io_plugins/
INSTALL_PATH_BIN       = $${ROOT_DIR}bin
TRANSLATE_PATH         = $${INSTALL_PATH_BIN}/translate
INSTALL_PATH_LIB       = $${INSTALL_PATH_BIN}/lib
INSTALL_PATH_PLUGINS   = $${INSTALL_PATH_BIN}/plugins/
INSTALL_PATH_CTRL_PLUGINS  = $${INSTALL_PATH_BIN}/controllers/
INSTALL_PATH_IOPLUGINS = $${INSTALL_PATH_BIN}/$${IO_PLUGINS_PATH}
INSTALL_PATH_INCLUDE   = $${ROOT_DIR} \
                         ../
BUILD_PATH = ./
ARCH = ./

#share qmake defines
OBJECTS_DIR = $${BUILD_PATH}objects
MOC_DIR     = $${BUILD_PATH}mocs
UI_DIR      = $${BUILD_PATH}uics
RCC_DIR     = $${BUILD_PATH}rcc

#build translation files
isEmpty(QMAKE_LRELEASE)
{
    win32:QMAKE_LRELEASE = $$[QT_INSTALL_BINS]/lrelease.exe
    else:QMAKE_LRELEASE = $$[QT_INSTALL_BINS]/lrelease
}

isEmpty(TRANSLATIONS){
updateqm.input = TRANSLATIONS
updateqm.output = $${TRANSLATE_PATH}/$${TARGET}.qm
updateqm.commands = $$QMAKE_LRELEASE ${QMAKE_FILE_IN} -qm $${TRANSLATE_PATH}/$${TARGET}.qm
updateqm.CONFIG += no_link

QMAKE_EXTRA_COMPILERS += updateqm
PRE_TARGETDEPS += compiler_updateqm_make_all
}
QMAKE_CXXFLAGS += -std=c++11 -D_GLIBCXX_USE_NANOSLEEP

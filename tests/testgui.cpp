#include <QtTest/QtTest>
#include <QApplication>

#include "testaddsource.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    TestAddVncSource test1;

    auto ok = QTest::qExec(&test1, argc, argv);

    return ok;
}

//QTEST_MAIN(Test)
//#include "testgui.moc"

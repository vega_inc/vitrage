include(../common.pri)

SOURCES = testgui.cpp \
        ../vitrage/vsourcemanager.cpp

HEADERS = \
        ../vitrage/vsourcemanager.h \
        ../vsource/vsource.h \
    testaddsource.h \
    testdatapath.h

CONFIG  += qtestlib

QT += svg network

INCLUDEPATH += $$INSTALL_PATH_INCLUDE

LIBS += -L$${INSTALL_PATH_LIB} \
        -lvsource \
        -lvcontroller \
        -lqjson

DESTDIR = $$INSTALL_PATH_BIN
unix {
     target.path = ../bin/
     INSTALLS += target
}

#ifndef TESTADDSOURCE_H
#define TESTADDSOURCE_H
#include <QObject>
#include <QTest>
#include <QSignalSpy>
#include "../vitrage/sourcemanager.h"
#include "../vitrage/spdlog/spdlog.h"

class TestAddVncSource : public QObject
{
    Q_OBJECT
private slots:
    void initTestCase()
    {
        try {
            QDir().mkdir("logs");
            if (!QDir("logs").exists())
                throw spdlog::spdlog_ex("fail to create dir logs");
            spdlog::daily_logger_mt("test", "logs/test");
        }
        catch (const spdlog::spdlog_ex& ex) {
            qDebug() << ex.what();
        }
        QVERIFY2(VSrcManager::Instance().loadPlugins(), "Plugins is not loaded");
    }
    void testAddVncSource()
    {
        for (int i = 0; i < 20; ++i) {
            QScopedPointer<VSource> src(VSrcManager::Instance().createVSource("VNCClient"));
            QScopedPointer<VSource> src2(VSrcManager::Instance().createVSource("VNCClient"));
            QScopedPointer<VSource> src3(VSrcManager::Instance().createVSource("VNCClient"));
            auto source = src.data();
            auto source2 = src2.data();
            auto source3 = src3.data();
            //         QSignalSpy spy(source, SIGNAL(signalForClones()));

            source->setname("lalala");
            source->start();
            source2->start();
            source3->start();
            auto t = source->getThread();

            QSignalSpy spy(t, SIGNAL(statusChanged(int)));
            QTest::qWait(1000*3);
            //QCOMPARE(spy.count(), 1);
            //QList<QVariant> arguments = spy.takeLast();
            //QVERIFY(arguments.at(0).toInt() == 0);
            QVERIFY(spy.count() > 0);
            src.reset();
            src2.reset();
            src3.reset();
        }
    }
};

#endif // TESTADDSOURCE_H


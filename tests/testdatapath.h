#ifndef TESTDATAPATH_H
#define TESTDATAPATH_H

#include <QtGui>
#include <QtTest/QtTest>
#include <QSettings>
#include <chrono>
#include "../vitrage/controlleroptions.h"
#include "../vitrage/sourcemanager.h"
#include "../vitrage/spdlog/spdlog.h"
#include "support.h"

// 1600*1200
//constexpr uint pixs_w = 320000;
//constexpr uint pixs_h = 240000;
// 1980*1080
constexpr uint pixs_w = 213333;
constexpr uint pixs_h = 120000;
const char datapathAddress[] = "10.1.1.209";

static const std::chrono::minutes waitConnectTime(3);
static const std::chrono::minutes waitDisconnectTime(3);
static const std::chrono::minutes waitSetSceneTime(5);

class TestDatapath: public QObject
{
    Q_OBJECT
    VController *m_controller;

private slots:
    void initTestCase()
    {
        try {
            QDir().mkdir("logs");
            if (!QDir("logs").exists())
                throw spdlog::spdlog_ex("fail to create dir logs");
            spdlog::daily_logger_mt("test", "logs/test");
        }
        catch (const spdlog::spdlog_ex& ex) {
            qDebug() << ex.what();
        }
    }
    void cleanupTestCase()
    {
        if (!m_controller)
            QSKIP("No controller", SkipAll);
        Sources sources;

        QSignalSpy spy(m_controller, SIGNAL(sceneFinished(bool)));
        m_controller->setScene(sources);

        QElapsedTimer t; t.start();
        while (spy.count() == 0 &&
               t.elapsed() < std::chrono::duration_cast<std::chrono::milliseconds>(waitSetSceneTime).count()) {
            QTest::qWait(200);
        }

        QString failSetScene = QString("Scene is not finished in %1 min").arg(waitSetSceneTime.count());
        QVERIFY2(spy.count() == 1, failSetScene.toAscii());

        QList<QVariant> arguments = spy.takeFirst();
        QVERIFY(arguments.at(0).toBool() == true);

        delete m_controller;
    }
    void testPlugins()
    {
        QVERIFY2(VSrcManager::Instance().loadPlugins(), "Plugins is not loaded");
    }
    void testDatapathConnect()
    {
        auto pluginName = QCoreApplication::applicationDirPath() + "/controllers/libdatapath.so";
        QPluginLoader loader(pluginName);
        m_controller = qobject_cast<VController*>(loader.instance());
        if (!m_controller) {
            qDebug() << "Plugin name" << pluginName;
            qDebug() << loader.errorString();
            QFAIL("Datapath plugin not load");
        }
        m_controller->setAddress(QHostAddress(datapathAddress));
        QSignalSpy spy(m_controller, SIGNAL(connSig()));

        QVERIFY(m_controller->isValid());
        m_controller->conn();

        QCOMPARE(spy.count(), 1);
        QList<QVariant> arguments = spy.takeFirst(); // take the first signal
        QVERIFY(arguments.isEmpty());

        QElapsedTimer t; t.start();
        while (m_controller->status() != status_t::connected &&
               t.elapsed() < std::chrono::duration_cast<std::chrono::milliseconds>(waitConnectTime).count()) {
            QTest::qWait(200);
        }
        if (m_controller->status() != status_t::connected)
            QFAIL("Fail, controller not connected in 10 sec");
        else {
            m_controller->setProperty("pixs_w", pixs_w);
            m_controller->setProperty("pixs_h", pixs_h);
        }
    }
    void testDatapathCycleReconnect()
    {
        QSKIP("Skip", SkipAll);
        using std::chrono::milliseconds;

        if (!m_controller)
            QSKIP("No controller", SkipAll);
        int count = 10;
        for (int i = 0; i <= count; i++) {
            if (m_controller->status() != status_t::connected) {
                QVERIFY2(m_controller->isValid(), "Invalid ip address");
                QSignalSpy spy(m_controller, SIGNAL(connSig()));
                m_controller->conn();

                QCOMPARE(spy.count(), 1);
                QList<QVariant> arguments = spy.takeFirst(); // take the first signal
                QVERIFY(arguments.isEmpty());

                QElapsedTimer t; t.start();
                while (m_controller->status() != status_t::connected && t.elapsed() < std::chrono::duration_cast<milliseconds>(waitConnectTime).count()) {
                    QTest::qWait(200);
                }
                if (m_controller->status() != status_t::connected)
                    QFAIL("Fail, controller not connected in 10 sec");
            }
            if (i != count) {
                QElapsedTimer t; t.start();
                m_controller->disconn();
                while (m_controller->status() != status_t::disc && t.elapsed() < std::chrono::duration_cast<milliseconds>(waitDisconnectTime).count()) {
                    QTest::qWait(200);
                }
                if (m_controller->status() != status_t::disc)
                    QFAIL("Fail, controller not connected in 10 sec");
            }
        }
    }
    void testDatapathClearAll()
    {
        if (!m_controller)
            QSKIP("No controller", SkipAll);
        Sources sources;

        QSignalSpy spy(m_controller, SIGNAL(sceneFinished(bool)));
        m_controller->setScene(sources);

        QElapsedTimer t; t.start();
        while (spy.count() == 0 &&
               t.elapsed() < std::chrono::duration_cast<std::chrono::milliseconds>(waitSetSceneTime).count()) {
            QTest::qWait(200);
        }

        QString failSetScene = QString("Scene is not finished in %1 min").arg(waitSetSceneTime.count());
        QVERIFY2(spy.count() == 1, failSetScene.toAscii());

        QList<QVariant> arguments = spy.takeFirst();
        QVERIFY(arguments.at(0).toBool() == true);
    }
    void testDatapathAddNamedSources()
    {
        QSKIP("### Delete later!!!", SkipAll);
        if (!m_controller)
            QSKIP("No controller", SkipAll);

        // add sources
        {
            Sources sources;
            int id = 1;
            sources << addNamedSource(id++, VPosition({0,0}, {5,5}));
            sources << addNamedSource(id++, VPosition({5,0}, {10,5}));
            sources << addNamedSource(id++, VPosition({0,5}, {5,10}));
            sources << addNamedSource(id++, VPosition({5,5}, {10,10}));

            QSignalSpy spy(m_controller, SIGNAL(sceneFinished(bool)));
            m_controller->setScene(sources);

            QElapsedTimer t; t.start();
            while (spy.count() == 0 &&
                   t.elapsed() < std::chrono::duration_cast<std::chrono::milliseconds>(waitSetSceneTime).count()) {
                QTest::qWait(200);
            }
            QVERIFY2(spy.count() == 1, "Scene is not finished in 10 sec");
            QList<QVariant> arguments = spy.takeFirst();
            auto status = arguments.at(0).toBool();
            QVERIFY2(status, "Scene is not build");
        }
        QTest::qSleep(300);
        // remove 1 source
        {
            Sources sources;
            int id = 1;
            sources << addNamedSource(id++, VPosition({0,0}, {7,7}));
            sources << addNamedSource(id++, VPosition({5,0}, {8,5}));
            //        sources << addNamedSource(id++, VPosition({0,5}, {5,10}));

            QSignalSpy spy(m_controller, SIGNAL(sceneFinished(bool)));
            m_controller->setScene(sources);

            QElapsedTimer t; t.start();
            while (spy.count() == 0 &&
                   t.elapsed() < std::chrono::duration_cast<std::chrono::milliseconds>(waitSetSceneTime).count()) {
                QTest::qWait(200);
            }
            QVERIFY2(spy.count() == 1, "Scene is not finished in 10 sec");
            QList<QVariant> arguments = spy.takeFirst();
            auto status = arguments.at(0).toBool();
            QVERIFY2(status, "Scene is not build");
        }
    }
    void testDatapathManySource()
    {
        QSKIP("### Delete later !!!", SkipAll);
        if (!m_controller)
            QSKIP("No controller", SkipAll);

        Sources sources;
        int x = 0;
        int y = 0;
        int w = 2;
        int h = 2;
        for (int id = 0; id < 15; ++id) {
            sources << addNamedSource(15, VPosition({x,y}, {x+w,y+h}));
            x += 2;
            if (x > 8) {
                x = 0;
                y += 2;
            }
        }
        QSignalSpy spy(m_controller, SIGNAL(sceneFinished(bool)));
        m_controller->setScene(sources);

        QElapsedTimer t; t.start();
        while (spy.count() == 0 &&
               t.elapsed() < std::chrono::duration_cast<std::chrono::milliseconds>(waitSetSceneTime).count()) {
            QTest::qWait(200);
        }

        QString failSetScene = QString("Scene is not finished in %1 min").arg(waitSetSceneTime.count());
        QVERIFY2(spy.count() == 1, failSetScene.toAscii());

        QList<QVariant> arguments = spy.takeFirst();
        auto status = arguments.at(0).toBool();
        QVERIFY2(status, "Scene is not build");
    }
    void testDatapathAddAppSource()
    {
        //    QSKIP("Skip", SkipAll);
        if (!m_controller) {
            QSKIP("No controller", SkipAll);
        }

        {
            Sources sources;
            sources << addAppSources("app1", "notepad", VPosition({0,0}, {5,5}));
            sources << addAppSources("app2", "notepad", VPosition({5,5}, {10,10}));
            sources << addAppSources("app3", "notepad", VPosition({0,0}, {5,5}));
            sources << addAppSources("app4", "notepad", VPosition({5,5}, {10,10}));
            sources << addAppSources("app5", "notepad", VPosition({0,0}, {5,5}));
            sources << addAppSources("app6", "notepad", VPosition({5,5}, {10,10}));
            sources << addAppSources("app7", "notepad", VPosition({0,0}, {5,5}));
            sources << addAppSources("app8", "notepad", VPosition({5,5}, {10,10}));
            QSignalSpy spy(m_controller, SIGNAL(sceneFinished(bool)));
            m_controller->setScene(sources);

            QElapsedTimer t; t.start();
            while (spy.count() == 0 &&
                   t.elapsed() < std::chrono::duration_cast<std::chrono::milliseconds>(waitSetSceneTime).count()) {
                QTest::qWait(200);
            }

            QString failSetScene = QString("Scene is not finished in %1 min").arg(waitSetSceneTime.count());
            QVERIFY2(spy.count() == 1, failSetScene.toAscii());

            QList<QVariant> arguments = spy.takeFirst();
            auto status = arguments.at(0).toBool();
            QVERIFY2(status, "Scene is not build");
        }
        QTest::qSleep(300);
        // resize app source
        {
            Sources sources;
            sources << addAppSources("app1", "notepad", VPosition({0,0}, {8,5}));
            QSignalSpy spy(m_controller, SIGNAL(sceneFinished(bool)));
            m_controller->setScene(sources);

            QElapsedTimer t; t.start();
            while (spy.count() == 0 &&
                   t.elapsed() < std::chrono::duration_cast<std::chrono::milliseconds>(waitSetSceneTime).count()) {
                QTest::qWait(200);
            }

            QString failSetScene = QString("Scene is not finished in %1 min").arg(waitSetSceneTime.count());
            QVERIFY2(spy.count() == 1, failSetScene.toAscii());

            QList<QVariant> arguments = spy.takeFirst();
            auto status = arguments.at(0).toBool();
            QVERIFY2(status, "Scene is not build");
        }
        QTest::qSleep(300);
        // move app source
        {
            Sources sources;
            sources << addAppSources("app1", "notepad", VPosition({0,3}, {8,8}));
            QSignalSpy spy(m_controller, SIGNAL(sceneFinished(bool)));
            m_controller->setScene(sources);

            QElapsedTimer t; t.start();
            while (spy.count() == 0 &&
                   t.elapsed() < std::chrono::duration_cast<std::chrono::milliseconds>(waitSetSceneTime).count()) {
                QTest::qWait(200);
            }

            QString failSetScene = QString("Scene is not finished in %1 min").arg(waitSetSceneTime.count());
            QVERIFY2(spy.count() == 1, failSetScene.toAscii());

            QList<QVariant> arguments = spy.takeFirst();
            auto status = arguments.at(0).toBool();
            QVERIFY2(status, "Scene is not build");
        }
    }
};

#endif // TESTDATAPATH_H

#ifndef SUPPORT_H
#define SUPPORT_H

static void setSourceProperty(VSource* source, const QString &name, int id)
{
    QString logicName = name;
    QString physName = name;

    source->setProperty("logicName", logicName);
    source->setname(logicName);
    source->setProperty("physName",physName);
    source->setPhysName(physName);

    source->setProperty("named", id);
}

static Sources addNamedSource(int id, const VPosition &position)
{
    const QString type = "NamedSource";
    auto src = QSharedPointer<VSource>(VSrcManager::Instance().createVSource(type));
    VSource *source = src.data();
    if(source) {
        const QString name = "named #" + QString::number(id);
        setSourceProperty(source, name, id);
        source->setPosition(position);
        return Sources() << src;
    }
    return Sources();
}

static Sources addAppSources(const QString &name, const QString &app, const VPosition &position)
{
    const QString type = "APPSource";
    auto src = QSharedPointer<VSource>(VSrcManager::Instance().createVSource(type));
    VSource *source = src.data();
    if(source) {
        src->setname(name);
        src->setProperty("Command", app);
        source->setPosition(position);
        return Sources() << src;
    }
    return Sources();
}

#endif // SUPPORT_H

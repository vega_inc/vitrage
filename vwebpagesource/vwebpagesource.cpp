#include "vwebpagesource/vwebpagesource.h"
#include "vwebpagesource/vwebpagethread.h"
#include <QDebug>
#include <QResizeEvent>

#define SZ_POLICY QSizePolicy::Preferred



VWebPageSource::VWebPageSource(VWebPageSource * p)
{
	if(!p) {
	VWebPageThread *th = new VWebPageThread();
    setThread(th); 
	//setname(IPCAM_SOURCE);
	setStarted(true);
    setStatus(VSource::RunStatus);
    } else {
        setThread(p->getThread());
        p->copyProperty(this->m_propMap);
        this->m_images = p->m_images;
        setStatus(p->getStatus());
        setPrototype(p);
        setEnabledFlag(p->getEnabledFlag());
        setPhysName(p->getPhysName());
	}

    this->type = WEBPAGE_SOURCE;
    m_propMap.insert("type",WEBPAGE_SOURCE);
    m_propMap.insert(F1000_TYPE_PROP,VSource::SubSystemKind_CPWeb);
}


void VWebPageSource::start()
{
	QVariant var;
	getProperty(QString("host"),var);
	VWebPageThread *th = static_cast<VWebPageThread *>(m_thread);
	th->setUrl(QUrl(var.toString()));
	setStarted(true);
    if (!m_thread->isRunning()) {
        m_thread->start();
    }
}	


/**
* @brief dest
*/
VWebPageSource::~VWebPageSource()
{
}


/**
* @brief stop main cycle
*/
void VWebPageSource::stop()
{
if (isStarted()) {
	setStarted(false);
	}
}

/**
* @brief resize event function
*
* @param event -[in] QResizeEvent
*/
void VWebPageSource::resizeEvent(QResizeEvent *event)
{
	emit this->setThreadSize(event->size(),event->oldSize());
}



/**
* @brief resize source image
*
* @param w - width uint32_t
* @param h - height uint32_t
*/
void VWebPageSource::resize_src(uint32_t w,uint32_t h) 
{
	resize(w,h);
	setThreadSize(QSize(w,h),QSize(0,0));
}



void VWebPageSource::updateImage()
{
    image = m_thread->Image(size()).copy();
	update();
}

void VWebPageSource::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    if (image.isNull() || image.format() == QImage::Format_Invalid) {
//        qDebug() << "no valid image to paint";
//        m_bare.scaled(size());
//        painter.drawImage(QPoint(0,0),m_bare);
        paintIcons(&painter);
        paintAll(&painter);
        QWidget::paintEvent(event);
        painter.end();
        return;
    }
        painter.drawImage(QPoint(0,0),image);
        paintIcons(&painter);
        paintAll(&painter);
        event->accept();
        painter.end();
}

QSharedPointer<VSource> VWebPageSource::clone()
{
    QSharedPointer<VSource> ip(new VWebPageSource(this));
    return ip;
}

void VWebPageSource::finalize()
{

}

Q_EXPORT_PLUGIN2(VWebPageSource, VWebPageCreator)



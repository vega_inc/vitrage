#ifndef VWEBPAGESOURCE_H
#define VWEBPAGESOURCE_H

#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <QtCore/qplugin.h>
#include <QWebPage>
#include <QWebView>

#include <vsource/vsource.h>

#define WEBPAGE_SOURCE "WebPage"

/**
* @brief class backend source device for web page 
*/
class VWebPageSource : public VSource {
    Q_OBJECT
public:
    VWebPageSource(VWebPageSource *p = 0);
    QSharedPointer<VSource> clone() override;
    ~VWebPageSource();
    void resize_src(uint32_t w,uint32_t h) override;
    void start() override;
    void stop() override;
    void setUrl(QUrl url){Q_UNUSED(url);}
    void finalize() override;
    void resizeEvent(QResizeEvent* event);
    VSourceOptions * getOptionsWindow(void);
    bool isInitialized();
    void setname(QString name) {
        this->name = name;
    }

private:
	QWebView *m_view;
	QWebPage *m_page;
    QTimer * timer;
    QNetworkRequest request;
    QPixmap pixmap_image;
    QImage image;
    void createOptions(void);
    void newImage();

protected:
   void	paintEvent(QPaintEvent *event);

public slots:
	void saveParams(void);
	void updateImage(void);

};


/**
* 	setImages(*currentImage);
    emit updatedImage();
@brief class Creator for IpCamSource
*/
class VWebPageCreator : public QObject , public VSourceCreator {
/* Builder for VWebPageSource */
    Q_OBJECT
    Q_INTERFACES(VSourceCreator)
public:
    VWebPageCreator() {}
   ~VWebPageCreator() {}
    QString getSourceType() {
        return QString(WEBPAGE_SOURCE);
    }

    VSource *createVSource() {
        qDebug("Create VWebPageSource");
        return (new VWebPageSource()); /* returned prototype of source */
    }

    QList<VWebPageCreator::Param> getParamsList()
    {
        QList<Param> lst;
        lst.push_back(Param("host",tr("URL")));
        lst.push_back(Param("icon",(":/web-page.png"),ICON));
        return lst;
    }


};




#endif /* VWEBPAGESOURCE_H */

#include "vwebpagesource/vwebpagethread.h"
#include <QPainter>
#include <QWebPage>
#include <QWebFrame>
#include <QWebView>

#include <QtCore/QCoreApplication>

VWebPageThread::VWebPageThread():
    VSourceThread(),
    w(110),
	h(110)
{
//    moveToThread(this);
    
    timer = new QTimer();
    m_view = new QWebView();
    m_page = m_view->page();
	QWebSettings *sett = m_page->settings();
	sett->setAttribute(QWebSettings::JavascriptEnabled,true);
    //sett->setAttribute(QWebSettings::PluginsEnabled,true);
    //sett->setAttribute(QWebSettings::PrivateBrowsingEnabled,true);
    //sett->setAttribute(QWebSettings::JavascriptCanOpenWindows,true);
    connect(timer,SIGNAL(timeout()),this,SLOT(timerSlot()));
    connect(m_page->mainFrame(),SIGNAL(loadFinished(bool)),this,SLOT(doneLoading(bool)));
	currentImage = new QImage(1280,1024,QImage::Format_RGB888);
//    m_page->setViewportSize(QSize(1280,1024));

}

VWebPageThread::~VWebPageThread()
{
  //delete m_page;
    timer->stop();
    delete m_view;
    delete currentImage;
    delete timer;
}

void VWebPageThread::setUrl(QUrl url)
{
	currentUrl = url;
    QString str = url.toString();
  //  qDebug() << str;
}






void VWebPageThread::run() 
{
    timer->start(100);
//	exec();
}





void VWebPageThread::timerSlot(void)
{
//    qDebug("Timer slot");
	timer->stop();
	sendRequest();
}

void VWebPageThread::newImageGrabbed(QImage *img)
{
	Q_UNUSED(img);
	setImages(*currentImage);
    emit updatedImage();
}





void VWebPageThread::sendRequest()
{
  //  qDebug() << "Start to load page";
    m_page->mainFrame()->load(currentUrl);
 //   qDebug() << "Stop to load page";

}


void VWebPageThread::doneLoading(bool ) 
{
	QImage	 im(m_page->mainFrame()->contentsSize(),QImage::Format_RGB888);
	im.fill(Qt::transparent);

	QPainter painter(&im);
	m_page->setViewportSize(m_page->mainFrame()->contentsSize());
	m_page->mainFrame()->render(&painter);
	painter.end();
	setImages(im);
    emit updatedImage();

    timer->start(100);
}


void VWebPageThread::loadStarted()
{
  //  qDebug() << "Load started";
}






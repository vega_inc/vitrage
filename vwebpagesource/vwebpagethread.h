#ifndef VWEBPAGETHREAD_H
#define VWEBPAGETHREAD_H
#include <QBuffer>
#include <QTimer>
#include <QWebPage>
#include <QWebView>

#include <vsource/vsource.h>

class  VWebPageThread : public VSourceThread {
    Q_OBJECT
public:
    VWebPageThread();
    ~VWebPageThread();
    void setUrl(QUrl  url);
    QString getDefaultSource() {return "http://127.0.0.1:8080/?action=stream";}
 
private slots:
    void timerSlot(void);
    void ImageUpdated(QImage *img);
	void doneLoading(bool);
    void loadStarted();
protected:
    void run();

private:
    QUrl currentUrl;
	void newImageGrabbed(QImage *img);
    void sendRequest();
    int currentImageSize;
    quint64 m_timestampInMs;
    QRegExp m_timestampRegexp;
    QBuffer *imageBuffer;
    QImageReader *imageReader;
    QImage *currentImage;
	QTimer *timer;
	int w;
	int h;
	QWebView *m_view;
	QWebPage *m_page;

};

#endif /* WWEBPAGETHREAD */

#ifndef STREAMHELPER
#define STREAMHELPER

#include <QDataStream>
#include <QTcpSocket>
#include "protocol/message.h"
#include "protocol/response_code.h"

static void QStringToQDataStream(const QString &str, int l, QDataStream& stream)
{
    if (str.isEmpty()) {
        for (int i = 0; i < l ; i++) {
            stream << (quint16)0;
        }
        return;
    }

    wchar_t array[l];

    str.toWCharArray(array);

    for (int i = 0; i < l ; i++) {
        if (i < str.size()) {
            stream << (quint16)array[i];
        } else {
            if (i == str.size()) {
                stream << (quint16)('\r');
            } else if (i == str.size()+1) {
                stream << (quint16)('\n');
            } else {
                stream << (quint16)(0);
            }
        }
    }
}

static void QDataStreamToQString(QDataStream& stream, int32_t l, QString &str)
{
   for (int i = 0; i < l ; i++) {
       quint16 ch;
       stream >> ch ;
       if (ch)
            str.append(QChar(ch));
   }
}

using namespace datapath_message;

/// 5 Common data structures
// 5.2 Style data
static QDataStream & operator<< (QDataStream& stream, const common::style_data& style)
{
    stream << style.window_style;
    QStringToQDataStream(/*style.string_title_bar*/"", 128, stream );
    QStringToQDataStream(/*style.string_tool_tip*/"", 128, stream );
    QStringToQDataStream(/*style.client_string*/"", 128, stream );
    stream << style.client_string_size;
    stream << style.R;
    stream << style.G;
    stream << style.B;
    stream << style.display_position;
    return stream;
}
static QDataStream & operator>> (QDataStream& stream, common::style_data& data)
{
    stream >> data.window_style;
    stream >> data.client_string_size;
    for (qint16 j = 0, c = 0; j < 128; ++j) {
        stream >> c;
        data.string_title_bar[j] = c?c:'\0';
    }
    for (qint16 j = 0, c = 0; j < 128; ++j) {
        stream >> c;
        data.string_tool_tip[j] = c?c:'\0';
    }
    for (qint16 j = 0, c = 0; j < 128; ++j) {
        stream >> c;
        data.client_string[j] = c?c:'\0';
    }
    stream >> data.R;
    stream >> data.G;
    stream >> data.B;
    stream >> data.display_position;
    return stream;
}
// 5.3 Fit data
static QDataStream & operator<< (QDataStream& stream, const common::fit_data& fit)
{
    stream << fit.on;
    stream << fit.pos_hor;
    stream << fit.pos_ver;
    stream << fit.width;
    stream << fit.height;
    stream << fit.grid_interval;
    stream << fit.aspect_ratio;
    stream << fit.reserve;
    return stream;
}
// 5.4 Crop data
static QDataStream & operator<< (QDataStream& stream, const common::crop_data& crop)
{
    stream << crop.pos_hor;
    stream << crop.pos_ver;
    stream << crop.size_hor;
    stream << crop.size_ver;
    return stream;
}
static QDataStream & operator>> (QDataStream& in, common::crop_data& data)
{
    in >> data.pos_hor;
    in >> data.pos_ver;
    in >> data.size_hor;
    in >> data.size_ver;
    return in;
}
// 5.5. ServerPC overlay window data
static QDataStream & operator>> (QDataStream& in, common::serverPC_overlay_window_data& data)
{
    in >> data.number;
    in >> data.status;
    in >> data.image_display_status;
    in >> data.type;
    in >> data.image_source_number;
    in >> data.x;
    in >> data.y;
    in >> data.w;
    in >> data.h;
    in >> data.X;
    in >> data.Y;
    in >> data.W;
    in >> data.H;
    in >> data.muting;
    in >> data.fixed_display;
    in >> data.const_aspect_ratio_flag;
    in >> data.always_on_top;
    in >> data.reserve1;
    in >> data.reserve2;
    in >> data.reserve3;
    in >> data.style;
    in >> data.crop;
    return in;
} // 888 bytes
// 5.11 ServerPC overlay window source data
static QDataStream & operator<< (QDataStream& stream, const common::serverPC_window_source_data& data)
{
    stream << data.source_number;
    QStringToQDataStream(data.source_name, 64, stream);
    stream << data.source_type;
    stream << data.display_method;
    stream << data.input_port;
    stream << data.connection_dest_device;
    stream << data.board_type;
    stream << data.input_channel;
    stream << data.switcher_number;
    stream << data.switcher_input_channel;
    stream << data.reserve1;
    stream << data.distributor_number;
    stream << data.r1;
    stream << data.r2;
    stream << data.r3;
    stream << data.r4;
    stream << data.r5;
    stream << data.r6;
    stream << data.r7;
    stream << data.r8;
    stream << data.r9;
    stream << data.r10;
    stream << data.r11;
    stream << data.r12;
    stream << data.r13;
    stream << data.previous_size_width;
    stream << data.previous_size_height;
    stream << data.previous_constant_aspect_ratio;
    stream << data.crop;
    stream << data.reserve2;
    stream << data.previous_position_hor;
    stream << data.previous_position_ver;
    stream << data.rr1;
    stream << data.rr2;
    stream << data.rr3;
    stream << data.rr4;
    stream << data.rr5;
    stream << data.rr6;
    stream << data.rr7;
    stream << data.rr8;

    return stream;
}
static QDataStream & operator>> (QDataStream& in, common::serverPC_window_source_data& data)
{
    in >> data.source_number;
    QDataStreamToQString(in, 64, data.source_name);
    in >> data.source_type;
    in >> data.display_method;
    in >> data.input_port;
    in >> data.connection_dest_device;
    in >> data.board_type;
    in >> data.input_channel;
    in >> data.switcher_number;
    in >> data.switcher_input_channel;
    in >> data.reserve1;
    in >> data.distributor_number;
    in >> data.r1;
    in >> data.r2;
    in >> data.r3;
    in >> data.r4;
    in >> data.r5;
    in >> data.r6;
    in >> data.r7;
    in >> data.r8;
    in >> data.r9;
    in >> data.r10;
    in >> data.r11;
    in >> data.r12;
    in >> data.r13;
    in >> data.previous_size_width;
    in >> data.previous_size_height;
    in >> data.previous_constant_aspect_ratio;
    in >> data.crop;
    in >> data.reserve2;
    in >> data.previous_position_hor;
    in >> data.previous_position_ver;
    in >> data.rr1;
    in >> data.rr2;
    in >> data.rr3;
    in >> data.rr4;
    in >> data.rr5;
    in >> data.rr6;
    in >> data.rr7;
    in >> data.rr8;
    return in;
}
// 5.19. Application window data
static QDataStream & operator>> (QDataStream& stream, application::application_window_data& response)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    stream >> response.handle;
    stream >> response.status;
    stream >> response.type;
    stream >> response.source_number;
    stream >> response.x;
    stream >> response.y;
    stream >> response.w;
    stream >> response.h;
    stream >> response.r1;
    stream >> response.r2;
    stream >> response.r3;
    stream >> response.r4;
    stream >> response.r5;
    QDataStreamToQString(stream, 512, response.command);
    QDataStreamToQString(stream, 260, response.start_in);
    QDataStreamToQString(stream, 256, response.title);
    QDataStreamToQString(stream, 128, response.class_name);
    return stream;
}

/// 3.2 ServerPC overlay window function
// 3.2.1 [ServerPC overlay window function]
inline static QDataStream & operator<< (QDataStream& stream, const serverPC::new_window_creation& request)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    header h = { 912 };
    stream << h.data_length;
    stream << request.code;
    stream << request.process_number;
    stream << request.image_window_number;
    stream << request.window_type;
    stream << request.source_number;
    stream << request.display_start_position_hor;
    stream << request.display_start_position_ver;
    stream << request.display_width;
    stream << request.display_height;
    stream << request.position_size_settings;
    stream << request.const_aspect_ratio;
    stream << request.max_min;
    stream << request.display_priority_seq;
    stream << request.fixed_display;
    stream << request.muting;
    stream << request.reserve1;
    stream << request.notification_flag;
    stream << request.reserve2;
    stream << request.style;
    stream << request.fit;
    stream << request.crop;
    return stream;
}
inline static QDataStream & operator>> (QDataStream& stream, serverPC::new_window_creation_response& response)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    uint32_t dummyHeader = 0;
    stream >> dummyHeader;
    stream >> response.code;
    serverPC::new_window_creation empty;
    if (response.code != empty.code)
        return stream;
    stream >> response.status;
    stream >> response.windows_number;
    stream >> response.window_type;
    return stream;
}
// 3.2.2. [ServerPC overlay window function] Window movement, resizing, priority sequence change, fit, cropping and style change
inline static QDataStream & operator<< (QDataStream& stream, const serverPC::window_move_resize_etc& request)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    header h = { 900 };
    stream << h.data_length;
    stream << static_cast<int32_t>(request.code);
    stream << request.process_number;
    stream << request.image_window_number;
    stream << request.type;
    stream << request.image_source_number;
    stream << request.x;
    stream << request.y;
    stream << request.w;
    stream << request.h;
    stream << request.position_and_size_settings;
    stream << request.const_aspect_ratio;

    if (request.code == serverPC::type_code::movement) {
        stream << (int32_t)0x7FFFFFFF;
        stream << (int32_t)1;
        stream << (int32_t)0;
    }
    else {
        stream << request.reserve;
        stream << request.display_priority_seq;
        stream << request.operation_fixed_display;
    }
    stream << request.style;
    stream << request.fit;
    stream << request.crop;
    stream << request.always_on_top;
    return stream;
}

inline static QDataStream & operator>> (QDataStream& in, serverPC::window_move_resize_etc_response& response)
{
    in.setByteOrder(QDataStream::LittleEndian);
    uint32_t header = 0;
    in >> header;
    if (header != 8)
        return in;
    int code;
    in >> code;
    response.code = static_cast<serverPC::type_code>(code);
    serverPC::window_move_resize_etc_response empty;
    if (response.code != empty.code)
        return in;
    in >> response.status;
    return in;
}

// 3.2.3 [ServerPC overlay window function]
inline static QDataStream & operator<< (QDataStream& stream, const serverPC::window_close& request)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    header h = { 16 };
    stream << h.data_length;
    stream << request.code;
    stream << request.process_number;
    stream << request.window_number;
    stream << request.window_type;
    return stream;
}
inline static QDataStream & operator>> (QDataStream& in, serverPC::window_close_response& response)
{
    in.setByteOrder(QDataStream::LittleEndian);
    uint32_t header = 0;
    in >> header;
    if (header != 8)
        return in;
    in >> response.code;
    serverPC::window_close empty;
    if (response.code != empty.code)
        return in;
    in >> response.status;
    return in;
}
// 3.2.10 [ServerPC overlay window function]
inline static QDataStream & operator<< (QDataStream& out, const serverPC::window_source_addition& request)
{
    out.setByteOrder(QDataStream::LittleEndian);
    header h = { 312 };
    out << h.data_length;
    out << request.code;
    out << request.process_number;
    out << request.reserve;
    out << request.source_type;
    out << request.data;
    return out;
}
inline static QDataStream & operator>> (QDataStream& stream, serverPC::window_source_addition_response& response)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    uint32_t header = 0;
    stream >> header;
    if (header != 16)
        return stream;
    stream >> response.code;
    serverPC::window_source_addition empty;
    if (response.code != empty.code)
        return stream;
    stream >> response.status;
    stream >> response.source_number;
    stream >> response.reserve;
    return stream;
}
// 3.2.7. [ServerPC overlay window function] Window status acquisition
inline static QDataStream & operator<< (QDataStream& stream, const serverPC::window_status_acquisition& request)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    header h = { 16 };
    stream << h.data_length;
    stream << request.code;
    stream << request.process_number;
    stream << request.window_number;
    stream << request.window_type;
    return stream;
}
inline static QDataStream & operator>> (QDataStream& stream, serverPC::window_status_acquisition_response& response)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    uint32_t header = 0;
    stream >> header;
    if (header < 16)
        return stream;
    stream >> response.code;
    serverPC::window_status_acquisition empty;
    if (response.code != empty.code)
        return stream;
    stream >> response.status;
    stream >> response.number;
    stream >> response.data_length;
    response.data.clear();
    if (response.number == 0) {
        response.data_length = 888;
        response.data.resize(1);
        stream >> response.data[0];
        response.data.clear();
        return stream;
    }
    response.data.resize(response.number);
    for (int i = 0; i < response.number; i++) {
        stream >> response.data[i];
    }
    return stream;
}
// 3.2.14 [ServerPC overlay window function]
inline static QDataStream & operator<< (QDataStream& stream, const serverPC::acq_window_source_data& request)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    header h = { 24 };
    stream << h.data_length;
    stream << request.code;
    stream << request.process_number;
    stream << request.source_number;
    stream << request.window_type;
    stream << request.source_type;
    stream << request.reserve;
    return stream;
}
inline static QDataStream & operator>> (QDataStream& stream, serverPC::acq_window_source_data_response& response)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    uint32_t header = 0;
    stream >> header;
    if (header < 312)
        return stream;
    stream >> response.code;
    serverPC::acq_window_source_data empty;
    if (response.code != empty.code)
        return stream;
    stream >> response.status;
    stream >> response.number_sources;
    stream >> response.data_length;
    if (response.number_sources == 0)
        response.data_length = 296;
    response.data.resize(response.number_sources);
    for (int i = 0; i < response.number_sources; i++) {
        stream >> response.data[i];
    }
    return stream;
}
// 3.2.15 [ServerPC overlay window function] ServerPC test pattern display
inline static QDataStream & operator<< (QDataStream& out, const serverPC::test_pattern_display& request)
{
    out.setByteOrder(QDataStream::LittleEndian);
    header h = { 16 };
    out << h.data_length;
    out << request.code;
    out << request.process_number;
    out << request.on_off;
    out << request.pattern_number;
    return out;
}
inline static QDataStream & operator>> (QDataStream& in, serverPC::test_pattern_display_response& response)
{
    in.setByteOrder(QDataStream::LittleEndian);
    uint32_t header = 0;
    in >> header;
    if (header != 8)
        return in;
    in >> response.code;
    serverPC::test_pattern_display empty;
    if (response.code != empty.code)
        return in;
    in >> response.status;
    return in;
}

/// 3.13 User
inline static QDataStream & operator<< (QDataStream& stream, const login::login1& request)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    header h = { 272 };
    stream << h.data_length;
    stream << request.code;
    stream << request.process_number;
    stream << request.reserve1;
    stream << request.reserve2;
    stream.writeRawData(request.username.toAscii(), 128);
    stream.writeRawData(request.password.toAscii(), 128);
    return stream;
}
inline static QDataStream & operator<< (QDataStream& stream, const login::login2& request)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    header h;
    h.data_length = 16 + request.username.size()*2 + request.password.size()*2;
    stream << h.data_length;
    stream << request.code;
    stream << request.process_number;
    stream << request.username_length*2;
    stream << request.password_length*2;
    QStringToQDataStream(request.username, request.username_length, stream );
    QStringToQDataStream(request.password, request.password_length, stream );
    return stream;
}
inline static QDataStream & operator>> (QDataStream& stream, login::response& response)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    uint32_t header = 0;
    stream >> header;
    if (header != 16)
        return stream;
    stream >> response.code;
    login::response empty;
    if (response.code != empty.code)
        return stream;
    stream >> response.status;
    stream >> response.user_id;
    stream >> response.reserve;
    return stream;
}

/// 3.4 Display
// 3.4.27. [Monitor overlay window function] Acquisition of window status
inline static QDataStream & operator<< (QDataStream& out, const display::acq_window_status& request)
{
    out.setByteOrder(QDataStream::LittleEndian);
    header h = { 16 };
    out << h.data_length;
    out << request.code;
    out << request.process_number;
    out << request.window_number;
    out << request.window_type;
    return out;
}

/// 3.5 Layout
inline static QDataStream & operator<< (QDataStream& stream, const layout::open &data)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    header h = { 16 };
    stream << h.data_length;
    stream << data.code;
    stream << data.process_number;
    stream << data.number;
    stream << data.reserve;
    return stream;
}
inline static QDataStream & operator>> (QDataStream& stream, layout::open_response& response)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    stream >> response.code;
    layout::open_response empty;
    if (response.code != empty.code)
        return stream;
    stream >> response.status;
    return stream;
}

inline static QDataStream & operator<< (QDataStream& stream, const layout::saving &data)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    header h = { 144 };
    stream << h.data_length;
    stream << data.code;
    stream << data.process_number;
    stream << data.layout_number;
    stream << data.overwrite;
    QStringToQDataStream(data.layout_name, 64, stream);
    return stream;
}
inline static QDataStream & operator>> (QDataStream& stream, layout::saving_response& response)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    stream >> response.code;
    layout::saving_response empty;
    if (response.code != empty.code)
        return stream;
    stream >> response.status;
    stream >> response.layout_number;
    stream >> response.reserve;
    return stream;
}

inline static QDataStream & operator<< (QDataStream& stream, const layout::acquisition_list &data)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    header h = { 8 };
    stream << h.data_length;
    stream << data.code;
    stream << data.process_number;
    return stream;
}
static QDataStream & operator>> (QDataStream& stream, layout::layout_info& response)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    stream >> response.number;
    stream >> response.reserve;

    return stream;
}

inline static QDataStream & operator>> (QDataStream& stream, layout::acquisition_list_re& response)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    stream >> response.code;
    layout::acquisition_list empty;
    if (response.code != empty.code)
        return stream;
    stream >> response.status;
    stream >> response.number;
    stream >> response.data_length;
    stream >> response.r1;
    stream >> response.r2;
    stream >> response.r3;
    stream >> response.r4;
    int n = (response.data_length == 0)?1:response.data_length;
    response.info.resize(n);
    std::vector<layout::layout_info> info;
    for (int i = 0; i < n; i++) {
        stream >> info.at(i);
    }
    return stream;
}
inline static QDataStream & operator<< (QDataStream& stream, common::fit_data& request)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    stream << request.on;
    stream << request.pos_hor;
    stream << request.pos_ver;
    stream << request.width;
    stream << request.height;
    stream << request.grid_interval;
    stream << request.aspect_ratio;
    stream << request.reserve;
    return stream;
}

/// 3.8 Application window
// 3.8.1. [Application window] New window creation (execution)
inline static QDataStream & operator<< (QDataStream& stream, const application::new_window_creation& request)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    header h = { 1624};
    stream << h.data_length;
    stream << request.code;
    stream << request.process_number;
    stream << request.source_number;
    stream << request.display_start_position_hor;
    stream << request.display_start_position_ver;
    stream << request.display_width;
    stream << request.display_height;
    stream << request.maximaze_flag;
    stream << request.priotiy;
    stream << request.reserved1;
    stream << request.reserved2;
    QStringToQDataStream(request.command,request.command_size,stream );
    QStringToQDataStream(request.start_in,request.start_in_size,stream );
    stream << request.start_time;
    stream << request.fit;
    return stream;
}
inline static QDataStream & operator>> (QDataStream& stream, application::new_window_creation_response& response)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    uint32_t dummyHeader = 0;
    stream >> dummyHeader;
    stream >> response.code;
    stream >> response.status;
    return stream;
}
// 3.8.5. [Application window] Acquisition of application window status
inline static QDataStream & operator>> (QDataStream& stream, application::acquisition_windows_re& response)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    uint32_t dummyHeader = 0;
    stream >> dummyHeader;
    stream >> response.code;
    application::acquisition_windows_re empty;
    if (response.code != empty.code)
        return stream;
    stream >> response.status;
    stream >> response.number;
    stream >> response.lenght;
    auto n = response.number;
    if (n > 0)
        response.info.resize(n);
    else
        response.info.resize(1);
    for (uint i = 0; i < n ; i++) {
        application::application_window_data awd;
        stream >> awd;
        response.info.push_back(awd);
    }
    if (response.number == 0)
        response.info.clear();
    if (n == 0) response.info.clear();
    return stream;
}

// 3.8.10. [Application window] Acquisition of application window source
inline static QDataStream & operator<< (QDataStream& stream, const application::acquisition_sources& request)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    header h = { 16 };
    stream << h.data_length;
    stream << request.code;
    stream << request.process;
    stream << request.source_numbler;
    stream << request.reserved;
    return stream;
}

inline static QDataStream & operator>> (QDataStream& stream, application::application_source& response)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    stream >> response.source_number;
    QDataStreamToQString(stream,64,response.source_name);
    QDataStreamToQString(stream,512,response.command_line);
    QDataStreamToQString(stream,260,response.start_in);
    QDataStreamToQString(stream,256,response.title);
    QDataStreamToQString(stream,128,response.window_class);
    stream >> response.start_time;
    return stream;
}

inline static QDataStream & operator>> (QDataStream& stream, application::acquisition_source_rep& response)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    uint32_t dummyHeader = 0;
    stream >> dummyHeader;
    stream >> response.command;
    stream >> response.status;
    stream >> response.number;
    stream >> response.length;
    for (int i = 0; i < response.number; i++) {
        application::application_source src;
        stream >> src;
        response.sources.push_back(src);
    }
    return stream;
}

/// 3.7 Server
inline static QDataStream & operator<< (QDataStream& stream, const server::acquisition_window_list& request)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    header h = { 24};
    stream << h.data_length;
    stream << request.code;
    stream << request.process_number;
    stream << request.widnowHandle;
    stream << request.r1;
    stream << request.r2;
    return stream;
}

inline static QDataStream & operator>> (QDataStream& stream, server::window_data& response)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    stream >> response.hadle;
    stream >> response.number;
    stream >> response.status;

    stream >> response.type;
    stream >> response.source_number;
    stream >> response.source_type;
    QDataStreamToQString(stream, 256, response.titleQS);
    stream >> response.start_position_hor;
    stream >> response.start_position_ver;
    stream >> response.width;
    stream >> response.height;
    stream >> response.start_position_hor_nf;
    stream >> response.start_position_ver_nf;
    stream >> response.width_nf;
    stream >> response.height_nf;
    stream >> response.muting;
    stream >> response.fixed_display;
    stream >> response.ratio_w;
    stream >> response.ratio_h;
    stream >> response.ratio_f;
    stream >> response.on_top;
    stream >> response.r1;
    return stream;
}



inline static QDataStream &parseServerWindows (QDataStream& stream, serverPC::window_status_acquisition_response& response, QTcpSocket *sock)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    uint32_t header = 0;
    stream >> header;
    if (header < 16)
        return stream;
    stream >> response.code;
    serverPC::window_status_acquisition empty;
    if (response.code != empty.code)
        return stream;
    stream >> response.status;
    stream >> response.number;
    stream >> response.data_length;
    response.data.clear();
    if (response.number == 0) {
        while (sock->bytesAvailable() < 888) {
            if (!sock->waitForReadyRead(1000)) {
                sock->disconnectFromHost();
                break;
            }
        }
        response.data_length = 888;
        response.data.resize(1);
        stream >> response.data[0];
        response.data.clear();
        return stream;
    }
    response.data.resize(response.number);
    for (int i = 0; i < response.number; i++) {
        while (sock->bytesAvailable() < 888) {
            if (!sock->waitForReadyRead(1000)) {
                sock->disconnectFromHost();
                break;
            }
        }
        stream >> response.data[i];
    }
    return stream;
}







//static QDataStream & parseServerWindows(QDataStream& stream, datapath_message::serverPC::window_status_acquisition_response& response,
//                                 QTcpSocket *sock)
//{
//    stream.setByteOrder(QDataStream::LittleEndian);
//    uint32_t header = 0;
//    stream >> header;
//    if (header < 616) {
//        response.status = -1;
//        return stream;
//    }
//    stream >> response.code;
//    stream >> response.status;
//    stream >> response.number;
//    stream >> response.data_length;
//    qDebug() << "Parse " << response.data_length << response.number << response.status << response.code;
//    int stop = response.data_length/600;
//    if (response.number == 0) stop = 1;
//    for (int i = 0; i < stop ; i++) {
//        while (sock->bytesAvailable() < 600) {
//            if (!sock->waitForReadyRead(1000)) {
//                sock->disconnectFromHost();
//                break;
//            }
//        }
//        server::window_data wd;
//        stream >> wd;
//        response.info.push_back(wd);
//    }
//    if (!response.number)
//        response.info.clear();

//    return stream;
//}

inline static QDataStream & operator<< (QDataStream& stream, const move_resize_fit& request)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    header h = { 72 };
    stream << h.data_length;
    stream << static_cast<quint32>(request.message_type);
    stream << request.process_number;
    stream << request.handle;
    stream << request.x;
    stream << request.y;
    stream << request.w;
    stream << request.h;
    stream << request.priority;
    stream << request.reserve;
    stream << request.fit;
    return stream;
}

inline static QDataStream & operator<< (QDataStream& stream, application::window_deletion& request)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    header h = { 24 };
    stream << h.data_length;
    stream << request.code;
    stream << request.process;
    stream << request.handle;
    stream << request.r1;
    stream << request.r2;
    return stream;
}

inline static QDataStream & operator<< (QDataStream& stream, application::adding_application_source& request)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    header h = { 2536 };
    stream << h.data_length;
    stream << request.type;
    stream << request.process;
    stream << request.r1;
    stream << request.r2;
    stream << quint32(0);
    QStringToQDataStream(request.source_data.source_name,64 ,stream);
    QStringToQDataStream(request.source_data.command_line,512 ,stream);
    QStringToQDataStream(request.source_data.start_in,260 ,stream);
    QStringToQDataStream(request.source_data.title,256 ,stream);
    QStringToQDataStream(request.source_data.window_class,128 ,stream);
    stream << request.source_data.start_time;
    stream << qint32(0);
    stream << qint32(0);
    char rr[16*4] = {0, };
    stream.writeBytes(rr,16*4);

    return stream;
}

inline static QDataStream & operator<< (QDataStream& stream, application::acquisition_windows& request)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    header h = { 24 };
    stream << h.data_length;
    stream << request.type;
    stream << request.process;
    stream << request.handle;
    stream << request.r1;
    stream << request.r2;
    return stream;
}

inline static QDataStream & parseWindows(QDataStream& stream, application::acquisition_windows_re& response, QTcpSocket *sock)
{
    stream.setByteOrder(QDataStream::LittleEndian);
    uint32_t dummyHeader = 0;
    stream >> dummyHeader;
    stream >> response.code;
    stream >> response.status;
    stream >> response.number;
    stream >> response.lenght;
    int stop = response.lenght/2368;
    if (response.number == 0) stop = 1;
    for (int i = 0; i < stop ; i++) {
        while (sock->bytesAvailable() < 2368) {
            if (!sock->waitForReadyRead(1000)) {
                sock->disconnectFromHost();
                break;
            }
        }
        application::application_window_data wd;
        stream >> wd;
        response.info.push_back(wd);
    }
    if (response.number == 0)
        response.info.clear();

    return stream;
}

#endif // STREAMHELPER

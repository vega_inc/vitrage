#ifndef DATAPATH_H
#define DATAPATH_H

#include <vcontroller/vcontroller.h>
#include <QTcpSocket>
#include <QHostAddress>
#include <QObject>
#include <QTimer>
#include <QDataStream>
#include "protocol/message.h"
#include <atomic>

class Datapath : public VController
{
    Q_OBJECT
    Q_INTERFACES(AbstractVController)
public:
    enum class WINDOW_TYPE {RGB = 1, VIDEO = 2, SDI = 13};
    Datapath();
    ~Datapath();
    void conn() override;
    void disconn() override;
    bool createSource(VSource &) override;
    bool startSource(VSource &) override;
    bool stopSource(VSource &) override;
    bool enableSourceFlag(VSource &,bool) override;
    bool setScene(const Sources &) override;
    VController *clone() override;
    status_t status() const override;
    int move_resize_fit(datapath_message::window_operation operation, quint64 handle, const QRect &rect, int order = 1);
    bool compareFrames(datapath_message::application_window_source window, QSharedPointer<VSource> src);
    bool compareSimple(QString window, QSharedPointer<VSource> src);
    bool getSourcesInfo(quint32 number,datapath_message::application::acquisition_source_rep &response);

    bool getSourcesForWindows();
    bool compareSize(datapath_message::application_window_source window, QSharedPointer<VSource> src);
    int closeWindowApp(datapath_message::application::application_window_data s);
    bool createSources();
    bool replaceSource(QSharedPointer<VSource> src, quint64 i);
    int createApplicationSource(QSharedPointer<VSource> src);
    bool reconnect();
    bool checkAndReconnect();
    void run();
    int getAllAppWindows(datapath_message::application::acquisition_windows_re &result);
    bool compareApplication(datapath_message::application::application_window_data window, QSharedPointer<VSource> src);
    bool compareApplicationSize(datapath_message::application::application_window_data window, QSharedPointer<VSource> src);
    int createApplicationWindow(const QSharedPointer<VSource> &src);
    int createNamedWindow(const QSharedPointer<VSource> &src);
    int getAllServerPCWindows();
    bool compareNamed(datapath_message::server::window_data w, QSharedPointer<VSource> src);
    bool compareNamed(datapath_message::common::serverPC_overlay_window_data data, QSharedPointer<VSource> src);
    bool compareNamdeSize(datapath_message::server::window_data window, QSharedPointer<VSource> src);
    int closeNamedApp(const datapath_message::common::serverPC_overlay_window_data &data);
    bool replaceLayers();
    bool isVisited(const QSharedPointer<VSource> &src) const;
    void setVisited(QSharedPointer<VSource> src, bool visited);
    void setListPosition(QSharedPointer<VSource> src,int i);
    int listPosition(const QSharedPointer<VSource> &src) const;
    void setHandler(QSharedPointer<VSource> src, int handler);
    int handler(QSharedPointer<VSource> src);
private slots:
    void onDisconnect();
    void displayError(QAbstractSocket::SocketError error);
    void getNotification();
    void setSceneSlot();
    void onTimeout();
    void onStateChanged();
    bool connSlot();
signals:
    void setSceneSig();
    void connSig();
private:
    QList<QSharedPointer<VSource> > m_sources; /*list for set Scene method */
    QTcpSocket m_socket_control;
    QTcpSocket m_socket_notification;
    const uint m_port_control = 20200;
    const uint m_port_notification = 20201;
    std::atomic<bool> m_busy;
    QTimer timer;
    QVector<VPosition> positions;

    bool checkResponse(int code);
    datapath_message::serverPC::window_status_acquisition_response m_Windows;
    datapath_message::application::acquisition_windows_re m_AppWindows;
    QVector<datapath_message::application_window_source> m_Applications;
    void printWindowStatus(QDataStream &in);
    int saveLayout();
    int openLayout();
    int closeAppAll();
    int closeServerPCAll(WINDOW_TYPE windowType);
    std::pair<int, QRect> windowStatusAcquisition(int number);
    int appStatusAcquisition(QRect &rect, int number);
    int moveResizeEtcNamedSource(datapath_message::serverPC::type_code code, int number, const QRect &rect);
    int sendNoOperation();
    QMutex m_mutex;
    void emitSceneFinished(bool f);
};

#endif // DATAPATH_H

#ifndef MESSAGE
#define MESSAGE

#include <QString>
#include <cassert>

namespace datapath_message {

struct header
{
    uint32_t data_length;
};

/// 5 Common data structures
namespace common {
// 5.2 Style data
struct style_data
{
    /* 0 - with frame and title bar
     * 1 - with frame, without title bar
     * 2 - without frame and title bar */
    int window_style = 2;
    wchar_t string_title_bar[128];
    wchar_t string_tool_tip[128];
    wchar_t client_string[128];
    int client_string_size = 0x7FFFFFFF;     // 1 - small, 2 medium, 3 - large
    int R=0x7FFFFFFF;
    int G=0x7FFFFFFF;
    int B=0x7FFFFFFF; // 0-255
    /* display position of client char string
     * 0 - 0x7FFFFFFF upper left
     * 1 - top center
     * 2 - upper right
     * 6 - lower left
     * 7 - bottom center
     * 8 - lower right */
    int display_position = 0x7FFFFFFF;
};// 792 bytes
// 5.3 Fit data
struct fit_data
{
    int on = 0x7FFFFFFF; // 0 - not fitted, 1 - fitted
    int pos_hor = 0x7FFFFFFF;
    int pos_ver = 0x7FFFFFFF;
    int width = 0x7FFFFFFF;
    int height = 0x7FFFFFFF;
    int grid_interval = 0x7FFFFFFF; // 0 - 1/1 screen, 1 - 1/2, 2 - 1/3, 3 - 1/4
    int aspect_ratio = 0x7FFFFFFF; // 0 - fit to display, 1 - is retained
    int reserve = 0;
};// 32 bytes
// 5.4 Crop data
struct crop_data
{
    int pos_hor = 0x7FFFFFFF; // 0-999
    int pos_ver = 0x7FFFFFFF; // 0-999
    int size_hor = 0x7FFFFFFF; // 1-1000
    int size_ver = 0x7FFFFFFF; // 1-1000
};// 16 bytes
// 5.5. ServerPC overlay window data
struct serverPC_overlay_window_data
{
    int32_t number; // 1 to MAX
    /* 0: Not displayed
     * 1: Displayed
     * 2: Minimized
     * 3: Maximized */
    int32_t status;
    /* 0: Displayed using the fixed memory
     * 1: Automatic scanning
     * 2: Detection value
     * 3: No signal
     * 4: Outside range */
    int32_t image_display_status;
    int32_t type; // 1: RGB overlay, 2: Video overlay, 13: SDI overlay
    int32_t image_source_number; // 1-512
    wchar_t title[256];
    int32_t x;
    int32_t y;
    int32_t w;
    int32_t h;
    int32_t X;
    int32_t Y;
    int32_t W;
    int32_t H;
    int32_t muting;
    int32_t fixed_display;
    int32_t const_aspect_ratio_flag;
    int32_t always_on_top;
    int32_t reserve1 = 0;
    int32_t reserve2 = 0;
    int32_t reserve3 = 0;
    common::style_data style;
    common::crop_data crop;
}; // 888 bytes
// 5.8. Window data
struct window_data
{
    quint64 handle;
    int32_t number;
    int32_t status;
    int32_t type;
    int32_t image_source_number;
    int32_t image_source_type;
    wchar_t title[256];
    int32_t x;
    int32_t y;
    int32_t w;
    int32_t h;
    int32_t X;
    int32_t Y;
    int32_t W;
    int32_t H;
    int32_t muting;
    int32_t fixed_display;
    int32_t aspect_ratio_w;
    int32_t aspect_ratio_h;
    int32_t const_aspect_ratio_flag;
    int32_t always_on_top;
    int32_t reserve = 0;
}; // 600 bytes
// 5.11 ServerPC overlay window source data
struct serverPC_window_source_data
{
    int source_number = 0; // 0: Automatic, 1-512: source number
    QString source_name = "New serverPC overlay window";
    int source_type = 1; // 1:RGB, 2:Video, 4:SDI
    int display_method = 1; // 0: no automatic scan, 1: automatic scan
    int input_port = 0; // 0: Composite, 1: Y/C, 0x7FFFFFFF
    int connection_dest_device = 1; // 0: None, 1: ServerPC, 4: Switcher, 0x7FFFFFFF
    int board_type = 1;
    int input_channel = 0;
    int switcher_number = 1;
    int switcher_input_channel = 1;
    int reserve1 = 0;
    int distributor_number = 1; // 1-38
    int r1 = 0;
    int r2 = 0;
    int r3 = 0;
    int r4 = 0;
    int r5 = 0;
    int r6 = 0;
    int r7 = 0;
    int r8 = 0;
    int r9 = 0;
    int r10 = 0;
    int r11 = 0;
    int r12 = 0;
    int r13 = 0;
    int previous_size_width = 0;
    int previous_size_height = 0;
    int previous_constant_aspect_ratio = 0;
    crop_data crop;
    int reserve2 = 0;
    int previous_position_hor = 0;
    int previous_position_ver = 0;
    int rr1 = 0;
    int rr2 = 0;
    int rr3 = 0;
    int rr4 = 0;
    int rr5 = 0;
    int rr6 = 0;
    int rr7 = 0;
    int rr8 = 0;
};
// 5.26. Monitor overlay window data
struct monitor_overlay_window_data
{
    int number;
    int status;
    int type;
    int image_source_number;
    int x,y,w,h; // including frame
    int X,Y,W,H; // excluding frame
    int muting;
    int fixed_display;
    int const_aspect_ratio;
    int bezel_mode;
    int always_on_top;
    int r1;
    int r2;
    int r3;
    common::style_data style;
    common::crop_data crop;
}; // 888 bytes
// 5.35 Video and audio output connection information
struct connection_dest_device
{
    int connection = -1; // 0 - no connection, 4 - matrix
    int matrix_switch_number = 0; // 1-16
    int matrix_switch_output_channel_number = 0; // 1-4096
    int reserve = 0;
}; // 16 bytes
// 5.36 Video and audio output source data
struct source_data
{
    int source_number = -1;
    int reserve1 = 0;
    QString name; // 128 bytes
    int reserve2 = 0;
    connection_dest_device device; // 16 bytes
    int reserve3 = 0;
    int dest_number1_32 = 0; // 0 - invalid, 1 - valid
    int dest_number33_64 = 0; // 0 - invalid, 1 - valid
    int reserve4 = 0;
    int reserve5 = 0;
    int reserve6 = 0;
    int reserve7 = 0;
    int reserve8 = 0;
    int reserve9 = 0;
}; // 192 bytes
} // end of namespace

/// 3.1. General
namespace general {
// 3.1.1. [ALL] No operation
struct no_operation
{
    uint32_t code = 0x00000000;
    int32_t number = 0x00000000;
}; // 8 bytes
} // end of namespace General

/// 3.2 ServerPC overlay window function
namespace serverPC {
// 3.2.1 [ServerPC overlay window function] New window creation
struct new_window_creation
{
    int code = 0x00000001;
    int process_number = 0x00000001;
    int image_window_number = 0;                // 0 - Automatic
    int window_type = 1;                        // 1 - RGB, 2 - Video, 13 - SDI
    int source_number = 1;                      // 1-512
    int display_start_position_hor = 0x7FFFFFFF;// default - Automatic
    int display_start_position_ver = 0x7FFFFFFF;// default - Automatic
    int display_width = 0x7FFFFFFF;             // default - Automatic
    int display_height = 0x7FFFFFFF;            // default - Automatic
    int position_size_settings = 0;             // 0 - windows frame & title bar exluded, 1 - included
    int const_aspect_ratio = 0;                 // 0 - is not const, 1 - is const at adjustment value
    int max_min = 0;                            // 0 -not max or min, 1 - max, 2 - min
    int display_priority_seq = 1;               // 1 - foremost screen
    int fixed_display = 0;                      // 0 - not fixed, 1 - fixed
    int muting = 0;                             // 0 - muting off, 1 - muting on
    int reserve1 = 0;
    int notification_flag = 0x00000001;
    int reserve2 = 0;
    common::style_data style;
    common::fit_data fit;
    common::crop_data crop;
}; // 912 bytes
struct new_window_creation_response
{
    int code = 0x00000001;
    int status; // 0 - OK
    int windows_number;
    int window_type; // 1 - RGB, 2 - Video, 13 - SDI
}; // 16bytes
// 3.2.2 [ServerPC overlay window function] Window movement, resizing, priority sequence change, fit, cropping and style change
enum class type_code : int {
    none = 0,
    movement = 0x00000002,
    resizing = 0x00000003
};
struct window_move_resize_etc
{
    type_code code = type_code::none;
    int process_number = 0;
    int image_window_number = 1;
    int type = 1; // 1: RGB overlay    2: Video overlay    13: SDI overlay
    int image_source_number = 0x7FFFFFFF;
    int x = 0x7FFFFFFF;
    int y = 0x7FFFFFFF;
    int w = 0x7FFFFFFF;
    int h = 0x7FFFFFFF;
    int position_and_size_settings = 1;
    int const_aspect_ratio = 0;
    int reserve = 0;
    int display_priority_seq = 1;
    int operation_fixed_display = 1;
    common::style_data style;
    common::fit_data fit;
    common::crop_data crop;
    int always_on_top = 0x7FFFFFFF;
}; // 900 bytes
struct window_move_resize_etc_response
{
    type_code code = type_code::none;
    int status;
}; // 8 bytes
// 3.2.3 [ServerPC overlay window function] Window close
struct window_close
{
    uint32_t code = 0x00000009;
    int32_t process_number = /*0x00000001*/0;
    int32_t window_number = 0x7FFFFFFF; // 1-MAX, 0x7FFFFFFF: all cleared
    int32_t window_type = 1; // 1:RGB, 2:Video, 13:SDI
}; // 16 bytes
struct window_close_response
{
    uint32_t code;
    int32_t status = -1;
}; // 8 bytes
// 3.2.7. [ServerPC overlay window function] Window status acquisition
struct window_status_acquisition
{
    int code = 0x0000000D;
    int process_number = 0x00000001;
    int window_number = 0;  // 0: All, 1 to MAX: Window number, 0x7FFFFFFF: All
    int window_type = 1;    // 1: RGB, 2: Video, 4: SDI
}; // 16 bytes
struct window_status_acquisition_response {
    int code;
    int status = -1;
    int number = 0;
    int data_length = 0;
    std::vector<common::serverPC_overlay_window_data> data;
}; // 16 + (888*n) bytes
// 3.2.10 [ServerPC overlay window function] Window source addition
struct window_source_addition
{
    int code = 0x00000019;
    int process_number = 0x00000001;
    int reserve = 0;
    int source_type = 1; // 1:RGB, 2:Video, 4:SDI
    common::serverPC_window_source_data data;
}; // 312 bytes
struct window_source_addition_response {
    int code;
    int status = -1;
    int source_number = -1;
    int reserve = 0;
}; // 16 bytes
// 3.2.13 [ServerPC overlay window function] Window source deletion
// 3.2.14 [ServerPC overlay window function] Acquisition of window source data
struct acq_window_source_data {
    int code = 0x0000001D;
    int process_number = 0x00000001;
    int source_number = 0; // 0: all, 1-512: source number
    int window_type = 1; // 1: RGB, 2: Video overlay, 13: SDI overlay
    int source_type = 1; // 1: RGB, 2: Video, 13: SDI
    int reserve = 0;
};
struct acq_window_source_data_response {
    int code;
    int status = -1;
    int number_sources;
    int data_length; // byte length of serverPC window source data section, if n==0 - 296
    std::vector<common::serverPC_window_source_data> data;
}; // 16+(296*n)
// 3.2.15 [ServerPC overlay window function] ServerPC test pattern display
struct test_pattern_display {
    int code = 0x0000001E;
    int process_number = 0x00000001;
    int on_off = 1; // 0 - Off, 1 - On
    int pattern_number = 0; // 0-fullscreen, 1-individual crosshatch, 2-full white
};
struct test_pattern_display_response {
    int code = 0x0000001E;
    int status; // 0-OK, other error
};
} // end of namespace serverPC

/// 3.4 Display
namespace display {
struct acq_window_status
{
    int code = 0x0000025C;
    int process_number = 0x00000001;
    int window_number = 0; // 0: All, 1 and up: Window number
    int window_type = 10; // 10: Monitor overlay
}; // 16 bytes
struct acq_window_status_response
{
    int code;
    int status = -1;
    int number_of_windows;
    int data_length = 0;
    std::vector<common::monitor_overlay_window_data> data;
};
} // end of namespace

/// 3.5 Layout
namespace layout {
/// 3.5.1 Saving layout
struct saving
{
    int code = 0x00000506;
    int process_number = 0;
    int layout_number = 0x7FFFFFFF;
    int overwrite = 0;      // 0 - not permitted, 1 - is permitted
    QString layout_name; // 64 wchars
}; // 144 bytes
struct saving_response
{
    int code = 0x00000506;
    int status = -1;        // 0 - OK
    int layout_number = -1; // 1-128
    int reserve = 0;
}; // 16 bytes
/// 3.5.2 Layout open
struct open
{
    int code = 0x00000507;
    int process_number = 0x00000001;
    int number = -1; // 1 - 128
    int reserve = 0;
}; // 16 bytes
struct open_response
{
    int code = 0x00000507;
    int status = -1;
};
/// 3.5.3 Layout deletion
struct deletion;
/// 3.5.4 Layout name change
struct name_change;
/// 3.5.5 Acquisition of layout settings
struct acquisition_settings;
/// 3.5.6 Acquisition of layout list
struct acquisition_list
{
    int code = 0x0000050B;
    int process_number = 0x00000001;
}; // 8 bytes
struct layout_info
{
    int number; // 1 - 128
    int reserve = 0;
    QString name; // 64 chars, 128 bytes
}; // 136 bytes
struct acquisition_list_re
{
    int code = 0x0000050B;
    int status = -1; // 0 - OK
    int number; // 0 - 128
    int data_length; // 0 - 136 , 136*n
    int r1;
    int r2;
    int r3;
    int r4;
    std::vector<layout_info> info;
}; // 32+(136*n) bytes
} // end of namespace

/// 3.7 Server
namespace server {
enum  WINDOW_TYPES{
    OTHER = 0,
    SERVERPC_RGB =1,
    SERVERPC_VIDEOOVERLAY = 2,
    SERVERPC_SDI = 13,
    VC_MK4000 = 3,
    NETWORK = 4,
    APPLICATION = 5,
    TICKER_WINDOW = 6,
    MONITOR_OVERLAY = 10,
    IP_STREAM = 12
} ;


///3.7.3
struct window_data
{
    quint64 hadle     = 0x00000001;
    int number = 0x00000001;
    int status = 0;                // 0 - Automatic
    int type = 1;                        // 1 - RGB, 2 - Video, 13 - SDI
    int source_number = 1;                      // 1-512
    int source_type = 2;
    wchar_t title[256];
    QString titleQS;
    int start_position_hor = 0x7FFFFFFF;// default - Automatic
    int start_position_ver = 0x7FFFFFFF;// default - Automatic
    int width = 0x7FFFFFFF;             // default - Automatic
    int height = 0x7FFFFFFF;
    int start_position_hor_nf = 0x7FFFFFFF;// default - Automatic
    int start_position_ver_nf = 0x7FFFFFFF;// default - Automatic
    int width_nf = 0x7FFFFFFF;             // default - Automatic
    int height_nf = 0x7FFFFFFF; // default - Automatic
    int muting;
    int fixed_display;
    int ratio_w;
    int ratio_h;
    int ratio_f;
    int on_top;
    int r1;
};
struct acquisition_window_list
{
    int code = 0x00000504;
    int process_number = 0x00000001;
    quint64 widnowHandle = 0x7FFFFFFF;
    int r1;
    int r2;
}; // 24 bytes

struct acquisition_list_re
{
    int code = 0x00000504;
    int status = -1; // 0 - OK
    int number; // 0 - 128
    int data_length; // 0 - 136 , 600*n
    QVector<server::window_data> info;
}; // 16+(600*n) bytes
} // end of namespace

/// 3.8 Application window
namespace application {
struct new_window_creation
{
    int code = 0x00000801;
    int process_number = 0x00000001;
    int source_number = 1;                      // 1-512
    int display_start_position_hor = 0x7FFFFFFF;// default - Automatic
    int display_start_position_ver = 0x7FFFFFFF;// default - Automatic
    int display_width = 0x7FFFFFFF;             // default - Automatic
    int display_height = 0x7FFFFFFF;            // default - Automatic
    int maximaze_flag = 0;                      // 0 - normal 1 - maximize 2 minimaze
    int priotiy  = 1;                           // 1 - foremost
    int reserved1 = 0;
    int reserved2 = 0 ;
    QString command= "C:\\Windows\\System32\\cmd.exe";
    int command_size = 512;
    QString start_in = "C:\\";
    int start_in_size = 260;
    int start_time = 2; // time to start
    common::fit_data fit;
}; // 1624 bytes
struct new_window_creation_response
{
    int code = 0x00000001;
    int status; // 0 - OK
}; // 8bytes
struct acquisition_sources {
    quint32 code = 0x0000080C;
    qint32 process = 0x00000001;
    qint32 source_numbler = 0;
    qint32 reserved = 0;
};
struct application_source {
    qint32 source_number;
    QString source_name;
    QString command_line = "";
    QString start_in = "";
    QString title;
    QString window_class;
    qint32 start_time= 1;
    quint8 reserved[18*4];
};
struct acquisition_windows {
    quint32 type    = 0x00000807;
    qint32 process = 0x00000001;
    quint64 handle = 0;
    qint32 r1 = 0;
    qint32 r2 = 0;
}; // 24 bytes
struct application_window_data {
    quint64 handle = 0;
    qint32 status = -1;
    quint32 type;
    qint32 source_number;
    qint32 x;
    qint32 y;
    qint32 w;
    qint32 h;
    qint32 r1;
    qint32 r2;
    qint32 r3;
    qint32 r4;
    qint32 r5;
    QString command;
    QString start_in;
    QString title;
    QString class_name;

};
struct acquisition_windows_re {
    quint32 code    = 0x00000807;
    qint32 status = -1;
    quint32 number = 0;
    qint32 lenght = 0;
    QVector<application_window_data> info;
};
struct adding_application_source {
    quint32 type    = 0x00000808;
    qint32 process = 0x00000001;
    qint32 r1 = 0;
    qint32 r2 = 0;
    application_source source_data;
};
struct acquisition_source_rep {
    quint32 command = 0x0000080C;
    qint32  status = 0;
    qint32  number = 0;
    qint32  length = 0;
    QVector<application_source> sources;
};
// 3.8.3. [Application window] Deletion
struct window_deletion {
    quint32 code = 0x00000805;
    qint32 process = 0;
    quint64 handle = 0x7FFFFFFF;
    quint32 r1 = 0;
    quint32 r2 = 0;
};
//
struct window_deletion_response {
    quint32 code = 0x00000805;
    qint32 status = -1;
};
}

enum class window_operation : quint32 {
    MOVEMENT = 0x00000802,
    RESIZING = 0x00000803,
    FIT      = 0x00000804,
    CURRENT  = 0x7FFFFFFF
};

struct move_resize_fit {
    window_operation message_type = window_operation::MOVEMENT;
    quint32 process_number = 0x00000001;
    quint64 handle         = 0;
    quint32 x = 0;
    quint32 y = 0;
    quint32 w = 0;
    quint32 h = 0;
    quint32 priority = 0;
    quint32 reserve = 0;
    common::fit_data fit;
};

/// 3.13 User
namespace login {
struct login1
{
    int code = 0x00000D01;
    int process_number = 0x00000001;
    int reserve1 = 0;
    int reserve2 = 0;
    QString username = "Administrator";
    QString password = "";
};
struct login2
{
    int code = 0x10000D01;
    int process_number = 0x00000001;
    int username_length = 1;
    int password_length = 0;
    QString username = "Administrator";
    QString password = "";
};
struct response
{
    uint32_t code = 0x10000D01;
    uint32_t status = -1;    // 0 - OK
    uint32_t user_id = -1;   // 3 - Administrator
    uint32_t reserve = 0;
}; // 16 bytes
} // end of namespace

/// 3.14 Video & Audio output
namespace videoAudioOutput {
/// 3.14.1
struct settings
{
    int code = 0x00000E01;
    int process_number = 0x00000001;
    int number = -1; // 1-64
    int source_number = -1; // 1-512
}; // 16 bytes
/// 3.14.2
struct off
{
    int code = 0x00000E02;
    int process_number = 0x00000001;
    int number = -1; // 1-64, 0 - all
    int all_of_flag = 0; // 0 - all, 1 for outputs association with open layout flag(if number is 0)
}; // 16 bytes
/// 3.14.3
struct acquisition_status
{
    int code = 0x00000E03;
    int process_number = 0x00000001;
}; // 8 bytes
/// 3.14.4
struct adding_source
{
    int code = 0x00000E04;
    int process_number = 0x00000001;
    int reserve1 = 0;
    int reserve2 = 0;
    common::source_data data;
}; //208 bytes
/// 3.14.5
struct changing_source
{
    int code = 0x00000E05;
    int process_number = 0x00000001;
    common::source_data data;
};
/// 3.14.6
struct copying_source
{
    int code = 0x00000E06;
    int process_number = 0x00000001;
    int source_number_of_copy = 0; // 1-512
    int source_number_of_copy_dest = 0; // 0 - automatic number, 1-512
    QString name_of_copy_dest; // 64 chars - 128 bytes
};
/// 3.14.7
struct deleting_source
{
    int code = 0x00000E07;
    int process_number = 0x00000001;
    int source_number = 0; // 1-512
    int reserve = 0;
};
/// 3.14.8
struct acquisition_source
{
    int code = 0x00000E08;
    int process_number = 0x00000001;
    int source_number = 0; // 0 - all sources, 1-512
    int reserve = 0;
};
} // end of namespace

struct application_window_source {
    application::application_window_data window;
    application::application_source source;
};

} // namespace datapath_message

#endif // MESSAGE

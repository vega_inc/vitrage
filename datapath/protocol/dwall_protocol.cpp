#include <QStringList>
#include "dwall_protocol.h"
#include "streamhelper.h"

using namespace dwall_protocol::general_line;

const int waitLoginTime = 3000;

/// 3.13.1. [User] User log-in 1
int user::userLogin1(QTcpSocket &socket, const QString &username, const QString &password)
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    datapath_message::login::login1 request;
    if (!username.isEmpty()) {
        request.username = username;
    }
    if (!password.isEmpty()) {
        request.password = password;
    }
    request.username.resize(64);
    request.password.resize(64);
    out << request;
    qDebug() << "block size:" << block.size();
    if (socket.write(block) < 0)
        return -1;
    if (socket.waitForReadyRead(60000)) {
        QByteArray block;
        QDataStream stream(&block, QIODevice::ReadOnly);
        datapath_message::login::response response;
        stream >> response;
        return 0;
    }
    return -1;
}
/// 3.13.2. [User] User log-in 2
int user::userLogin2(QTcpSocket &socket, const QString &username, const QString &password)
{
    qDebug() << "Send [User] User log-in 2";

    using namespace datapath_message;
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    login::login2 request;
    request.username = username;
    request.password = password;
    request.username_length = username.size();
    request.password_length = password.size();
    out << request;

    if (socket.write(block) < 16)
        return static_cast<int>(datapath_socket_error::write_small_data);

    if (socket.waitForReadyRead(waitLoginTime)) {
        if (socket.bytesAvailable() != 20) {
            qDebug() << Q_FUNC_INFO << socket.bytesAvailable();
            return static_cast<int>(datapath_socket_error::read_incorrect_length_data);
        }
        QDataStream stream(&socket);
        login::response response;
        stream >> response;
        return response.status;
    }
    return static_cast<int>(datapath_socket_error::wait_error);
}
/// 3.7.3. [Server] Acquisition of window list
int dwall_protocol::general_line::server::getAllWindows(QTcpSocket &socket, datapath_message::server::acquisition_list_re &response)
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    datapath_message::server::acquisition_window_list request;
    out << request;
    qDebug() << block.size();
    if (socket.write(block) < 28)
        return static_cast<int>(datapath_socket_error::write_small_data);
    if (socket.waitForReadyRead(60000)) {
        QDataStream stream(&socket);
        //stream >> response;
        qDebug() << " Datapath::getAllWindows rest" << response.code << " " << response.status;
        return response.status;
    }
    return static_cast<int>(datapath_socket_error::wait_error);
}
/// 3.2.15. [ServerPC overlay window function] ServerPC test pattern display
int serverPC_overlay_window_function::testPatternDisplay(QTcpSocket &socket)
{
    qDebug() << "Send [ServerPC overlay window function] ServerPC test pattern display";

    using namespace datapath_message;
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    serverPC::test_pattern_display request;
    out << request;

    if (socket.write(block) < 16)
        return static_cast<int>(datapath_socket_error::write_small_data);

    if (socket.waitForReadyRead(60000)) {
        if (socket.bytesAvailable() != 12) {
            qDebug() << Q_FUNC_INFO << socket.bytesAvailable();
            return static_cast<int>(datapath_socket_error::read_incorrect_length_data);
        }
        QDataStream stream(&socket);
        serverPC::test_pattern_display_response response;
        stream >> response;
        return response.status;
    }
    return static_cast<int>(datapath_socket_error::wait_error);
}
/// 3.2.1. [ServerPC overlay window function] New window creation
int serverPC_overlay_window_function::newWindowCreation(QTcpSocket &socket, datapath_message::serverPC::new_window_creation request)
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out << request;

    if (socket.write(block) != 916)
        return static_cast<int>(datapath_socket_error::write_small_data);

    if (socket.waitForReadyRead(60000)) {
        if (socket.bytesAvailable() < 16) {
            qDebug() << Q_FUNC_INFO << "error bytes:" << socket.bytesAvailable();
            return static_cast<int>(datapath_socket_error::read_incorrect_length_data);
        }
        QDataStream stream(&socket);
        datapath_message::serverPC::new_window_creation_response response;
        stream >> response;
        return response.status;
    }
    return static_cast<int>(datapath_socket_error::wait_error);
}
/// 3.2.3. [ServerPC overlay window function] Window close
int serverPC_overlay_window_function::windowClose(QTcpSocket &socket, int window_number, int window_type)
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    datapath_message::serverPC::window_close request;
    request.window_number = window_number;
    request.window_type = window_type;
    out << request;

    if (socket.write(block) != 20)
        return static_cast<int>(datapath_socket_error::write_small_data);

    if (socket.waitForReadyRead(60000)) {
        if (socket.bytesAvailable() < 8) {
            qDebug() << Q_FUNC_INFO << "error bytes:" << socket.bytesAvailable();
            return static_cast<int>(datapath_socket_error::read_incorrect_length_data);
        }
        QDataStream stream(&socket);
        datapath_message::serverPC::window_close_response response;
        stream >> response;
        return response.status;
    }
    return static_cast<int>(datapath_socket_error::wait_error);
}
/// 3.2.10. [ServerPC overlay window function] Window source addition
std::pair<int, int> serverPC_overlay_window_function::windowSourceAddition(QTcpSocket &socket, int source_type)
{
    using namespace datapath_message;

    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    serverPC::window_source_addition request;
    request.source_type = source_type;
    request.data.source_number = 0;
    request.data.source_type = source_type;
    request.data.connection_dest_device = 1;
    out << request;
    if (socket.write(block) != 316)
        return {static_cast<int>(datapath_socket_error::write_small_data), 0};

    if (socket.waitForReadyRead(60000)) {
        if (socket.bytesAvailable() < 16) {
            qDebug() << Q_FUNC_INFO << socket.bytesAvailable();
            return {static_cast<int>(datapath_socket_error::read_incorrect_length_data), 0};
        }
        QDataStream stream(&socket);
        serverPC::window_source_addition_response response;
        stream >> response;
        qDebug() << "Status:" << response.status << "source number:" << response.source_number;
        return {response.status, response.source_number};
    }
    return {static_cast<int>(datapath_socket_error::wait_error), 0};
}
/// 3.2.14. [ServerPC overlay window function] Acquisition of window source data
int serverPC_overlay_window_function::acqWindowSourceData(QTcpSocket &socket, int source_number, int window_type, int source_type)
{
    qDebug() << "Send [ServerPC overlay window function] Acquisition of window source data";
    using namespace datapath_message;

    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    serverPC::acq_window_source_data request;
    request.source_number = source_number;
    request.window_type = window_type;
    request.source_type = source_type;
    out << request;
    if (socket.write(block) != 28)
        return static_cast<int>(datapath_socket_error::write_small_data);

    if (socket.waitForReadyRead(60000)) {
        if (socket.bytesAvailable() < 16) {
            qDebug() << Q_FUNC_INFO << socket.bytesAvailable();
            return static_cast<int>(datapath_socket_error::read_incorrect_length_data);
        }
        QDataStream stream(&socket);
        serverPC::acq_window_source_data_response response;
        stream >> response;
        return response.status;
    }
    return static_cast<int>(datapath_socket_error::wait_error);
}
/// 3.5.2. [Layout] Layout open
int dwall_protocol::general_line::layout::openLayout(QTcpSocket &socket, int number)
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    datapath_message::layout::open request;
    request.number = number;
    out << request;
    if (socket.write(block) < 0)
        return false;
    if (socket.waitForReadyRead(60000)) {
        QByteArray block;
        QDataStream stream(&block, QIODevice::ReadOnly);
        datapath_message::layout::open_response response;
        stream >> response;
        return !response.status;
    }
    return false;
}
/// 3.5.1. [Layout] Saving layout
std::pair<bool, int> dwall_protocol::general_line::layout::savingLayout(QTcpSocket &socket, int number, bool overwrite, const QString &name)
{
    qDebug() << "Datapath::savingLayout" << number;
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    datapath_message::layout::saving request;
    request.layout_number = number;
    request.overwrite = overwrite;
    request.layout_name = name;

    out << request;

    if (socket.write(block) < 0)
        return std::make_pair(false, -1);
    if (socket.waitForReadyRead(60000)) {
        QByteArray block;
        QDataStream stream(&block, QIODevice::ReadOnly);
        datapath_message::layout::saving_response response;
        stream >> response;
        return std::make_pair(!response.status, response.layout_number);
    }
    return std::make_pair(false, -1);
}
/// 3.5.6. [Layout] Acquisition of layout list
std::pair<bool, QStringList> dwall_protocol::general_line::layout::layoutList(QTcpSocket &socket)
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    datapath_message::layout::acquisition_list request;
    out << request;

    if (socket.write(block) < 0)
        return std::make_pair(false, QStringList());
    if (socket.waitForReadyRead(60000)) {
        QByteArray block;
        QDataStream stream(&block, QIODevice::ReadOnly);
        datapath_message::layout::acquisition_list_re response;
        stream >> response;
        return std::make_pair(!response.status, QStringList());
    }
    return std::make_pair(false, QStringList());
}

#ifndef DWALL_PROTOCOL_H
#define DWALL_PROTOCOL_H

#include <utility>
#include <QTcpSocket>
#include "protocol/message.h"

namespace dwall_protocol {

/// 3. General control line messages
namespace general_line {

/// 3.2. ServerPC overlay window function
namespace serverPC_overlay_window_function {
// 3.2.1. [ServerPC overlay window function] New window creation
int newWindowCreation(QTcpSocket &socket, datapath_message::serverPC::new_window_creation request);
// 3.2.15. [ServerPC overlay window function] ServerPC test pattern display
int testPatternDisplay(QTcpSocket &socket);
// 3.2.3. [ServerPC overlay window function] Window close
int windowClose(QTcpSocket &socket, int window_number, int window_type);
// 3.2.10. [ServerPC overlay window function] Window source addition
std::pair<int, int> windowSourceAddition(QTcpSocket &socket, int source_type);
// 3.2.14. [ServerPC overlay window function] Acquisition of window source data
int acqWindowSourceData(QTcpSocket &socket, int source_number, int window_type, int source_type);
} // namespace serverPC_overlay_window_function

/// 3.5. Layout
namespace layout {
int openLayout(QTcpSocket &socket, int number);
std::pair<bool, int> savingLayout(QTcpSocket &socket, int number, bool overwrite, const QString &name);
std::pair<bool, QStringList> layoutList(QTcpSocket &socket);
}

/// 3.7. Server
namespace server {
int getAllWindows(QTcpSocket &socket, datapath_message::server::acquisition_list_re &response);
}

/// 3.13. User
namespace user {
int userLogin1(QTcpSocket &socket, const QString &username, const QString &password);
int userLogin2(QTcpSocket &socket, const QString &username, const QString &password);
} // namespace user
} // namespace general_line

/// 4. Notification line messages
namespace notification_line {

} // namespace notification_line
}

#endif // DWALL_PROTOCOL_H

#ifndef RESPONSE_CODE
#define RESPONSE_CODE
#include <map>
#include <string>

enum class controller_error {
    property_error = -100
};

enum class datapath_socket_error {
    write_small_data = -10,
    read_incorrect_length_data,
    wait_error,
    reconnect_error
};

namespace response_code_table {
const std::map<int, std::string> codes = {
    {static_cast<int>(controller_error::property_error), "invalid property"},
    {static_cast<int>(datapath_socket_error::write_small_data), "socket write small data"},
    {static_cast<int>(datapath_socket_error::read_incorrect_length_data), "incorrect response length"},
    {static_cast<int>(datapath_socket_error::wait_error), "read timeout error"},
    {static_cast<int>(datapath_socket_error::reconnect_error), "reconnect"},
    {0, "OK"},
    {12, "not enough storage is available to complete this operation"},
    {487, "Attempt to access invalid address"},
    {1057, "The account name is invalid or does not exist"},
    {1168, "Element not found"},
    {1244, "The operation being requested was not performed because the user has not been authenticated"},
    {20001, "Initialization file is not found"},
    {20002, "Initialization file error"},
    {20003, "Parameter error"},
    {20010, "Socket is not connected"},
    {20011, "Can not execute because system is busy"},
    {20012, "Message error"},
    {20013, "Message length error"},
    {20014, "Timeout"},
    {20015, "Send error"},
    {20016, "Receive error"},
    {20017, "Can not open COM port"},
    {20101, "Under adjustment"},
    {20102, "The operation is interrupted."},
    {20103, "This window is fixed."},
    {20104, "Pattern display now shown"},
    {20185, "Volume controller is abnormal."},
    {20380, "Can not connect to the server."},
    {20381, "Can not make remote multi cursor."},
    {20382, "A remote cursor is illegal."},
    {20383, "Can not start remote multi cursor by the limit."},
    {20384, "Can not control remote multi cursor."},
    {20385, "Can not input character."},
    {20386, "Failed to quit the Remote Cursor."},
    {20400, "OCX initialization error."},
    {20420, "Setting of the type is abnormal."},
    {20421, "Setting of the name is abnormal."},
    {20422, "Image number is abnormal."},
    {20423, "The registry sources counts exceed the number of the upper limits."},
    {20440, "The number of the display exceeds the upper limit."},
    {20441, "Switcher transmission error."},
    {20442, "Can not control switcher."},
    {20480, "The monitor transmission error."},
    {20481, "The monitor cannot be controlled"},
    {20540, "Can not write to the registry."},
    {20560, "Can not initialize the internal data."},
    {20580, "Can not open the file."},
    {20581, "Can not write to the file."},
    {20582, "Can not read from the file."},
    {20601, "[VC-MK4000]Can not control due to initialization."},
    {20623, "[VC-MK4000] isinput has no signal"},
    {20630, "The control is abnormal of VC-MK4000"},
    {20701, "Specified object is undefined"},
    {20702, "Can not delete this user"},
    {20703, "Can not delete this group"},
    {20704, "Can not add more than 64 shedules"},
    {20705, "Can not add more than 3 addresses"},
    {20706, "Can not add more than 128 users"},
    {20708, "User name is not valid"},
    {20709, "Password is incorrect"},
    {20710, "Can not change the password"},
    {20711, "Control module is abnormal"},
    {20712, "Overlay board control is abnormal"},
    {20713, "Input settings is invalid"},
    {20714, "Others error"},
    {20715, "No signal or unrecognized signal"},
    {20716, "Can not set the past time"},
    {20717, "Out of range"},
    {20718, "Out of range"},
    {20719, "Monitot communication error"},
    {20720, "Can not output this source"},
    {20721, "Can not output this source"},
    {20722, "Can not output this source due to limit"},
    {20723, "Can not use the destination"},
    {20724, "Can not operate due to the zone limitation"},
    {20729, "Unauthorized signal"},
    {20732, "Login failed because the number of connections has reached the limit"},
    {20880, "The layout is unregistered"},
    {20881, "Can not save more than 128 layouts"}
};
} // namespace response_code_table

namespace notification_code {
const std::map<int, std::string> codes = {
    {0x00001001, "4.1.1. [Notification] Window status"},
    {0x00001005, "4.1.2. [Notification] Notification of addition, change, and copy of ServerPC overlay window source"},
    {0x00001006, "4.1.3. [Notification] Notification of deletion of ServerPC overlay window source"},
    {0x0000100F, "4.1.7. [Notification] Notification of addition, change, and copy of application window source"},
    {0x00001023, "4.1.23. [Notification] Notification of addition and change of user"},
    {0x00001039, "4.1.34. [Notification] Notification of sender's property"},
    {0x00001045, "4.1.39. [Notification] Notification of addition, change, and copy of video and audio output source"}
};
} // namespace response_code_table

#endif // RESPONSE_CODE


include(../common.pri)

TEMPLATE = lib
TARGET = datapath
CONFIG += plugin debug
VERSION = 1.0.0

QT += network

DESTDIR = $$INSTALL_PATH_CTRL_PLUGINS

INCLUDEPATH += $$INSTALL_PATH_INCLUDE

SOURCES += datapath.cpp \
    protocol/dwall_protocol.cpp
HEADERS += datapath.h \
    streamhelper.h \
    protocol/notification.h \
    protocol/message.h \
    protocol/response_code.h \
    protocol/dwall_protocol.h

unix {
       target.path = /$(DESTDIR)
       INSTALLS += target
     }
QMAKE_CXXFLAGS += -Wno-vla

#include <QtPlugin>
#include <QDir>

#include "datapath.h"
#include "protocol/dwall_protocol.h"
#include "streamhelper.h"

#include "../vitrage/spdlog/spdlog.h"

constexpr int time_wait_connect = 5000; // ms
constexpr int time_wait_read = 3000;    // ms
constexpr int time_no_operation = 2000; // ms

const std::string logname = "datapath";

Datapath::Datapath()
{
    m_name = "datapath";
    m_busy = false;
    try {
        QDir().mkdir("logs");
        if (!QDir("logs").exists())
            throw spdlog::spdlog_ex("fail to create dir logs");
        spdlog::daily_logger_mt(logname, "logs/datapath");
    }
    catch (const spdlog::spdlog_ex& ex) {
        qDebug() << ex.what();
    }


    if (!isRunning()) {
        moveToThread(this);
        m_socket_control.moveToThread(this);
        m_socket_notification.moveToThread(this);
           start();
    }
}

Datapath::~Datapath()
{
    timer.stop();
    quit();
    wait();
}

void Datapath::conn()
{

    emit connSig();
}

void Datapath::disconn()
{
    qDebug() << Q_FUNC_INFO;
    timer.stop();
    m_busy = false;
    m_socket_control.disconnectFromHost();
    m_socket_notification.disconnectFromHost();
}

bool Datapath::reconnect()
{
    m_socket_control.abort();
    m_socket_control.connectToHost(m_address, m_port_control);
    if (m_socket_control.waitForConnected(time_wait_connect)) {
        qDebug() << Q_FUNC_INFO;
        using namespace dwall_protocol::general_line;
        if (!checkResponse(user::userLogin2(m_socket_control, "Administrator", ""))) {
            qCritical("Fail to login administrator");
            return false;
        }
        timer.start();
        return true;
    }
    return false;
}

void Datapath::printWindowStatus(QDataStream &in)
{
    int data_length;
    int total_numbers_of_windows;
    int reserve;
    in >> data_length;
    in >> total_numbers_of_windows;
    in >> reserve;
    qDebug() << "Notification:" << "total numbers of windows =" << total_numbers_of_windows;
    for (int i = 0; i < total_numbers_of_windows; i++) {
        datapath_message::common::window_data data;
        in >> data.handle;
        in >> data.number;
        in >> data.status;
        in >> data.type;
        in >> data.image_source_number;
        in >> data.image_source_type;
        for (int j = 0; j < 256; ++j) {
            qint16 c;
            in >> c;
            data.title[j] = c?c:'\0';
        }
        in >> data.x;
        in >> data.y;
        in >> data.w;
        in >> data.h;
        in >> data.X;
        in >> data.Y;
        in >> data.W;
        in >> data.H;
        in >> data.muting;
        in >> data.fixed_display;
        in >> data.aspect_ratio_w;
        in >> data.aspect_ratio_h;
        in >> data.const_aspect_ratio_flag;
        in >> data.always_on_top;
        in >> data.reserve;
        qDebug() << "Notification:" << "Title:" << QString::fromWCharArray(data.title);
    }
}

int Datapath::saveLayout()
{
    QByteArray ba;
    QDataStream out(&ba, QIODevice::WriteOnly);
    datapath_message::layout::saving request;
    request.layout_number = 128; // last number of layouts
    request.overwrite = 1;
    request.layout_name = "VEGA";
    out << request;
    if (m_socket_control.write(ba) != 148) {
        qCritical("Request size = %d", ba.size());
        return static_cast<int>(datapath_socket_error::write_small_data);
    }
    if (m_socket_control.waitForReadyRead(60000)) {
        if (m_socket_control.bytesAvailable() != 20) {
            qDebug() << Q_FUNC_INFO << "failed with bytes" << m_socket_control.bytesAvailable();
            return static_cast<int>(datapath_socket_error::read_incorrect_length_data);
        }
        QDataStream stream(&m_socket_control);
        stream.setByteOrder(QDataStream::LittleEndian);

        uint32_t header;
        datapath_message::layout::saving_response re;
        stream >> header;
        stream >> re;
        return re.status;
    }
    return static_cast<int>(datapath_socket_error::wait_error);
}

int Datapath::openLayout()
{
    QByteArray ba;
    QDataStream out(&ba, QIODevice::WriteOnly);
    datapath_message::layout::open request;
    request.number = 128; // last number of layouts
    out << request;
    if (m_socket_control.write(ba) != 20) {
        qCritical("Request size = %d", ba.size());
        return static_cast<int>(datapath_socket_error::write_small_data);
    }
    if (m_socket_control.waitForReadyRead(60000)) {
        if (m_socket_control.bytesAvailable() != 12) {
            qDebug() << Q_FUNC_INFO << "failed with bytes" << m_socket_control.bytesAvailable();
            return static_cast<int>(datapath_socket_error::read_incorrect_length_data);
        }
        QDataStream stream(&m_socket_control);
        stream.setByteOrder(QDataStream::LittleEndian);
        uint32_t header;
        datapath_message::layout::open_response re;
        stream >> header;
        stream >> re;
        return re.status;
    }
    return static_cast<int>(datapath_socket_error::wait_error);
}

int Datapath::closeAppAll()
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    datapath_message::application::window_deletion request;
    request.process = 0;
    request.handle = 0x7FFFFFFF;
    out << request;
    if (m_socket_control.write(block) != 28) {
        qCritical("Request size = %d", block.size());
        return static_cast<int>(datapath_socket_error::write_small_data);
    }
    if (m_socket_control.waitForReadyRead(60000)) {
        if (m_socket_control.bytesAvailable() != 12) {
            qDebug() << Q_FUNC_INFO << "failed with bytes" << m_socket_control.bytesAvailable();
            return static_cast<int>(datapath_socket_error::read_incorrect_length_data);
        }
        QDataStream stream(&m_socket_control);
        stream.setByteOrder(QDataStream::LittleEndian);
        uint32_t header;
        uint32_t code;
        int32_t status;
        stream >> header;
        stream >> code;
        stream >> status;
        return status;
    }
    return static_cast<int>(datapath_socket_error::wait_error);
}

int Datapath::closeServerPCAll(WINDOW_TYPE windowType)
{
    if (!checkResponse(getAllServerPCWindows())) {
        return -1;
    }
    if (m_Windows.number == 0)
        return 0;
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    datapath_message::serverPC::window_close request;
    request.window_type = static_cast<int32_t>(windowType);
    out << request;
    if (m_socket_control.write(block) != 20) {
        qCritical("Request size = %d", block.size());
        return static_cast<int>(datapath_socket_error::write_small_data);
    }
    if (m_socket_control.waitForReadyRead(time_wait_read)) {
        if (m_socket_control.bytesAvailable() != 12) {
            qDebug() << Q_FUNC_INFO << "failed with bytes" << m_socket_control.bytesAvailable();
            return static_cast<int>(datapath_socket_error::read_incorrect_length_data);
        }
        datapath_message::serverPC::window_close_response response;
        QDataStream stream(&m_socket_control);
        stream >> response;
        return response.status;
    }
    return static_cast<int>(datapath_socket_error::wait_error);
}

std::pair<int, QRect> Datapath::windowStatusAcquisition(int number)
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    datapath_message::serverPC::window_status_acquisition request;
    request.window_number = number;
    if (!checkAndReconnect())
        return {static_cast<int>(datapath_socket_error::reconnect_error), QRect()};
    out << request;
    if (m_socket_control.write(block) != 20) {
        qCritical("Request size = %d", block.size());
        return {static_cast<int>(datapath_socket_error::write_small_data), QRect()};
    }
    QRect rect;
    if (m_socket_control.waitForReadyRead(60000)) {
        QDataStream stream(&m_socket_control);
        datapath_message::serverPC::window_status_acquisition_response response;
        parseServerWindows(stream, response, &m_socket_control);
//        stream >> response;
        if (response.status == 0) {
            for (auto data : response.data) {
                if (number == data.number) {
                    rect = {data.x, data.y, data.w, data.h};
                    break;
                }
            }
        }
        return {response.status, rect};
    }
    return {static_cast<int>(datapath_socket_error::wait_error), QRect()};
}

int Datapath::appStatusAcquisition(QRect &rect, int number)
{
    Q_UNUSED(rect)
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    datapath_message::application::acquisition_windows request;
    request.handle = number;
    if (!checkAndReconnect())
        return static_cast<int>(datapath_socket_error::reconnect_error);
    out << request;
    if (m_socket_control.write(block) != 28) {
        qCritical("Request size = %d", block.size());
        return static_cast<int>(datapath_socket_error::write_small_data);
    }
    if (m_socket_control.waitForReadyRead(60000)) {
        QDataStream stream(&m_socket_control);
        datapath_message::application::acquisition_windows_re response;
        parseWindows(stream,response,&m_socket_control);
        return response.status;
    }
    return static_cast<int>(datapath_socket_error::wait_error);

}

int Datapath::moveResizeEtcNamedSource(datapath_message::serverPC::type_code code, int number, const QRect &rect)
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    datapath_message::serverPC::window_move_resize_etc request;
    request.code = code;
    request.image_window_number = number;
    request.x = rect.x();
    request.y = rect.y();
    request.w = rect.width();
    request.h = rect.height();
//    request.display_priority_seq = i;
    out << request;
    if (m_socket_control.write(block) != 904)
        return static_cast<int>(datapath_socket_error::write_small_data);
    if (m_socket_control.waitForReadyRead(60000)) {
        if (m_socket_control.bytesAvailable() != 12) {
            qDebug() << Q_FUNC_INFO << "failed, bytes =" << m_socket_control.bytesAvailable();
            return static_cast<int>(datapath_socket_error::read_incorrect_length_data);
        }
        QDataStream stream(&m_socket_control);
        stream.setByteOrder(QDataStream::LittleEndian);
        qint32 head, code, status;
        stream >> head;
        stream >> code;
        stream >> status;
        qDebug() << Q_FUNC_INFO;
        qDebug() << "Response: code =" << code << "status =" << status;
        return status;
    }
    return static_cast<int>(datapath_socket_error::wait_error);
}

int Datapath::sendNoOperation()
{
    datapath_message::general::no_operation request;
    uint32_t h = 8;
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setByteOrder(QDataStream::LittleEndian);
    out << h;
    out << request.code;
    out << request.number;
    if (m_socket_control.write(block) != (h+4))
        return static_cast<int>(datapath_socket_error::write_small_data);
    if (m_socket_control.waitForReadyRead(time_wait_read)) {
        if (m_socket_control.bytesAvailable() != 12) {
            qDebug() << Q_FUNC_INFO << m_socket_control.bytesAvailable();
            return static_cast<int>(datapath_socket_error::read_incorrect_length_data);
        }
        QDataStream stream(&m_socket_control);
        stream.setByteOrder(QDataStream::LittleEndian);
        uint32_t header, code;
        int32_t status;
        stream >> header;
        stream >> code;
        stream >> status;
        return status;
    }
    return static_cast<int>(datapath_socket_error::wait_error);
}

bool Datapath::createSource(VSource &)
{
    return false;
}

int Datapath::createApplicationWindow(const QSharedPointer<VSource> &src)
{
    if (!checkAndReconnect())
        return static_cast<int>(datapath_socket_error::reconnect_error);
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    datapath_message::application::new_window_creation request;

    QVariant pixs_w = getProperty("pixs_w");
    QVariant pixs_h = getProperty("pixs_h");
    if (pixs_w.isNull() || pixs_h.isNull())
        return static_cast<int>(controller_error::property_error);
    bool ok;
    auto w = pixs_w.toInt(&ok)/1000.f;
    auto h = pixs_h.toInt(&ok)/1000.f;
    if (!ok)
        return static_cast<int>(controller_error::property_error);
    VPosition src_pos = src->getPosition();
    int xn = src_pos.lt.x()*w;
    int yn = src_pos.lt.y()*h;
    int wn = (src_pos.rb.x()  - src_pos.lt.x())*w;
    int hn = (src_pos.rb.y()  - src_pos.lt.y())*h;

    request.display_start_position_hor = xn;
    request.display_start_position_ver = yn;
    request.display_width = wn;
    request.display_height = hn;
    QVariant command_var;
    if(src->getProperty("Command", command_var)) {
        request.command = command_var.toString();
    }
    request.source_number = 0;
    request.start_in = "";
    out << request;
    if (m_socket_control.write(block) != 1628)
        return static_cast<int>(datapath_socket_error::write_small_data);
    if (m_socket_control.waitForReadyRead(6000000)) {
        if (m_socket_control.bytesAvailable() != 12) {
            qDebug() << "createApplicationg failed " << m_socket_control.bytesAvailable();
            return static_cast<int>(datapath_socket_error::read_incorrect_length_data);
        }
        QDataStream stream(&m_socket_control);
        datapath_message::application::new_window_creation_response response;
        stream >> response;
        qDebug() << " createApplicationg code:" << response.code << ", status:" << response.status;
        return response.status;
    }
    return static_cast<int>(datapath_socket_error::wait_error);
}

bool Datapath::startSource(VSource &)
{
    return false;
}

bool Datapath::stopSource(VSource &)
{
    return false;
}

bool Datapath::enableSourceFlag(VSource &, bool)
{
    return false;
}

bool Datapath::setScene(const Sources &lst)
{
    QMutexLocker a(&m_mutex);
    if (m_socket_control.state() != QTcpSocket::ConnectedState) {
        messSig("Контроллер не подключен");
        emitSceneFinished(false);
        m_busy = false;
        return false;
    }
    if (m_busy) {
        messSig("Контроллер занят");
      //  emitSceneFinished(false);
        return false;
    }
    m_busy = true;
//    m_sources.clear();
    for (int i = 0; i < lst.count();i++) {
        QSharedPointer<VSource> src = lst.at(i);
        if (src->gettype() == "NamedSource"         ||
                src->gettype() == EXTRONMIXER_TYPE  ||
                src->gettype() == MIXER_TYPE        ||
                src->gettype() == EXTRON_TYPE       ||
                src->gettype() == "APPSource") {
            setVisited(src, false);
            setListPosition(src, i+2);
            m_sources.push_back(src);
        }
    }
    emit setSceneSig();
    return true;
}

VController *Datapath::clone()
{
    return nullptr;
}

status_t Datapath::status() const
{
    if (m_socket_control.state() == QTcpSocket::ConnectingState)
        return status_t::start;
    if (m_socket_control.state() == QTcpSocket::ConnectedState)
        return status_t::connected;
    return status_t::disc;
}

void Datapath::displayError(QAbstractSocket::SocketError error)
{
    if (error == QAbstractSocket::RemoteHostClosedError) {
        auto log = spdlog::get(logname);
        bool ok = reconnect();
        if (ok) {
            if (log) log->error() << "Успешное переподключение после таймаута";
            return;
        }
        else {
            if (log) log->error() << "Переподключиться не удалось";
        }
    }
/*
QAbstractSocket::ConnectionRefusedError	0	The connection was refused by the peer (or timed out).
QAbstractSocket::RemoteHostClosedError	1	The remote host closed the connection. Note that the client socket (i.e., this socket) will be closed after the remote close notification has been sent.
QAbstractSocket::HostNotFoundError      2	The host address was not found.
QAbstractSocket::SocketAccessError      3	The socket operation failed because the application lacked the required privileges.
QAbstractSocket::SocketResourceError	4	The local system ran out of resources (e.g., too many sockets).
QAbstractSocket::SocketTimeoutError     5	The socket operation timed out.
QAbstractSocket::DatagramTooLargeError	6	The datagram was larger than the operating system's limit (which can be as low as 8192 bytes).
QAbstractSocket::NetworkError           7	An error occurred with the network (e.g., the network cable was accidentally plugged out).
QAbstractSocket::AddressInUseError      8	The address specified to QUdpSocket::bind() is already in use and was set to be exclusive.
QAbstractSocket::SocketAddressNotAvailableError     9	The address specified to QUdpSocket::bind() does not belong to the host.
QAbstractSocket::UnsupportedSocketOperationError	10	The requested socket operation is not supported by the local operating system (e.g., lack of IPv6 support).
QAbstractSocket::ProxyAuthenticationRequiredError	12	The socket is using a proxy, and the proxy requires authentication.
QAbstractSocket::SslHandshakeFailedError        13	The SSL/TLS handshake failed, so the connection was closed (only used in QSslSocket) (This value was introduced in 4.4.)
QAbstractSocket::UnfinishedSocketOperationError	11	Used by QAbstractSocketEngine only, The last operation attempted has not finished yet (still in progress in the background). (This value was introduced in 4.4.)
QAbstractSocket::ProxyConnectionRefusedError	14	Could not contact the proxy server because the connection to that server was denied (This value was introduced in 4.5.)
QAbstractSocket::ProxyConnectionClosedError     15	The connection to the proxy server was closed unexpectedly (before the connection to the final peer was established) (This value was introduced in 4.5.)
QAbstractSocket::ProxyConnectionTimeoutError	16	The connection to the proxy server timed out or the proxy server stopped responding in the authentication phase. (This value was introduced in 4.5.)
QAbstractSocket::ProxyNotFoundError	17	The proxy address set with setProxy() (or the application proxy) was not found. (This value was introduced in 4.5.)
QAbstractSocket::ProxyProtocolError	18	The connection negotiation with the proxy server because the response from the proxy server could not be understood. (This value was introduced in 4.5.)
QAbstractSocket::UnknownSocketError	-1	An unidentified error occurred.
*/
    qDebug() << Q_FUNC_INFO << error;
    m_socket_control.disconnectFromHost();
}

void Datapath::getNotification()
{
    QDataStream in(&m_socket_notification);
    in.setByteOrder(QDataStream::LittleEndian);
    static int header;
    forever {
        if (!header) {
            if ((uint)m_socket_notification.bytesAvailable() < sizeof(int))
                break;
            in >> header;
        }
        if ((uint)m_socket_notification.bytesAvailable() < sizeof(int))
            break;
        int code = 0;
        in >> code;
        auto it = notification_code::codes.find(code);
        if (it != notification_code::codes.end()) {
            qDebug() << QString::fromStdString(it->second);
            if (it->first == 0x00001001) {
                printWindowStatus(in);
                return;
            }
        }
        if (code == 0x00001005)
            return;
        if (code == 0x00001006)
            return;
        if (code == 0x0000100A)
            return;
        if (code == 0x0000100B)
            return;
        if (code == 0x0000100E)
            return;
        if (code == 0x0000100F)
            return;
        if (code == 0x00001010)
            return;
        if (code == 0x00001011)
            return;
        if (code == 0x00001012)
            return;
        if (code == 0x00001013)
            return;
        if (code == 0x00001014)
            return;
        if (code == 0x00001016)
            return;
        if (code == 0x00001017)
            return;
        if (code == 0x00001018)
            return;
        if (code == 0x00001019)
            return;
        if (code == 0x0000101A)
            return;
        if (code == 0x0000101B)
            return;
        if (code == 0x0000101F)
            return;
        if (code == 0x00001020)
            return;
        if (code == 0x00001021)
            return;
        if (code == 0x00001022)
            return;
        if (code == 0x00001023)
            return;
        if (code == 0x00001024)
            return;
        if (code == 0x00001025)
            return;
        if (code == 0x00001031)
            return;
        if (code == 0x00001032)
            return;
        if (code == 0x00001033)
            return;
        if (code == 0x00001034)
            return;
        if (code == 0x00001035)
            return;
        if (code == 0x00001036)
            return;
        if (code == 0x00001037)
            return;
        if (code == 0x00001038)
            return;
        if (code == 0x00001039)
            return;
        if (code == 0x00001041)
            return;
        if (code == 0x00001042)
            return;
        if (code == 0x00001043)
            return;
        if (code == 0x00001044)
            return;
        if (code == 0x00001045)
            return;
        if (code == 0x00001046)
            return;
        if (code == 0x00001047)
            return;
        if (code == 0x00001049)
            return;
        if (code == 0x0000104A)
            return;
        return;
        qCritical("Unknown notification code");
        break;
    }
}

void Datapath::setSceneSlot()
{
    m_busy = true;

    qDebug() << Q_FUNC_INFO;
    auto log = spdlog::get(logname);
    if (log) log->info() << "set scene slot";

    if (m_sources.isEmpty()) {
        auto ok = checkResponse(closeServerPCAll(WINDOW_TYPE::RGB));
        ok = ok || checkResponse(closeAppAll());
        m_busy = false;
        qDebug() << "Datapath::setSceneSlot:sceneFinished";
        emitSceneFinished(ok);
        return;
    }
    bool changed = true;
    if (positions.size() == m_sources.size()) {
        for (auto pos : positions) {
            for (auto source : m_sources) {
                if (source->getPosition() != pos) {
                    changed = false;
                    break;
                }
            }
            changed = false;
        }
    }
    if (!changed) {
        bool ok = checkResponse(openLayout());
        if (ok) {
            m_busy = false;
            emitSceneFinished(false);
            return;
        }
    }

    checkResponse(getAllAppWindows(m_AppWindows));
    checkResponse(getAllServerPCWindows());

    if (!checkResponse(getAllAppWindows(m_AppWindows))) {
        emitSceneFinished(false);
        m_busy = false;
        return;
    }

    if (!checkResponse(getAllServerPCWindows())) {
        emitSceneFinished(false);
        m_busy = false;
        return;
    }

    qDebug() << "Application number =" << m_AppWindows.info.size();
    qDebug() << "ServerPC number =" << m_Windows.data.size();

    for (int j = 0; j < m_AppWindows.info.count(); j++) {
        bool find = false;
        for (int i = 0; i < m_sources.count();i++) {
            auto src = m_sources.at(i);
            if (isVisited(src)) continue;
            if (src->gettype() != "APPSource") continue;
            if (compareApplication(m_AppWindows.info.at(j), src)){
                if (!src->getEnabledFlag()) continue;
                if (!compareApplicationSize(m_AppWindows.info.at(j), src)) {
                    setHandler(src,m_AppWindows.info.at(j).handle);
                }
                setVisited(src, true);
                find = true;
                break;
            }
        }
        if (!find) {
            closeWindowApp(m_AppWindows.info.at(j));
        }
    }
    for (auto data : m_Windows.data) {
        bool find = false;
        if (data.type != 1) continue;
        for (auto src : m_sources) {
            if (!src) {
                m_busy = false;
                emitSceneFinished(false);
                return;
            }
            if (src->gettype() != "NamedSource")
                continue;
            auto visited = isVisited(src);
            if (visited) continue;
            auto compared = compareNamed(data, src);
            if (compared) {
                if (!src->getEnabledFlag())
                    continue;
                setHandler(src,data.number);
                setVisited(src, true);
                find = true;
                break;
            }
        }
        if (!find) {
            int status = closeNamedApp(data);
            if (!checkResponse(status)) {
                m_busy = false;
                emitSceneFinished(false);
                return;
            }
        }
    }
    bool ok = createSources();
//    if (changed) {
//        bool ok = checkResponse(saveLayout());
//        if (ok) setProperty("isChanged", false);
//    }
    m_busy = false;
    emitSceneFinished(true);
}


void Datapath::emitSceneFinished(bool f) {
    emit sceneFinished(true);
    m_sources.clear();
}

void Datapath::onTimeout()
{
    if (m_busy)
        return;

    auto status = sendNoOperation();
    m_busy = false;
    if (!checkResponse(status)) {
        timer.stop();
        disconn();
    }
}

void Datapath::onStateChanged()
{
    emit statusChanged();
}

bool Datapath::connSlot()
{
    auto log = spdlog::get(logname);
    m_socket_control.connectToHost(m_address, m_port_control);
    m_socket_notification.connectToHost(m_address, m_port_notification);
    if (m_socket_control.waitForConnected(time_wait_connect)) {
        using namespace dwall_protocol::general_line;
        if (!checkResponse(user::userLogin2(m_socket_control, "Administrator", ""))) {
            return false;
        }
        timer.start();
        timer.setInterval(time_no_operation);
        connect(&timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
        if (log) log->info() << "D-WAll connected";
        m_status = status_t::connected;
        messSig("Контроллер подключен");
        return true;
    } else {
        if (log) log->info() << "D-WAll not connected";
        m_status = status_t::error;
        messSig("Контроллер не подключен");
    }
    if (log) log->info() << "D-WAll not connected";
    qDebug("Not connected in 3 sec!!!");
    return false;
}

bool Datapath::checkResponse(int code)
{
    if (code == 0)
        return true;
    if (code == static_cast<int>(datapath_socket_error::wait_error)) {
        sleep(3);
        m_socket_control.disconnectFromHost();
        bool ok = checkAndReconnect();
        if (ok)
            qDebug("Reconnect to dwall after wait error");
        else
            qDebug("Fail to reconnect to dwall after wait error");
        return false;
    }
    auto error = response_code_table::codes.find(code);
    if (error != response_code_table::codes.end()) {
        qCritical() << QString::fromStdString(error->second);
        return false;
    }
    qCritical() << "unknown datapath error, code" << code;
    return false;
}

bool Datapath::getSourcesForWindows() {
    if (!checkAndReconnect()) return false;
    for (auto window: m_AppWindows.info) {
        datapath_message::application::acquisition_source_rep as;
        datapath_message::application_window_source ws;
        if (window.source_number == 0) continue;
        getSourcesInfo(window.source_number,as);
        if (as.number == 1) {
            ws.window = window;
            ws.source = as.sources.at(0);
            m_Applications.push_back(ws);
        }
    }
    return true;
}

int Datapath::move_resize_fit(window_operation operation, quint64 handle, const QRect &rect, int order)
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    datapath_message::move_resize_fit request;
    request.message_type = operation;
    request.handle = handle;
    request.x = rect.x();
    request.y = rect.y();
    request.w = rect.width();
    request.h = rect.height();
    request.priority = order;
    out << request;
    if (m_socket_control.write(block) != 76)
        return static_cast<int>(datapath_socket_error::write_small_data);
    if (m_socket_control.waitForReadyRead(60000)) {
        if (m_socket_control.bytesAvailable() != 12) {
            qDebug() << Q_FUNC_INFO << "failed, bytes =" << m_socket_control.bytesAvailable();
            return static_cast<int>(datapath_socket_error::read_incorrect_length_data);
        }
        QDataStream stream(&m_socket_control);
        stream.setByteOrder(QDataStream::LittleEndian);
        qint32  head, code, status;
        stream >> head;
        stream >> code;
        stream >> status;
        qDebug() << Q_FUNC_INFO;
        qDebug() << "Response: code =" << code << "status =" << status;
        return status;
    }
    return static_cast<int>(datapath_socket_error::wait_error);
}

bool Datapath::compareFrames(datapath_message::application_window_source window, QSharedPointer<VSource> src)
{
    bool ret = false;
    switch(window.window.type) {
    case datapath_message::server::APPLICATION:
        ret = compareSimple(window.source.source_name,src);
        break;
    default:
        break;
    }
    return ret;
}

bool Datapath::compareSimple(QString name, QSharedPointer<VSource> src) {
    qDebug( ) << "Compare Simple " << name << " " << src->getname();
    return name == src->getname();
}

bool Datapath::getSourcesInfo(quint32 number,datapath_message::application::acquisition_source_rep &response)
{
    if (!checkAndReconnect()) return false;
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    datapath_message::application::acquisition_sources request;
    request.source_numbler = number;

    out << request;
    qDebug() << block.size();
    if (m_socket_control.write(block) < 20)
        return false;
    if (m_socket_control.waitForReadyRead(60000)) {
        QDataStream stream(&m_socket_control);
        stream >> response;
        qDebug() << " Datapath::getSourcesInfo  rest" << response.command << " " << response.status;
        return !response.status;
    }
    return false;
}

bool Datapath::compareSize(datapath_message::application_window_source window,QSharedPointer<VSource> src)
{
    QVariant pixs_w;
    QVariant pixs_h;
    int w;
    int h;

    getProperty(QString("pixs_w"),pixs_w);
    getProperty(QString("pixs_h"),pixs_h);
    w = pixs_w.toInt()/1000;
    h = pixs_h.toInt()/1000;
    VPosition src_pos = src->getPosition();

    int dx = (window.window.x - ((src_pos.lt.x() * pixs_w.toInt()))/1000);
    int dy = window.window.y - (src_pos.lt.y() * pixs_h.toInt())/1000;
    int dw = window.window.w - (((src_pos.rb.x()  - src_pos.lt.x()) * pixs_w.toInt()))/1000;
    int dh = window.window.h - (((src_pos.rb.y()  - src_pos.lt.y()) * pixs_h.toInt()))/1000;
    return (!
            (((dx <= -w) || (dx >= w)) ||
             ((dy <= -h) || (dy >= h)) ||
             ((dw <= -w) || (dw >= w)) ||
             ((dh <= -h) || (dh >= h))));
}

bool Datapath::compareApplicationSize(datapath_message::application::application_window_data window,QSharedPointer<VSource> src)
{
    QVariant pixs_w;
    QVariant pixs_h;

    getProperty("pixs_w",pixs_w);
    getProperty("pixs_h",pixs_h);
    auto w = pixs_w.toInt()/1000;
    auto h = pixs_h.toInt()/1000;
    VPosition src_pos = src->getPosition();

    int dx = (window.x - ((src_pos.lt.x() * pixs_w.toInt()))/1000);
    int dy = window.y - (src_pos.lt.y() * pixs_h.toInt())/1000;
    int dw = window.w - (((src_pos.rb.x()  - src_pos.lt.x()) * pixs_w.toInt()))/1000;
    int dh = window.h - (((src_pos.rb.y()  - src_pos.lt.y()) * pixs_h.toInt()))/1000;
    return (!
            (((dx <= -w) || (dx >= w)) ||
             ((dy <= -h) || (dy >= h)) ||
             ((dw <= -w) || (dw >= w)) ||
             ((dh <= -h) || (dh >= h))));
}

bool Datapath::compareNamdeSize(datapath_message::server::window_data window,QSharedPointer<VSource> src)
{
    QVariant pixs_w;
    QVariant pixs_h;
    int w;
    int h;

    getProperty(QString("pixs_w"),pixs_w);
    getProperty(QString("pixs_h"),pixs_h);
    w = pixs_w.toInt()/1000;
    h = pixs_h.toInt()/1000;
    VPosition src_pos = src->getPosition();

    int dx = (window.start_position_hor - ((src_pos.lt.x() * pixs_w.toInt()))/1000);
    int dy = window.start_position_ver - (src_pos.lt.y() * pixs_h.toInt())/1000;
    int dw = window.width - (((src_pos.rb.x()  - src_pos.lt.x()) * pixs_w.toInt()))/1000;
    int dh = window.height - (((src_pos.rb.y()  - src_pos.lt.y()) * pixs_h.toInt()))/1000;
    return (!
            (((dx <= -w) || (dx >= w)) ||
             ((dy <= -h) || (dy >= h)) ||
             ((dw <= -w) || (dw >= w)) ||
             ((dh <= -h) || (dh >= h))));
}

bool Datapath::replaceSource(QSharedPointer<VSource> src, quint64 handler)
{
    if (!checkAndReconnect())
        return false;

    QVariant pixs_w = getProperty("pixs_w");
    QVariant pixs_h = getProperty("pixs_h");
    if (pixs_w.isNull() || pixs_h.isNull())
        return false;
    bool ok;
    auto w = pixs_w.toInt(&ok)/1000.f;
    auto h = pixs_h.toInt(&ok)/1000.f;
    if (!ok)
        return false;
    VPosition src_pos = src->getPosition();
    int xn = src_pos.lt.x()*w;
    int yn = src_pos.lt.y()*h;
    int wn = (src_pos.rb.x() - src_pos.lt.x())*w;
    int hn = (src_pos.rb.y() - src_pos.lt.y())*h;
    QRect rect(xn, yn, wn, hn);

    if (src->gettype()  == "NamedSource") {
        auto ret = windowStatusAcquisition(handler);
        auto status = ret.first;
        QRect rectOld = ret.second;
        if (status)
            return false;
        //auto pos = listPosition(src);
        if (rectOld.topLeft() != rect.topLeft() || rectOld.isEmpty()) {
            auto status = moveResizeEtcNamedSource(datapath_message::serverPC::type_code::movement, handler, rect);
//            auto status = move_resize_fit(datapath_message::window_operation::MOVEMENT, handler, rect, pos);
            bool ok = checkResponse(status);
            if (!ok)
                return false;
        }
//        if (rectOld.size() != rect.size() || rectOld.isEmpty()) {
            if (true) {
            auto status = moveResizeEtcNamedSource(datapath_message::serverPC::type_code::resizing, handler, rect);
//            auto status = move_resize_fit(datapath_message::window_operation::RESIZING, handler, rect, pos);
            bool ok = checkResponse(status);
            return ok;
        }
    }
    else if (src->gettype() == "APPSource") {
        QRect rectOld;
        int status = appStatusAcquisition(rectOld, handler);
        if (status)
            return false;
        auto pos = listPosition(src);
        if (rectOld.topLeft() != rect.topLeft() || rectOld.isEmpty()) {
            auto status = move_resize_fit(datapath_message::window_operation::MOVEMENT, handler, rect, pos);
            bool ok = checkResponse(status);
            if (!ok)
                return false;
        }
        if (rectOld.size() != rect.size() || rectOld.isEmpty()) {
            auto status = move_resize_fit(datapath_message::window_operation::RESIZING, handler, rect, pos);
            bool ok = checkResponse(status);
            return ok;
        }
    }
    return true;
}

int Datapath::closeWindowApp(datapath_message::application::application_window_data s)
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    datapath_message::application::window_deletion  request;
    if (s.title.contains("avst")) return 0; /* FIX for my WM */
    request.handle = s.handle;
    out << request;
    if (m_socket_control.write(block) != 28)
        return static_cast<int>(datapath_socket_error::write_small_data);
    if (m_socket_control.waitForReadyRead(60000)) {
        if (m_socket_control.bytesAvailable() != 12) {
            qDebug() << "closeWindowApp failed " << m_socket_control.bytesAvailable();
            return static_cast<int>(datapath_socket_error::read_incorrect_length_data);
        }
        QDataStream stream(&m_socket_control);
        stream.setByteOrder(QDataStream::LittleEndian);
        qint32 type,status,h;
        stream >> h;
        stream >> type;
        stream >> status;
        qDebug() << Q_FUNC_INFO << "status:" << status << "title: " << s.title;
        return !status;
    }
    return static_cast<int>(datapath_socket_error::wait_error);
}

int Datapath::closeNamedApp(const datapath_message::common::serverPC_overlay_window_data &data)
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    datapath_message::serverPC::window_close request;
    request.window_number = data.number;
    out << request;
    if (m_socket_control.write(block) != 20)
        return static_cast<int>(datapath_socket_error::write_small_data);
    if (m_socket_control.waitForReadyRead(60000)) {
        if (m_socket_control.bytesAvailable() != 12) {
            qDebug() << "closeWindowApp failed " << m_socket_control.bytesAvailable();
            return static_cast<int>(datapath_socket_error::read_incorrect_length_data);
        }
        QDataStream stream(&m_socket_control);
        datapath_message::serverPC::window_close_response response;
        stream >> response;
        return response.status;
    }
    return static_cast<int>(datapath_socket_error::wait_error);
}

bool Datapath::createSources()
{
    QSet<bool> ok;
    positions.clear();
    foreach(QSharedPointer<VSource> src, m_sources) {
        positions << src->getPosition();
        auto enabled = src->getEnabledFlag();
        if (!enabled) continue;
        if (src->gettype() == "APPSource" && !isVisited(src)) {
            auto code = createApplicationWindow(src);
            ok << checkResponse(code);
        }
        else if ((src->gettype() == "NamedSource" ||
                  src->gettype() == EXTRONMIXER_TYPE ||
                  src->gettype() == MIXER_TYPE ||
                  src->gettype() == EXTRON_TYPE
                  )
                 && !isVisited(src)) {
            auto code = createNamedWindow(src);
            ok << checkResponse(code);
        }
        else if (isVisited(src)) {
            replaceSource(src,handler(src));
//            qDebug() << Q_FUNC_INFO << "Unvisited source" << src->gettype() << src->getname();
//            return false;
        }
    }
    if (ok.contains(false))
        return false;
    if (!checkResponse(getAllAppWindows(m_AppWindows))) {
        emitSceneFinished(false);
        m_busy = false;
        return false;
    }
    return true;
}

int Datapath::createApplicationSource(QSharedPointer<VSource> src)
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    datapath_message::application::adding_application_source request;
    QVariant command_var;
    if (!checkAndReconnect())
        return static_cast<int>(datapath_socket_error::reconnect_error);
    if(src->getProperty("Command", command_var)) {
        request.source_data.command_line = command_var.toString();
    }
    request.source_data.start_in = "C:\\";
    request.source_data.source_name = src->getname();
    out << request;
    int n = m_socket_control.write(block) ;
    if (n < 2540)
        return static_cast<int>(datapath_socket_error::write_small_data);
    if (m_socket_control.waitForReadyRead(60000)) {
        int bytes = m_socket_control.bytesAvailable();
        if (bytes != 20) {
            qDebug() << "FAILED" << Q_FUNC_INFO << m_socket_control.bytesAvailable();
            return static_cast<int>(datapath_socket_error::read_incorrect_length_data);
        }
        QDataStream stream(&m_socket_control);
        stream.setByteOrder(QDataStream::LittleEndian);
        quint32 header, code, status, number, r;
        stream >> header;
        stream >> code;
        stream >> status;
        stream >> number;
        stream >> r;
        return status;
    }
    return static_cast<int>(datapath_socket_error::wait_error);
}

int Datapath::getAllAppWindows(datapath_message::application::acquisition_windows_re &result)
{    
    if (!checkAndReconnect())
        return static_cast<int>(datapath_socket_error::reconnect_error);
    result.info.clear();
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    datapath_message::application::acquisition_windows request;
    out << request;
    if (m_socket_control.write(block) != 28)
        return static_cast<int>(datapath_socket_error::write_small_data);
    if (m_socket_control.waitForReadyRead(60000)) {
        QDataStream stream(&m_socket_control);
        datapath_message::application::acquisition_windows_re response;
        parseWindows(stream, response, &m_socket_control);

        result = response;
//        qDebug() << Q_FUNC_INFO;
//        qDebug() << "Response: code =" << response.code << " status =" << response.status;
        return response.status;
    }
    return static_cast<int>(datapath_socket_error::wait_error);
}

bool Datapath::compareApplication(datapath_message::application::application_window_data window, QSharedPointer<VSource> src)
{
    QVariant command_var;
    QString command;
    if(src->getProperty("Command", command_var)) {
        command = command_var.toString();
    }
    return window.command.contains(command);
}

bool Datapath::compareNamed(datapath_message::server::window_data w, QSharedPointer<VSource> src)
{
    QVariant named_var;
    if(src->getProperty("named", named_var)) {
        bool ok;
        auto number = named_var.toInt(&ok);
        auto window_data_number = w.source_number;
        if (ok) return window_data_number == number;
    }
    return false;
}

bool Datapath::compareNamed(datapath_message::common::serverPC_overlay_window_data data, QSharedPointer<VSource> src)
{
    QVariant named_var;
    if(src->getProperty("named", named_var)) {
        bool ok;
        auto number = named_var.toInt(&ok);
        if (ok)
            return data.image_source_number == number;
    }
    return false;
}

int Datapath::createNamedWindow(const QSharedPointer<VSource> &src)
{
    if (!checkAndReconnect())
        return static_cast<int>(datapath_socket_error::reconnect_error);
    datapath_message::serverPC::new_window_creation request;
    QVariant var = src->getProperty("named");
    if (var.isNull())
        return static_cast<int>(controller_error::property_error);
    bool ok;
    int number = var.toInt(&ok);
    if (!ok)
        return static_cast<int>(controller_error::property_error);
    request.source_number = number;
    QVariant pixs_w = getProperty("pixs_w");
    QVariant pixs_h = getProperty("pixs_h");
    if (pixs_w.isNull() || pixs_h.isNull())
        return static_cast<int>(controller_error::property_error);
    auto wpx = pixs_w.toInt(&ok)/1000.f;
    auto hpx = pixs_h.toInt(&ok)/1000.f;
    if (!ok)
        return static_cast<int>(controller_error::property_error);
    VPosition src_pos = src->getPosition();

    int xn = src_pos.lt.x() * wpx;
    int yn = src_pos.lt.y() * hpx;
    int w = (src_pos.rb.x() - src_pos.lt.x())*wpx;
    int h = (src_pos.rb.y() - src_pos.lt.y())*hpx;
    request.display_start_position_hor = xn;
    request.display_start_position_ver = yn;
    request.display_height = h;
    request.display_width = w;
    if (src->isCropped()){
        int cx = src_pos.getCropLxW()*1000;
        int cy = src_pos.getCropTyH()*1000;
        int cw = src_pos.getCropSowOnWiw()*1000;
        int ch = src_pos.getCropSohOnWih()*1000;
        if (cx > 999) cx = 999;
        if (cy > 999) cy = 999;
        if (cx < 1) cx = 0;
        if (cy < 1) cy = 0;
        if (cw > 999) cw = 999;
        if (ch > 999) cw = 999;
        if (cw < 2) cw = 1;
        if (ch < 2) ch = 1;
        request.crop.pos_hor = cx;
        request.crop.pos_ver = cy;
        request.crop.size_hor = cw;
        request.crop.size_ver = ch;
    }
    else {
        request.crop.pos_hor = 0x7FFFFFFF;
        request.crop.pos_ver = 0x7FFFFFFF;
        request.crop.size_hor = 1000;
        request.crop.size_ver = 1000;
    }

    return dwall_protocol::general_line::serverPC_overlay_window_function::newWindowCreation(m_socket_control,request);
}

int Datapath::getAllServerPCWindows()
{
    if (!checkAndReconnect())
        return static_cast<int>(datapath_socket_error::reconnect_error);
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    datapath_message::serverPC::window_status_acquisition request;
    out << request;
    if (m_socket_control.write(block) != 20)
        return static_cast<int>(datapath_socket_error::write_small_data);
    if (m_socket_control.waitForReadyRead(60000)) {
        QDataStream stream(&m_socket_control);
        m_Windows.data.clear();
        parseServerWindows(stream,m_Windows,&m_socket_control);
//        stream >> m_Windows;
        return m_Windows.status;
    }
    return static_cast<int>(datapath_socket_error::wait_error);
}

bool Datapath::checkAndReconnect()
{
    if (
            (m_socket_control.state()  != QAbstractSocket::ConnectedState) ||
            (!m_socket_control.isValid()) ||
            (!m_socket_control.isWritable()))
    {
        return reconnect();
    } else {
        return true;
    }
}

bool Datapath::isVisited(const QSharedPointer<VSource> &src) const
{
    QVariant visited;
    if(src->getProperty("visited", visited)) {
        return visited.toBool();
    }
    return false;
}

void Datapath::setVisited(QSharedPointer<VSource> src, bool visited)
{
   src->setProperty("visited", visited);
}

void Datapath::setListPosition(QSharedPointer<VSource> src, int i)
{
    src->setProperty("listPosition", i);
}

int Datapath::listPosition(const QSharedPointer<VSource> &src) const
{
    QVariant listPos;
    if(src->getProperty("listPosition", listPos)) {
        bool ok;
        auto position = listPos.toInt(&ok);
        if (ok) return position;
    }
    return 1; // default foremost
}

void Datapath::onDisconnect()
{
    disconn();
}

void Datapath::run()
{




    qRegisterMetaType<QAbstractSocket::SocketState>("QAbstractSocket::SocketState");
    connect(&m_socket_control, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(onStateChanged()), Qt::DirectConnection);
    connect(&m_socket_control, SIGNAL(error(QAbstractSocket::SocketError)), this,
            SLOT(displayError(QAbstractSocket::SocketError)),Qt::DirectConnection);
    //connect(&m_socket_control, SIGNAL(disconnected()), this, SLOT(onDisconnect()), Qt::DirectConnection);
    connect(&m_socket_notification, SIGNAL(readyRead()), this, SLOT(getNotification()), Qt::DirectConnection);
    connect(this, SIGNAL(setSceneSig()), this, SLOT(setSceneSlot()), Qt::QueuedConnection);
    connect(this, SIGNAL(connSig()), this, SLOT(connSlot()), Qt::QueuedConnection);
    exec();
}

void Datapath::setHandler(QSharedPointer<VSource> src, int handler) {
   src->setProperty(QString("handler"),QVariant(handler));
}

int Datapath::handler(QSharedPointer<VSource> src) {
    QVariant handler;
    int handeler_int = 0;
    if(src->getProperty("handler", handler)) {
        handeler_int = handler.toInt();
    }
    return handeler_int;
}

Q_EXPORT_PLUGIN2(datapath,Datapath)

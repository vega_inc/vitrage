#ifndef VCONTROLLER_H
#define VCONTROLLER_H

#include "../vsource/vsource.h"
#include <QHostAddress>

#define CONTROLLER

enum class status_t {
    start,
    connected,
    stopped,
    disc,
    error,
    busy,
    unbusy
};

static const QString CONTR_HOST = "host";
static const QString CONTR_PORT = "port";
static const QString CONTR_NAME = "username";
static const QString CONTR_PASSWD = "passwd";
static const QString CONTR_MODEL  = QString("model");
static const QString PIC_TYPE = "FileSource"; /* Picture view type */
static const QString VNC_TYPE = QString("VNCClient"); /* VNC Client type */
static const QString IPCAMERA_TYPE = QString("IpCameraMJpeg"); /* MJpeg stream */
static const QString WBPAGE_TYPE = QString("WebPage");
static const QString NAMED_TYPE = QString("NamedSource");
static const QString EXTRON_TYPE = QString("ExtronInput");
static const QString MIXER_TYPE = QString("MixerVNCClient");
static const QString EXTRONMIXER_TYPE = QString("MixerExtronVNCClient");

using Sources = QList<QSharedPointer<VSource>>;

class VController;
class AbstractVController {
public:
    virtual void conn()                                     = 0;
    virtual void disconn()                                  = 0;
    virtual bool createSource(VSource &)                    = 0;
    virtual bool startSource(VSource &)                     = 0;
    virtual bool stopSource(VSource &)                      = 0;
    virtual bool enableSourceFlag(VSource &,bool )          = 0;
    virtual bool setScene(const Sources &)                  = 0;
    virtual VController *clone()                            = 0;
    virtual void bridge(VController *v, const QList<QSharedPointer<VSource>> &l) { Q_UNUSED(v); Q_UNUSED(l); }
};


Q_DECLARE_INTERFACE(AbstractVController,"com.Vega.Vitrage.AbstractVController/1.0")


class VController : public QThread, public AbstractVController
{
    Q_OBJECT
    Q_INTERFACES(AbstractVController)

public:
    VController(){}
    VController& operator=(const VController&);
    explicit VController(const VController&);
    virtual ~VController(){}
    void setName(QString str);
    void setProperty(const QString&, const QVariant&);
    bool getProperty(const QString&, QVariant&);
    QVariant getProperty(const QString &name) const;

    QMap<QString, QVariant> getMapProperty() const { return m_propMap; }
    bool isStarted() const;	/* return false on success */
    QString errorMsg() const { return m_errorMsg; }
    virtual status_t status() const;
    void setStatus(status_t );
    QString getName() const { return m_name; }
    bool isValid() const { return !m_address.isNull(); }
    void setAddress(const QHostAddress &address) { m_address = address; }
    void setPort(uint port) { m_port = port; }
    void setLogin(const QString &login) { m_login = login; }
    void setPassword(const QString &password) { m_password = password; }
    QString address() const { return m_address.toString(); }
    uint port() const { return m_port; }

signals:
    void errorSig(QString error_string);
    void messSig(QString error_string);
    void statusChanged();
    void srcsCreated();
    void connectedS();
    void sceneFinished(bool);

protected:
    QMap<QString, QVariant> m_propMap;
    QString m_name;
    QString m_errorMsg = "Not started";
    status_t m_status = status_t::disc;
    bool m_started = true;
    QHostAddress m_address = QHostAddress::LocalHost;
    uint m_port = 0;
    QString m_login;
    QString m_password;
};

#endif /* VCONTROLLER_H */

#include "vcontroller.h"

VController::VController(const VController &S):QThread()
{
    m_propMap = S.getMapProperty();
}

void VController::setName(QString name_str) 
{
    m_name = name_str;
}

void VController::setProperty(const QString& name, const QVariant& val)
{
    m_propMap.insert(name, val);
}

bool VController::getProperty(const QString& name, QVariant& value)
{
    bool ret_val = m_propMap.contains(name);
    if (ret_val)
        value = m_propMap.value(name);
    return ret_val;
}

QVariant VController::getProperty(const QString& name) const
{
    return m_propMap.value(name);
}

bool VController::isStarted() const
{
    return m_started;
}

status_t VController::status() const
{
    return m_status;
}

void VController::setStatus(status_t status)
{
    m_status = status;
}

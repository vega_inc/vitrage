#-------------------------------------------------
#
# Project created by QtCreator 2012-12-13T14:59:03
#
#-------------------------------------------------
include(../common.pri)


TARGET = vcontroller

QT += network

DESTDIR = $$INSTALL_PATH_LIB

TEMPLATE = lib

DEFINES += VCONTROLLER_LIBRARY

INCLUDEPATH += $$INSTALL_PATH_INCLUDE \
		/usr/include/

SOURCES += vcontroller.cpp

HEADERS += vcontroller.h

unix {
    target.path = /$(DESTDIR)
    INSTALLS += target
    header_files.files = $$HEADERS
    header_files.path = /$(INCLUDEDIR)
    INSTALLS += header_files
}


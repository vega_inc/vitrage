#ifndef VAPPSOURCE_H
#define VAPPSOURCE_H

#include <vsource/vsource.h>
#include <qurl.h>
#define  DEFAULT_FILE "./config/backImage.jpg"

#define APP_SOURCE "APPSource"

class VAppSource : public VSource {
    Q_OBJECT
public:
   VAppSource(QWidget *parent, QUrl &url);
   VAppSource();
   VAppSource(VAppSource &src);
   VAppSource(VAppSource* src);
   virtual ~VAppSource();

  void resize_src(uint32_t width, uint32_t height);
  void startQuitting();
  QSize framebufferSize();

   void start();
   void stop(){}
   void finalize() {}
   void setUrl(QUrl r){Q_UNUSED(r)}
   QSharedPointer<VSource>  clone();
public slots:
  void scaleResize(int w, int h);

signals: 
	void setFrameSize(QSize);

protected:
    void paintEvent(QPaintEvent *);

private:
    QString m_fileName;
    bool m_initDone;
    int m_buttonMask;
    QMap<unsigned int, bool> m_mods;
    int m_x, m_y, m_w, m_h;
    bool m_repaint;
    bool m_quitFlag;
    bool m_firstPasswordTry;
    bool m_dontSendClipboard;
    qreal m_horizontalFactor;
    qreal m_verticalFactor;

    QImage m_frame;

    bool isQuitting();
    void loadImage();

private slots:
    void setCut(const QString &text);
    void requestPassword();
    void outputErrorMessage(const QString &message);
};

class VAppCreator : public QObject , public VSourceCreator {
    Q_OBJECT
    Q_INTERFACES(VSourceCreator)
public:
	
    VAppCreator();
    ~VAppCreator();
    QString getSourceType(); 
    VSource *createVSource();
    QList<VAppCreator::Param> getParamsList();
};

#endif  /* VAPPSOURCE_H */

include(../common.pri)
TEMPLATE = lib

CONFIG += dll \
    plugin

TARGET = vappsource

DESTDIR = $$INSTALL_PATH_PLUGINS

INCLUDEPATH += $$INSTALL_PATH_INCLUDE \
	       /usr/include/


LIBS += -L$${INSTALL_PATH_LIB} \
    -lvsource \

HEADERS += vappsource.h 
SOURCES += vappsource.cpp

unix {
       target.path = /$(DESTDIR)
       INSTALLS += target
     }


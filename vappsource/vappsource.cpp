#include "vappsource.h"
#include <QPainter>
#include <QPaintEvent>
#include <QImageReader>
#include <QDebug>

VAppSource::VAppSource(QWidget *parent, QUrl &url)
    : m_fileName(DEFAULT_FILE)
{
    Q_UNUSED(parent);
    url = url;
    host = url.host();
    setAutoFillBackground(true);
    loadImage();
    setMouseTracking(true);
    pressed = false;
    m_propMap.insert(F1000_TYPE_PROP,VSource::SubSystemKind_PictureViewer);
}

VAppSource::VAppSource()
    : m_fileName(DEFAULT_FILE)
{
    setAutoFillBackground(true);
    loadImage();
    setMouseTracking(true);
    pressed = false;
    type = QString(APP_SOURCE);
}

VAppSource::VAppSource(VAppSource &src)
    : VSource(src)
{
    setAutoFillBackground(true);
    loadImage();
    setMouseTracking(true);
    pressed = false;
    m_propMap = src.m_propMap;
    auto status = src.getStatus();
    setStatus(status);
    auto flag = src.getEnabledFlag();
    setEnabledFlag(flag);
    setPrototype(&src);
    setPhysName(src.getPhysName());
    m_propMap.insert(F1000_TYPE_PROP,VSource::SubSystemKind_PictureViewer);
    type = QString(APP_SOURCE);
}


VAppSource::VAppSource(VAppSource *src)
    : VSource(src)
    , m_fileName(DEFAULT_FILE)
{
    setAutoFillBackground(true);
    loadImage();
    setMouseTracking(true);
    pressed = false;
    m_propMap = src->m_propMap;
    setStatus(src->getStatus());
    setEnabledFlag(src->getEnabledFlag());
    setPrototype(src);
    setPhysName(src->getPhysName());
    m_propMap.insert(F1000_TYPE_PROP,VSource::SubSystemKind_PictureViewer);
    type = QString(APP_SOURCE);
}

VAppSource::~VAppSource()
{
}



QSize VAppSource::framebufferSize()
{
    return m_frame.size();
}


void VAppSource::startQuitting()
{
    m_quitFlag = true;
}

bool VAppSource::isQuitting()
{
    return m_quitFlag;
}

void VAppSource::start()
{
    loadImage();
}


void VAppSource::requestPassword()
{
}

void VAppSource::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    auto w = width();
    auto h = height();
    QRect rect(0, 0, w, h);
    painter.fillRect(rect, QBrush(Qt::black));
    QVariant var;
    if(getProperty("Command", var)) {
        auto command = var.toString();
        if (command.isEmpty())
            return;
        painter.setPen(Qt::white);
        painter.drawText(rect, Qt::AlignCenter, command);
    }
}

void VAppSource::resize_src(uint32_t width, uint32_t height)
{
    m_verticalFactor = (qreal) height / m_frame.height();
    m_horizontalFactor = (qreal) width / m_frame.width();
}

void VAppSource::loadImage()
{
    QVariant var;
    if(getProperty("picture", var)) {
        //qDebug() <<  QImageReader::supportedImageFormats();
        m_frame.load(var.toString());
    }
    if(m_frame.isNull()) {
        m_frame.load(DEFAULT_FILE);
        if(m_frame.isNull()) {
            QImage dummy(1024,1080,QImage::Format_RGB32);
            m_frame = dummy;
        }
    }
}


void VAppSource::outputErrorMessage(const QString &str)
{
    qDebug() << str;
}



QSharedPointer<VSource> VAppSource::clone(void)
{
    return QSharedPointer<VSource>(new VAppSource(*this));
}



void VAppSource::setCut(const QString &text)
{
    Q_UNUSED(text);
}

VAppCreator::VAppCreator()
{
}

VAppCreator::~VAppCreator() {
}


QString VAppCreator::getSourceType()
{
    return APP_SOURCE;
}

QList<VAppCreator::Param> VAppCreator::getParamsList()
{
    QList<Param> lst;
    lst.push_back(Param("Command",tr("path to exe file)"),STRING));
    lst.push_back(Param("picture",tr("Path to file for vitrage"), STRING, false));
    lst.push_back(Param("icon",(":/ava5.png"),ICON));
    return lst;
}


VSource *VAppCreator::createVSource()
{
    return (new VAppSource());
}



Q_EXPORT_PLUGIN2(VAppSource,VAppCreator)


#!/bin/bash

ARCH=$1

PROJECT="vitrage"

#set tarball name 
TAR_NAME=$2

# enter to the bin dir
cd bin

# tar only needed files

tar zcf "$TAR_NAME.tar.gz" ./*

mv "$TAR_NAME.tar.gz" /media/bark/projects/$PROJECT/binary/$ARCH/


cd /media/bark/projects/$PROJECT/binary/$ARCH/


`rm -f "$PROJECT"_build.tar.gz`

`ln -s "$TAR_NAME".tar.gz "$PROJECT"_build.tar.gz`

cd -

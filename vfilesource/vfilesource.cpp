#include <QPainter>
#include <QPaintEvent>
#include <QImageReader>
#include <QDebug>

#include "vfilesource.h"

static const char *FILE_SOURCE = "FileSource";

VFileSource::VFileSource(QWidget *parent, QUrl &url)
{
    Q_UNUSED(parent);
    url = url;
    host = url.host();
    setAutoFillBackground(true);
    loadFile();
    setMouseTracking(true);
    pressed = false;
    m_propMap.insert(F1000_TYPE_PROP,VSource::SubSystemKind_PictureViewer);
}

VFileSource::VFileSource()
{
    setAutoFillBackground(true);
    loadFile();
    setMouseTracking(true);
    pressed = false;
    type = FILE_SOURCE;
}

VFileSource::VFileSource(VFileSource &src):VSource(src)
{
    setAutoFillBackground(true);
    loadFile();
    setMouseTracking(true);
    pressed = false;
    m_propMap = src.m_propMap;
    setStatus(src.getStatus());
    setEnabledFlag(src.getEnabledFlag());
    setPrototype(&src);
    setPhysName(src.getPhysName());
    m_propMap.insert(F1000_TYPE_PROP,VSource::SubSystemKind_PictureViewer);
    type = FILE_SOURCE;
}

VFileSource::VFileSource(VFileSource *src)
    : VSource(src)
{
    setAutoFillBackground(true);
    loadFile();
    setMouseTracking(true);
    pressed = false;
    m_propMap = src->m_propMap;
    setStatus(src->getStatus());
    setEnabledFlag(src->getEnabledFlag());
    setPrototype(src);
    setPhysName(src->getPhysName());
    m_propMap.insert(F1000_TYPE_PROP,VSource::SubSystemKind_PictureViewer);
    type = FILE_SOURCE;
}

void VFileSource::start()
{
    loadFile();
}

void VFileSource::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    auto w = width();
    auto h = height();
    QRect rect(0, 0, w, h);
    painter.fillRect(rect, QBrush(Qt::black));
    QVariant var;
    if(getProperty("linuxhost", var)) {
        auto fname = var.toString();
        if (m_type == TYPE::PICTURE && QFile(fname).exists()) {
            painter.drawImage(rect, QImage(fname));
        }
        auto list = fname.split("/");
        if (list.isEmpty())
            return;
        auto text = list.last();
        painter.setPen(Qt::white);
        painter.drawText(rect, Qt::AlignCenter, text);
    }
}

void VFileSource::loadFile()
{
    QVariant var;
    if(getProperty("linuxhost", var)) {
        auto fname = var.toString();
        setType(fname);
        if (m_type == TYPE::NONE)
            return;
        setProperty("path", fname);
    }
}
/// set type of vsource from file name
void VFileSource::setType(const QString &filename)
{
    QStringList video = {"mp4", "mov", "avi", "mkv", "wmv"};
    for (auto ext : video) {
        if (filename.contains(ext)) {
            m_type = TYPE::VIDEO;
            return;
        }
    }
    for (auto ext : QImageReader::supportedImageFormats()) {
        if (filename.contains(ext)) {
            m_type = TYPE::PICTURE;
            return;
        }
    }
}

void VFileSource::outputErrorMessage(const QString &str)
{
    qDebug() << Q_FUNC_INFO;
    qDebug() << str;
}

QSharedPointer<VSource> VFileSource::clone(void)
{
    return QSharedPointer<VSource>(new VFileSource(*this));
}

VFileCreator::VFileCreator()
{
}

VFileCreator::~VFileCreator() {
}

QString VFileCreator::getSourceType()
{
    return FILE_SOURCE;
}

QList<VFileCreator::Param> VFileCreator::getParamsList()
{
    QList<Param> lst;
    lst.push_back(Param("muraHost",tr("Path to file for Matrox controller(used by Vega controller)"),TYPE::STRING,FALSE));
    lst.push_back(Param("host",tr("Path to file for Jupiter controller")));
    lst.push_back(Param("linuxhost",tr("Path to file for vitrage")));
    lst.push_back(Param("icon",(":/ava5.png"),TYPE::ICON));
    return lst;
}

VSource *VFileCreator::createVSource()
{
    return (new VFileSource());
}

Q_EXPORT_PLUGIN2(VFileSource,VFileCreator)

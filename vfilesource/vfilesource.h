#ifndef VFILESOURCE_H
#define VFILESOURCE_H

#include <QUrl>
#include "../vsource/vsource.h"

class VFileSource : public VSource {
    Q_OBJECT
public:
    enum class TYPE { NONE, VIDEO, PICTURE };
    VFileSource(QWidget *parent, QUrl &url);
    VFileSource();
    VFileSource(VFileSource &src);
    VFileSource(VFileSource* src);

    void start() final;
    void stop() final {}
    void finalize() final {}
    QSharedPointer<VSource> clone();
    void resize_src(uint32_t, uint32_t) final {}
    void startQuitting();

public slots:
    void scaleResize(int w, int h);

signals:
    void setFrameSize(QSize);

protected:
    void paintEvent(QPaintEvent *);

private:
    TYPE m_type = TYPE::NONE;
    void loadFile();
    TYPE getType() const { return m_type; }
    void setType(const QString &filename);

private slots:
    void outputErrorMessage(const QString &message);
};

class VFileCreator : public QObject , public VSourceCreator {
    Q_OBJECT
    Q_INTERFACES(VSourceCreator)
public:

    VFileCreator();
    ~VFileCreator();
    QString getSourceType();
    VSource *createVSource();
    QList<VFileCreator::Param> getParamsList();
};

#endif  /* VFILESOURCE_H */

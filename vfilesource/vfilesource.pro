include(../common.pri)
TEMPLATE = lib
CONFIG += dll \
    plugin

TARGET = vfilesource

DESTDIR = $$INSTALL_PATH_PLUGINS

INCLUDEPATH += $$INSTALL_PATH_INCLUDE \
	       /usr/include/


LIBS += -L$${INSTALL_PATH_LIB} \
    -lvsource \

# Input
HEADERS += vfilesource.h 
SOURCES += vfilesource.cpp

unix {
       target.path = /$(DESTDIR)
       INSTALLS += target
     }


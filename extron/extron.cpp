#include <qplugin.h>
#include "extron.h"

#define DEF_LAG 10000               // default lag in milliseconds
#define DEF_HOST "192.168.254.254"  // default host
#define DEF_PORT 23                 // default port

#define Extron_SOURCE "ExtronInput"
#define VNCMIXEREXTRON_SOURCE "MixerExtronVNCClient"

Extron::Extron()
    : m_success("OK")
{
    m_name = "extron";

    setProperty("host","localhost");
    setProperty("port","23");
    setProperty("passwd","");
    setProperty("username","Admin");
    setStatus(status_t::stopped);
    time_f.start();
    if (!isRunning()) {
        m_socket  = new QTcpSocket();
        moveToThread(this);
        m_socket->moveToThread(this);
        start();



    }
}

Extron::~Extron()
{
    qDebug() << Q_FUNC_INFO << time_f.elapsed();
    exit();
    quit();
}

void Extron::newState(QAbstractSocket::SocketState state)
{
    qDebug() << Q_FUNC_INFO << time_f.elapsed();
    qDebug() << "State changed" << state;
}

void Extron::run()
{
    connect(m_socket,SIGNAL(connected()),this,SLOT(conn_slot()), Qt::DirectConnection);
    connect(m_socket,SIGNAL(disconnected()),this,SLOT(disconnected()), Qt::DirectConnection);
    qRegisterMetaType<QAbstractSocket::SocketError>("QAbstractSocket::SocketError") ;
    connect(m_socket,SIGNAL(error(QAbstractSocket::SocketError)),
            this,SLOT(sock_err(QAbstractSocket::SocketError)), Qt::DirectConnection);
    connect(m_socket,SIGNAL(readyRead()),this,SLOT(ready()), Qt::DirectConnection);
    //connect(&m_timer,SIGNAL(timeout()),this,SLOT(timer_timeout()));
    qRegisterMetaType<QAbstractSocket::SocketState>("QAbstractSocket::SocketState");
    connect(m_socket,SIGNAL(stateChanged(QAbstractSocket::SocketState)),this
            ,SLOT(newState(QAbstractSocket::SocketState)), Qt::DirectConnection);
    connect(m_socket,SIGNAL(readChannelFinished()),this,SLOT(readFinished()), Qt::DirectConnection);
    connect(this,SIGNAL(dissig()),this,SLOT(disSlot()), Qt::QueuedConnection);
    connect(this,SIGNAL(consig()),this,SLOT(conSlot()), Qt::QueuedConnection);
    setProperty(QString("model").toAscii().data(),QString("Mura"));
    connect(this,SIGNAL(setSceneSig()),this,SLOT(setSceneSlot()),Qt::QueuedConnection );
    qDebug() << Q_FUNC_INFO << time_f.elapsed();
    exec();
}

void Extron::conn()
{
    qDebug() << Q_FUNC_INFO << time_f.elapsed();

    emit consig();
}

void Extron::conSlot()
{
    qDebug() << Q_FUNC_INFO << time_f.elapsed();
    m_socket->flush();
    m_socket->close();


    QVariant host_var, port_var;
    getProperty("host",host_var);
    if(host_var.isValid()) {
        m_url.setHost(host_var.toString());
    } else m_url.setHost(DEF_HOST);


    getProperty("port",port_var);
    qDebug() << port_var.toString();
    if(port_var.isValid()) {
        m_url.setPort(port_var.toInt());
    } else m_url.setPort(DEF_PORT);
    startTimer();

    if (!m_socket->isOpen()) {
        m_socket->connectToHost(m_url.host(),m_url.port());
     //   m_socket->waitForConnected();
       }
    //emit connectedS();
}

void Extron::timer_timeout()
{

    qDebug() << Q_FUNC_INFO << time_f.elapsed();
    m_timer.stop();
    QString str("Extron ERROR:Timout error");
    setStatus(status_t::error);
    m_busy = false;
    emit statusChanged();
    emit errorSig(str);
}

void Extron::startTimer()
{
    m_timer.start(DEF_LAG);
}

void Extron::disconn()
{
    emit dissig();
}

void Extron::disSlot()
{
    qDebug() << Q_FUNC_INFO << time_f.elapsed();
    if (m_socket->state() == QAbstractSocket::UnconnectedState) {
        setStatus(status_t::disc);
        emit statusChanged();
        m_busy = false;
        return;
    }
    m_socket->disconnectFromHost();
    if (m_socket->state() == QAbstractSocket::UnconnectedState ||
            m_socket->waitForDisconnected(1000))
        setStatus(status_t::disc);
    m_errorMsg = QString(tr("Disconnected"));
    emit statusChanged();
    m_busy = false;
}

void Extron::conn_slot()
{
    qDebug() << Q_FUNC_INFO << time_f.elapsed();
    qDebug() << "Connected Slot" ;
    QVariant name;
    QVariant passwd;
    getProperty(QString("username"),name);
    getProperty(QString("passwd"),passwd);
    getResponse();
    QString er_str;
    QString str =("\r") + name.toString() + ("\r\n");
    //QString str = ("\r");
    m_socket->write(str.toAscii().data(),str.count());
    str = passwd.toString() + ("\r\n");

    m_socket->waitForBytesWritten(300000);
    if ((!getResponse())  || (m_resp != QString("OK\r\n"))) {
        er_str = QString("Extron ERROR: Connect command error") + m_resp;
        setStatus(status_t::error);
        m_errorMsg = er_str;
        //m_socket->close();
        emit errorSig(er_str);
    } else {
        er_str = QString("Extron Connected");
        m_errorMsg = er_str;
        setStatus(status_t::connected);
        messSig(er_str);
    }
    m_socket->write(str.toAscii().data(),str.count());
    m_timer.stop();
    m_socket->waitForBytesWritten(300000);
    m_respFlag= false;
    er_str = QString(tr("Extron Connected"));
    m_errorMsg = er_str;
    setStatus(status_t::connected);
    messSig(er_str);
    emit statusChanged();
}

void Extron::disconnected()
{
    qDebug() << Q_FUNC_INFO << time_f.elapsed();
    qDebug() << "Disconnnected signal;";
}

void Extron::sock_err(QAbstractSocket::SocketError err)
{
    qDebug() << Q_FUNC_INFO << time_f.elapsed();
    QString str = QString("Mura ERROR: Socker Error ") + QString::number(err);
    qDebug() << str;
    bool flag  = false;
    QString msg;
    switch (err) {
    case (QAbstractSocket::ConnectionRefusedError) :
        flag = true;
        msg = QString("ConnectionRefusedError");
        break;
    case (QAbstractSocket::NetworkError) :
        flag = true;
        msg = QString("NetworkError");
        break;
    default:
        break;
    }
    if (flag) {
        setStatus(status_t::error);
        m_errorMsg = msg;
        emit statusChanged();
    }
}

void Extron::ready()
{
    qDebug() << Q_FUNC_INFO << time_f.elapsed();
    qDebug() << "Ready slot";
    m_respFlag = true;
}

bool Extron::getResponse()
{
    qDebug() << Q_FUNC_INFO << time_f.elapsed();
    m_resp.clear();
    bool ret = true;
    if (m_socket->state() != QAbstractSocket::ConnectedState) {
        m_busy = false;
        return true;
    }
    ret = m_socket->waitForReadyRead(13000);
    if (!m_socket->isValid()) {
        m_busy = false;
        return true;
    }
    QByteArray buffer = m_socket->readAll();
    m_resp = QString(buffer);
    qDebug() << Q_FUNC_INFO << m_resp;
    if (!m_resp.count()){
        qDebug() << Q_FUNC_INFO << "Empty responce";
        ret = false;
    }
    return ret;
}

bool Extron::sendRequest(char *data, uint32_t count)
{
    qDebug() << Q_FUNC_INFO << time_f.elapsed();
    m_busy  = true;
    m_respFlag =false;
    int len = m_socket->write(data,count);
    bool ret =	m_socket->waitForBytesWritten(10000);
    if (!ret) {
        qDebug() << "sendRequest timeout elapsed";
    }
    if (!len) return true;
    qDebug() << "senRequest";
    m_resp.clear();
    if (!getResponse()) {
        return true;
    }
    //    QJson::Parser parser;
    bool ok = false;
    //    m_result = parser.parse (m_resp.toAscii(), &ok).toMap();
    return ok;
}

bool Extron::createSource(VSource &) {
    return false;
}

bool Extron::startSource(VSource &) {
    return false;
}

bool Extron::stopSource(VSource &) {
    return false;
}

bool Extron::enableSourceFlag(VSource &s, bool f)
{
    Q_UNUSED(s)
    Q_UNUSED(f)
    return false;
}

bool Extron::setScene(const Sources &lst)
{
    qDebug() << Q_FUNC_INFO << time_f.elapsed();
    if (m_busy)  {
        return 0;
    }
    m_busy = true;
    m_sources.clear();
    m_sources_.clear();

    foreach(QSharedPointer<VSource> src, lst) {
        m_sources.push_back(src);
    }
    emit setSceneSig();
    return 0;
}

void Extron::unsetAll() {
    for (int i = 5; i <= 8; i++) {
        setOutput(i,0);
    }
}

void Extron::setSources(const QList<QSharedPointer<VSource>> &srcList)
{
    qDebug() << Q_FUNC_INFO << time_f.elapsed();
    foreach (const QSharedPointer<VSource> &src, srcList) {
        QVariant outv;
        QVariant extronv;
        src->getProperty("ExtronOutput",outv);
        src->getProperty("ExtronInput",extronv);
        setOutput(extronv.toInt(), outv.toInt());
    }
}

void Extron::setOutput(int input, int output)
{
    qDebug() << Q_FUNC_INFO << time_f.elapsed();
    qDebug() << Q_FUNC_INFO << "Set Output " << QString::number(input);
    QString in_s =  QString::number(input);
    QString out_s = QString::number(output);
    QString result = in_s+SET_COMM + out_s +  END_COMM + "\r\n";
    auto ok = sendRequest(result.toAscii().data(),result.size());
    qDebug() << Q_FUNC_INFO << m_resp << "ok =" << ok;
}

VController *Extron::clone()
{
    return new Extron();
}

Q_EXPORT_PLUGIN2(Extron,Extron)

void Extron::readFinished()
{
}

void Extron::setSceneSlot()
{
    qDebug() << Q_FUNC_INFO << time_f.elapsed();
    if (m_socket->state() != QAbstractSocket::ConnectedState) {
        emit consig();
        if(!m_socket->waitForConnected(3000)) {
            return;
        }
    }

    QMap<int,int> freeOut(m_UsedInputs);
    QList<int> usedIN;
    QList<QSharedPointer<VSource> > toView;
    for (auto it = m_sources.begin(); it != m_sources.end(); it++) {
        QSharedPointer<VSource> src = *it;
        if (src->gettype() == VNCMIXEREXTRON_SOURCE) {
            QVariant namedv;
            src->getProperty("named",namedv);
            QVariant extInp;
            src->getProperty("ExtronInput",extInp);
            //usedIN.push_back(freeOut.value(namedv.toInt()));
            usedIN.push_back(extInp.toInt());
            src->setProperty("ExtronOutput",freeOut.value(namedv.toInt()));
            freeOut.remove(namedv.toInt());
            toView.push_back(src);
        }
    }
    foreach(int j, freeOut.keys()) {
        foreach (QSharedPointer<VSource> src, m_sources) {
            if (src->gettype() == Extron_SOURCE) {
                src->setProperty("named",QVariant(j));
                //freeOut.remove(nam);
                QVariant extInp;
                src->getProperty("ExtronInput",extInp);
                src->setProperty("ExtronOutput",freeOut.value(j));
                usedIN.push_back(extInp.toInt());
                toView.push_back(src);
                m_sources.removeOne(src);
                break;
            }
        }
    }

    setSources(toView);

    /*
    for (int i = 1 ; i <= 32; i++) {
        //   if (!usedIN.contains(i))
        //    setOutput(i,0);
    }*/
    m_busy = false;
    emit sceneFinished(true);
}

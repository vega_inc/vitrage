#ifndef EXTRON_H
#define EXTRON_H

#include "../vcontroller/vcontroller.h"
#include <QTimer>
#include <QTime>
#include <QTcpSocket>

static const QString SET_COMM = "*";
static const QString END_COMM = "!";

typedef QMap<QString,QVariant> InnerMap;
class Extron : public VController {
    Q_OBJECT
    Q_INTERFACES(AbstractVController)
public:
    Extron();
    ~Extron();
    void conn() override;
    void disconn() override;
    bool createSource(VSource &) override;
    bool startSource(VSource &) override;
    bool stopSource(VSource &) override;
    bool enableSourceFlag(VSource &s,bool f) override;
    bool setScene(const Sources &) override;
    VController *clone() override;
    void run(void);
    void setOutput(int input, int output);
    void unsetAll();
    void setSources(const QList<QSharedPointer<VSource> > &srcList);
private slots:
    void newState(QAbstractSocket::SocketState);
    void conn_slot(void);
    void disconnected(void);
    void sock_err(QAbstractSocket::SocketError );
    void ready(void);
    void timer_timeout(void);
    bool checkRespForSucc(void);
    void readFinished(void);
    void setSceneSlot(void);
    void disSlot();
    void conSlot();
private:
    int m_currentSceneId = 0;
    QVariantMap m_result;

    static const uint32_t CODE_LENGHT = 8;
    bool sendRequest(char *data,uint32_t count);
    const QString m_success;
    QTcpSocket *m_socket;
    QThread *m_thread;
    QMutex m_mutex;
    QString m_resp; /* response */
    bool m_rxespFlag;
    QUrl m_url;
    QTimer m_timer;
    QTimer m_timerF;
    QTime  time_f;
    bool m_busy = false;
    bool m_respFlag;
//    const std::map<int,int> m_UsedInputs = {{5,5}, {6,6}, {7,7}, {8,8}};
    const std::map<int,int> m_UsedInputs = {{1,1}, {2,2}, {3,3}, {4,4}, {5,5}, {6,6}, {7,7}, {8,8}};


    void startTimer();
    bool getResponse();
    QList<QSharedPointer<VSource> > m_sources; /*list for set Scene method */
    QList<QSharedPointer<VSource> > m_sources_; /*list for set Scene method (to set Z order)*/
signals:
    void createSourceSig();
    void setSceneSig();
    void dissig();
    void consig();
};

#endif /* VEGA_H */

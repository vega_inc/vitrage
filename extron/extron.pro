include(../common.pri)

TARGET = extron 
CONFIG +=   rtti \
            debug \
            plugin

QT += network

DESTDIR = $$INSTALL_PATH_CTRL_PLUGINS 

TEMPLATE = lib

INCLUDEPATH += $$INSTALL_PATH_INCLUDE \
		/usr/include/

SOURCES += extron.cpp
HEADERS += extron.h 


unix {
       target.path = /$(DESTDIR)
       INSTALLS += target
     }

